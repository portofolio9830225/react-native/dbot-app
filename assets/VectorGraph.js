import React from "react";
import Svg, { Path } from "react-native-svg";
import { black } from "../utils/ColorPicker";

export const GraphLine = ({ width, height, to, color }) => {
	let toHeight = height * (to / 2);

	return (
		<Svg
			width={width}
			height={height}
			viewBox={`0 0 ${width} ${height}`}
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<Path
				fill={color || black}
				strokeWidth="1"
				stroke={color || black}
				d={`M 0 0 C 60 0, 60 ${toHeight}, ${width}, ${toHeight} V ${
					height - toHeight
				} H 0 M 0 ${height} C 60 ${height}, 60 ${height - toHeight}, ${width}, ${
					height - toHeight
				}V ${toHeight} H 0 M 0 ${toHeight} H ${width} V ${height - toHeight} H 0`}
			/>
		</Svg>
	);
};
