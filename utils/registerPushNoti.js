import * as Notifications from "expo-notifications";
import { Platform } from "react-native";
import store from "../store";
import { sendDeviceToken } from "../store/actions/authAction";
import { accentLight } from "./ColorPicker";
import getPushNotiToken from "./getPushNotiToken";

import isEmpty from "./isEmpty";

const registerForPushNotificationsAsync = async token => {
	const { status: existingStatus } = await Notifications.getPermissionsAsync();
	let finalStatus = existingStatus;

	// only ask if permissions have not already been determined, because
	// iOS won't necessarily prompt the user a second time.
	if (existingStatus !== "granted") {
		// Android remote notification permissions are granted during the app
		// install, so this will only ask on iOS
		const { status } = await Notifications.requestPermissionsAsync({
			ios: {
				allowAlert: true,
				allowBadge: true,
				allowSound: true,
			},
		});
		finalStatus = status;
	}

	// Stop here if the user did not grant permissions
	if (finalStatus !== "granted") {
		store.dispatch(sendDeviceToken("", token));
		return;
	}
	// Get the token that uniquely identifies this device

	let dToken = await getPushNotiToken();
	if (!isEmpty(dToken)) {
		console.log("deviceToken captured");
		let data = {
			device_token: dToken,
		};
		store.dispatch(sendDeviceToken(data, token));
	} else {
		console.log("No deviceToken");
		store.dispatch(sendDeviceToken("", token));
		return;
	}

	Notifications.setNotificationHandler({
		handleNotification: async () => ({
			shouldShowAlert: true,
			shouldPlaySound: true,
			shouldSetBadge: false,
		}),
	});

	if (Platform.OS === "android") {
		Notifications.setNotificationChannelAsync("order", {
			name: "order",
			importance: Notifications.AndroidImportance.MAX,
			vibrationPattern: [0, 250, 250, 250],
			lightColor: accentLight,
			sound: "default",
		});
		Notifications.setNotificationChannelAsync("payout", {
			name: "payout",
			importance: Notifications.AndroidImportance.MAX,
			vibrationPattern: [0, 250, 250, 250],
			lightColor: accentLight,
			sound: "default",
		});
		Notifications.setNotificationChannelAsync("test", {
			name: "test",
			importance: Notifications.AndroidImportance.MAX,
			vibrationPattern: [0, 250, 250, 250],
			lightColor: accentLight,
			sound: "default",
		});
	}
};

export default registerForPushNotificationsAsync;
