import isEmpty from "./isEmpty";

const TimeConverter = (time) => {
  let newTime = "";
  if (!isEmpty(time)) {
    let hour, minute, ampm;
    let arr = time.split(":");

    hour = parseInt(arr[0]);
    minute = parseInt(arr[1]);

    if (hour >= 12) {
      if (hour !== 12) {
        hour -= 12;
      }
      ampm = "P.M.";
    } else if (hour === 0) {
      hour = 12;
      ampm = "A.M.";
    } else {
      ampm = "A.M.";
    }
    newTime = `${hour}:${minute < 10 ? `0${minute}` : minute} ${ampm}`;
  }

  return newTime;
};

export default TimeConverter;
