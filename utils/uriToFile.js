const uriToFile = (uri) => {
  let localUri = uri;
  let filename = localUri.split("/").pop();

  let match = /\.(\w+)$/.exec(filename);

  let type = match
    ? match[1] === "pdf"
      ? "application/pdf"
      : `image/${match[1]}`
    : `image`;

  return { uri: localUri, name: filename, type };
};

export default uriToFile;
