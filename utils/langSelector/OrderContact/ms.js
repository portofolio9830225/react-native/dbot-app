const ms = {
	title: "Pilih info",
	infoList: [
		{
			id: 1,
			title: "Senarai produk",
		},
		{
			id: 2,
			title: "Pautan pembayaran",
		},
		// {
		//   id: 3,
		//   title: "Pautan nombor pengesanan",
		// },
	],
	deselectAll: "Nyahpilih semua",
	selectAll: "Pilih semua",
	submitButton: "Hantar",
};

export default ms;
