import en from "./en";
import ms from "./ms";

const index = [
	{
		lang: "en",
		data: en,
	},
	{
		lang: "ms",
		data: ms,
	},
];

export default index;
