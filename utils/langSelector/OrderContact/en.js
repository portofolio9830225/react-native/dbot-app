const en = {
	title: "Choose info",
	infoList: [
		{
			id: 1,
			title: "List of products",
		},
		{
			id: 2,
			title: "Payment link",
		},
		// {
		//   id: 3,
		//   title: "Tracking number link",
		// },
	],
	deselectAll: "Deselect all",
	selectAll: "Select all",
	submitButton: "Send",
};

export default en;
