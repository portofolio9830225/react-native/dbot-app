const ms = {
	morning: "Selamat pagi",
	afternoon: "Selamat tengahari",
	evening: "Selamat petang",
	sales: "Jualan",
	productsSold: "Produk terjual",
	funnel: "Corong",
	views: "Lawatan",
	leads: "Minat",
	emptyFunnel: "Tiada data",
};

export default ms;
