const en = {
	morning: "Good morning",
	afternoon: "Good afternoon",
	evening: "Good evening",
	sales: "Sales",
	productsSold: "Products sold",
	funnel: "Funnel",
	views: "Views",
	leads: "Leads",
	emptyFunnel: "No data yet",
};

export default en;
