const ms = {
	title: "Bahasa",
	langChoice: [
		{
			title: "English",
			value: "en",
		},
		{
			title: "Bahasa Melayu",
			value: "ms",
		},
	],
	submitButton: "Simpan",
};

export default ms;
