const en = {
	title: "Language",
	langChoice: [
		{
			title: "English",
			value: "en",
		},
		{
			title: "Bahasa Melayu",
			value: "ms",
		},
	],
	submitButton: "Save",
};

export default en;
