const en = {
	searchTitle: "Search",
	orderID: "Order ID",
	searchText: "Search by order ID",
	emptyText: "No order found",
};

export default en;
