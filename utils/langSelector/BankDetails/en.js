const en = {
	title: "Bank details",
	choose: "Choose",
	personal: "Personal bank",
	company: "Business bank",
	detailsTitle: "Details",
	name: "Beneficiary name",
	bankName: "Bank institution",
	defaultBank: "Choose bank",
	accNum: "Account number",
	personalIDNum: "MyKad number",
	businessIDNum: "SSM number",
	submitButton: "Save",
	pending: "Pending",
	verified: "Verified",
	rejected: "Rejected",
	SSMQuestion: "Which SSM number should I use?",
	SSMAnswer: "SSM number used for business bank account registration",
	alertPending:
		"Please ensure that information is correct and valid. Approval process may take up to 3 business days and cannot be edited while in process for security measures.",
	alertElse:
		"If you would like to change your bank details, please inform our tech support team via Whatsapp by clicking ",
	alertElseContactText: "here",
};

export default en;
