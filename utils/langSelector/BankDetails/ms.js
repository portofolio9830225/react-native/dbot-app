const ms = {
	title: "Info bank",
	choose: "Pilih",
	personal: "Akaun bank peribadi",
	company: "Akaun bank syarikat",
	detailsTitle: "Butiran",
	name: "Nama Penerima",
	bankName: "Institusi bank",
	defaultBank: "Pilih bank",
	accNum: "Nombor akaun",
	personalIDNum: "Nombor MyKad",
	businessIDNum: "Nombor SSM",
	submitButton: "Simpan",
	pending: "Menunggu pengesahan",
	verified: "Disahkan",
	rejected: "Ditolak",
	SSMQuestion: "Nombor SSM manakah yang patut digunakan?",
	SSMAnswer: "Nombor SSM yang digunakan ketika pendaftaran akaun bank syarikat",
	alertPending:
		"Sila pastikan maklumat anda betul dan sah. Proses pengesahan akan mengambil 3 hari bekerja dan anda tidak boleh mengemaskini semasa dalam proses sebagai langkah keselamatan.",
	alertElse:
		"Jika anda ingin mengubah maklumat bank anda, sila maklumkan ke StoreUp tech support melalui Whatsapp dengan menekan ",
	alertElseContactText: "pautan ini",
};

export default ms;
