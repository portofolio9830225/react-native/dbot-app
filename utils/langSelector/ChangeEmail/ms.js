const ms = {
	title: "Ubah email",
	currentEmail: "Email semasa",
	newEmail: "Email baru",
	password: "Kata kunci",
	submitButton: "Hantar",
};

export default ms;
