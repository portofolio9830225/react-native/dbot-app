const en = {
	title: "Change email",
	currentEmail: "Current email",
	newEmail: "New email",
	password: "Password",
	submitButton: "Submit",
};

export default en;
