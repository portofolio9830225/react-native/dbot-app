const ms = {
	title: "Info peribadi",
	name: "Nama",
	phone: "Nombor telefon",
	email: "Email",
	password: "Kata kunci",
	submitButton: "Simpan",
	emailVerified: "Email yang sah",
	askEmailVerify: "Dapatkan pengesahan email",
};

export default ms;
