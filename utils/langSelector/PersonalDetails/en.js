const en = {
	title: "Personal details",
	name: "Name",
	phone: "Phone number",
	email: "Email",
	password: "Password",
	submitButton: "Save",
	emailVerified: "Email verified",
	askEmailVerify: "Get email verification",
};

export default en;
