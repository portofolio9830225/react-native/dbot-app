const en = {
	title: "Payout reports",
	cardTitle: "Payout date",
	cardAmount: "Amount",
	emptyText: "No report yet",
};

export default en;
