const ms = {
	title: "Pesanan",
	search: "Carian",
	select: "Pilih",
	action: "Pilihan",
	done: "Selesai",
	selected: "dipilih",
	orderID: "ID pesanan",
	view: "Lihat",
	paid: "Dibayar",
	unpaid: "Tak dibayar",
	emptyTitle: "Tiada pesanan",
	emptyText: "Salin & kongsi pautan kedai anda.",
	emptyButton: "Salin pautan",
};

export default ms;
