const en = {
	title: "Orders",
	search: "Search",
	select: "Select",
	action: "Action",
	done: "Done",
	selected: "selected",
	orderID: "Order ID",
	view: "View",
	paid: "Paid",
	unpaid: "Unpaid",
	emptyTitle: "No order",
	emptyText: "Copy & share your store link now.",
	emptyButton: "Copy link",
};

export default en;
