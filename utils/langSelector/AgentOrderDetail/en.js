const en = {
	title: "Order ID",
	orderID: "Order ID",
	orderDate: "Order date",
	orderStatus: "Order status",
	product: "Products",
	pending: "Pending",
	accepted: "Accepted",
	canceled: "Cancelled",
	rejected: "Rejected",
	shipping: "Shipping",
	delivered: "Delivered",
	done: "Completed",
	received: "received",
	copyToStore: "Copy to main store",
	total: "Total",
	deliveredModalTitle: "Package delivered and copy all products to your store?",
	copyToStoreModalTitle: "Copy all products to your store?",
	modalNo: "Back",
	modalYes: "Yes",
};

export default en;
