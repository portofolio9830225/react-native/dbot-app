const ms = {
	title: "ID pesanan",
	orderID: "ID pesanan",
	orderDate: "Tarikh pesanan",
	orderStatus: "Status pesanan",
	product: "Produk",
	pending: "Menunggu",
	accepted: "Diterima",
	canceled: "Dibatalkan",
	rejected: "Ditolak",
	shipping: "Dalam perjalanan",
	delivered: "Dihantar",
	done: "Selesai",
	received: "diterima",
	copyToStore: "Salin ke kedai utama",
	total: "Jumlah",
	deliveredModalTitle: "Pakej diterima dan salin semua produk ke kedai utama?",
	copyToStoreModalTitle: "Salin semua produk ke kedai utama?",
	modalNo: "Kembali",
	modalYes: "Ya",
};

export default ms;
