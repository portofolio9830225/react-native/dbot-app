const en = {
	title: "Payout details",
	payoutDate: "Payout date",
	payoutAmount: "Total payout",
	salesAmount: "Total sales",
	completed: "Completed",
	cardTitle: "Order ID",
	cardDesc: "Date",
	emptyText: "No report yet",
};

export default en;
