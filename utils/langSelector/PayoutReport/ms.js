const ms = {
	title: "Info bayaran",
	payoutDate: "Tarikh bayaran",
	payoutAmount: "Jumlah bayaran",
	salesAmount: "Jumlah jualan",
	completed: "Selesai",
	cardTitle: "ID pesanan",
	cardDesc: "Tarikh",
	emptyText: "Tiada laporan",
};

export default ms;
