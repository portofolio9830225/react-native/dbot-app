const en = {
	title: "Payment",
	desc: "Please complete your bank details to allow easier & faster transactions from your customers",
	submitButton: "Save",
	message: [
		"A transaction fee of RM1.50 will be applied",
		"RM0 fee. Your account number will be shared to your customer during checkout for easier transaction",
	],
};

export default en;
