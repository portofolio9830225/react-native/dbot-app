const ms = {
	title: "Pembayaran",
	desc: "Isi info bank anda untuk memudahkan & melajukan proses transaksi dari pelanggan anda",
	submitButton: "Simpan",
	message: [
		"RM1.50 caj transaksi akan di kenakan",
		"RM0 caj. Nombor akaun bank anda akan dikongsi ke pelanggan untuk memudahkan proses transaksi terus ke anda",
	],
};

export default ms;
