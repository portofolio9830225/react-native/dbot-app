const ms = {
	title: "Penghantaran",
	courierTitle: "Nama kurier",
	courierDesc: "Pilih kurier",
	trackTitle: "Nombor pengesanan",
	setShipped: "Ubah status pesanan kepada 'Dihantar'",
	submitButton: "Simpan",
};

export default ms;
