const en = {
	title: "Shipping",
	courierTitle: "Courier name",
	courierDesc: "Select courier",
	trackTitle: "Tracking number",
	setShipped: "Set order status to 'Shipped'",
	submitButton: "Save",
};

export default en;
