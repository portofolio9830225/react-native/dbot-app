const en = {
	title: "Record payment",
	amount: "Amount",
	proofTitle: "Proof of payment (optional)",
	paymentDetail: "Payment detail",
	optional: "Optional",
	submitButton: "payment received",
};

export default en;
