const ms = {
	title: "Rekod pembayaran",
	amount: "Jumlah",
	proofTitle: "Bukti pembayaran (tak wajib)",
	paymentDetail: "Info pembayaran",
	optional: "Tak wajib",
	submitButton: "pembayaran diterima",
};

export default ms;
