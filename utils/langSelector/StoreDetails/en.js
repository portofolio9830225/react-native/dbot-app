const en = {
	title: "Store details",
	name: "Store name",
	desc: "Store description",
	url: "Website",
	urlDesc: "Your website is",
	urlEmptyDesc: "Enter few characters",
	companyTitle: "Company details",
	regName: "Registered name",
	ssmNum: "SSM number",
	storeLook: "Store look",
	showSold: "Show products sold",
	socMedTitle: "Social media",
	socMedModalTitle: "Edit social media",
	socMedPlatform: "Platform",
	selectSocMed: "Select platform",
	socMedLink: "Social media link",
	addSocMed: "Add social media",
	imageModalTitle: "Choose logo",
	imageModalChoose: "Choose from library",
	imageModalTake: "Take photo",
	socMedModalDelete: "Delete",
	socMedModalNo: "Back",
	socMedModalYes: "Save",
	editLogo: "Edit logo",
	addLogo: "Add logo",
	submitButton: "Save",
	nextButton: "Next",
};

export default en;
