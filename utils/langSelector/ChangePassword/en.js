const en = {
	title: "Change password",
	password: "Old password",
	newPassword: "New password",
	submitButton: "Submit",
};

export default en;
