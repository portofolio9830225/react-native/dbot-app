const ms = {
	title: "Ubah kata kunci",
	newPassword: "Kata kunci baru",
	password: "Kata kunci lama",
	submitButton: "Hantar",
};

export default ms;
