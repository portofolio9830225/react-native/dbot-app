const ms = {
	title: "Alamat kedai",
	address1: "Alamat baris 1",
	address2: "Alamat baris 2",
	address3: "Alamat baris 3",
	address4: "Alamat baris 4",
	postcode: "Poskod",
	city: "Bandar",
	state: "Negeri",
	selectState: "Pilih negeri",
	submitButton: "Simpan",
};

export default ms;
