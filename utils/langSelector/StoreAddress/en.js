const en = {
	title: "Store address",
	address1: "Address line 1",
	address2: "Address line 2",
	address3: "Address line 3",
	address4: "Address line 4",
	postcode: "Postcode",
	city: "City",
	state: "State",
	selectState: "Select state",
	submitButton: "Save",
};

export default en;
