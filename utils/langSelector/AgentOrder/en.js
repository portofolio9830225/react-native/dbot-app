const en = {
	title: "Purchases",
	cardTitle: "Purchase ID",
	emptyText: "No purchase",
	pending: "Pending",
	accepted: "Accepted",
	canceled: "Cancelled",
	rejected: "Rejected",
	shipping: "Shipping",
	delivered: "Delivered",
	done: "Completed",
	incompleted: "Incompleted",
};

export default en;
