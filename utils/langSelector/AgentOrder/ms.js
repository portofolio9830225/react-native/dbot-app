const ms = {
	title: "Belian",
	cardTitle: "ID belian",
	emptyText: "Tiada belian",
	pending: "Menunggu",
	accepted: "Diterima",
	canceled: "Dibatalkan",
	rejected: "Ditolak",
	shipping: "Dalam perjalanan",
	delivered: "Dihantar",
	done: "Selesai",
	incompleted: "Tak selesai",
};

export default ms;
