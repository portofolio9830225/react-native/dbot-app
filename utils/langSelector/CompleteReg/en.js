const en = {
	titleStore: "Complete store details",
	titlePayout: "Complete bank details",
	desc: "You're just a few steps away!",
	yesButton: "Yes",
	noButton: "Skip",
};

export default en;
