const en = {
	title1: "Choose orders",
	desc1: "Select orders to be included in bulk shipping file",
	emptyText: "No order",
	title2: "Choose courier",
	desc2: "Select shipping courier of your bulk shipping file",
	orderID: "Order ID",
	deselectAll: "Deselect all",
	selectAll: "Select all",
	title3: "Review",
	product: "product",
	title4: "Your file has been sent to your email",
	desc4: "Set orders to 'Shipped'?",
	yes4: "Yes, set all to 'Shipped'",
	no4: "Skip",
	next: "next",
	request: "request",
};

export default en;
