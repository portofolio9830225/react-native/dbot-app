const ms = {
	title1: "Pilih pesanan",
	desc1: "Pilih pesanan untuk dimasukkan ke dalam fail penghantaran pukal",
	emptyText: "Tiada pesanan",
	title2: "Pilih kurier",
	desc3: "Pilih kurier fail penghantaran pukal",
	orderID: "ID pesanan",
	deselectAll: "Nyahpilih semua",
	selectAll: "Pilih semua",
	title3: "Rumusan",
	product: "produk",
	title4: "Fail telah dihantar ke emel anda",
	desc4: "Tukar status pesanan yang berkaitan kepada 'Dihantar'?",
	yes4: "Ya, tukar kepada 'Dihantar'",
	no4: "Abai",
	next: "seterusnya",
	request: "mohon",
};

export default ms;
