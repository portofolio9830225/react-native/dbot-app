const en = {
	title: "Choose date",
	desc: "Choose orders' date range to be exported",
	submitButton: "Send statement",
};

export default en;
