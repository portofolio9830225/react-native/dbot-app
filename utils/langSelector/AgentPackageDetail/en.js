const en = {
	total: "Total",
	product: "Products",
	option: "Payment method",
	codTitle: "Cash on Delivery",
	codDesc: "Pay whenever the package arrives at your doorstep",
	onlineTitle: "Online transfer",
	onlineDesc: "Pay through online banking",
	submit: "Buy",
};

export default en;
