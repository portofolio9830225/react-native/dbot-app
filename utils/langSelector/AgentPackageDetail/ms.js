const ms = {
	total: "Jumlah",
	product: "Produk",
	option: "Cara pembayaran",
	codTitle: "Cash on Delivery",
	codDesc: "Bayar semasa penghantaran dibuat",
	onlineTitle: "Online transfer",
	onlineDesc: "Bayar melalui perbankan atas talian",
	submit: "Beli",
};

export default ms;
