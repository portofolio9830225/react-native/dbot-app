const en = {
	title: "Upload product",
	desc: "Quickly setup your online store by uploading all your product simultaneously. Press 'REQUEST' button below to begin",
	submitButton: "Request",
};

export default en;
