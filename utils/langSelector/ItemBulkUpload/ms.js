const ms = {
	title: "Muatnaik produk",
	desc: "Lengkapkan kedai atas talian anda dengan memuatnaik semua produk anda secara pukal. Tekan 'MOHON' untuk memulakan proses.",
	submitButton: "Mohon",
};

export default ms;
