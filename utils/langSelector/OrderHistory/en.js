const en = {
	title: "Orders",
	orderID: "Order ID",
	view: "View",
	completed: "Completed",
	rejected: "Rejected",
	emptyText: "No order",
};

export default en;
