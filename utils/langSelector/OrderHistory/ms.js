const ms = {
	title: "Pesanan",
	orderID: "ID pesanan",
	view: "Lihat",
	completed: "Selesai",
	rejected: "Ditolak",
	emptyText: "Tiada pesanan",
};

export default ms;
