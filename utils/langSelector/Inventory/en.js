const en = {
	title: "Inventory",
	stock: "Stock",
	emptyTitle: "No product yet",
	emptyText: "Start publishing your product",
	emptyButton: "Add a product",
	create: "Create",
	available: "Available",
	unavailable: "Unavailable",
};

export default en;
