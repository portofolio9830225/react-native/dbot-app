const ms = {
	title: "Inventori",
	stock: "Stok",
	emptyTitle: "Tiada produk",
	emptyText: "Letakkan produk anda disini",
	emptyButton: "Tambah produk",
	create: "Cipta",
	available: "Tersedia",
	unavailable: "Tersembunyi",
};

export default ms;
