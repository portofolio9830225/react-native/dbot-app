const ms = {
	title: "Nombor pengesanan",
	desc: "Lengkapkan nombor pengesanan untuk setiap pesanan secara serentak. Tekan 'MOHON' untuk memulakan proses.",
	submitButton: "Mohon",
};

export default ms;
