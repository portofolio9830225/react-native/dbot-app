const en = {
	title: "Tracking number",
	desc: "Set tracking number for each order simultaneously. Press 'REQUEST' button below to begin",
	submitButton: "Request",
};

export default en;
