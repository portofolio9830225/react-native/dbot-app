import BankDetails from "./BankDetails";
import Dashboard from "./Dashboard";
// import CategoryDetail from "./CategoryDetail";
import ChangeEmail from "./ChangeEmail";
import ChangePassword from "./ChangePassword";
import CompleteReg from "./CompleteReg";
import Inventory from "./Inventory";
import ItemPage from "./ItemPage";
import ItemBulkUpload from "./ItemBulkUpload";
import TrackingBulkUpload from "./TrackingBulkUpload";
import CatalogShipping from "./CatalogShipping";
import OrderCsv from "./OrderCsv";
import OrderDetail from "./OrderDetail";
import OrderShipping from "./OrderShipping";
import OrderContact from "./OrderContact";
import OrderSearch from "./OrderSearch";
import OrderHistory from "./OrderHistory";
import OrderBulkShipping from "./OrderBulkShipping";
import OrderPaymentRecord from "./OrderPaymentRecord";
import Orders from "./Orders";
import PaymentMethod from "./PaymentMethod";
import PayoutList from "./PayoutList";
import PayoutReport from "./PayoutReport";
import PersonalDetails from "./PersonalDetails";
import Settings from "./Settings";
import StoreDetails from "./StoreDetails";
import StoreAddress from "./StoreAddress";
import SetItemWeight from "./SetItemWeight";
import LanguageSet from "./LanguageSet";
import AgentHome from "./AgentHome";
import AgentOrder from "./AgentOrder";
import AgentPackageDetail from "./AgentPackageDetail";
import AgentOrderDetail from "./AgentOrderDetail";

const LangIndex = {
	BankDetails,
	Dashboard,
	// CategoryDetail,
	ChangeEmail,
	ChangePassword,
	CompleteReg,
	Inventory,
	ItemPage,
	ItemBulkUpload,
	TrackingBulkUpload,
	OrderCsv,
	OrderDetail,
	OrderShipping,
	OrderContact,
	OrderSearch,
	OrderHistory,
	Orders,
	OrderBulkShipping,
	OrderPaymentRecord,
	PaymentMethod,
	BankDetails,
	PersonalDetails,
	Settings,
	StoreDetails,
	StoreAddress,
	LanguageSet,
	CatalogShipping,
	SetItemWeight,
	PayoutList,
	PayoutReport,
	AgentHome,
	AgentOrder,
	AgentPackageDetail,
	AgentOrderDetail,
};

export default LangIndex;
