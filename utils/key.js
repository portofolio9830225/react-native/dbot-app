import axios from "axios";
axios.defaults.headers.common["X-Data-Source"] = process.env.NODE_ENV !== "development" ? "live" : "test";

const isOffice = true;
const HOME = "192.168.0.5";
const OFFICE = "192.168.4.12";

export const API_LINK =
	process.env.NODE_ENV !== "production"
		? "https://api.pinjam.co/dbot"
		: `http://${isOffice ? `${OFFICE}` : `${HOME}`}:3000/dbot`;

export const XANO_API_LINK =
	process.env.NODE_ENV === "production"
		? "https://api.storeup.io/api:BOIrhKJQ"
		: "https://api.storeup.io/api:BOIrhKJQ:dev";

export const activateAccLink = `${
	process.env.NODE_ENV === "production" ? "app.storeup.io" : "localhost:3004"
}/activate-account`;

export const setPassLink = `${
	process.env.NODE_ENV === "production" ? "app.storeup.io" : "localhost:3004"
}/set-password`;

export const isTestAppLoading = false;
