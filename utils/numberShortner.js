const numberShortner = num => {
	let suffix = "";
	let newNum = num;
	if (num >= 1000000) {
		suffix = "m";
		newNum = newNum * (0.000001).toFixed(2);
	} else if (num >= 1000) {
		suffix = "k";
		newNum = (newNum * 0.001).toFixed(2);
	} else {
	}

	return `${newNum}${suffix}`;
};

export default numberShortner;
