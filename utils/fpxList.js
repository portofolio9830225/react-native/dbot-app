const fpxList = [
	{
		code: "ABMB0212",
		name: "Alliance Bank",
		status: false,
		image: require("../../assets/bank/AllianceBank.jpg"),
	},
	{
		code: "ABB0233",
		name: "Affin Bank",
		status: false,
		image: require("../../assets/bank/affinBank.png"),
	},
	{
		code: "AMBB0209",
		name: "AmBank",
		status: false,
		image: require("../../assets/bank/ambank.png"),
	},
	{
		code: "BCBB0235",
		name: "CIMB Clicks",
		status: false,
		image: require("../../assets/bank/cimbclicks.png"),
	},
	{
		code: "BIMB0340",
		name: "Bank Islam",
		status: false,
		image: require("../../assets/bank/bankIslam.jpg"),
	},
	{
		code: "BKRM0602",
		name: "Bank Rakyat",
		status: false,
		image: require("../../assets/bank/bankrakyat.png"),
	},
	{
		code: "BMMB0341",
		name: "Bank Muamalat",
		status: false,
		image: require("../../assets/bank/bankMuamalat.png"),
	},
	{
		code: "BSN0601",
		name: "BSN",
		status: false,
		image: require("../../assets/bank/bsn.jpg"),
	},
	{
		code: "CIT0217",
		name: "Citibank Berhad",
		status: false,
		image: require("../../assets/bank/citibank.jpeg"),
	},
	{
		code: "HLB0224",
		name: "Hong Leong Bank",
		status: false,
		image: require("../../assets/bank/hongleongBank.png"),
	},
	{
		code: "HSBC0223",
		name: "HSBC Bank",
		status: false,
		image: require("../../assets/bank/hsbc.jpg"),
	},
	{
		code: "KFH0346",
		name: "Kuwait Finance House",
		status: false,
		image: require("../../assets/bank/kuwaitFH.jpg"),
	},
	{
		code: "MB2U0227",
		name: "Maybank2u",
		status: false,
		image: require("../../assets/bank/maybank.png"),
	},
	{
		code: "MBB0227",
		name: "Maybank2E",
		status: false,
		image: require("../../assets/bank/maybank.png"),
	},
	{
		code: "MBB0228",
		name: "Maybank2E",
		status: false,
		image: require("../../assets/bank/maybank.png"),
	},
	{
		code: "OCBC0229",
		name: "OCBC Bank",
		status: false,
		image: require("../../assets/bank/ocbc.png"),
	},
	{
		code: "PBB0233",
		name: "Public Bank",
		status: false,
		image: require("../../assets/bank/publicBank.png"),
	},
	{
		code: "RHB0218",
		name: "RHB Now",
		status: false,
		image: require("../../assets/bank/rhb.jpg"),
	},
	{
		code: "SCB0216",
		name: "Standard Chartered",
		status: false,
		image: require("../../assets/bank/standardcharted.png"),
	},
	{
		code: "UOB0226",
		name: "UOB Bank",
		status: false,
		image: require("../../assets/bank/uob.jpg"),
	},
];

export default fpxList;
