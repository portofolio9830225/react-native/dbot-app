import { blue, green, grey, greyblack, pink } from "./ColorPicker";

const orderColor = status => {
	switch (status) {
		case "unpaid":
			return greyblack;
		case "paid":
			return blue;
		case "completed":
			return green;
		case "rejected":
			return pink;
		default:
			return grey;
	}
};

export default orderColor;
