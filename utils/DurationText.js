const DurationText = val => {
	return `${val} ${val <= 1 ? "day" : "days"}`;
};

export default DurationText;
