const stateList = [
  {
    name: "Johor",
    code: "Johor",
  },
  {
    name: "Kedah",
    code: "Kedah",
  },

  {
    name: "Kelantan",
    code: "Kelantan",
  },
  {
    name: "Melaka",
    code: "Melaka",
  },
  {
    name: "Negeri Sembilan",
    code: "Negeri Sembilan",
  },
  {
    name: "Pahang",
    code: "Pahang",
  },
  {
    name: "Perak",
    code: "Perak",
  },
  {
    name: "Perlis",
    code: "Perlis",
  },
  {
    name: "Pulau Pinang",
    code: "Pulau Pinang",
  },
  {
    name: "Selangor",
    code: "Selangor",
  },
  {
    name: "Terengganu",
    code: "Terengganu",
  },
  {
    name: "Kuala Lumpur",
    code: "Kuala Lumpur",
  },
  {
    name: "Putrajaya",
    code: "Putrajaya",
  },
  {
    name: "Sarawak",
    code: "Sarawak",
  },
  {
    name: "Sabah",
    code: "Sabah",
  },
  {
    name: "Labuan",
    code: "Labuan",
  },
];

export default stateList;
