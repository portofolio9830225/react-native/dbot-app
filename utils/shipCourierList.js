const shipCourierList = [
	{
		name: "EasyParcel",
		val: "2",
		img: "https://pinjamla.s3-ap-southeast-1.amazonaws.com/photo/1625647998564.png",
	},
	{
		name: "Ninja Van",
		val: "1",
		img: "https://pinjamla.s3-ap-southeast-1.amazonaws.com/photo/1625648033751.png",
	},
	{
		name: "Delyva",
		val: "3",
		img: "https://pinjamla.s3-ap-southeast-1.amazonaws.com/photo/1630070268792.png",
	},
];

export default shipCourierList;
