import axios from "axios";
import base64 from "react-native-base64";

let key = base64.encode(
	process.env.NODE_ENV === "production"
		? "private_3Ky9jRYagXJRylFPUKr+Y+SQKS8=:"
		: "private_Nkkv83ffSuVfaM+Obxoqtwmbhwc=:"
);

export const imagekitUpload = data => {
	return axios.post("https://upload.imagekit.io/api/v1/files/upload", data, {
		headers: { Authorization: `Basic ${key}` },
	});
};

export const imagekitDelete = id => {
	return axios.delete(`https://api.imagekit.io/v1/files/${id}`, {
		headers: { Authorization: `Basic ${key}` },
	});
};
