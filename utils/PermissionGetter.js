import * as ImagePicker from "expo-image-picker";

export const getGalleryPermissionAsync = async () => {
  const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
  if (status !== "granted") {
    alert("Sorry, we need camera roll permissions to make this work!");
  }
  return status;
};

export const getCameraPermissionAsync = async () => {
  const { status } = await ImagePicker.requestCameraPermissionsAsync();
  if (status !== "granted") {
    alert("Sorry, we need camera permissions to make this work!");
  }
  return status;
};
