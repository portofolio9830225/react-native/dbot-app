export default getDateRange = (sDate, eDate) => {
  let arr = [];
  let i = new Date(sDate);
  let end = new Date(eDate);

  while (i <= end) {
    let year = i.getFullYear();
    let month =
      i.getMonth() + 1 >= 10 ? i.getMonth() + 1 : `0${i.getMonth() + 1}`;
    let day = i.getDate() >= 10 ? i.getDate() : `0${i.getDate()}`;
    arr.push(`${year}-${month}-${day}`);
    let newDate = i.setDate(i.getDate() + 1);
    i = new Date(newDate);
  }
  return arr;
};
