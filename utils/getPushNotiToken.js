import * as Notifications from "expo-notifications";
import { Platform } from "react-native";

const getPushNotiToken = async () => {
	let token = "";
	if (Platform.OS !== "web") {
		await Notifications.getExpoPushTokenAsync()
			.then(res => {
				token = res.data.slice(18, -1);
			})
			.catch(err => {
				console.log("cannot get noti token", err);
			});
	}

	return token;
};

export default getPushNotiToken;
