export const transparent = "transparent";
export const mainBgColor = "#F2F2F7";

export const white = "white";
export const black = "#000000";
export const blackTransparent = "rgba(0,0,0,0.2)";

export const greyblack = "#8E8E93"; // SystemGrey
export const greydark = "#AEAEB2"; // SystemGrey2
export const grey = "#C7C7CC"; // SystemGrey3
export const greylight = "#D1D1D6"; // SystemGrey4
export const greywhite = "#E5E5EA"; // SystemGrey5

export const accentDark = "#003EBD";
export const accent = "#0D67F1";
export const accentLight = "#6994FF";

export const green = "#34C759";
export const blue = "#13BBCF";
export const red = "#FF3B30";
export const yellow = "#FFCC00";
export const orange = "#FF9500";
export const orangepurple = "#E78145";
export const purpleorange = "#D06D85";
export const purple = "#AF52DE";
export const purplepink = "#C14AC1";
export const pinkpurple = "#DC3E93";
export const pink = "#FF2D55";

export const whatsappColor = "#0DC043";
