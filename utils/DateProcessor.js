import isEmpty from "./isEmpty";

const DateProcessor = (date) => {
  let newDate = "";

  let day, month, year;

  day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
  month =
    date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
  year = date.getFullYear();

  newDate = `${year}-${month}-${day}`;

  return newDate;
};

export default DateProcessor;
