import { Keyboard } from "react-native";
import Toast from "react-native-tiny-toast";
import Clipboard from "expo-clipboard";
import store from "../store";
import isEmpty from "./isEmpty";

const handleCopyText =
	(val, key = "") =>
	() => {
		const lang = store.getState().preference.lang;
		Clipboard.setString(val);
		Toast.show(
			lang === "ms"
				? `${key}${!isEmpty(key) ? " t" : "T"}elah disalin`
				: `${key}${!isEmpty(key) ? " c" : "C"}opied`,
			{
				containerStyle: {
					backgroundColor: "rgba(0,0,0,0.4)",
					height: 40,
					borderRadius: 20,
				},
				textStyle: {
					paddingHorizontal: 5,
					fontSize: 14,
					fontFamily: "regular",
				},
			}
		);
		Keyboard.dismiss();
	};

export default handleCopyText;
