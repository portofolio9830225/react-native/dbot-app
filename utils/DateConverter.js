import moment from "moment";

const DateConverter = (date, format = "DD MMM YYYY") => {
	let d = new Date(date);
	return moment(d).format(format);
};

export default DateConverter;
