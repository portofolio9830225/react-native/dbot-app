import DateConverter from "./DateConverter";

const timeConverter = UNIX_timestamp => {
  var a = new Date(UNIX_timestamp);
  let now = new Date();
  var year = a.getFullYear();
  var month = a.getMonth();
  var date = a.getDate();

  let time = DateConverter(`${year}-${month + 1}-${date}`);

  if (year === now.getFullYear() && month === now.getMonth()) {
    if (date === now.getDate()) {
      time = "Today";
    }
    if (date === now.getDate() - 1) {
      time = "Yesterday";
    }
  }

  return time;
};

export default timeConverter;
