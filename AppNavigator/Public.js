import { createSwitchNavigator } from "react-navigation";

import EntryPoint from "../screens/1EntryPoint";
import ForgotPassword from "../screens/ForgotPassword";
// import AppDownload from "../screens/2AppDownload";
import SignInEmail from "../screens/SignInEmail";
import SignInEmailWithPassword from "../screens/SignInEmailWithPassword";
import SignInPasscode from "../screens/SignInPasscode";
import SignInCreatePassword from "../screens/SignInCreatePassword";
// import SignUp from "../screens/SignUp";
// import ForgotPassword from "../screens/ForgotPassword";

const PublicNavigator = createSwitchNavigator(
	{
		EntryPoint: { screen: EntryPoint },
		// AppDownload: { screen: AppDownload },
		SignIn: { screen: SignInEmail },
		SignInPasscode: { screen: SignInPasscode },
		SignInEmailWithPassword: { screen: SignInEmailWithPassword },
		SignInCreatePassword: { screen: SignInCreatePassword },
		// SignUp: { screen: SignUp },
		ForgotPassword: { screen: ForgotPassword },
	},
	{ initialRouteName: "EntryPoint" }
);

export default PublicNavigator;
