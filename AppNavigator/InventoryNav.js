import React from "react";
import { createStackNavigator } from "react-navigation-stack";

import Inventory from "../screens/Inventory";
import ItemPage from "../screens/ItemPage";
import CatalogShipping from "../screens/CatalogShipping";
import ItemVariant from "../screens/ItemVariant";
import ProductNav from "./ProductNav";
import CategoryNav from "./CategoryNav";
import CategoryAdd from "../screens/CategoryAdd";

const InventoryNav = createStackNavigator(
	{
		Inventory: {
			screen: Inventory,
		},
		Product: {
			screen: ProductNav,
		},
		Category: {
			screen: CategoryNav,
		},
		CategoryAdd: {
			screen: CategoryAdd,
		},
		// ItemPage: {
		// 	screen: ItemPage,
		// },
		// ItemVariant: {
		// 	screen: ItemVariant,
		// },
	},
	{
		defaultNavigationOptions: {
			headerStyle: {
				height: 30,
			},
		},

		initialRouteName: "Inventory",
		headerMode: "none",
	}
);

InventoryNav.navigationOptions = ({ navigation }) => {
	let tabBarVisible = true;
	if (navigation.state.index > 0) {
		tabBarVisible = false;
	}

	return {
		tabBarVisible,
	};
	// return {
	//   tabBarVisible: true,
	// };
};

export default InventoryNav;
