import React from "react";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createBrowserApp } from "@react-navigation/web";
import * as Linking from "expo-linking";

import Public from "./Public";
import Private from "./Private";
import ListOfBusiness from "./ListOfBusiness";
import RegistrationNav from "./RegistrationNav";
import NavigationService from "./NavigationService";
import LanguageSet from "../screens/LanguageSet";
import { Platform } from "react-native";

const createApp = Platform.select({
	web: config => createBrowserApp(config, { history: "hash" }),
	default: config => createAppContainer(config),
});

const Navigator = createApp(
	createSwitchNavigator(
		{
			Public: { screen: Public },
			Private: { screen: Private },
			ListOfBusiness: { screen: ListOfBusiness },
			Registration: { screen: RegistrationNav },
			LanguageSet: { screen: LanguageSet },
		},
		{ initialRouteName: "Public" }
	)
);

const prefix = Linking.createURL("/");

class AppNavigator extends React.Component {
	render() {
		return (
			<Navigator
				uriPrefix={prefix}
				ref={navigatorRef => {
					NavigationService.setTopLevelNavigator(navigatorRef);
				}}
			/>
		);
	}
}

export default AppNavigator;
