import React from "react";
import { createStackNavigator } from "react-navigation-stack";

import Promotion from "../screens/Promotion";
import PromotionAdd from "../screens/PromotionAdd";

const PromotionNav = createStackNavigator(
	{
        Promotion: {
            screen: Promotion,
        },
		PromotionAdd: {
			screen: PromotionAdd,
		},
	},
	{
		defaultNavigationOptions: {
			headerStyle: {
				height: 30,
			},
		},

		initialRouteName: "Promotion",
		headerMode: "none",
	}
);

PromotionNav.navigationOptions = ({ navigation }) => {
	let tabBarVisible = true;
	if (navigation.state.index > 0) {
		tabBarVisible = false;
	}

	return {
		tabBarVisible,
	};
	// return {
	//   tabBarVisible: true,
	// };
};

export default PromotionNav;
