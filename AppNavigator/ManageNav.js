import React from "react";
import { createStackNavigator } from "react-navigation-stack";
import Manage from "../screens/Manage";

import PaymentMethod from "../screens/PaymentMethod";
import PayoutList from "../screens/PayoutList";
import PayoutReport from "../screens/PayoutReport";
import CatalogShipping from "../screens/CatalogShipping";
import SetItemWeight from "../screens/SetItemWeight";
import OrderDetail from "../screens/OrderDetail";
import OrderContact from "../screens/OrderContact";
import OrderCustomer from "../screens/OrderCustomer";
import PromotionNav from "./PromotionNav";

const ManageNav = createStackNavigator(
	{
		Manage: {
			screen: Manage,
		},

		PaymentMethod: {
			screen: PaymentMethod,
		},
		PayoutList: {
			screen: PayoutList,
		},
		PayoutReport: {
			screen: PayoutReport,
		},
		OrderDetail: {
			screen: OrderDetail,
		},
		OrderContact: {
			screen: OrderContact,
		},
		OrderCustomer: {
			screen: OrderCustomer,
		},
		CatalogShipping: {
			screen: CatalogShipping,
		},
		SetItemWeight: {
			screen: SetItemWeight,
		},
		Promotion: {
			screen: PromotionNav,
		},
	},
	{
		defaultNavigationOptions: {
			headerStyle: {
				height: 30,
			},
		},

		initialRouteName: "Manage",
		headerMode: "none",
	}
);

ManageNav.navigationOptions = ({ navigation }) => {
	let tabBarVisible = true;
	if (navigation.state.index > 0) {
		tabBarVisible = false;
	}

	return {
		tabBarVisible,
	};
	// return {
	//   tabBarVisible: true,
	// };
};

export default ManageNav;
