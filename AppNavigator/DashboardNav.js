import React from "react";
import { createStackNavigator } from "react-navigation-stack";

import Dashboard from "../screens/Dashboard";

const DashboardNav = createStackNavigator(
  {
    Dashboard: {
      screen: Dashboard,
    },
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        height: 30,
      },
    },

    initialRouteName: "Dashboard",
    headerMode: "none",
  }
);

DashboardNav.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
  // return {
  //   tabBarVisible: true,
  // };
};

export default DashboardNav;
