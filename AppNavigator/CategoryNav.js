import React from "react";
import { createStackNavigator } from "react-navigation-stack";

import CategoryAdd from "../screens/CategoryAdd";
import CategoryProduct from "../screens/CategoryProduct";
import CategoryDetail from "../screens/CategoryDetail";

const CategoryNav = createStackNavigator(
	{
		CategoryAdd: {
			screen: CategoryAdd,
		},
		CategoryDetail: {
			screen: CategoryDetail,
		},
		CategoryProduct: {
			screen: CategoryProduct,
		},
	},
	{
		defaultNavigationOptions: {
			headerStyle: {
				height: 30,
			},
		},

		initialRouteName: "CategoryDetail",
		headerMode: "none",
	}
);

CategoryNav.navigationOptions = ({ navigation }) => {
	let tabBarVisible = true;
	if (navigation.state.index > 0) {
		tabBarVisible = false;
	}

	return {
		tabBarVisible,
	};
	// return {
	//   tabBarVisible: true,
	// };
};

export default CategoryNav;
