import React from "react";

import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
// import { createBottomTabNavigator } from "react-navigation-tabs";
import TabBarIcon from "./common/TabBarIcon";

import Dashboard from "./DashboardNav";
import Orders from "./OrdersNav";
import Settings from "./SettingsNav";
import Manage from "./ManageNav";
import Inventory from "./InventoryNav";
import { white } from "../utils/ColorPicker";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";

const PrivateNavigator = createMaterialBottomTabNavigator(
	{
		Dashboard: {
			screen: Dashboard,
			navigationOptions: {
				tabBarIcon: ({ focused }) => <TabBarIcon iconPack={MaterialIcons} focused={focused} name="dashboard" />,
			},
		},
		Orders: {
			screen: Orders,
			navigationOptions: {
				tabBarIcon: ({ focused }) => <TabBarIcon iconPack={Ionicons} focused={focused} name="cart" />,
			},
		},
		Inventory: {
			screen: Inventory,
			navigationOptions: {
				tabBarIcon: ({ focused }) => <TabBarIcon iconPack={Ionicons} focused={focused} name="book" />,
			},
		},
		Manage: {
			screen: Manage,
			navigationOptions: {
				tabBarIcon: ({ focused }) => <TabBarIcon iconPack={Ionicons} focused={focused} name="settings" />,
			},
		},
		Settings: {
			screen: Settings,
			navigationOptions: {
				tabBarIcon: ({ focused }) => <TabBarIcon iconPack={Ionicons} focused={focused} name="person" />,
			},
		},
	},
	{
		initialRouteName: "Dashboard",
		order: ["Dashboard", "Orders", "Inventory", "Manage", "Settings"],
		// tabBarComponent: (props) => {
		//   return <MainBottomBar {...props} />;
		// },
		labeled: false,
		activeColor: white,
		inactiveColor: white,
		barStyle: {
			backgroundColor: white,
			elevation: 12,
			borderTopColor: "#eee",
			borderTopWidth: 0.5,
		},
	}
);

export default PrivateNavigator;
