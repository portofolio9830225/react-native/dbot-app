import React from "react";
import { createStackNavigator } from "react-navigation-stack";
import Settings from "../screens/Settings";
import ChangeEmail from "../screens/ChangeEmail";
import ChangePassword from "../screens/ChangePassword";
import BankDetails from "../screens/BankDetails";
import PersonalDetails from "../screens/PersonalDetails";
import StoreDetails from "../screens/StoreDetails";
import StoreAddress from "../screens/StoreAddress";

import ItemBulkUpload from "../screens/ItemBulkUpload";
import PaymentMethod from "../screens/PaymentMethod";
import PayoutList from "../screens/PayoutList";
import PayoutReport from "../screens/PayoutReport";
import OrderDetail from "../screens/OrderDetail";
import CatalogShipping from "../screens/CatalogShipping";
import SetItemWeight from "../screens/SetItemWeight";
import LanguageSet from "../screens/LanguageSet";
import TrackingBulkUpload from "../screens/TrackingBulkUpload";
import OrderContact from "../screens/OrderContact";
import OrderCustomer from "../screens/OrderCustomer";

const SettingsNav = createStackNavigator(
	{
		Settings: {
			screen: Settings,
		},
		LanguageSet: {
			screen: LanguageSet,
		},
		PersonalDetails: {
			screen: PersonalDetails,
		},
		ChangeEmail: {
			screen: ChangeEmail,
		},
		ChangePassword: {
			screen: ChangePassword,
		},
		StoreDetails: {
			screen: StoreDetails,
		},
		StoreAddress: {
			screen: StoreAddress,
		},
		BankDetails: {
			screen: BankDetails,
		},

		ItemBulkUpload: {
			screen: ItemBulkUpload,
		},
		TrackingBulkUpload: {
			screen: TrackingBulkUpload,
		},
		PaymentMethod: {
			screen: PaymentMethod,
		},
		PayoutList: {
			screen: PayoutList,
		},
		PayoutReport: {
			screen: PayoutReport,
		},
		SetItemWeight: {
			screen: SetItemWeight,
		},
	},
	{
		defaultNavigationOptions: {
			headerStyle: {
				height: 30,
			},
		},

		initialRouteName: "Settings",
		headerMode: "none",
	}
);

SettingsNav.navigationOptions = ({ navigation }) => {
	let tabBarVisible = true;
	if (navigation.state.index > 0) {
		tabBarVisible = false;
	}

	return {
		tabBarVisible,
	};
	// return {
	//   tabBarVisible: true,
	// };
};

export default SettingsNav;
