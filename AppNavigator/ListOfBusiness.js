import { createSwitchNavigator } from "react-navigation";

import ListOfBusiness from "../screens/ListOfBusiness";
import SuccessAddStore from "../screens/SuccessAddStore";

const ListOfBusinessNavigator = createSwitchNavigator(
  {
    ListOfBusiness: { screen: ListOfBusiness },
    SuccessAddStore: { screen: SuccessAddStore },
  },
  { initialRouteName: "ListOfBusiness" }
);

export default ListOfBusinessNavigator;
