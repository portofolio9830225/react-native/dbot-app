import React from "react";
import { View } from "react-native";
import { connect } from "react-redux";
import { mainBgColor } from "../../utils/ColorPicker";
import getWidth from "../../utils/getWidth";
import TabBarIcon from "./TabBarIcon";

const iconPair = {
	Dashboard: "grid",
	Orders: "basket-loaded",
	Inventory: "list",
	Settings: "equalizer",
	Logout: "logout",
};

class MainBottomBar extends React.Component {
	render() {
		const { navigation } = this.props;
		const { routes, index } = navigation.state;
		const tabHeight = 50;

		return (
			<View
				style={{
					width: "100%",
					alignItems: "center",
					paddingBottom: 10,
					paddingTop: 6,
					backgroundColor: mainBgColor,
				}}
			>
				{routes.map((route, i) => (
					<TabBarIcon
						key={i}
						isActive={index === i}
						navigation={navigation}
						icon={iconPair[route.routeName]}
						{...route}
					/>
				))}
			</View>
		);
	}
}

const mapStateToProps = state => ({
	auth: state.auth,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps)(MainBottomBar);
