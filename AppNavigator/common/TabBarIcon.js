import React from "react";
import { Animated, Easing } from "react-native";
import { SimpleLineIcons } from "@expo/vector-icons";
import { accent, accentDark, black, greydark, greylight, transparent } from "../../utils/ColorPicker";

const AnimatedIcon = Animated.createAnimatedComponent(SimpleLineIcons);

const TabBarIcon = ({ iconPack: Icon, ...props }) => (
	<Icon
		name={props.name}
		size={23}
		//style={{ marginTop: -3 }}
		color={props.focused ? accent : greydark}
	/>
);

export default TabBarIcon;
