import { createStackNavigator } from "react-navigation-stack";

import CompleteReg from "../screens/CompleteReg";
import CompleteChecker from "../screens/CompleteChecker";
import CompleteStore from "../screens/CompleteStore";
import ItemPage from "../screens/ItemPage";
import ItemVariant from "../screens/ItemVariant";
import BankDetails from "../screens/BankDetails";
import ProductNav from "./ProductNav";

const RegistrationNavigator = createStackNavigator(
	{
		CompleteChecker: { screen: CompleteChecker },
		CompleteReg: { screen: CompleteReg },
		CompleteStore: { screen: CompleteStore },
		ProductStarter: {
			screen: ProductNav,
		},
		// ItemPageStarter: { screen: ItemPage },
		// ItemVariantStarter: { screen: ItemVariant },
		BankDetailsStarter: { screen: BankDetails },
	},
	{
		defaultNavigationOptions: {
			headerStyle: {
				height: 30,
			},
		},

		initialRouteName: "CompleteChecker",
		headerMode: "none",
	}
);

export default RegistrationNavigator;
