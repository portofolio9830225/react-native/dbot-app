import React from "react";
import { createStackNavigator } from "react-navigation-stack";

import Orders from "../screens/Orders";
import OrderBulkShipping from "../screens/OrderBulkShipping";
import OrderPaymentRecord from "../screens/OrderPaymentRecord";
import OrderHistory from "../screens/OrderHistory";
import OrderSearch from "../screens/OrderSearch";
import OrderDetail from "../screens/OrderDetail";
import OrderContact from "../screens/OrderContact";
import OrderShipping from "../screens/OrderShipping";
import SetItemWeight from "../screens/SetItemWeight";
import OrderCustomer from "../screens/OrderCustomer";
import OrderCsv from "../screens/OrderCsv";

const OrdersNav = createStackNavigator(
	{
		Orders: {
			screen: Orders,
		},
		OrderBulkShipping: {
			screen: OrderBulkShipping,
		},
		OrderPaymentRecord: {
			screen: OrderPaymentRecord,
		},
		OrderHistory: {
			screen: OrderHistory,
		},
		OrderSearch: {
			screen: OrderSearch,
		},
		OrderDetail: {
			screen: OrderDetail,
		},
		OrderContact: {
			screen: OrderContact,
		},
		OrderCustomer: {
			screen: OrderCustomer,
		},
		OrderShipping: {
			screen: OrderShipping,
		},
		OrderCsv: {
			screen: OrderCsv,
		},
		SetItemWeight: {
			screen: SetItemWeight,
		},
	},
	{
		defaultNavigationOptions: {
			headerStyle: {
				height: 30,
			},
		},

		initialRouteName: "Orders",
		headerMode: "none",
	}
);

OrdersNav.navigationOptions = ({ navigation }) => {
	let tabBarVisible = true;
	if (navigation.state.index > 0) {
		tabBarVisible = false;
	}

	return {
		tabBarVisible,
	};
	// return {
	//   tabBarVisible: true,
	// };
};

export default OrdersNav;
