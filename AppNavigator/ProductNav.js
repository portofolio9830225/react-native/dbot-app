import React from "react";
import { createStackNavigator } from "react-navigation-stack";

import ItemPage from "../screens/ItemPage";
import ItemVariant from "../screens/ItemVariant";
import CategoryAdd from "../screens/CategoryAdd";

const ProductNav = createStackNavigator(
	{
		ItemPage: {
			screen: ItemPage,
		},
		ItemVariant: {
			screen: ItemVariant,
		},
		CategoryAdd: {
			screen: CategoryAdd,
		},
	},
	{
		defaultNavigationOptions: {
			headerStyle: {
				height: 30,
			},
		},

		initialRouteName: "ItemPage",
		headerMode: "none",
	}
);

ProductNav.navigationOptions = ({ navigation }) => {
	let tabBarVisible = true;
	if (navigation.state.index > 0) {
		tabBarVisible = false;
	}

	return {
		tabBarVisible,
	};
	// return {
	//   tabBarVisible: true,
	// };
};

export default ProductNav;
