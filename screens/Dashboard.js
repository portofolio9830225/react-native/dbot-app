import React from "react";
import { StyleSheet, ScrollView, View, Text, RefreshControl } from "react-native";
import { connect } from "react-redux";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import NavHeader from "../component/NavHeader";
import isEmpty from "../utils/isEmpty";
import { withNavigationFocus } from "react-navigation";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import getWidth from "../utils/getWidth";
import { getDashboardInfo } from "../store/actions/dashboardAction";
import numberShortner from "../utils/numberShortner";
import { GraphLine } from "../assets/VectorGraph";
import { accent, black, grey, greyblack, greylight, white } from "../utils/ColorPicker";
import langSelector from "../utils/langSelector";
import { setLoading, setRefresh } from "../store/actions/loadingAction";
import EmptyText from "../component/EmptyText";
import { TouchableRipple } from "react-native-paper";
import Typography from "../component/Typography";

class Dashboard extends React.Component {
	state = {
		greetTime: null,
	};

	fetchDashboard = () => {
		let time = new Date();
		this.props.getDashboardInfo(this.props.auth.token);
		this.setState({
			greetTime: time.getHours(),
		});
	};

	componentDidMount() {
		this.fetchDashboard();
	}

	componentDidUpdate(prevProps, prevState) {
		if (!prevProps.isFocused && this.props.isFocused) {
			this.fetchDashboard();
		}
		if (!prevProps.loading.refresh && this.props.loading.refresh) {
			setTimeout(() => {
				this.fetchDashboard();
			}, 1500);
		}
	}

	handleFilter = () => {
		console.log("dashboard:filter");
	};

	render() {
		const { greetTime } = this.state;
		const { store } = this.props.profile;
		const { funnel, order, sale, sold } = this.props.dashboard;
		const lang = langSelector.Dashboard.find(e => e.lang === this.props.preference.lang).data;

		const firstSectionFunnel = getWidth * 0.36;
		const secondSectionFunnel = getWidth * 0.36;
		const thirdSectionFunnel = getWidth * 0.26;
		const funnelHeight = 250;

		const GreetTimeText = ({ val }) => {
			let text = "day";

			if (val < 12) {
				text = lang.morning;
			} else if (val >= 15) {
				text = lang.evening;
			} else if (val >= 12 && val < 15) {
				text = lang.afternoon;
			}

			return `Good ${text},`;
		};

		const InfoCountBox = ({ title, count, color, first, onPress }) => {
			return (
				<TouchableWithoutFeedback onPress={onPress} style={[styles.infoCountBox]}>
					<View style={styles.infoCountContent}>
						<Typography style={[styles.infoCountBoxTitleText, { color }]}>{title}</Typography>
						<Typography type="title2" style={styles.infoCountBoxNumber}>
							{count}
						</Typography>
					</View>
				</TouchableWithoutFeedback>
			);
		};

		return (
			<LayoutView>
				<NavHeader />

				<ScrollView
					showsVerticalScrollIndicator={false}
					refreshControl={
						<RefreshControl
							refreshing={this.props.loading.refresh}
							onRefresh={() => {
								this.props.setRefresh(true);
							}}
						/>
					}
					contentContainerStyle={{ width: "100%" }}
				>
					<View style={styles.container}>
						<View style={{ width: "100%", alignSelf: "center" }}>
							{/* {!isEmpty(greetTime) && (
                <>
                  <Text
                    style={{
                      fontSize: 22,
                      fontFamily: "medium",
                      marginBottom: 3,
                    }}
                  >
                    <GreetTimeText val={greetTime} />
                  </Text>

                  <PageTitle fontSize={32}>{store.name}</PageTitle>    
                </>
              )} */}
							<PageTitle fontSize={32}>Dashboard</PageTitle>
							<View style={styles.sectionHeaderContainer}>
								<Text style={styles.sectionTitle}>Summary</Text>
								{/* <TouchableRipple rippleColor="transparent" onPress={this.handleFilter}>
									<Text style={styles.filterTextButton}>All Time</Text>
								</TouchableRipple> */}
							</View>

							<View style={styles.infoContainer}>
								<InfoCountBox
									color="#C40829"
									title={`${lang.sales} (RM)`}
									count={!sale ? "-" : sale < 1000 ? sale.toFixed(2) : numberShortner(sale)}
								/>

								<InfoCountBox color="#13BBCF" title={lang.productsSold} count={!sold ? "-" : sold} />
							</View>

							{!isEmpty(funnel) ? (
								<View style={styles.funnelContainer}>
									<View style={[styles.funnel]}>
										<View
											style={{
												width: firstSectionFunnel,
											}}
										>
											<View
												style={[
													styles.funnelHeader,
													// { paddingLeft: getWidth * 0.05 },
												]}
											>
												<Text style={styles.funnelHeaderTitle}>{lang.views}</Text>
												<Text style={styles.funnelHeaderDesc}>{funnel.views_count || 0}</Text>
											</View>
										</View>
										<View style={{ width: secondSectionFunnel }}>
											<View style={styles.funnelHeader}>
												<Text style={styles.funnelHeaderTitle}>{lang.leads}</Text>
												<Text style={styles.funnelHeaderDesc}>{funnel.leads_count || 0}</Text>
												<Text style={styles.funnelHeaderPercent}>
													{funnel.leads_count / funnel.views_count === 0 ||
													isNaN(funnel.leads_count / funnel.views_count)
														? "--"
														: ((funnel.leads_count / funnel.views_count) * 100).toFixed(1)}
													%
												</Text>
											</View>
										</View>
										<View style={{ width: thirdSectionFunnel }}>
											<View style={styles.funnelHeader}>
												<Text style={styles.funnelHeaderTitle}>{lang.sales}</Text>
												<Text style={styles.funnelHeaderDesc}>
													{funnel.purchased_count || 0}
												</Text>
												<Text style={styles.funnelHeaderPercent}>
													{funnel.purchased_count / funnel.leads_count === 0 ||
													isNaN(funnel.purchased_count / funnel.leads_count)
														? "--"
														: ((funnel.purchased_count / funnel.leads_count) * 100).toFixed(
																1
														  )}
													%
												</Text>
											</View>
										</View>
									</View>
									{funnel.views_count &&
									funnel.views_count >= funnel.leads_count &&
									funnel.leads_count >= funnel.purchased_count ? (
										<View
											style={[
												styles.funnel,
												{
													height: funnelHeight,

													// paddingVertical: 20,
												},
											]}
										>
											<GraphLine
												width={firstSectionFunnel}
												height={funnelHeight}
												to={(funnel.views_count - funnel.leads_count) / funnel.views_count}
												color={greylight}
											/>

											<View style={{ justifyContent: "space-between" }}>
												<View />

												<GraphLine
													width={secondSectionFunnel}
													height={funnelHeight * (funnel.leads_count / funnel.views_count)}
													to={
														funnel.leads_count
															? (funnel.leads_count - funnel.purchased_count) /
															  funnel.leads_count
															: 0
													}
													color={greylight}
												/>
												<View />
											</View>

											<View
												style={{
													justifyContent: "space-between",
													height: "100%",
												}}
											>
												<View />

												<View
													style={{
														borderColor: greylight,
														height:
															(funnel.purchased_count / funnel.views_count) *
															funnelHeight,
														width: thirdSectionFunnel,
														backgroundColor: greylight,
													}}
												/>
												<View />
											</View>
										</View>
									) : (
										<View style={styles.funnelContainer}>
											<EmptyText title="Not enough data" noImage />
										</View>
									)}
								</View>
							) : null}
						</View>
					</View>
				</ScrollView>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
	sectionHeaderContainer: {
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-between",
		height: 36,
		alignItems: "flex-end",
		marginTop: 8,
	},
	sectionTitle: {
		fontFamily: "bold",
		fontSize: 20,
	},
	filterTextButton: {
		color: accent,
		fontSize: 16,
		fontFamily: "regular",
	},
	infoContainer: {
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		marginTop: 12,
		marginBottom: 10,
	},
	infoCountBox: {
		width: getWidth * 0.5 - 5,
		//marginRight: getWidth * 0.03,
		borderRadius: 10,
		overflow: "hidden",
		//borderWidth: 1.5,
		///borderColor: "#eee",
		backgroundColor: "white",
	},
	infoCountBoxBackground: {
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		width: "100%",
		height: "100%",
	},
	infoCountContent: {
		width: "100%",
		paddingVertical: 12,
		paddingHorizontal: 16,
	},
	infoCountBoxTitleText: {
		color: grey,
		fontFamily: "semibold",
		marginBottom: 3,
	},
	infoCountBoxNumber: {
		fontFamily: "semibold",
	},
	infoCountBoxTitleBox: {
		width: "100%",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
	},
	funnelContainer: {
		backgroundColor: white,
		paddingVertical: 15,
		marginBottom: 50,
		borderRadius: 10,
	},
	funnel: {
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-between",
	},
	funnelHeader: {
		width: "100%",
		alignSelf: "center",
		marginVertical: 10,
		paddingLeft: 15,
	},
	funnelHeaderTitle: {
		fontFamily: "regular",
		color: greyblack,
		fontSize: 12,
		marginBottom: 5,
	},
	funnelHeaderDesc: {
		fontFamily: "bold",
		fontSize: 22,
	},
	funnelHeaderPercent: {
		fontSize: 12,
		fontFamily: "light",
		marginTop: 5,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	dashboard: state.dashboard,
	profile: state.profile,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, { getDashboardInfo, setRefresh, setLoading })(withNavigationFocus(Dashboard));
