import React from "react";
import { StyleSheet, View, Text, RefreshControl, FlatList } from "react-native";
import { connect } from "react-redux";
import { setRefresh } from "../store/actions/loadingAction";
import Animated, { EasingNode } from "react-native-reanimated";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import EmptyText from "../component/EmptyText";
import NavHeader from "../component/NavHeader";
import isEmpty from "../utils/isEmpty";
import { withNavigationFocus } from "react-navigation";
import langSelector from "../utils/langSelector";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { black, blue, green, greyblack, orange, red, transparent } from "../utils/ColorPicker";
import { TouchableRipple } from "react-native-paper";
import OrderBox from "../component/OrderBox";
import DateConverter from "../utils/DateConverter";
import { getAllAgentOrders } from "../store/actions/agentAction";
import getWidth from "../utils/getWidth";
import Loading from "../component/Loading";

class AgentOrderHistory extends React.Component {
	constructor(props) {
		super(props);
		this.transValue = new Animated.Value(1);
	}

	state = {
		order: [],
		active: 1,
	};

	componentDidMount() {
		this.props.getAllAgentOrders(this.props.auth.token);
	}

	componentDidUpdate(prevProps, prevState) {
		if (!prevProps.loading.refresh && this.props.loading.refresh) {
			setTimeout(() => {
				this.props.getAllAgentOrders(this.props.auth.token);
			}, 1500);
		}
	}

	handleActive = active => () => {
		Animated.timing(this.transValue, {
			toValue: active,
			duration: 200,
			easing: EasingNode.quad, // Easing is an additional import from react-native
			useNativeDriver: true, // To make use of native driver for performance
		}).start();
		this.setState({
			active,
		});
	};

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handlePage =
		(route, params = {}) =>
		() => {
			this.props.navigation.push(route, params);
		};

	render() {
		const { active } = this.state;
		const { orderList, isOrderFetched } = this.props.agent;
		const lang = langSelector.AgentOrder.find(e => e.lang === this.props.preference.lang).data;

		const tabIndicatorMove = this.transValue.interpolate({
			inputRange: [1, 2],
			outputRange: [0, getWidth * 0.3],
		});

		const StatusText = ({ status }) => {
			let color = greyblack;
			let text = lang.pending;
			let icon = "progress-clock";

			switch (status) {
				case 1:
					{
						text = lang.accepted;
						color = green;
						icon = "progress-check";
					}
					break;
				case 2:
					{
						text = lang.shipping;
						color = orange;
						icon = "truck-fast";
					}
					break;
				case 3:
					{
						text = lang.done;
						color = blue;
						icon = "check-circle";
					}
					break;
				case 8:
					{
						text = lang.canceled;
						color = red;
						icon = "block-helper";
					}
					break;
				case 9:
					{
						text = lang.rejected;
						color = red;
						icon = "close-circle-outline";
					}
					break;
			}

			return (
				<View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
					<MaterialCommunityIcons name={icon} color={color} />
					<Text
						style={{
							fontSize: 12,
							fontFamily: "medium",
							color: color,
							width: "100%",
							paddingLeft: 3,
						}}
					>
						{text}
					</Text>
				</View>
			);
		};

		const RentalTabBox = props => (
			<TouchableRipple
				onPress={this.handleActive(props.val)}
				rippleColor={transparent}
				style={[
					styles.rentalTabItem,
					props.active === props.val ? styles.rentalTabItemInactive : styles.rentalTabItemInactive,
					{
						width: "30%",
					},
				]}
			>
				<View style={styles.rentalTabTextContainer}>
					<Text
						style={[
							styles.rentalTabText,
							props.active === props.val ? styles.rentalTabTextActive : styles.rentalTabTextInactive,
						]}
					>
						{props.text}
					</Text>
				</View>
			</TouchableRipple>
		);

		const RentalTab = props => (
			<View style={{ width: "100%", paddingBottom: 15 }}>
				<View style={styles.rentalTabContainer}>
					<RentalTabBox active={props.active} val={1} text={lang.done} />
					<RentalTabBox active={props.active} val={2} text={lang.incompleted} />
				</View>
				<Animated.View
					style={{
						width: "30%",
						marginTop: 8,
						height: 2,
						borderRadius: 1,
						backgroundColor: black,
						transform: [{ translateX: tabIndicatorMove }],
					}}
				/>
			</View>
		);

		return (
			<LayoutView>
				<NavHeader onBack={this.handleBack} />

				{isOrderFetched ? (
					<FlatList
						refreshControl={
							<RefreshControl
								refreshing={this.props.loading.refresh}
								onRefresh={() => {
									this.props.setRefresh(true);
								}}
							/>
						}
						showsVerticalScrollIndicator={false}
						style={{ width: "100%", alignSelf: "center", position: "relative" }}
						contentContainerStyle={{
							width: "100%",
							paddingVertical: 15,
						}}
						ListHeaderComponent={
							<View
								style={{
									width: "100%",
									alignItems: "flex-start",
									alignSelf: "center",
									marginBottom: 10,
								}}
							>
								<PageTitle>{lang.title}</PageTitle>
								<RentalTab active={active} />
							</View>
						}
						data={
							active === 1
								? orderList.filter(e => e.status === 3)
								: orderList.filter(e => e.status === 9 || e.status === 8)
						}
						ListEmptyComponent={<EmptyText title={lang.emptyText} />}
						keyExtractor={e => e.order_id}
						renderItem={({ item, index, separators }) => {
							return (
								<OrderBox
									statusComponent={<StatusText status={item.status} />}
									statusType={2}
									onPress={this.handlePage("AgentOrderDetail", {
										oID: item.order_id,
									})}
									key={index}
									oTitle={`${lang.cardTitle}: ${item.order_id}`}
									id={item.id}
									nameIcon="archive-outline"
									name={item.package_name}
									date={DateConverter(item.created_ts)}
									price={item.total_price}

									// quantity={item.item_count}
									// status={item.status}
									// onPressStatus={() => {
									//   this.setState({
									//     statusCard: { id: item.order_id, status: item.status },
									//   });
									// }}
								/>
							);
						}}
					/>
				) : (
					<Loading />
				)}
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
	rentalTabContainer: {
		width: "100%",
		flexDirection: "row",
		//justifyContent: "center",
		alignItems: "center",
	},
	rentalTabItem: {
		paddingTop: 10,
		//marginBottom: 15,
		justifyContent: "center",
		alignItems: "center",
		borderBottomWidth: 2,
	},
	rentalTabItemActive: {
		borderBottomColor: black,
	},
	rentalTabItemInactive: {
		borderBottomColor: transparent,
	},
	rentalTabTextContainer: {
		width: "100%",
		flexDirection: "column",
		alignItems: "center",
	},
	rentalTabText: {
		fontSize: 13,
	},
	indicator: {
		alignSelf: "flex-end",
		width: 8,
		height: undefined,
		aspectRatio: 1,
		borderRadius: 4,
		marginRight: 3,
	},
	rentalTabTextActive: {
		fontFamily: "bold",
	},
	rentalTabTextInactive: {
		fontFamily: "light",
		color: greyblack,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	agent: state.agent,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, { getAllAgentOrders, setRefresh })(withNavigationFocus(AgentOrderHistory));
