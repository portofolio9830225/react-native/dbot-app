import React, { useEffect, useState } from "react";

import AsyncStorage from "@react-native-async-storage/async-storage";
import { StyleSheet, Text, View, Keyboard } from "react-native";
import * as WebBrowser from "expo-web-browser";
import registerPushNoti from "../utils/registerPushNoti";

import { connect } from "react-redux";
import { sendUserEmail, checkEmail, forgotPassword, registerUser, loginUser } from "../store/actions/authAction";
import { setLoading } from "../store/actions/loadingAction";
import { customNoti } from "../store/actions/alertAction";

import { Button, TouchableRipple } from "react-native-paper";

import { LayoutView } from "../component/Layout";

import PageTitle from "../component/PageTitle";
import NavHeader from "../component/NavHeader";
import isEmpty from "../utils/isEmpty";
import { grey, greywhite } from "../utils/ColorPicker";
import PasswordField from "../component/PasswordField";
import TextField from "../component/TextField";
import { Ionicons, Feather } from "@expo/vector-icons";
import FadeInOut from "react-native-fade-in-out";
import CustomButton from "../component/CustomButton";
import Typography from "../component/Typography";
import { SectionBox, SectionDivider } from "../component/SectionComponents";

// const SignInEmail = props => {
// 	const [email, emailSet] = useState("");
// 	const [isRegister, isRegisterSet] = useState(false);

// 	useEffect(() => {
// 		return () => {
// 			props.setLoading(false);
// 		};
// 	}, []);

// 	useEffect(() => {
// 		if (!isEmpty(props.alert.redirect)) {
// 			let params = {};
// 			if (props.alert.redirect === "SignInPasscode") {
// 				params = { email: email.toLowerCase() };
// 			}
// 			props.navigation.navigate(props.alert.redirect, params);
// 		}
// 	}, [props.alert.redirect]);

// 	handleGoogleLogin = () => {
// 		console.log("google");
// 	};

// 	handleSubmit = () => {
// 		Keyboard.dismiss();
// 		let data = {
// 			email: email.toLowerCase(),
// 		};
// 		props.sendUserEmail(data);
// 	};

// 	return (
// 		<LayoutView>
// 			{/* <NavHeader
// 				onPress={this.handleSubmit}
// 				actionText="Next"
// 				// actions={[
// 				// 	{
// 				// 		onPress: this.handleURL("https://app.storeup.io/registration"),
// 				// 		icon: <Text style={styles.signUpText}>Sign Up</Text>,
// 				// 	},
// 				// ]}
// 			/> */}
// 			<View style={styles.container}>
// 				{/* <PageTitle> </PageTitle> */}
// 				<Ionicons name={isRegister ? "person" : "mail"} size={86} color="#0D65FB" />
// 				<Typography type="largetitle" style={styles.pageTitle}>
// 					{isRegister ? "Register an Account" : "Login with Your StoreUp ID"}
// 				</Typography>

// 				<TextField
// 					keyboardType="email-address"
// 					style={styles.input}
// 					label="Email"
// 					value={email}
// 					onChangeText={val => {
// 						emailSet(val);
// 					}}
// 					autoCapitalize="none"
// 				/>
// 				<CustomButton style={styles.button} onPress={this.handleSubmit}>
// 					Continue
// 				</CustomButton>

// 				<TouchableRipple
// 					rippleColor="transparent"
// 					onPress={() => {
// 						isRegisterSet(!isRegister);
// 					}}
// 				>
// 					<Text style={styles.buttonText}>
// 						{isRegister ? "Already have StoreUp ID? Login here" : "Don't have a StoreUp ID? Register here"}
// 					</Text>
// 				</TouchableRipple>
// 				<TouchableRipple rippleColor="transparent" onPress={handleGoogleLogin} style={{ marginTop: 10 }}>
// 					<Text style={styles.buttonText}>Google login (ALPHA)</Text>
// 				</TouchableRipple>
// 			</View>
// 		</LayoutView>
// 	);
// };

class SignInEmail extends React.Component {
	state = {
		email: "",
		password: "",
		isAppUsed: null,
		isRegister: null,
		isPassword: false,
	};

	async componentDidMount() {
		await AsyncStorage.getItem("@dbot:language").then(data => {
			this.setState({
				isAppUsed: !isEmpty(data),
			});
		});

		this.setState({
			isRegister: !isEmpty(this.props.navigation.state.params)
				? this.props.navigation.state.params.isRegister
				: false,
			email: !isEmpty(this.props.navigation.state.params) ? this.props.navigation.state.params.email : "",
		});
	}

	componentDidUpdate(prevProps, prevState) {
		if (!isEmpty(this.props.alert.redirect) && isEmpty(prevProps.alert.redirect)) {
			let params = {};
			if (this.props.alert.redirect === "SignInPasscode") {
				params = { email: this.state.email.toLowerCase() };
			}
			this.props.navigation.navigate(this.props.alert.redirect, params);
		}

		if (prevState.email !== this.state.email) {
			this.setState({
				isPassword: false,
			});
		}

		if (!isEmpty(this.props.auth.token) && isEmpty(prevProps.auth.token)) {
			AsyncStorage.setItem("@dbot:businessToken", this.props.auth.token);
			AsyncStorage.setItem("@dbot:refreshToken", this.props.auth.refreshToken);
			console.log("token");
		}
		if (!isEmpty(this.props.profile.store) && isEmpty(prevProps.profile.store)) {
			registerPushNoti(this.props.auth.token);
			console.log("registerPushNoti");
		}
		if (this.props.auth.regNotiTry && !prevProps.auth.regNotiTry) {
			console.log("regNotiTry");
			if (!isEmpty(this.props.alert.notiScreen)) {
				this.props.navigation.navigate(this.props.alert.notiScreen, this.props.alert.notiParams);
			} else {
				this.props.navigation.navigate("Registration");
			}
		}
	}

	componentWillUnmount() {
		this.props.setLoading(false);
	}

	handleChange = name => value => {
		this.setState({
			[name]: value,
		});
	};

	handleSubmit = async () => {
		Keyboard.dismiss();

		if (!this.state.isPassword) {
			let data = {
				email: this.state.email.toLowerCase(),
			};
			let checkEmail = await this.props.checkEmail(data);

			console.log(checkEmail);

			if (this.state.isRegister) {
				if (checkEmail.user_exist === true) {
					this.props.customNoti("Email already exists");
				} else {
					this.setState({
						isPassword: true,
					});
				}
			} else {
				if (checkEmail.user_exist === true) {
					console.log(checkEmail);
					if (checkEmail.google_exist && checkEmail.password_exist) {
						this.props.customNoti("Unknown Error");
					} else if (checkEmail.google_exist && !checkEmail.password_exist) {
						this.props.customNoti("This account use google Sign In");
					} else if (!checkEmail.google_exist && checkEmail.password_exist) {
						this.setState({
							isPassword: true,
						});
					} else if (!checkEmail.google_exist && !checkEmail.password) {
						let params = { email: this.state.email.toLowerCase() };
						this.props.navigation.navigate("SignInCreatePassword", params);
					}
				} else {
					this.props.customNoti("Email not found");
				}
			}
		} else {
			let data = {
				email: this.state.email.toLowerCase(),
				password: this.state.password,
			};
			if (this.state.isRegister) {
				this.props.registerUser(data);
			} else {
				this.props.loginUser(data);
			}
		}
	};

	handleGoogle = () => {
		//link to SigninPassword
		this.props.navigation.navigate("SignInCreatePassword");
	};

	render() {
		const { email, pass, isRegister, isPassword } = this.state;
		const isReady = isRegister !== null;

		if (!isReady) {
			return null;
		} else {
			return (
				<LayoutView>
					{/* <NavHeader
						onPress={this.handleSubmit}
						actionText="Next"
						// actions={[
						// 	{
						// 		onPress: this.handleURL("https://app.storeup.io/registration"),
						// 		icon: <Text style={styles.signUpText}>Sign Up</Text>,
						// 	},
						// ]}
					/> */}
					<View style={styles.container}>
						<Ionicons name={isRegister ? "person" : "mail"} size={86} color="#0D65FB" />
						<Typography type="largetitle" style={styles.pageTitle}>
							{isRegister ? "Register an Account" : "Login with Your StoreUp ID"}
						</Typography>
						{/* <TouchableRipple
							rippleColor="transparent"
							onPress={this.handleGoogle}
							style={{ width: "100%" }}
						>
							<View
								style={{
									width: "100%",
									backgroundColor: "white",
									height: 50,
									borderRadius: 5,
									flexDirection: "row",
									alignItems: "center",
									justifyContent: "center",
									padding: 10,
								}}
							>
								<Ionicons name="logo-google" size={24} color="black" />
								<Text
									style={{
										fontSize: 16,
										fontWeight: "bold",
										marginLeft: 20,
										// width: "100%",
									}}
								>
									Continue with Google
								</Text>
							</View>
						</TouchableRipple>

						<View
							style={{
								width: "100%",
								flexDirection: "row",
								justifyContent: "space-between",
								alignItems: "center",
								paddingVertical: 15,
							}}
						>
							<View
								style={{
									width: "40%",
									borderBottomColor: greywhite,
									borderBottomWidth: 1,
								}}
							></View>
							<Typography type="subtitle" style={styles.subtitle}>
								or
							</Typography>
							<View
								style={{
									width: "40%",
									borderBottomColor: greywhite,
									borderBottomWidth: 1,
								}}
							></View>
						</View> */}

						<TextField
							keyboardType="email-address"
							style={styles.input}
							label="Email"
							value={email}
							onChangeText={this.handleChange("email")}
							autoCapitalize="none"
						/>
						{isPassword ? (
							<TextField
								secureTextEntry
								style={styles.input}
								label="Password"
								// value={email}
								onChangeText={this.handleChange("password")}
								autoCapitalize="none"
							/>
						) : null}

						<CustomButton style={styles.button} onPress={this.handleSubmit}>
							Continue
						</CustomButton>

						<TouchableRipple>
							<Text
								style={styles.buttonText}
								onPress={() => {
									this.props.navigation.navigate("ForgotPassword", { email: this.state.email });
								}}
							>
								Forgot your password?
							</Text>
						</TouchableRipple>

						<TouchableRipple
							rippleColor="transparent"
							onPress={() => {
								this.setState({ isRegister: !this.state.isRegister });
							}}
						>
							<Text style={styles.buttonText}>
								{isRegister
									? "Already have StoreUp ID? Login here"
									: "Don't have a StoreUp ID? Register here"}
							</Text>
						</TouchableRipple>
					</View>
				</LayoutView>
			);
		}
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		paddingBottom: 100,
		//backgroundColor: "red",
		justifyContent: "center",
	},
	pageTitle: {
		width: "100%",
		fontFamily: "bold",
		textAlign: "center",
		marginBottom: 25,
	},
	input: {
		width: "100%",
		marginVertical: 5,
	},
	buttonText: {
		color: "#0D65FB",
		textAlign: "center",
		width: "100%",
		fontSize: 16,
		fontFamily: "regular",
		//marginTop: 50,
		paddingVertical: 10,
	},
	button: {
		justifyContent: "center",
		width: "100%",
		marginBottom: 20,
		marginTop: 20,
	},
	googleLogo: {
		height: 21,
		width: 21,
	},
	signUpText: {
		fontFamily: "regular",
		paddingVertical: 5,
		fontSize: 16,
	},
	forgotText: {
		fontFamily: "light",
		//marginTop: 5,
		paddingVertical: 5,
		fontSize: 14,
		color: grey,
		alignSelf: "flex-start",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	profile: state.profile,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	sendUserEmail,
	checkEmail,
	forgotPassword,
	registerUser,
	loginUser,
	setLoading,
	customNoti,
})(SignInEmail);
