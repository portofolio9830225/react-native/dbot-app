import React from "react";
import { StyleSheet, View, Text, ScrollView } from "react-native";
import { connect } from "react-redux";

import { customNoti } from "../store/actions/alertAction";
import { LayoutView } from "../component/Layout";

import isEmpty from "../utils/isEmpty";

import NavHeader from "../component/NavHeader";
import { withNavigationFocus } from "react-navigation";
import { accent, greyblack, mainBgColor } from "../utils/ColorPicker";
import {
	editPaymentMethod,
	editTransactionFeeCharges,
	getAllPaymentMethod,
	getPaymentMethod,
} from "../store/actions/catalogAction";

import { setLoading } from "../store/actions/loadingAction";
import { SectionBox, SectionDivider } from "../component/SectionComponents";
import SwitcherWithLabel from "../component/SwitcherWithLabel";
import { TouchableRipple } from "react-native-paper";
import Typography from "../component/Typography";
import { Ionicons } from "@expo/vector-icons";
import PortalSafeView from "../component/PortalSafeView";
import TextField from "../component/TextField";

class PaymentMethod extends React.Component {
	state = {
		isFetched: false,
		list: [
			{
				payment_method_id: 1,
				method_name: "Online banking (FPX)",
				message: "A transaction fee of RM1.50 will be applied.\nPayout may take up to 3 business days.",
			},
			{
				payment_method_id: 2,
				method_name: "Manual payment",
				message: "Your bank information will be shown to your customer on checkout.",
			},
		],
		arr: [],
		transFeeDialog: false,
		fpxTransFee: "",
	};

	processInfo = () => {
		let arr = [];
		this.props.profile.store.payment_method.map(e => {
			arr.push(e.id);
		});

		this.setState(
			{
				arr: arr,
			},
			() => {
				this.setState({
					isFetched: true,
				});
			}
		);
	};

	componentDidMount() {
		this.processInfo();
	}

	componentDidUpdate(prevProps, prevState) {
		if (!prevState.isFetched && this.state.isFetched) {
			this.props.setLoading(false);
		}
		if (isEmpty(prevProps.alert.redirect) && !isEmpty(this.props.alert.redirect)) {
			if (this.props.alert.redirect === "editCatalogPaymentMethod") {
				this.handleBack();
			} else if (this.props.alert.redirect === "editTransactionCharges") {
				setTimeout(() => {
					this.handleTransFeeDialog(false)();
				}, 1);
			}
		}
		if (!prevState.transFeeDialog && this.state.transFeeDialog) {
			this.setState({
				fpxTransFee: this.props.profile.store.fpx_transaction_fee,
			});
		}
	}

	handlePage = page => () => {
		this.props.navigation.push(page);
	};

	handleFilter = data => () => {
		let array = [];
		let checked = this.state.arr.findIndex(f => f === data) != -1;

		if (!checked) {
			array = [...this.state.arr, data];
		} else {
			array = this.state.arr.filter(e => e !== data);
		}

		this.setState({
			arr: array,
		});
	};

	handleBack = () => {
		this.props.navigation.pop();
	};

	handleTransFeeDialog = state => () => {
		this.setState({
			transFeeDialog: state,
		});
	};

	handleChangeFpx = val => () => {
		this.setState({
			fpxTransFee: val,
		});
	};

	handleSubmit = () => {
		if (this.state.transFeeDialog) {
			let data = {
				fpx_transaction_fee: this.state.fpxTransFee,
			};

			this.props.editTransactionFeeCharges(this.props.profile.store.id, data, this.props.auth.token);
		} else {
			let data = {
				method_id: this.state.arr,
			};
			this.props.editPaymentMethod(this.props.profile.store.id, data, this.props.auth.token);
		}
	};

	render() {
		const { arr, list, transFeeDialog, fpxTransFee } = this.state;

		const SettingsList = ({ label, onPress }) => {
			return (
				<TextField
					inputStyle={{ paddingHorizontal: 0 }}
					onPressInput={onPress}
					value={label}
					type="multiline"
				/>
			);
		};

		const ItemSelector = ({ title, desc, onPress, active }) => {
			return (
				<TouchableRipple rippleColor="transparent" onPress={onPress}>
					<View
						style={{
							width: "100%",
							flexDirection: "row",
							justifyContent: "space-between",
							alignItems: "center",
							paddingVertical: 10,
						}}
					>
						<View style={{ flex: 1 }}>
							<Typography>{title}</Typography>

							{desc ? (
								<Typography type="caption" style={{ color: greyblack }}>
									{desc}
								</Typography>
							) : null}
						</View>
						<View
							style={{
								width: 22,
								height: 22,
								alignItems: "center",
								justifyContent: "center",
							}}
						>
							{active ? <Ionicons color={accent} size={20} name="checkmark" /> : null}
						</View>
					</View>
				</TouchableRipple>
			);
		};

		return (
			<LayoutView>
				<NavHeader
					onBack={this.handleBack}
					onPress={this.handleSubmit}
					actionText="Done"
					title="Payment Methods"
				/>
				{transFeeDialog ? (
					<PortalSafeView>
						<View
							style={{
								width: "100%",
								height: "100%",
								backgroundColor: mainBgColor,
								paddingHorizontal: 16,
							}}
						>
							<NavHeader
								onBack={this.handleTransFeeDialog(false)}
								actionText="Done"
								onPress={this.handleSubmit}
							/>
							<SectionBox title="FPX Charge">
								<ItemSelector
									title="Merchant"
									onPress={this.handleChangeFpx("merchant")}
									active={fpxTransFee == "merchant"}
								/>
								<SectionDivider />
								<ItemSelector
									title="Customer"
									onPress={this.handleChangeFpx("customer")}
									active={fpxTransFee == "customer"}
								/>
							</SectionBox>
							<Typography type="caption" style={{ color: greyblack, paddingLeft: 12 }}>
								Selected party will cover the transaction fee.
							</Typography>
						</View>
					</PortalSafeView>
				) : null}
				<View style={{ width: "100%", marginTop: 12 }}>
					{list.map((e, i) => {
						let id = e.payment_method_id;
						return (
							<SwitcherWithLabel
								key={i}
								label={e.method_name}
								desc={e.message}
								enabled={arr.findIndex(f => f === id) != -1}
								value={id}
								onValueChange={this.handleFilter(id)}
								style={{ paddingVertical: 8 }}
							/>
						);
					})}
					<SectionBox title="Advanced settings">
						<SettingsList label="Transaction Fee Charges" onPress={this.handleTransFeeDialog(true)} />
					</SectionBox>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	pageDesc: {
		color: greyblack,
		fontFamily: "regular",
		fontSize: 13,
		marginBottom: 10,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	catalog: state.catalog,
	profile: state.profile,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	getAllPaymentMethod,
	getPaymentMethod,
	editPaymentMethod,
	editTransactionFeeCharges,
	setLoading,
	customNoti,
})(withNavigationFocus(PaymentMethod));
