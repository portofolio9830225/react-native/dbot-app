import React from "react";
import { StyleSheet, View, Text, Animated, Easing, RefreshControl, FlatList } from "react-native";
import { isTablet } from "react-native-device-detection";
import { connect } from "react-redux";

import { setLoading, setRefresh } from "../store/actions/loadingAction";
import { deleteCategory, getSingleCategory } from "../store/actions/categoryAction";

import { LayoutView } from "../component/Layout";
import NavHeader from "../component/NavHeader";

import { TouchableRipple } from "react-native-paper";
import { MaterialCommunityIcons, Feather } from "@expo/vector-icons";
import { accent, greywhite, transparent } from "../utils/ColorPicker";
import getWidth from "../utils/getWidth";
import isEmpty from "../utils/isEmpty";
import ProductRow from "../component/ProductRow";
import Typography from "../component/Typography";
import AlertDialog from "../component/AlertDialog";
import EmptyText from "../component/EmptyText";

// import { SectionDivider } from "../component/SectionDivider";

class CategoryDetail extends React.Component {
	constructor(props) {
		super(props);
		this.scrollViewRef = React.createRef();
		this.transValue = new Animated.Value(1);
	}

	state = {
		details: [],
		removeDialog: false,
	};

	// fetchData = () => {
	// 	if (!isEmpty(this.props.navigation.state.params.id)) {
	// 		this.setState(
	// 			{
	// 				isEdit: true,
	// 				viewOnly: this.props.navigation.state.params.viewOnly,
	// 			},
	// 			() =>
	// 			{
	// 				this.props.getSingleCategory(
	// 					this.props.navigation.state.params.id,
	// 					this.props.auth.token,
	// 					// this.props.navigation.state.params.bID
	// 				);
	// 			}
	// 		);
	// 	}
	// };

	componentDidMount() {
		this.props.setLoading(true);
		this.props.getSingleCategory(this.props.navigation.state.params.id, this.props.auth.token);
	}

	componentDidUpdate(prevProps, prevState) {
		if (!prevProps.loading.refresh && this.props.loading.refresh) {
			setTimeout(() => {
				this.props.getSingleCategory(this.props.navigation.state.params.id, this.props.auth.token);
			}, 1500);
		}
		if (isEmpty(prevProps.alert.status) && this.props.alert.status === "success") {
			if (this.props.alert.redirect === "deleteCategory") {
				this.handleBack();
			}
		}
	}

	componentWillUnmount() {
		this.props.getSingleCategory();
	}

	handleAddProduct = () => {
		this.props.navigation.push("CategoryProduct");
	};

	handleEdit = () => {
		this.props.navigation.push("CategoryAdd", { fromPage: "CategoryDetail" });
	};

	handleBack = () => {
		this.props.navigation.pop();
	};

	handleRemoveDialog = state => () => {
		this.setState({
			removeDialog: state,
		});
	};

	handleRemove = () => {
		this.setState(
			{
				removeDialog: false,
			},
			() => {
				this.props.deleteCategory(this.props.category.singleCategory.id, this.props.auth.token);
			}
		);
	};

	tabWidth = getWidth;

	render() {
		const { singleCategory } = this.props.category;

		const AddButton = props => {
			return (
				<TouchableRipple rippleColor={transparent} onPress={this.handleAddProduct}>
					<View style={styles.socialMediaBoxContainer}>
						<MaterialCommunityIcons name="plus" size={23} color={accent} />

						<Typography
							type="headline"
							style={{
								color: accent,
								fontFamily: "regular",
								marginLeft: 12,
							}}
						>
							Add Products
						</Typography>
					</View>
				</TouchableRipple>
			);
		};

		return (
			<LayoutView>
				<NavHeader
					onBack={this.handleBack}
					actions={[
						{
							title: "Edit Category",
							iconType: Feather,
							iconName: "edit-3",
							onPress: this.handleEdit,
						},
						{
							title: "Remove Category",
							iconType: Feather,
							iconName: "divide-circle",
							onPress: this.handleRemoveDialog(true),
						},
					]}
					title={singleCategory.name}
				/>
				<AlertDialog
					actions={[
						{
							title: "Remove",
							onPress: this.handleRemove,
						},
						{
							title: "Cancel",
							onPress: this.handleRemoveDialog(false),
						},
					]}
					title="Remove this category?"
					active={this.state.removeDialog}
					onDismiss={this.handleRemoveDialog(false)}
				/>

				<View style={styles.container}>
					<FlatList
						refreshControl={
							<RefreshControl
								refreshing={this.props.loading.refresh}
								onRefresh={() => {
									this.props.setRefresh(true);
								}}
							/>
						}
						showsVerticalScrollIndicator={false}
						style={styles.container}
						contentContainerStyle={[
							{
								width: "100%",
								paddingTop: 16,
							},
							isEmpty(singleCategory.products) ? { flex: 1, paddingBottom: 100 } : {},
						]}
						ListEmptyComponent={
							<View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
								<EmptyText
									style={{ flex: undefined }}
									title="No products in this category"
									text="Start adding product into this category"
								/>
								<View style={{ marginBottom: 16 }} />
								<AddButton />
							</View>
						}
						ListFooterComponent={
							!isEmpty(singleCategory.products) ? (
								<View
									style={{
										borderTopWidth: 1,
										borderTopColor: greywhite,
										borderBottomWidth: 1,
										borderBottomColor: greywhite,
									}}
								>
									<AddButton />
								</View>
							) : null
						}
						data={singleCategory.products}
						renderItem={({ item, index, separators }) => {
							return (
								<ProductRow
									key={index}
									isFirst={index === 0}
									image={isEmpty(item.images) ? null : item.images[0].url_s}
									imageKey={`${item.images[0].file.imagekit_id}-s`}
									title={item.name}
									price={item.price}
									another_price={item.another_price}
								/>
							);
						}}
					/>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		width: "100%",
		flex: 1,
	},
	circleIcon: {
		width: 24,
		height: 24,
		borderRadius: 12,
		borderColor: accent,
		borderWidth: 1,
		alignItems: "center",
		justifyContent: "center",
		// backgroundColor: "green",
	},
	orderStatusBox: {
		width: getWidth * 0.49,
		borderRadius: 15,
		alignItems: "center",
		justifyContent: "center",
		paddingVertical: 10,
	},
	orderStatusText: {
		fontSize: 11,
		textAlign: "center",
		textTransform: "capitalize",
	},
	socialMediaBoxContainer: {
		flexDirection: "row",
		width: "100%",
		paddingHorizontal: 12,
		paddingVertical: 15,
		alignItems: "center",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	profile: state.profile,
	preference: state.preference,
	item: state.item,
	loading: state.loading,
	error: state.error,
	category: state.category,
	alert: state.alert,
});

export default connect(mapStateToProps, {
	getSingleCategory,
	deleteCategory,
	setRefresh,
	setLoading,
})(CategoryDetail);
