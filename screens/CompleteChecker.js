import React from "react";

import * as SplashScreen from "expo-splash-screen";

import { connect } from "react-redux";
import { setLoading } from "../store/actions/loadingAction";
import { customNoti } from "../store/actions/alertAction";

import { LayoutView } from "../component/Layout";

import isEmpty from "../utils/isEmpty";
import { getBankDetails } from "../store/actions/profileAction";
import { getAllItems } from "../store/actions/itemAction";

class CompleteChecker extends React.Component {
	state = {
		fetched: 0,
	};

	componentDidMount() {
		SplashScreen.hideAsync();

		this.props.setLoading(true);
		let { store } = this.props.profile;
		//console.log("mount", store);
		if (isEmpty(store.name)) {
			this.props.navigation.replace("CompleteReg");
		} else {
			this.props.getAllItems(this.props.auth.token);
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (!prevProps.item.isItemFetched && this.props.item.isItemFetched) {
			//	console.log("item fetched");
			let { store } = this.props.profile;
			this.setState({
				fetched: this.state.fetched + 1,
			});
			this.props.getBankDetails(store.id, this.props.auth.token);
		}
		if (isEmpty(prevProps.profile.payout) && !isEmpty(this.props.profile.payout)) {
			//console.log("payout fetched");
			this.setState({
				fetched: this.state.fetched + 1,
			});
		}
		if (prevState.fetched !== this.state.fetched && this.state.fetched >= 2) {
			if (!isEmpty(this.props.item.userItem) && !isEmpty(this.props.profile.payout.bank_acc)) {
				//console.log("private");
				this.props.navigation.navigate("Private");
			} else {
				//console.log("complete store");
				this.props.navigation.replace("CompleteStore");
			}
		}
	}

	componentWillUnmount() {
		this.props.setLoading(false);
	}

	render() {
		return <LayoutView />;
	}
}

const mapStateToProps = state => ({
	auth: state.auth,
	profile: state.profile,
	item: state.item,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	getAllItems,
	getBankDetails,
	setLoading,
	customNoti,
})(CompleteChecker);
