import React from "react";

import { StyleSheet, Text, View, Keyboard, TextInput } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

import { connect } from "react-redux";
import { sendUserEmail, sendUserPasscode } from "../store/actions/authAction";
import { setLoading } from "../store/actions/loadingAction";
import { customNoti } from "../store/actions/alertAction";

import { Button, TouchableRipple } from "react-native-paper";

import { LayoutView } from "../component/Layout";

import isEmpty from "../utils/isEmpty";
import { accent, grey, greyblack, mainBgColor } from "../utils/ColorPicker";
import { Foundation } from "@expo/vector-icons";
import registerPushNoti from "../utils/registerPushNoti";
import NavHeader from "../component/NavHeader";
import CustomButton from "../component/CustomButton";
import Typography from "../component/Typography";

class SignInPasscode extends React.Component {
	constructor() {
		super();
		this.passInput1 = null;
		this.passInput2 = null;
		this.passInput3 = null;
		this.passInput4 = null;
	}

	state = {
		email: "",
		pass1: "",
		pass2: "",
		pass3: "",
		pass4: "",
		passcode: "",
	};

	componentDidMount() {
		if (!isEmpty(this.props.navigation.state.params)) {
			let { email } = this.props.navigation.state.params;
			this.setState({
				email,
			});
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (!isEmpty(this.props.alert.redirect) && isEmpty(prevProps.alert.redirect)) {
			this.props.navigation.navigate(this.props.alert.redirect);
		}

		if (!isEmpty(this.props.auth.token) && isEmpty(prevProps.auth.token)) {
			AsyncStorage.setItem("@dbot:businessToken", this.props.auth.token);
			AsyncStorage.setItem("@dbot:refreshToken", this.props.auth.refreshToken);
		}
		if (!isEmpty(this.props.profile.store) && isEmpty(prevProps.profile.store)) {
			registerPushNoti(this.props.auth.token);
		}
		if (this.props.auth.regNotiTry && !prevProps.auth.regNotiTry) {
			if (!isEmpty(this.props.alert.notiScreen)) {
				this.props.navigation.navigate(this.props.alert.notiScreen, this.props.alert.notiParams);
			} else {
				this.props.navigation.navigate("Registration");
			}
		}

		if (!isEmpty(prevProps.auth.token) && isEmpty(this.props.auth.token)) {
			this.props.navigation.navigate("SignInEmail");
		}
	}

	componentWillUnmount() {
		this.props.setLoading(false);
	}

	handlePasscode =
		(name, onNext = null) =>
		value => {
			this.setState({
				passcode: value.length <= 4 ? value : this.state.passcode,
			});
		};

	handlePasscodeKeypress = ({ nativeEvent }) => {
		if (nativeEvent.key === "Backspace") {
			this.setState({
				passcode: this.state.passcode.slice(0, -1),
			});
		}
	};

	handleResend = () => {
		Keyboard.dismiss();
		let data = {
			email: this.state.email.toLowerCase(),
		};
		this.props.sendUserEmail(data);
	};

	handleSubmit = () => {
		Keyboard.dismiss();
		let data = {
			email: this.state.email.toLowerCase(),
			passcode: this.state.passcode,
		};
		this.props.sendUserPasscode(data);
	};

	render() {
		const { passcode } = this.state;

		return (
			<LayoutView>
				<NavHeader
					onBack={() => {
						this.props.navigation.navigate("SignIn");
					}}
				/>
				<View style={styles.container}>
					<Foundation name="key" size={86} color={accent} />
					<Typography type="largetitle" style={styles.pageTitle}>
						Enter StoreUp Passcode
					</Typography>
					<Typography
						type="headline"
						style={{ width: "90%", textAlign: "center", marginTop: 12, color: greyblack }}
					>
						Enter the 4-digit code we sent to{" "}
						<Typography type="headline" style={{ fontFamily: "semibold" }}>
							{this.state.email}
						</Typography>{" "}
						to continue
					</Typography>
					<View style={styles.hiddenPassInput}>
						<TextInput
							selectionColor="rgba(0,0,0,0)"
							style={styles.passInput}
							onChangeText={this.handlePasscode()}
							keyboardType="numeric"
							ref={input => {
								this.passcodeInput = input;
							}}
							value={passcode}
						/>
					</View>
					<View style={styles.passInputContainer}>
						<View style={[styles.passInputBox, { borderTopLeftRadius: 10, borderBottomLeftRadius: 10 }]}>
							<TextInput
								selectionColor="rgba(0,0,0,0)"
								style={[styles.passInput]}
								maxLength={1}
								onFocus={() => {
									this.passcodeInput.focus();
								}}
								keyboardType="numeric"
								value={passcode[0]}
							/>
						</View>
						<View style={styles.passInputBox}>
							<TextInput
								selectionColor="rgba(0,0,0,0)"
								style={styles.passInput}
								maxLength={1}
								onFocus={() => {
									this.passcodeInput.focus();
								}}
								keyboardType="numeric"
								value={passcode[1]}
							/>
						</View>
						<View style={styles.passInputBox}>
							<TextInput
								selectionColor="rgba(0,0,0,0)"
								style={styles.passInput}
								maxLength={1}
								onFocus={() => {
									this.passcodeInput.focus();
								}}
								keyboardType="numeric"
								value={passcode[2]}
							/>
						</View>
						<View style={[styles.passInputBox, { borderTopRightRadius: 10, borderBottomRightRadius: 10 }]}>
							<TextInput
								selectionColor="rgba(0,0,0,0)"
								style={[styles.passInput]}
								maxLength={1}
								onFocus={() => {
									this.passcodeInput.focus();
								}}
								keyboardType="numeric"
								value={passcode[3]}
							/>
						</View>
					</View>
					<CustomButton style={styles.button} onPress={this.handleSubmit}>
						Continue
					</CustomButton>

					<TouchableRipple rippleColor="transparent" onPress={this.handleResend} style={{ width: "90%" }}>
						<Typography type="headline" style={styles.buttonText}>
							Have not received your passcode? Resend here
						</Typography>
					</TouchableRipple>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		paddingBottom: 100,
		//backgroundColor: "red",
		justifyContent: "center",
	},
	pageTitle: {
		width: "100%",
		fontFamily: "bold",
		textAlign: "center",
	},
	passInputContainer: {
		width: "100%",
		flexDirection: "row",
		marginVertical: 25,
	},
	hiddenPassInput: {
		width: 1,
		height: 1,
		transform: [{ scale: 0 }],
	},
	passInputBox: {
		width: "25%",
		height: undefined,
		aspectRatio: 1,
		backgroundColor: "white",
		borderColor: mainBgColor,
		borderWidth: 1,
		borderStyle: "solid",
	},
	passInput: {
		width: "100%",
		height: "100%",
		textAlign: "center",
		fontSize: 35,
		fontFamily: "bold",
	},
	buttonText: {
		color: accent,
		width: "100%",
		textAlign: "center",
	},
	button: {
		justifyContent: "center",
		width: "100%",
		marginBottom: 20,
	},
	googleLogo: {
		height: 21,
		width: 21,
	},
	signUpText: {
		fontFamily: "regular",
		paddingVertical: 5,
		fontSize: 16,
	},
	forgotText: {
		fontFamily: "light",
		//marginTop: 5,
		paddingVertical: 5,
		fontSize: 14,
		color: grey,
		alignSelf: "flex-start",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	profile: state.profile,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	sendUserEmail,
	sendUserPasscode,
	setLoading,
	customNoti,
})(SignInPasscode);
