import React from "react";
import { StyleSheet, ScrollView, View, Keyboard, Text } from "react-native";
import { connect } from "react-redux";

import { LayoutView } from "../component/Layout";
import { TouchableRipple } from "react-native-paper";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";

import NavHeader from "../component/NavHeader";
import { accent, black, grey, greyblack, greywhite, transparent } from "../utils/ColorPicker";

import isEmpty from "../utils/isEmpty";

import { withNavigationFocus } from "react-navigation";
import { setLoading } from "../store/actions/loadingAction";
import { customNoti } from "../store/actions/alertAction";
import ItemVariantBox from "../component/ItemVariantBox";
import Typography from "../component/Typography";

class ItemVariant extends React.Component {
	state = {
		variants: [],
	};

	componentDidMount() {
		if (!isEmpty(this.props.navigation.state.params)) {
			let varArr = [...this.props.navigation.state.params.variants];
			if (varArr.length <= 1) {
				varArr.push({ stock: "", variant_name: "", variant_number: 2, min_limit: null, max_limit: null });
			}
			this.setState({
				variants: varArr,
			});
		}
	}

	componentDidUpdate(prevProps, prevState) {}

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handleChangeVariant = data => {
		let arr = [...this.state.variants];

		let foundIndex = arr.findIndex(f => f.variant_number === data.variant_number);

		arr[foundIndex] = {
			...data,
		};

		this.setState({
			variants: arr,
		});
	};

	handleDeleteVariant = index => () => {
		let arr = [...this.state.variants];
		if (arr.length <= 1) {
			this.props.customNoti("Need to set at least one variant", "error");
		} else {
			const foundIndex = arr.findIndex(f => f.variant_number === index);
			arr.splice(foundIndex, 1);

			let newArr = [];
			arr.map((e, i) => {
				newArr.push({ ...e, variant_number: i + 1 });
			});

			this.setState(
				{
					variants: [],
				},
				() => {
					this.setState({
						variants: newArr,
					});
				}
			);
		}
	};

	handleAddVariant = () => {
		let arr = [...this.state.variants];
		let newArr = [];

		arr.push({ stock: "", variant_name: "", min_limit: "", max_limit: "" });
		arr.map((e, i) => {
			newArr.push({ ...e, variant_number: i + 1 });
		});

		this.setState({
			variants: newArr,
		});
	};

	handleSubmit = () => {
		//TODO: When new modal error, remove min<max checking on onPress, check min<max here and send notiError
		this.props.navigation.navigate("ItemPage", {
			variants: this.state.variants,
		});
		Keyboard.dismiss();
	};

	render() {
		const { variants } = this.state;

		const Divider = props => (
			<View
				style={{
					width: "100%",
					borderBottomColor: greywhite,
					borderBottomWidth: 0.5,
					alignSelf: "center",
				}}
			/>
		);

		return (
			<LayoutView>
				<NavHeader
					onBack={this.handleBack}
					onPress={
						variants.findIndex(
							f =>
								isEmpty(f.name) ||
								isEmpty(f.stock) ||
								(!isEmpty(f.min_limit) &&
									!isEmpty(f.max_limit) &&
									parseInt(f.min_limit) > parseInt(f.max_limit))
						) === -1
							? this.handleSubmit
							: null
					}
					actionText="Done"
					title="Variants"
				/>

				<ScrollView
					showsVerticalScrollIndicator={false}
					contentContainerStyle={{ width: "100%", paddingBottom: 30 }}
				>
					<View style={styles.container}>
						{variants.map((v, i) => {
							return (
								<View key={i} style={{ width: "100%" }}>
									{i === 0 ? null : <Divider />}
									<ItemVariantBox
										key={i}
										data={v}
										onChange={this.handleChangeVariant}
										onDelete={this.handleDeleteVariant(v.variant_number)}
										canDelete={variants.length > 1}
									/>
								</View>
							);
						})}
						<Divider />
						<TouchableRipple
							style={{ width: "100%" }}
							rippleColor="transparent"
							onPress={this.handleAddVariant}
						>
							<View
								style={{
									width: "100%",
									flexDirection: "row",
									alignItems: "center",
									justifyContent: "flex-start",
									height: 52,
								}}
							>
								<MaterialCommunityIcons name="plus" size={23} color={accent} />
								<Typography
									type="headline"
									style={{
										color: accent,
										fontFamily: "regular",
										marginLeft: 12,
									}}
								>
									Add
								</Typography>
							</View>
						</TouchableRipple>
						<Divider />
					</View>
				</ScrollView>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
	sectionBox: {
		//width: "100%",
		display: "flex",
		flexDirection: "column",
		// borderBottomWidth: 1,
		// borderBottomColor: greywhite,
		paddingVertical: 14,
	},
	sectionTitle: {
		fontFamily: "light",
		letterSpacing: 0.1,
		textTransform: "uppercase",
		marginBottom: 10,
	},
	sectionContent: {
		width: "100%",
		backgroundColor: "white",
		borderRadius: 10,
	},
	orderBox: {
		overflow: "hidden",
		flexDirection: "row",
		justifyContent: "space-between",
		marginBottom: 25,
	},
	orderBoxLeft: {
		width: "35%",
		height: "100%",
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "#f7f7ff",
	},
	orderBoxRight: {
		width: "60%",
		height: "100%",
		justifyContent: "center",
		paddingRight: 10,
	},
	orderName: {
		fontFamily: "medium",
		fontSize: 16,
		lineHeight: 20,
	},
	orderPrice: {
		fontFamily: "bold",
		fontSize: 15,
		lineHeight: 20,
	},
	orderStock: {
		fontFamily: "light",
		fontSize: 12,
		color: grey,
		marginTop: 8,
	},

	rentalTabContainer: {
		width: "100%",
		flexDirection: "row",
		//justifyContent: "center",
		alignItems: "center",
	},
	rentalTabItem: {
		paddingTop: 10,
		//marginBottom: 15,
		justifyContent: "center",
		alignItems: "center",
		borderBottomWidth: 2,
	},
	rentalTabItemActive: {
		borderBottomColor: black,
	},
	rentalTabItemInactive: {
		borderBottomColor: transparent,
	},
	rentalTabTextContainer: {
		width: "100%",
		flexDirection: "column",
		alignItems: "center",
	},
	rentalTabText: {
		fontSize: 13,
	},
	indicator: {
		alignSelf: "flex-end",
		width: 8,
		height: undefined,
		aspectRatio: 1,
		borderRadius: 4,
		marginRight: 3,
	},
	rentalTabTextActive: {
		fontFamily: "bold",
	},
	rentalTabTextInactive: {
		fontFamily: "light",
		color: greyblack,
	},
	textField: {
		marginBottom: 0,
	},
	halfRow: {
		flexDirection: "row",
		justifyContent: "space-between",
	},
	image: {
		width: "100%",
		height: undefined,
		aspectRatio: 1,
	},
	imageSection: {
		flexDirection: "row",
		marginBottom: 5,
		paddingHorizontal: 20,
	},

	dialogActionButton: {
		marginHorizontal: 5,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	item: state.item,
	profile: state.profile,
	catalog: state.catalog,
	loading: state.loading,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	setLoading,
	customNoti,
})(withNavigationFocus(ItemVariant));
