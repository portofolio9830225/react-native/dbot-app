import React from "react";
import { connect } from "react-redux";
import isEmpty from "../utils/isEmpty";
import * as WebBrowser from "expo-web-browser";

import {
	accent,
	black,
	blue,
	green,
	grey,
	greyblack,
	greydark,
	greylight,
	greywhite,
	orange,
	purple,
	red,
	transparent,
	whatsappColor,
	white,
} from "../utils/ColorPicker";
import DateConverter from "../utils/DateConverter";
import TimeConverter from "../utils/TimeConverter";
import { AntDesign, Entypo, Feather, FontAwesome5, Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Image, StyleSheet, Text, View, ScrollView, Platform } from "react-native";
import { getSingleOrder } from "../store/actions/orderAction";
import { LayoutView } from "../component/Layout";
import NavHeader from "../component/NavHeader";

import PageTitle from "../component/PageTitle";
import langSelector from "../utils/langSelector";
import { Button, TouchableRipple } from "react-native-paper";
import { customNoti } from "../store/actions/alertAction";
import StatusCard from "../component/StatusCard";
import { withNavigationFocus } from "react-navigation";
import orderColor from "../utils/orderColor";

class OrderDetail extends React.Component {
	state = {
		statusCard: false,
		confirmModal: null,
		isAction: this.props.navigation.state.params && this.props.navigation.state.params.isAction,
		productExpand: false,
	};

	componentDidMount() {
		this.props.getSingleOrder(this.props.navigation.state.params.oID, this.props.profile.store.id);
	}
	componentDidUpdate(prevProps, prevState) {
		if (!prevProps.isFocused && this.props.isFocused && !isEmpty(this.props.order.orderDetails)) {
			this.props.getSingleOrder(this.props.order.orderDetails.ref, this.props.profile.store.id);
		}
		if (isEmpty(prevProps.alert.redirect) && !isEmpty(this.props.alert.redirect)) {
			if (this.props.alert.redirect === "setMultipleOrderStatus") {
				this.props.getSingleOrder(this.props.order.orderDetails.ref, this.props.profile.store.id);
			}
		}

		// if (isEmpty(prevProps.order.orderDetails) && !isEmpty(this.props.order.orderDetails)) {
		// 	console.log(this.props.order.orderDetails);
		// 	console.log(this.props.order.orderPromo);
		// }
	}

	componentWillUnmount() {
		this.props.getSingleOrder();
	}

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handleStatusCard = state => () => {
		this.setState({
			statusCard: state,
		});
	};

	handlePage =
		(page, params = {}) =>
		() => {
			this.props.navigation.push(page, params);
		};

	handleRecordPayment = () => {
		this.handlePage("OrderPaymentRecord", this.props.order.orderPayment)();
	};

	handleReceipt = () => {
		// WebBrowser.openBrowserAsync(
		//   `https://${this.props.profile.store.subdomain}.storeup.site/o/${this.props.order.orderDetails.order_id}?show=true`
		// );
		WebBrowser.openBrowserAsync(`https://receipt.storeup.io/${this.props.order.orderDetails.order_id}?show=true`);
	};

	render() {
		const { statusCard, productExpand } = this.state;
		const { orderDetails, orderItems, orderPayment, orderShipping, orderPromo } = this.props.order;
		const isPaid = orderDetails.status === "paid" || orderDetails.status === "completed";
		const canChangeStatus =
			orderDetails.status === "unpaid" || orderDetails.status === "paid" || orderDetails.status === "completed";
		const lang = langSelector.OrderDetail.find(e => e.lang === this.props.preference.lang).data;

		const Divider = props => {
			return (
				<View
					style={{
						width: "100%",
						borderBottomColor: greywhite,
						borderBottomWidth: 0.5,
					}}
				/>
			);
		};

		const OrderNotes = ({ title, desc }) => {
			return (
				<View
					style={{
						display: "flex",
						flexDirection: "column",
						width: "100%",
						marginVertical: 7,
					}}
				>
					<Text
						style={{
							fontFamily: "medium",
							marginBottom: 5,
						}}
					>
						{title}
					</Text>
					<Text
						style={{
							fontFamily: "light",
							fontSize: 13,
							textAlign: "justify",
						}}
					>
						{desc}
					</Text>
				</View>
			);
		};

		const ItemBox = ({ name, desc, img, count, price, total, isFirst, another_price }) => {
			return (
				<View style={styles.itemBox}>
					{!isEmpty(another_price) && (
						<View
							style={{
								position: "absolute",
								paddingHorizontal: 5,
								paddingVertical: 2,
								// padding: 5,
								backgroundColor: orange,
								borderRadius: 10,
								bottom: 3,
								zIndex: 99,
								left: 15,
							}}
						>
							<Text style={{ fontSize: 11, color: white }}>
								{parseInt(((price - another_price) / price) * 100)}% OFF
							</Text>
						</View>
					)}
					<View style={styles.itemImg}>
						<Image
							source={{ uri: img }}
							style={{
								width: "100%",
								height: undefined,
								aspectRatio: 1,
							}}
						/>
					</View>
					<View style={styles.itemBoxBreakdown}>
						<Text style={styles.itemName} ellipsizeMode="tail" numberOfLines={1}>
							{name}
						</Text>
						{!isEmpty(desc) ? (
							<Text style={styles.itemDesc} ellipsizeMode="tail">
								{desc}
							</Text>
						) : null}
						<View
							style={{
								flexDirection: "row",
								justifyContent: "space-between",
								marginTop: 10,
								//backgroundColor: "red",
							}}
						>
							<Text style={styles.itemQuantity}>{`x ${count}`}</Text>
							{isEmpty(another_price) ? (
								<Text style={styles.itemPrice}>{`RM${price.toFixed(2)}`} </Text>
							) : (
								<View>
									<Text style={styles.itemPrice}>{`RM${another_price.toFixed(2)}`} </Text>
									<Text style={styles.itemAnotherPrice}>{`RM${price.toFixed(2)}`} </Text>
								</View>
							)}
						</View>
					</View>
				</View>
			);
		};

		const PriceList = ({ title, text, end }) => {
			return (
				<View style={styles.priceList}>
					<Text style={styles.priceListTitle}>{title}</Text>

					<Text style={[styles.priceListText, { fontFamily: end ? "bold" : "medium" }]}>{text}</Text>
				</View>
			);
		};

		const IconText = ({ iconType: Icon, iconName, title, text, onPress = null }) => {
			return (
				<TouchableRipple rippleColor="transparent" onPress={onPress}>
					<View
						style={{
							flexDirection: "row",
							marginVertical: 5,
							position: "relative",
							alignItems: "center",
							justifyContent: "space-between",
							paddingVertical: 5,
							//backgroundColor: "red",
						}}
					>
						<Icon name={iconName} size={30} color={greydark} />
						<View style={{ flex: 1, paddingHorizontal: 15 }}>
							<Text style={{ color: onPress ? accent : black, fontFamily: "regular" }}>{title}</Text>
							<Text style={{ color: greyblack, fontFamily: "regular" }}>{text}</Text>
						</View>
						{onPress ? <Entypo name="chevron-thin-right" size={17} color={greyblack} /> : null}
					</View>
				</TouchableRipple>
			);
		};

		const DateTimeGetter = ({ dt }) => {
			let date = new Date(dt);

			let h = date.getHours();
			let min = date.getMinutes();

			let d = date.getDate();
			let month = date.getMonth() + 1;
			let y = date.getFullYear();

			return `${DateConverter(
				`${y}-${month < 10 ? `0${month}` : month}-${d < 10 ? `0${d}` : d}`
			)}, ${TimeConverter(`${h < 10 ? `0${h}` : h}:${min < 10 ? `0${min}` : min}:00`)}`;
		};

		return (
			<LayoutView>
				<NavHeader
					onBack={this.handleBack}
					customContentRight={
						<View style={{ flexDirection: "row", justifyContent: "flex-end", alignItems: "center" }}>
							<TouchableRipple
								onPress={!isEmpty(orderDetails.customer) ? this.handlePage("OrderContact") : null}
								rippleColor="transparent"
								style={{
									paddingHorizontal: 10,
								}}
							>
								<MaterialCommunityIcons name="whatsapp" size={30} color={whatsappColor} />
							</TouchableRipple>
							<TouchableRipple
								rippleColor={transparent}
								onPress={canChangeStatus ? this.handleStatusCard(true) : null}
								style={{
									backgroundColor: orderColor(orderDetails.status),
									borderRadius: 14,
									height: 28,
									paddingHorizontal: 20,
									alignItems: "center",
									justifyContent: "center",
									marginLeft: 5,
								}}
							>
								<Text
									style={{
										textTransform: "capitalize",
										color: white,
										fontFamily: "medium",
										fontSize: 16,
									}}
								>
									{orderDetails.status}
								</Text>
							</TouchableRipple>
						</View>
					}
				/>

				{!isEmpty(orderDetails) && (
					<View style={styles.root}>
						<ScrollView
							showsVerticalScrollIndicator={false}
							contentContainerStyle={{
								width: "100%",
								//flex: 1,
							}}
						>
							<PageTitle small>Order Details</PageTitle>

							<View style={styles.orderContent}>
								{productExpand || orderItems.length <= 1 ? (
									orderItems.map((e, i) => {
										return (
											<ItemBox
												key={i}
												name={e.name}
												desc={e.variant_name}
												img={e.image}
												count={e.quantity}
												price={e.price}
												another_price={e.another_price}
												//total={e.subtotal}
											/>
										);
									})
								) : (
									<View style={styles.itemBox}>
										<View style={[styles.itemImg, { width: "35%" }]}>
											<Image
												source={{ uri: orderItems[0].image }}
												style={{
													width: "100%",
													height: undefined,
													aspectRatio: 1,
												}}
											/>
										</View>
										<View
											style={[
												styles.itemBoxBreakdown,
												{ justifyContent: "space-between", width: "60%" },
											]}
										>
											<View>
												<Text style={styles.itemName} ellipsizeMode="tail" numberOfLines={1}>
													{orderItems[0].name}
												</Text>

												<Text
													style={[styles.itemDesc, { color: greyblack }]}
													ellipsizeMode="tail"
												>
													and {orderItems.length - 1} more product
													{orderItems.length - 1 <= 1 ? "" : "s"}
												</Text>
											</View>
											<View>
												<TouchableRipple
													rippleColor="transparent"
													onPress={() => {
														this.setState({
															productExpand: true,
														});
													}}
												>
													<View
														style={{
															backgroundColor: greywhite,
															height: 30,
															borderRadius: 15,
															paddingHorizontal: 15,
															alignSelf: "flex-start",
															alignItems: "center",
															justifyContent: "center",
															marginTop: 15,
														}}
													>
														<Text
															style={{
																fontFamily: "regular",
																color: accent,
																fontSize: 16,
															}}
														>
															See All
														</Text>
													</View>
												</TouchableRipple>
											</View>
										</View>
									</View>
								)}
								<Divider />

								<View style={styles.orderHeader}>
									<IconText
										iconType={Feather}
										iconName="box"
										title={`Order ID: ${orderDetails.ref}`}
										text={`Date: ${DateConverter(orderDetails.created_at)}`}
									/>
									{!isEmpty(orderDetails.customer) ? (
										<IconText
											iconType={Feather}
											iconName="user"
											title={orderDetails.customer.name}
											text="Customer"
											onPress={this.handlePage("OrderCustomer")}
										/>
									) : null}
								</View>
								<Divider />
								<View style={styles.sectionBox}>
									<PageTitle small>Pricing</PageTitle>
									<View style={{ marginTop: 8 }}>
										<PriceList
											title="Subtotal"
											text={`RM${orderDetails.product_subtotal.toFixed(2)}`}
										/>
										{!isEmpty(orderShipping) ? (
											<PriceList
												title="Shipping fee"
												text={`RM${orderShipping.price.toFixed(2)}`}
											/>
										) : null}

										{!isEmpty(orderPromo) ? (
											<PriceList
												title={`Promo (${orderPromo.code})`}
												text={`-RM${orderPromo.amount.toFixed(2)}`}
											/>
										) : null}
										{!isEmpty(orderDetails) &&
										!isEmpty(orderDetails.additional_fee) &&
										orderDetails.additional_fee > 0 ? (
											<PriceList
												title="Transaction fee"
												text={`RM${orderDetails.additional_fee.toFixed(2)}`}
											/>
										) : null}
										<PriceList title="Total" text={`RM${orderDetails.grand_total.toFixed(2)}`} />

										{!isEmpty(orderPayment) ? (
											<PriceList title="Payment method" text={orderPayment.payment_method} />
										) : null}
									</View>
								</View>
								<Divider />

								{!isEmpty(orderPayment) &&
								orderPayment.payment_method_id !== 1 &&
								!orderPayment.paid ? (
									<>
										<TouchableRipple rippleColor="transparent" onPress={this.handleRecordPayment}>
											<View
												style={{
													//width: "100%",
													flexDirection: "row",
													justifyContent: "space-between",
													alignItems: "center",
													paddingVertical: 15,
												}}
											>
												<Text style={{ color: accent, fontSize: 15, fontFamily: "regular" }}>
													Record Payment
												</Text>
												<Ionicons name="receipt-outline" size={20} color={accent} />
											</View>
										</TouchableRipple>
										<Divider />
									</>
								) : null}
							</View>
						</ScrollView>
					</View>
				)}
				<StatusCard
					orderID={this.props.navigation.state.params.oID}
					open={statusCard}
					status={orderDetails.status}
					onClose={() => {
						this.setState({
							statusCard: false,
						});
					}}
				/>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	root: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		position: "relative",
	},
	businessCard: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-evenly",
		alignItems: "center",
		marginVertical: 17,
		width: "100%",
	},
	orderContent: {
		//width: "100%",
		paddingBottom: 25,
		//marginTop: 17,
	},
	orderHeader: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "space-between",
		paddingVertical: 10,
	},
	headerTitle: {
		fontSize: 12,
		fontFamily: "regular",
		marginBottom: 10,
	},
	iconTextText: {
		color: "black",
		fontSize: 14,
		fontFamily: "light",
	},

	sectionTitle: {
		fontSize: 15,
		fontFamily: "bold",
		textTransform: "uppercase",
		marginBottom: 10,
	},
	sectionBox: {
		//width: "100%",
		display: "flex",
		flexDirection: "column",
		// borderBottomWidth: 1,
		// borderBottomColor: greywhite,
		paddingVertical: 14,
	},
	itemBox: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingVertical: 10,
		position: "relative",
		//width: "100%",
	},
	itemBoxHeader: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		width: "100%",
	},
	itemBoxBreakdown: {
		display: "flex",
		flexDirection: "column",
		width: "70%",
	},
	itemImg: {
		width: "25%",
		height: undefined,
		aspectRatio: 1,
		borderRadius: 10,
		borderWidth: 0.5,
		borderColor: greywhite,
		overflow: "hidden",
	},
	itemName: {
		fontSize: 16,
		fontFamily: "medium",
	},
	itemDesc: {
		fontSize: 13,
		fontFamily: "regular",
	},
	itemPrice: {
		fontSize: 15,
		fontFamily: "regular",
		color: black,
		//lineHeight: 17,
	},
	itemAnotherPrice: {
		fontSize: 11,
		fontFamily: "regular",
		color: greyblack,
		textAlign: "right",
		textDecorationLine: "line-through",
		//lineHeight: 17,
	},
	itemQuantity: {
		fontSize: 15,
		fontFamily: "bold",
	},
	priceList: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "flex-start",
		marginBottom: 5,
	},
	priceListTitle: {
		width: "50%",
		fontSize: 14,
		fontFamily: "light",
	},
	priceListText: {
		width: "50%",
		fontSize: 14,
		textAlign: "right",
	},
	totalBox: {
		width: "100%",
		display: "flex",
		flexDirection: "column",
		marginTop: 14,
		paddingTop: 18,
	},
	viewText: {
		fontFamily: "medium",
		paddingTop: 5,
		paddingBottom: 3,
	},
	viewButton: {
		marginTop: 30,
	},
	staticButton: {
		width: "80%",
	},
	statusButton: {
		width: "100%",
		marginVertical: 5,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	order: state.order,
	profile: state.profile,
	loading: state.loading,
	alert: state.alert,
});

export default connect(mapStateToProps, {
	getSingleOrder,
	customNoti,
})(withNavigationFocus(OrderDetail));
