import React from "react";
import { StyleSheet, View, Text, Animated, Easing, RefreshControl, FlatList } from "react-native";
import { connect } from "react-redux";

import { setRefresh } from "../store/actions/loadingAction";
import { getSingleCategory, checkCategory } from "../store/actions/categoryAction";

import { LayoutView } from "../component/Layout";
import NavHeader from "../component/NavHeader";

import { Ionicons } from "@expo/vector-icons";
import { accent, greywhite, transparent } from "../utils/ColorPicker";
import getWidth from "../utils/getWidth";
import isEmpty from "../utils/isEmpty";
import ProductRow from "../component/ProductRow";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";

import { SectionDivider } from "../component/SectionComponents";

// import { SectionDivider } from "../component/SectionDivider";

class CategoryProduct extends React.Component {
	constructor(props) {
		super(props);
		this.scrollViewRef = React.createRef();
		this.transValue = new Animated.Value(1);
	}

	state = {
		ids: [],
	};

	categoryInfo = () => {
		let arr = [];
		this.props.category.singleCategory.products.map(e => {
			arr.push(e.id);
		});

		this.setState({
			ids: arr,
		});
	};

	componentDidMount() {
		this.categoryInfo();
	}

	componentDidUpdate(prevProps, prevState) {
		// console.log(this.props.category.singleCategory);
		// if (!prevProps.loading.refresh && this.props.loading.refresh) {
		// 	setTimeout(() => {
		// 		// this.props.getAllItems(this.props.auth.token);
		// 		this.props.getSingleCategory(this.props.navigation.state.params.id, this.props.auth.token);
		// 	}, 1500);
		// }

		if (isEmpty(prevProps.alert.status) && this.props.alert.status === "success") {
			if (this.props.alert.redirect === "checkCategory") {
				this.handleBack();
			}
		}
	}

	componentWillUnmount() {
		// this.props.getSingleCategory();
	}

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handleSubmit = () => {
		let data = {
			product_ids: this.state.ids,
		};
		this.props.checkCategory(this.props.category.singleCategory.id, data, this.props.auth.token);
	};

	handleProductCheck = id => () => {
		let array = [];
		let checked = this.state.ids.findIndex(f => f === id) != -1;

		if (!checked) {
			array = [...this.state.ids, id];
		} else {
			array = this.state.ids.filter(e => e !== id);
		}

		this.setState({
			ids: array,
		});
	};

	tabWidth = getWidth;

	render() {
		const { userItem } = this.props.item;
		const { ids } = this.state;

		const ProductRowCheckout = props => {
			return (
				<TouchableWithoutFeedback
					style={{
						flexDirection: "row",
						width: "100%",
						justifyContent: "space-between",
						alignItems: "center",
					}}
					onPress={props.onPress}
				>
					<View style={{ flex: 1 }}>
						<ProductRow
							isFirst={true}
							image={props.image}
							imageKey={props.imageKey}
							title={props.title}
							price={props.price}
							onPress={props.onPress}
						/>
					</View>
					<Ionicons
						name="checkmark-circle"
						size={24}
						color={props.enable ? accent : transparent}
						style={{ marginLeft: 12 }}
					/>
				</TouchableWithoutFeedback>
			);
		};

		return (
			<LayoutView>
				<NavHeader
					onBack={this.handleBack}
					onPress={this.handleSubmit}
					actionText="Done"
					title="Select Products"
				/>
				<SectionDivider />

				<FlatList
					data={userItem}
					showsVerticalScrollIndicator={false}
					style={styles.container}
					contentContainerStyle={{
						width: "100%",
					}}
					renderItem={({ item, index, separators }) => {
						return (
							<>
								{index !== 0 ? <SectionDivider color={greywhite} /> : null}
								<ProductRowCheckout
									image={item.images[0].url_s}
									imageKey={`${item.images[0].file.imagekit_id}-s`}
									title={item.name}
									price={item.price}
									enable={ids.findIndex(f => f === item.id) != -1}
									onPress={this.handleProductCheck(item.id)}
								/>
							</>
						);
					}}
				/>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		width: "100%",
		flex: 1,
	},
	circleIcon: {
		width: 24,
		height: 24,
		borderRadius: 12,
		borderColor: accent,
		borderWidth: 1,
		alignItems: "center",
		justifyContent: "center",
		// backgroundColor: "green",
	},
	orderStatusBox: {
		width: getWidth * 0.49,
		borderRadius: 15,
		alignItems: "center",
		justifyContent: "center",
		paddingVertical: 10,
	},
	orderStatusText: {
		fontSize: 11,
		textAlign: "center",
		textTransform: "capitalize",
	},
	socialMediaBoxContainer: {
		flexDirection: "row",
		width: "100%",
		//paddingHorizontal: 10,
		paddingVertical: 15,
		alignItems: "center",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	profile: state.profile,
	preference: state.preference,
	item: state.item,
	loading: state.loading,
	error: state.error,
	category: state.category,
	alert: state.alert,
});

export default connect(mapStateToProps, {
	getSingleCategory,
	checkCategory,
	setRefresh,
})(CategoryProduct);
