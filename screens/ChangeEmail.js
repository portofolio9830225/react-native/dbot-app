import React from "react";
import { StyleSheet, View, Keyboard } from "react-native";
import { connect } from "react-redux";
import { black } from "../utils/ColorPicker";

import { changeEmail } from "../store/actions/profileAction";
import { clearNoti, customNoti } from "../store/actions/alertAction";
import { Button } from "react-native-paper";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import isEmpty from "../utils/isEmpty";
import NavHeader from "../component/NavHeader";
import PasswordField from "../component/PasswordField";
import TextField from "../component/TextField";
import langSelector from "../utils/langSelector";

class ChangeEmail extends React.Component {
	state = {
		pass: "",
		email: "",
	};

	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.alert.status) && this.props.alert.status === "success") {
			this.handleBack();
		}
	}

	handleBack = () => {
		this.props.navigation.pop();
	};

	handleText = name => value => {
		this.setState({
			[name]: value,
		});
	};

	handleSubmit = () => {
		this.props.clearNoti();
		Keyboard.dismiss();
		if (isEmpty(this.state.email)) {
			this.props.customNoti("Empty email field");
		} else {
			if (isEmpty(this.state.pass)) {
				this.props.customNoti("Empty password field");
			} else {
				if (this.state.email === this.props.profile.user.email) {
					this.props.customNoti("The new email is the same as the old email");
				} else {
					let data = {
						email: this.props.profile.user.email,
						new_email: this.state.email,
						password: this.state.pass,
					};
					this.props.changeEmail(data, this.props.auth.token);
				}
			}
		}
	};

	render() {
		const { pass, email } = this.state;
		const { user } = this.props.profile;
		const lang = langSelector.ChangeEmail.find(e => e.lang === this.props.preference.lang).data;

		return (
			<LayoutView>
				<NavHeader onBack={this.handleBack} />

				<View style={styles.container}>
					<PageTitle>{lang.title}</PageTitle>
					{!isEmpty(user) && (
						<TextField
							disabled
							style={styles.textInput}
							mode="outlined"
							label={lang.currentEmail}
							value={user.user_email}
						/>
					)}
					<TextField
						keyboardType="email-address"
						style={styles.textInput}
						mode="outlined"
						label={lang.newEmail}
						value={email}
						onChangeText={this.handleText("email")}
						autoCapitalize="none"
					/>
					<PasswordField
						mode="outlined"
						style={styles.textInput}
						label={lang.password}
						value={pass}
						onChangeText={this.handleText("pass")}
					/>
				</View>
				<View
					style={{
						paddingVertical: 10,
						width: "100%",
						alignSelf: "center",
						alignItems: "center",
						justifyContent: "center",
					}}
				>
					<Button
						onPress={this.handleSubmit}
						mode="contained"
						color={black}
						style={styles.button}
						contentStyle={{ height: 50 }}
					>
						{lang.submitButton}
					</Button>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		// alignItems: "center"
	},

	textInput: {
		marginVertical: 5,
	},
	button: {
		width: "100%",
		marginTop: 20,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	profile: state.profile,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	changeEmail,
	clearNoti,
	customNoti,
})(ChangeEmail);
