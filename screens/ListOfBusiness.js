import React from "react";
import { StyleSheet, RefreshControl, Text, ScrollView, View, Image } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { connect } from "react-redux";
import { getBusinessToken, logoutUser } from "../store/actions/authAction";
import { customNoti } from "../store/actions/alertAction";
import { setRefresh, setLoading } from "../store/actions/loadingAction";

import { Button, Portal, Dialog, Card } from "react-native-paper";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import NavHeader from "../component/NavHeader";
import isEmpty from "../utils/isEmpty";
import { MaterialIcons } from "@expo/vector-icons";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import getWidth from "../utils/getWidth";
import { greywhite, greylight, red, grey, black } from "../utils/ColorPicker";
import { getRegProgress } from "../store/actions/profileAction";

// ROLE:
// 1: OWNER
// 2: MANAGER
// 3: STAFF

class ListOfBusiness extends React.Component {
	state = {
		logout: null,
	};

	componentWillUnmount() {
		this.props.setLoading(false);
	}

	handleBusiness = (id, name) => () => {
		let data = {
			business_id: id,
		};
		this.props.getBusinessToken(data, this.props.auth.userToken);
	};

	handleLogoutDialog = state => () => {
		this.setState({
			logout: state,
		});
	};

	handleLogOut = () => {
		AsyncStorage.removeItem("@dbot:userToken").then(val => {
			this.setState(
				{
					logout: false,
				},
				() => {
					this.props.logoutUser();
				}
			);
		});
	};

	render() {
		const { businessList } = this.props.auth;
		const { logout } = this.state;
		const isOwner = businessList.find(b => b.role === 1);

		const EmptyList = () => <Text style={styles.emptyText}>No business found</Text>;

		const RoleName = ({ val }) => {
			// 1: OWNER
			// 2: MANAGER
			// 3: STAFF
			switch (val) {
				case 1:
					return "Owner";
				case 2:
					return "Manager";
				case 3:
					return "Staff";
				default:
					return "";
			}
		};

		const BusinessBox = props => {
			return (
				<Card style={{ width: "100%", alignSelf: "center", marginVertical: 10 }} elevation={5}>
					<TouchableWithoutFeedback onPress={props.onPress} style={styles.businessBox}>
						<View style={styles.businessBoxImgContainer}>
							<Image
								style={[
									styles.businessBoxImg,
									props.logo ? {} : { padding: 10, boxSizing: "border-box" },
								]}
								source={
									props.logo ? { uri: props.logo } : require("../assets/image/noBusinessLogo.png")
								}
							/>
						</View>
						<View style={styles.businessBoxText}>
							<Text style={styles.businessBoxTextTitle}>{props.name}</Text>
							<Text style={styles.businessBoxTextContent}>
								<RoleName val={props.role} />
							</Text>
						</View>
					</TouchableWithoutFeedback>
				</Card>
			);
		};

		const TopIcon = props => (
			<TouchableWithoutFeedback onPress={props.onPress}>{props.icon}</TouchableWithoutFeedback>
		);

		return (
			<LayoutView>
				<NavHeader
					actions={
						isOwner ? (
							<TopIcon
								onPress={this.handleAdd}
								icon={<MaterialIcons style={styles.icon} name="add" size={30} color={black} />}
							/>
						) : null
					}
				/>

				<AlertDialog
					visible={logout}
					onDismiss={this.handleLogoutDialog(false)}
					title="Sign out account?"
					actions={[
						{ title: "Back", onPress: this.handleLogoutDialog(false) },
						{ title: "Sign out", color: red, onPress: this.handleLogOut },
					]}
				/>

				<View style={styles.container}>
					<ScrollView
						refreshControl={
							<RefreshControl
								refreshing={this.props.loading.refresh}
								onRefresh={() => {
									this.props.setRefresh(true);
								}}
							/>
						}
						showsVerticalScrollIndicator={false}
						style={{ width: "100%" }}
						contentContainerStyle={styles.itemContainer}
					>
						<View style={{ width: "100%", alignSelf: "center" }}>
							<PageTitle>Store list</PageTitle>
						</View>
						{isEmpty(businessList) ? (
							<EmptyList />
						) : (
							businessList.map(e => {
								return (
									<BusinessBox
										key={e.business_id}
										logo={e.logo}
										name={e.name}
										role={e.role}
										onPress={this.handleBusiness(e.business_id, e.name)}
									/>
								);
							})
						)}
					</ScrollView>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
	itemContainer: {
		width: "100%",
		flexDirection: "column",
	},
	businessBox: {
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		padding: 10,
		marginVertical: 10,
	},
	businessBoxImgContainer: {
		width: "20%",
		height: undefined,
		aspectRatio: 1,
		borderRadius: 7,
	},
	businessBoxImg: {
		borderRadius: getWidth * 0.25,
		borderColor: greywhite,
		borderWidth: 0.5,
		width: "100%",
		height: "100%",
	},
	businessBoxArrow: {
		width: "10%",
		justifyContent: "center",
		alignItems: "center",
	},
	businessBoxText: {
		flexDirection: "column",
		width: "75%",
	},
	businessBoxTextTitle: {
		fontFamily: "medium",
		fontSize: 17,
	},
	businessBoxTextContent: {
		fontFamily: "regular",
		color: grey,
		fontSize: 13,
	},
	emptyText: {
		fontFamily: "regular",
		width: "100%",
		color: greylight,
		fontSize: 17,
		paddingVertical: 20,
		textAlign: "center",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	profile: state.profile,
	item: state.item,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, {
	getRegProgress,
	logoutUser,
	getBusinessToken,
	customNoti,
	setRefresh,
	setLoading,
})(ListOfBusiness);
