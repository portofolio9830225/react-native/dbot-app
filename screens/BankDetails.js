import React from "react";
import { StyleSheet, View, Text, ScrollView, Keyboard, Platform } from "react-native";
import { connect } from "react-redux";
import * as WebBrowser from "expo-web-browser";

import { getBankDetails, editBankDetails, clearBank } from "../store/actions/profileAction";

import { customNoti } from "../store/actions/alertAction";
import { setLoading } from "../store/actions/loadingAction";

import { RadioButton, TouchableRipple, Button, Dialog, Portal } from "react-native-paper";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import isEmpty from "../utils/isEmpty";
import NavHeader from "../component/NavHeader";
import {
	accent,
	black,
	green,
	grey,
	greyblack,
	greylight,
	greywhite,
	mainBgColor,
	red,
	transparent,
} from "../utils/ColorPicker";
import TextField from "../component/TextField";
import langSelector from "../utils/langSelector";
import { AntDesign, Foundation, Ionicons, Octicons } from "@expo/vector-icons";
import Collapsible from "react-native-collapsible";
import getWidth from "../utils/getWidth";
import BankList from "../utils/BankList";
import AlertDialog from "../component/AlertDialog";
import Typography from "../component/Typography";
import { SectionBox, SectionDivider } from "../component/SectionComponents";

class BankDetails extends React.Component {
	state = {
		bankName: "",
		bankCode: "",
		accNum: "",
		fullName: "",
		organization: "company",
		id_num: "",
		reject_desc: "",
		status: "",
		isReady: false,
		fromRegProg: false,
		ssmInfo: false,
	};

	updateData = () => {
		let { acc_name, bank_code, bank_acc, id_type, id_number, verified } = this.props.profile.payout;
		this.props.setLoading(false);
		this.setState({
			bankCode: bank_code,
			accNum: bank_acc,
			fullName: acc_name,
			id_num: id_number,
			organization: id_type == "1" ? "company" : "personal",
			status: verified.toString(),
			isReady: true,
		});
	};

	componentDidMount() {
		this.props.setLoading(true);
		if (isEmpty(this.props.profile.payout)) {
			this.props.getBankDetails(this.props.profile.store.id, this.props.auth.token);
		} else {
			this.updateData();
		}

		if (this.props.navigation.state.params) {
			this.setState({
				fromRegProg: this.props.navigation.state.params.fromRegProg,
			});
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.profile.payout) && !isEmpty(this.props.profile.payout)) {
			this.updateData();
		}
		if (!prevState.isReady && this.state.isReady) {
			let a = BankList.find(f => f.code === this.props.profile.payout.bank_code);
			if (!isEmpty(a)) {
				this.setState({
					bankName: a.name,
				});
			}
		}

		if (isEmpty(prevProps.alert.redirect) && this.props.alert.redirect === "BankSaved") {
			if (this.state.fromRegProg) {
				this.props.navigation.pop();
				this.props.navigation.navigate("Inventory");
			} else {
				this.handleBack();
			}
		}
	}

	componentWillUnmount() {
		this.props.clearBank();
	}

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handleSSMInfo = state => () => {
		this.setState({
			ssmInfo: state,
		});
	};

	handleText = name => value => {
		this.setState({
			[name]: value,
		});
	};

	handleBankName = (name, code) => {
		this.setState({
			bankName: name,
			bankCode: code,
		});
	};

	handleOrg = val => {
		this.setState({
			organization: val,
		});
	};

	handleOrg2 = val => () => {
		this.setState({
			organization: val,
		});
	};

	handleSubmit = () => {
		Keyboard.dismiss();
		if (isEmpty(this.state.accNum) || isEmpty(this.state.bankName)) {
			this.props.customNoti("Incomplete bank details");
		} else {
			let data = {
				acc_name: this.state.fullName,
				id_number: this.state.id_num.replace(/[^0-9A-Za-z]/gi, "").toUpperCase(),
				bank_acc: this.state.accNum,
				bank_code: this.state.bankCode,
				id_type: this.state.organization === "company" ? "1" : "0",
			};

			this.props.editBankDetails(this.props.profile.store.id, data, this.props.auth.token);
		}
	};

	render() {
		const { accNum, bankName, fullName, id_num, organization, reject_desc, status, ssmInfo } = this.state;
		const { bankList, user, business } = this.props.profile;
		const org = this.state.organization === "company";
		const isEdit = !(status == "0" || status == "3");
		const lang = langSelector.BankDetails.find(e => e.lang === this.props.preference.lang).data;

		const BankTypeSelector = ({ iconType: Icon, ...props }) => {
			let isActive = props.value === this.state.organization;
			return (
				<TouchableRipple onPress={props.disabled ? null : props.handleChoose}>
					<View
						style={{
							alignItems: "center",
							justifyContent: "center",
							width: getWidth * 0.48,
							paddingVertical: 25,
						}}
					>
						<Icon color={accent} size={55} name={props.iconName} />
						<Typography style={{ fontSize: 16, paddingVertical: 10 }}>{props.title}</Typography>
						<View
							style={{
								borderRadius: 11,
								width: 22,
								height: 22,
								alignItems: "center",
								justifyContent: "center",
								borderWidth: 1,
								borderColor: isActive ? accent : greywhite,
								backgroundColor: isActive ? accent : "transparent",
							}}
						>
							{isActive ? <Ionicons name="checkmark" size={18} color={mainBgColor} /> : null}
						</View>
					</View>
				</TouchableRipple>
			);
		};

		const StatusBadge = ({ status }) => {
			let containerStyle = {
				flexDirection: "row",
				justifyContent: "flex-end",
				alignItems: "center",
			};
			let iconStyle = { marginTop: 1, marginRight: 3 };
			let iconSize = 12;

			switch (status) {
				case "0":
				case "3":
					return (
						<View style={containerStyle}>
							<Octicons name="unverified" color={greylight} size={iconSize} style={iconStyle} />
							<Typography type="subheadline" style={{ color: grey }}>
								{lang.pending}
							</Typography>
						</View>
					);
				case "1":
					return (
						<View style={containerStyle}>
							<Octicons name="verified" color={green} size={iconSize} style={iconStyle} />
							<Typography type="subheadline" style={{ color: green }}>
								{lang.verified}
							</Typography>
						</View>
					);
				case "2":
					return (
						<View style={containerStyle}>
							<Ionicons name="ios-close-circle" color={red} size={iconSize} style={iconStyle} />
							<Typography type="subheadline" style={{ color: red }}>
								{lang.rejected}
							</Typography>
						</View>
					);
				default:
					return <View />;
			}
		};

		return (
			<LayoutView>
				<NavHeader
					onBack={this.handleBack}
					title="Bank Details"
					actionText={isEdit ? "Done" : ""}
					onPress={this.handleSubmit}
				/>
				<AlertDialog
					title="Info"
					desc="Use the SSM number that you used for your bank account registration."
					active={ssmInfo}
					onDismiss={this.handleSSMInfo(false)}
					actions={[{ title: "OK", onPress: this.handleSSMInfo(false) }]}
				/>

				<View style={styles.container}>
					<ScrollView
						showsVerticalScrollIndicator={false}
						style={{ width: "100%" }}
						contentContainerStyle={{ paddingBottom: 30 }}
					>
						<SectionBox
							title="Type"
							innerStyle={{ flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 0 }}
						>
							<BankTypeSelector
								iconType={Ionicons}
								iconName="person"
								handleChoose={this.handleOrg2("personal")}
								title="Individual"
								value="personal"
								disabled={!isEdit}
							/>
							<BankTypeSelector
								iconType={Ionicons}
								iconName="business"
								handleChoose={this.handleOrg2("company")}
								title="Organization"
								value="company"
								disabled={!isEdit}
							/>
						</SectionBox>
						<SectionBox title={lang.detailsTitle} rightHeader={<StatusBadge status={status} />}>
							<TextField
								disabled={!isEdit}
								onChangeText={this.handleText("fullName")}
								inputStyle={{ paddingHorizontal: 0 }}
								style={styles.textInput}
								mode="outlined"
								label="Name"
								placeholder={lang.name}
								value={fullName}
								errorKey="acc_name"
							/>
							<SectionDivider />
							<TextField
								type="select"
								disabled={!isEdit}
								style={styles.textInput}
								inputStyle={{ paddingHorizontal: 0 }}
								selectorList={BankList}
								label="Instituition"
								onSelect={this.handleBankName}
								value={!isEmpty(bankName) ? bankName : lang.defaultBank}
							/>
							<SectionDivider />
							<TextField
								disabled={!isEdit}
								keyboardType="number-pad"
								style={styles.textInput}
								inputStyle={{ paddingHorizontal: 0 }}
								mode="outlined"
								label="Account no."
								placeholder="Bank account number"
								value={accNum}
								onChangeText={this.handleText("accNum")}
								errorKey="bank_acc"
							/>
							<SectionDivider />
							<TextField
								disabled={!isEdit}
								style={styles.textInput}
								inputStyle={{ paddingHorizontal: 0 }}
								mode="outlined"
								label={org ? "SSM no." : "NRIC"}
								value={id_num}
								onChangeText={this.handleText("id_num")}
								errorKey="id_number"
								endAdornment={
									org ? (
										<TouchableRipple rippleColor={transparent} onPress={this.handleSSMInfo(true)}>
											<Ionicons name="ios-information-circle-outline" size={18} color={accent} />
										</TouchableRipple>
									) : undefined
								}
							/>
						</SectionBox>

						<Typography type="caption" style={[styles.caption, { marginBottom: 12 }]}>
							Please ensure that information is correct and valid.
						</Typography>
						<Typography type="caption" style={styles.caption}>
							Approval process may take up to 3 business days and cannot be edited while in process for
							security measures.
						</Typography>
					</ScrollView>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		// alignItems: "center"
	},

	caption: {
		color: greyblack,
		paddingHorizontal: 16,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	profile: state.profile,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	getBankDetails,
	editBankDetails,
	clearBank,
	customNoti,
	setLoading,
})(BankDetails);
