import React from "react";
import { connect } from "react-redux";
import { StyleSheet, ScrollView, View } from "react-native";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import NavHeader from "../component/NavHeader";
import isEmpty from "../utils/isEmpty";
import { withNavigationFocus } from "react-navigation";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import getWidth from "../utils/getWidth";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import Typography from "../component/Typography";

class Manage extends React.Component {
	handlePage = page => () => {
		this.props.navigation.push(page);
	};
	// handlePromotion = page => () => {
	// 	this.props.navigation.push(PromotionAdd);
	// };

	render() {
		const InfoBox = ({ iconType: Icon, iconName, title, desc, color, topColor, bottomColor, onPress }) => {
			return (
				<TouchableWithoutFeedback onPress={onPress} style={[styles.infoBox]}>
					<LinearGradient style={styles.infoBoxBackground} colors={[topColor, bottomColor]} />
					<View style={[styles.infoBoxContent]}>
						<View
							style={{
								marginBottom: 15,
								width: "100%",
								flexDirection: "row",
								justifyContent: "space-between",
								alignItems: "center",
							}}
						>
							<Icon name={iconName} size={28} color="white" />
							<MaterialCommunityIcons name="chevron-right-circle" color="white" size={25} />
						</View>
						<View>
							<Typography type="headline" style={[styles.infoBoxTitleText]}>
								{title}
							</Typography>
							<Typography
								type="caption"
								style={styles.infoBoxDesc}
								numberOfLines={2}
								ellipsizeMode="tail"
							>
								{desc}
							</Typography>
						</View>
					</View>
				</TouchableWithoutFeedback>
			);
		};

		return (
			<LayoutView noLoading>
				<NavHeader />

				<ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ width: "100%" }}>
					<View style={styles.container}>
						<PageTitle>Manage</PageTitle>

						<View style={styles.infoContainer}>
							<InfoBox
								onPress={this.handlePage("PaymentMethod")}
								iconType={MaterialCommunityIcons}
								iconName="credit-card-outline"
								//color="#13BBCF"
								topColor="#13BBCF"
								bottomColor="#1DB2C3"
								title="Payments"
								desc="Online banking, manual transfer, COD ..."
							/>

							<InfoBox
								onPress={this.handlePage("CatalogShipping")}
								iconType={MaterialCommunityIcons}
								iconName="truck"
								//color="#6966CD"
								topColor="#6966CD"
								bottomColor="#6B69C1"
								title="Shipments"
								desc="Courier partners, rates, handling ..."
							/>
							<InfoBox
								onPress={this.handlePage("PayoutList")}
								iconType={MaterialCommunityIcons}
								iconName="bank-plus"
								//color="#FA656A"
								topColor="#FA656A"
								bottomColor="#EB666A"
								title="Payouts"
								desc="Show payouts, download statements ..."
							/>
							<InfoBox
								onPress={this.handlePage("Promotion")}
								iconType={MaterialCommunityIcons}
								iconName="tag-text"
								// color="#3FC75B"
								topColor="#3FC75B"
								bottomColor="#42bb5B"
								title="Promotions"
								desc="Promo codes"
							/>
						</View>
					</View>
				</ScrollView>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},

	infoContainer: {
		width: "100%",
		flexDirection: "row",
		alignItems: "center",
		flexWrap: "wrap",
		justifyContent: "space-between",
		marginTop: 16,
		marginBottom: 30,
	},
	infoBox: {
		width: getWidth * 0.5 - 5,
		height: 110,
		borderRadius: 10,
		overflow: "hidden",
		backgroundColor: "white",
		marginBottom: 10,
		// height: 163
	},
	infoBoxBackground: {
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		width: "100%",
		height: "100%",
	},
	infoBoxContent: {
		width: "100%",
		paddingVertical: 8,
		paddingHorizontal: 10,
		// height:
	},
	infoBoxTitleText: {
		fontFamily: "semibold",
		color: "white",
	},
	infoBoxDesc: {
		fontFamily: "regular",
		color: "white",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	profile: state.profile,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps)(withNavigationFocus(Manage));
