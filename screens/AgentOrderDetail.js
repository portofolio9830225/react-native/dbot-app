import React from "react";
import { connect } from "react-redux";
import isEmpty from "../utils/isEmpty";
import * as WebBrowser from "expo-web-browser";

import {
	black,
	blue,
	green,
	greyblack,
	greywhite,
	orange,
	red,
	transparent,
	whatsappColor,
} from "../utils/ColorPicker";
import DateConverter from "../utils/DateConverter";
import TimeConverter from "../utils/TimeConverter";

import { Image, StyleSheet, Text, View, ScrollView } from "react-native";
import { getSingleAgentOrder, setAgentOrderStatus } from "../store/actions/agentAction";
import { LayoutView } from "../component/Layout";
import NavHeader from "../component/NavHeader";

import PageTitle from "../component/PageTitle";
import langSelector from "../utils/langSelector";
import { customNoti } from "../store/actions/alertAction";
import { Button, Dialog, Portal, TouchableRipple } from "react-native-paper";
import { MaterialCommunityIcons } from "@expo/vector-icons";

class AgentOrderDetail extends React.Component {
	state = {
		confirmDialog: false,
	};

	componentDidMount() {
		this.props.getSingleAgentOrder(this.props.navigation.state.params.oID, this.props.auth.token);
	}

	// componentDidUpdate(prevProps, prevState) {
	//   if (
	//     prevProps.agent.orderDetail.status === 2 &&
	//     this.props.agent.orderDetail.status === 3
	//   ) {
	//     this.handleConfirmDialog(true)();
	//   }
	// }

	componentWillUnmount() {
		this.props.getSingleAgentOrder();
	}

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handleContact = () => {
		let itemArr = "";
		this.props.agent.orderDetail.items.map((e, i) => {
			itemArr += `*${i + 1}. ${e.item_name} (x${e.quantity})*\n`;
		});

		let text = `[StoreUp Agent - _Purchase ID:_ *${this.props.agent.orderDetail.order_id}*]\n\nHi ${this.props.agent.hqList[0].owner.business_name}, I'm ${this.props.profile.store.name}\n\nBelow are the products that I have ordered:-\n${itemArr}\n\nPlease review my request.`;

		WebBrowser.openBrowserAsync(
			`https://wa.me/60${this.props.agent.hqList[0].owner.phone}?text=${encodeURIComponent(text)}`
		);
	};

	handlePage =
		(route, params = {}) =>
		() => {
			this.props.navigation.push(route, params);
		};

	handleConfirmDialog = state => () => {
		this.setState({
			confirmDialog: state,
		});
	};

	handleConfirmDialogAction = status => () => {
		this.handleConfirmDialog(false)();
		this.props.setAgentOrderStatus(this.props.navigation.state.params.oID, { status }, this.props.auth.token);
	};

	render() {
		const { confirmDialog } = this.state;
		const { orderDetail } = this.props.agent;
		const lang = langSelector.AgentOrderDetail.find(e => e.lang === this.props.preference.lang).data;

		const ItemBox = ({ id, name, img, count, price }) => {
			return (
				<TouchableRipple
					rippleColor={transparent}
					onPress={this.handlePage("AgentItemPage", {
						id,
						isAgent: true,
						viewOnly: true,
					})}
				>
					<View style={styles.itemBox}>
						<View style={styles.itemImg}>
							<Image
								source={{ uri: img }}
								style={{
									width: "100%",
									height: undefined,
									aspectRatio: 1,
								}}
							/>
						</View>
						<View style={styles.itemBoxBreakdown}>
							<Text style={styles.itemName} ellipsizeMode="tail">
								{name}
							</Text>
							<View
								style={{
									flexDirection: "row",
									justifyContent: "space-between",
									marginTop: 10,
								}}
							>
								<Text style={styles.itemQuantity}>{`x ${count}`}</Text>
								<Text style={styles.itemPrice}>{`RM${price.toFixed(2)}`} </Text>
							</View>
						</View>
					</View>
				</TouchableRipple>
			);
		};
		const PriceList = ({ title, text, end }) => {
			return (
				<View style={styles.priceList}>
					<Text style={styles.priceListTitle}>{title}</Text>

					<Text style={[styles.priceListText, { fontFamily: end ? "bold" : "medium" }]}>{text}</Text>
				</View>
			);
		};

		const DateTimeGetter = ({ dt }) => {
			let date = new Date(dt);

			let h = date.getHours();
			let min = date.getMinutes();

			let d = date.getDate();
			let month = date.getMonth() + 1;
			let y = date.getFullYear();

			return `${DateConverter(
				`${y}-${month < 10 ? `0${month}` : month}-${d < 10 ? `0${d}` : d}`
			)}, ${TimeConverter(`${h < 10 ? `0${h}` : h}:${min < 10 ? `0${min}` : min}:00`)}`;
		};

		const StatusText = ({ status }) => {
			let color = greyblack;
			let text = lang.pending;

			switch (status) {
				case 1:
					{
						text = lang.accepted;
						color = green;
					}
					break;
				case 2:
					{
						text = lang.shipping;
						color = orange;
					}
					break;

				case 3:
					{
						text = lang.done;
						color = blue;
					}
					break;
				case 8:
					{
						text = lang.canceled;
						color = red;
					}
					break;
				case 9:
					{
						text = lang.rejected;
						color = red;
					}
					break;
			}

			return (
				<Text
					style={{
						marginTop: 20,
						fontSize: 16,
						fontFamily: "medium",
						color: color,
						width: "100%",
					}}
				>
					{lang.orderStatus}: {text}
				</Text>
			);
		};

		const ConfirmModalTitle = ({ status }) => {
			switch (status) {
				case 2:
					return lang.deliveredModalTitle;
				default:
					return "";
			}
		};

		return (
			<LayoutView>
				<NavHeader onBack={this.handleBack} />

				<Portal>
					<Dialog visible={confirmDialog} onDismiss={this.handleConfirmDialog(false)}>
						<Dialog.Title>
							<ConfirmModalTitle status={orderDetail.status} />
						</Dialog.Title>
						<Dialog.Actions>
							<Button compact style={{ marginHorizontal: 10 }} onPress={this.handleConfirmDialog(false)}>
								{lang.modalNo}
							</Button>
							<Button
								compact
								style={{ marginHorizontal: 10 }}
								color={green}
								onPress={this.handleConfirmDialogAction(orderDetail.status + 1)}
							>
								{lang.modalYes}
							</Button>
						</Dialog.Actions>
					</Dialog>
				</Portal>
				{!isEmpty(orderDetail) && (
					<View style={styles.root}>
						<ScrollView
							showsVerticalScrollIndicator={false}
							contentContainerStyle={{
								width: "100%",
							}}
						>
							<PageTitle>
								{lang.title}: {orderDetail.order_id}
							</PageTitle>
							<Text
								style={{
									fontFamily: "light",
									color: greyblack,
									width: "100%",
								}}
							>
								{lang.orderDate}: <DateTimeGetter dt={orderDetail.created_ts} />
							</Text>
							<StatusText status={orderDetail.status} />
							<View style={styles.orderContent}>
								<View style={styles.sectionBox}>
									<Text style={[styles.sectionTitle, { marginBottom: 13 }]}>{lang.product}</Text>

									{orderDetail.items.map((e, i) => {
										return (
											<ItemBox
												key={i}
												id={e.item_id}
												name={e.item_name}
												img={e.item_image}
												count={e.quantity}
												price={e.item_price}
											/>
										);
									})}
								</View>

								<View style={styles.totalBox}>
									<View style={[styles.priceList, { marginTop: 25, marginBottom: 0 }]}>
										<Text
											style={[
												styles.priceListTitle,
												{
													fontFamily: "bold",
												},
											]}
										>
											{lang.total}
										</Text>

										<Text
											style={styles.priceListText}
											style={{
												fontFamily: "bold",
												fontSize: 30,
												lineHeight: 33,
											}}
										>
											RM
											{orderDetail.total_price.toFixed(2)}
										</Text>
									</View>
								</View>
							</View>
						</ScrollView>
						<View
							style={{
								flexDirection: "row",
								justifyContent: "space-between",
								alignItems: "center",
								width: "100%",
								alignSelf: "center",
								paddingTop: 10,
								paddingBottom: Platform.OS === "ios" ? 30 : 10,
							}}
						>
							<TouchableRipple
								onPress={this.handleContact}
								rippleColor="transparent"
								style={{
									alignItems: "center",
									justifyContent: "center",
									width: "18%",
									borderColor: whatsappColor,
									borderWidth: 0.5,
									height: 50,
									borderRadius: 7,
								}}
							>
								<MaterialCommunityIcons name="whatsapp" size={35} color={whatsappColor} />
							</TouchableRipple>
							{/* </Button> */}
							<Button
								mode="contained"
								disabled={orderDetail.status !== 2}
								labelStyle={{ color: "white" }}
								color={black}
								onPress={orderDetail.status !== 2 ? null : this.handleConfirmDialog(true)}
								style={[styles.staticButton]}
								contentStyle={{
									height: 50,
								}}
							>
								{lang.received}
							</Button>
						</View>
					</View>
				)}
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	root: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		position: "relative",
	},
	businessCard: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-evenly",
		alignItems: "center",
		marginVertical: 17,
		width: "100%",
	},

	businessLogo: {
		width: 80,
		height: 80,
		borderRadius: 40,
	},
	businessDetails: {
		display: "flex",
		flexDirection: "column",
	},
	businessName: {
		fontSize: 17,
		fontFamily: "bold",
		textTransform: "uppercase",
		marginBottom: 7,
	},
	businessRegName: {
		fontSize: 14,
	},
	businessEmail: {
		fontSize: 14,
		fontFamily: "regular",
	},
	businessPhone: {
		fontSize: 14,
		fontFamily: "regular",
	},
	orderContent: {
		//width: "100%",
		// paddingVertical: 25,
		marginTop: 17,
	},

	sectionTitle: {
		fontSize: 15,
		fontFamily: "bold",
		textTransform: "uppercase",
	},
	sectionBox: {
		//width: "100%",
		display: "flex",
		flexDirection: "column",
		// borderBottomWidth: 1,
		// borderBottomColor: greywhite,
		paddingVertical: 14,
	},
	itemBox: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingVertical: 10,
		//width: "100%",
	},
	itemBoxHeader: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		width: "100%",
	},
	itemBoxBreakdown: {
		display: "flex",
		flexDirection: "column",
		width: "70%",
	},

	itemImg: {
		width: "25%",
		height: undefined,
		aspectRatio: 1,
		borderRadius: 10,
		borderWidth: 0.5,
		borderColor: greywhite,
		overflow: "hidden",
	},
	itemName: {
		fontSize: 16,
		fontFamily: "light",
	},
	itemPrice: {
		fontSize: 15,
		fontFamily: "regular",
		color: greyblack,
		lineHeight: 17,
	},
	itemQuantity: {
		fontSize: 15,
		fontFamily: "bold",
	},
	priceList: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "flex-start",
		marginBottom: 5,
	},
	priceListTitle: {
		//width: "53%",
		fontSize: 14,
		fontFamily: "light",
	},
	priceListText: {
		//width: "45%",
		fontSize: 14,
		textAlign: "right",
	},
	totalBox: {
		width: "100%",
		display: "flex",
		flexDirection: "column",
		marginTop: 14,
		paddingTop: 18,
	},
	viewText: {
		fontFamily: "medium",
		paddingTop: 5,
		paddingBottom: 3,
	},
	viewButton: {
		marginTop: 30,
	},
	staticButton: {
		width: "80%",
	},
	statusButton: {
		width: "100%",
		marginVertical: 5,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	agent: state.agent,
	profile: state.profile,
	loading: state.loading,
});

export default connect(mapStateToProps, {
	setAgentOrderStatus,
	getSingleAgentOrder,
	customNoti,
})(AgentOrderDetail);
