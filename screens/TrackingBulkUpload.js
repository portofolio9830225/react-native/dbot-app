import React from "react";
import { StyleSheet, View, Text, ScrollView, Platform } from "react-native";
import { connect } from "react-redux";

import { requestTrackingBulk } from "../store/actions/itemAction";
import { setLoading } from "../store/actions/loadingAction";

import { Button } from "react-native-paper";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import isEmpty from "../utils/isEmpty";
import NavHeader from "../component/NavHeader";
import { black, greyblack, transparent } from "../utils/ColorPicker";
import langSelector from "../utils/langSelector";

class TrackingBulkUpload extends React.Component {
	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.alert.redirect) && this.props.alert.redirect === "TrackingBulkUpload") {
			this.props.setLoading(false);
			this.handleBack();
		}
	}

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handleSubmit = () => {
		this.props.setLoading(true);

		this.props.requestTrackingBulk(this.props.auth.token);
	};

	render() {
		const lang = langSelector.TrackingBulkUpload.find(e => e.lang === (this.props.preference.lang || "en")).data;

		return (
			<LayoutView>
				<NavHeader onBack={this.handleBack} actionText="Submit" onPress={this.handleSubmit} />

				<View style={styles.container}>
					<PageTitle fontSize={28}>{lang.title}</PageTitle>
					<Text style={{ color: greyblack, fontFamily: "light", fontSize: 15 }}>{lang.desc}</Text>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		// alignItems: "center"
	},

	saveButton: {
		marginTop: 20,
		alignSelf: "flex-end",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	alert: state.alert,
});

export default connect(mapStateToProps, {
	requestTrackingBulk,
	setLoading,
})(TrackingBulkUpload);
