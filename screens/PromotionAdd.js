import React from "react";
import { StyleSheet, ScrollView, View, Text, Keyboard } from "react-native";
import { connect } from "react-redux";
import { LayoutView } from "../component/Layout";
import NavHeader from "../component/NavHeader";
import { accent, greyblack, red, mainBgColor } from "../utils/ColorPicker";
import TextField from "../component/TextField";
import { TouchableRipple } from "react-native-paper";
import isEmpty from "../utils/isEmpty";
import { isTablet } from "react-native-device-detection";
import { withNavigationFocus } from "react-navigation";
import { Ionicons } from "@expo/vector-icons";

import { createPromo, deleteSinglePromo, editSinglePromo, getSinglePromo } from "../store/actions/promoAction";

import { setLoading } from "../store/actions/loadingAction";
// import { getItemShipping } from "../store/actions/catalogAction";
import { customNoti } from "../store/actions/alertAction";
import Typography from "../component/Typography";
import { SectionBox, SectionDivider } from "../component/SectionComponents";
import SwitcherWithLabel from "../component/SwitcherWithLabel";
import PortalSafeView from "../component/PortalSafeView";
import AlertDialog from "../component/AlertDialog";
import DateConverter from "../utils/DateConverter";

class PromotionAdd extends React.Component {
	state = {
		isEdit: false,
		isActive: false,
		sDate: null,
		eDate: null,
		sMarked: {},
		eMarked: {},
		isNoExpired: false,
		isNoExpiredSent: false,
		marked: {},
		calendar: false,
		code: "",
		type: "percent",
		start_date: "",
		end_date: "",
		total_min: "",
		limited_use: true,
		quantity: "",
		discount: "",
		dateExpand: "",
		deleteDialog: false,
	};

	componentDidMount() {
		if (!isEmpty(this.props.navigation.state.params.id)) {
			this.props.setLoading(true);
			this.setState(
				{
					isEdit: true,
				},
				() => {
					this.props.getSinglePromo(
						this.props.navigation.state.params.id,
						this.props.auth.token,
						this.props.navigation.state.params.bID
					);
				}
			);
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (!isEmpty(this.props.promo.singlePromo) && isEmpty(prevProps.promo.singlePromo)) {
			let { singlePromo } = this.props.promo;
			this.setState({
				isActive: singlePromo.active,
				code: singlePromo.code,
				type: singlePromo.type,
				discount: singlePromo.value_fixed?.toFixed(2) || singlePromo.value_percent?.toFixed(0) || "",
				start_date: singlePromo.start_date || null,
				end_date: singlePromo.end_date || null,
				total_min: singlePromo.total_min?.toFixed(2) || "0.00",
				limited_use: singlePromo.limited_use,
				quantity: singlePromo.quantity?.toFixed(0) || "0",
				isNoExpiredSent: isEmpty(singlePromo.end_date),
			});
		}
		if (isEmpty(prevProps.alert.status) && this.props.alert.status === "success") {
			if (
				this.props.alert.redirect === "editPromo" ||
				this.props.alert.redirect === "deletePromo" ||
				this.props.alert.redirect === "createPromo"
			) {
				this.handleBack();
			}
		}
		if (!prevState.calendar && this.state.calendar) {
			this.setState({
				sDate: this.state.start_date,
				eDate: this.state.end_date,
				isNoExpired: this.state.isNoExpiredSent,
				dateExpand: "",
			});
		}
	}

	componentWillUnmount() {}

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handleType = val => {
		this.setState({
			type: val,
		});
	};

	handleType2 = val => () => {
		this.handleType(val);
	};

	handleText = name => value => {
		this.setState({
			[name]: value,
		});
	};

	handleDate = prefix => day => {
		let dateKey = "Date";
		dateKey = `${prefix}${dateKey}`;

		this.setState({
			[dateKey]: day,
		});
		if (prefix === "s") {
			let s = new Date(day);
			let e = new Date(this.state.eDate);
			this.setState({
				eDate: s > e ? null : this.state.eDate,
			});
		}
	};

	handleCalendar = state => () => {
		this.setState({
			calendar: state,
		});
	};

	handleDeleteDialog = state => () => {
		this.setState({
			deleteDialog: state,
		});
	};

	handleDateExpand = val => {
		this.setState({
			dateExpand: val === this.state.dateExpand ? "" : val,
		});
	};

	handleSubmitCalendar = state => () => {
		let data = {
			start_date: this.state.sDate,
			end_date: this.state.eDate,
		};

		this.handleCalendar(false)();
		if (state === true) {
			this.setState({
				start_date: data.start_date,
				end_date: this.state.isNoExpired ? null : data.end_date,
				isNoExpiredSent: this.state.isNoExpired,
			});
		}
	};

	handleDelete = () => {
		this.setState(
			{
				deleteDialog: false,
			},
			() => {
				this.props.deleteSinglePromo(this.props.navigation.state.params.id, this.props.auth.token);
			}
		);
	};

	handleSubmit = () => {
		Keyboard.dismiss();

		const { code, type, start_date, end_date, total_min, limited_use, quantity, discount } = this.state;

		//format start_date and end_date DD/MM/YYYY
		let data = {
			code,
			type,
			value_fixed: type == "fixed" ? parseFloat(discount) : null,
			value_percent: type == "percent" ? parseFloat(discount) : null,
			start_date,
			end_date,
			total_min: !isEmpty(total_min) ? parseFloat(total_min) : null,
			limited_use,
			quantity: limited_use ? parseInt(quantity) : null,
			active: true,
		};

		if (!this.state.isEdit) {
			this.props.createPromo(data, this.props.auth.token);
		} else {
			this.props.editSinglePromo(this.props.navigation.state.params.id, data, this.props.auth.token);
		}
	};

	render() {
		const {
			isEdit,
			code,
			type,
			start_date,
			end_date,
			total_min,
			limited_use,
			quantity,
			marked,
			sDate,
			eDate,
			sMarked,
			eMarked,
			isNoExpired,
			isNoExpiredSent,
			discount,
			calendar,
			dateExpand,
			deleteDialog,
		} = this.state;

		const isCalendarFilled = !isEmpty(sDate) && (isNoExpired || !isEmpty(eDate)); //perlu tengok balik
		const isFilled =
			!isEmpty(code) &&
			!isEmpty(discount) &&
			!isEmpty(start_date) &&
			(isNoExpiredSent || !isEmpty(end_date)) &&
			(!limited_use || (limited_use && !isEmpty(quantity)));

		const ItemSelector = ({ title, desc, val, onPress, active }) => {
			return (
				<TouchableRipple rippleColor="transparent" onPress={onPress}>
					<View
						style={{
							width: "100%",
							flexDirection: "row",
							justifyContent: "space-between",
							alignItems: "center",
							paddingVertical: 10,
						}}
					>
						<View style={{ flex: 1 }}>
							<Text style={{ fontSize: 15, fontFamily: "regular" }}>{title}</Text>
							{desc ? (
								<Text style={{ fontSize: 12, fontFamily: "regular", color: greyblack }}>{desc}</Text>
							) : null}
						</View>
						<View
							style={{
								width: 22,
								height: 22,
								alignItems: "center",
								justifyContent: "center",
							}}
						>
							{active ? <Ionicons color={accent} size={20} name="checkmark" /> : null}
						</View>
					</View>
				</TouchableRipple>
			);
		};

		return (
			<LayoutView>
				<AlertDialog
					active={deleteDialog}
					title="Confirm delete promotion?"
					desc="This cannot be undone"
					onDismiss={this.handleDeleteDialog(false)}
					actions={[
						{ title: "Yes", onPress: this.handleDelete },
						{ title: "Cancel", onPress: this.handleDeleteDialog(false) },
					]}
				/>
				{calendar ? (
					<PortalSafeView>
						<View
							style={{
								width: "100%",
								height: "100%",
								backgroundColor: mainBgColor,
								paddingHorizontal: 16,
							}}
						>
							<NavHeader
								onBack={this.handleSubmitCalendar(false)}
								actionText="Done"
								onPress={isCalendarFilled ? this.handleSubmitCalendar(true) : null}
							/>
							<ScrollView style={{ width: "100%" }} contentContainerStyle={{ paddingBottom: 30 }}>
								<SectionBox innerStyle={{ paddingHorizontal: 0 }}>
									<SwitcherWithLabel
										label="No expiry"
										enabled={isNoExpired}
										onValueChange={() => {
											this.setState(
												{
													isNoExpired: !this.state.isNoExpired,
												},
												() => {
													if (!this.state.isNoExpired && this.state.dateExpand === "end") {
														this.setState({
															dateExpand: "",
														});
													}
												}
											);
										}}
										//cardStyle={{ paddingHorizontal: 0 }}
									/>

									<TextField
										type="calendar"
										label="Start date"
										value={!isEmpty(sDate) ? DateConverter(sDate) : ""}
										onChangeText={this.handleDate("s")}
										date={sDate}
										handleExpand={this.handleDateExpand}
										expandValue="start"
										expandActive={dateExpand === "start"}
									/>
									{!isNoExpired && (
										<TextField
											type="calendar"
											label="End date"
											value={!isEmpty(eDate) ? DateConverter(eDate) : ""}
											onChangeText={this.handleDate("e")}
											date={eDate}
											handleExpand={this.handleDateExpand}
											expandValue="end"
											expandActive={dateExpand === "end"}
											minDate={sDate}
										/>
									)}
								</SectionBox>
							</ScrollView>
						</View>
					</PortalSafeView>
				) : null}

				<NavHeader
					onBack={this.handleBack}
					onPress={!isFilled ? null : this.handleSubmit}
					actionText="Done"
					title={isEdit ? "Promotion Details" : "Create Promotion"}
				/>

				<ScrollView
					contentContainerStyle={{ width: "100%", paddingBottom: 30 }}
					showsVerticalScrollIndicator={false}
				>
					<View style={styles.container}>
						<View
							style={{
								width: "100%",
								alignSelf: "center",
							}}
						>
							<SectionBox title="COUPON CODE">
								<TextField
									disabled={isEdit}
									inputStyle={{ paddingHorizontal: 0 }}
									style={styles.textField}
									label="Code"
									value={code}
									onChangeText={this.handleText("code")}
									placeholder="Coupon Name"
								/>
							</SectionBox>
							{isEdit ? null : (
								<Typography
									type="caption"
									style={{
										paddingHorizontal: 10,
									}}
								>
									This code is not editable later.
								</Typography>
							)}
							<SectionBox title="COUPON TYPE">
								<ItemSelector
									title="By percentage (%)"
									val={"percent"}
									onPress={isEdit ? null : this.handleType2("percent")}
									active={type == "percent"}
								/>
								<SectionDivider />
								<ItemSelector
									title="By amount (RM)"
									val={"fixed"}
									onPress={isEdit ? null : this.handleType2("fixed")}
									active={type == "fixed"}
								/>
							</SectionBox>
							<SectionBox title="DISCOUNT">
								<TextField
									disabled={isEdit}
									inputStyle={{ paddingHorizontal: 0 }}
									style={styles.textField}
									keyboardType="number-pad"
									//label="Discount"
									value={discount}
									onChangeText={this.handleText("discount")}
									// label="1-100"
									placeholder={type == "fixed" ? "" : "1-100"}
									startAdornment={type == "fixed" ? "RM" : null}
									endAdornment={type == "fixed" ? null : "%"}
								/>
							</SectionBox>
							<SectionBox title="VALIDITY">
								<TextField
									inputStyle={{ paddingHorizontal: 0 }}
									onPressInput={this.handleCalendar(true)}
									value={
										isEmpty(start_date)
											? "DD/MM/YYYY - DD/MM/YYYY"
											: !isEmpty(end_date)
											? `${DateConverter(start_date, "DD/MM/YYYY")} - ${DateConverter(
													end_date,
													"DD/MM/YYYY"
											  )}`
											: `${DateConverter(start_date, "DD/MM/YYYY")} - No expiry`
									}
									type="multiline"
								/>
							</SectionBox>
							<SectionBox title="MINIMUM ORDER AMOUNT">
								<TextField
									disabled={isEdit}
									// disabled={!viewOnly && !isAgentItem}
									inputStyle={{ paddingHorizontal: 0 }}
									style={styles.textField}
									// label="Code"
									value={total_min}
									onChangeText={this.handleText("total_min")}
									startAdornment="RM"
									keyboardType="number-pad"
								/>
							</SectionBox>
							<SwitcherWithLabel
								label="Limited redemption"
								enabled={limited_use}
								onValueChange={() => {
									this.setState({
										limited_use: !this.state.limited_use,
									});
								}}
							/>
							<Typography
								type="caption"
								style={{
									padding: 10,
								}}
							>
								A redemption limit restricts the number of times a coupon, code or voucher can be used.
							</Typography>
							{limited_use ? (
								<SectionBox title="Quantity Remaining">
									<TextField
										// disabled={!viewOnly && !isAgentItem}
										inputStyle={{ paddingHorizontal: 0 }}
										style={styles.textField}
										//label="Quantity Remaining"
										value={quantity}
										onChangeText={this.handleText("quantity")}
										keyboardType="number-pad"
										placeholder="Quantity remaining"
									/>
								</SectionBox>
							) : null}
							{isEdit ? (
								<TouchableRipple onPress={this.handleDeleteDialog(true)} rippleColor="transparent">
									<SectionBox
										innerStyle={{ height: 55, justifyContent: "center", alignItems: "center" }}
									>
										<Typography
											type="headline"
											style={{
												color: red,
												textAlign: "center",
												fontFamily: "semibold",
											}}
										>
											Delete promotion
										</Typography>
									</SectionBox>
								</TouchableRipple>
							) : null}
						</View>
					</View>
				</ScrollView>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
	textField: {
		marginBottom: 0,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	item: state.item,
	profile: state.profile,
	catalog: state.catalog,
	loading: state.loading,
	alert: state.alert,
	error: state.error,
	promo: state.promo,
});

export default connect(mapStateToProps, {
	setLoading,
	customNoti,
	createPromo,
	getSinglePromo,
	deleteSinglePromo,
	editSinglePromo,
})(withNavigationFocus(PromotionAdd));
