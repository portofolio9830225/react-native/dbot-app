import React from "react";

import AsyncStorage from "@react-native-async-storage/async-storage";
import { StyleSheet, Text, View, Keyboard } from "react-native";
import * as WebBrowser from "expo-web-browser";

import { connect } from "react-redux";
import { sendUserEmail, forgotPassword } from "../store/actions/authAction";
import { setLoading } from "../store/actions/loadingAction";
import { customNoti } from "../store/actions/alertAction";

import { Button, TouchableRipple } from "react-native-paper";

import { LayoutView } from "../component/Layout";

import PageTitle from "../component/PageTitle";
import NavHeader from "../component/NavHeader";
import isEmpty from "../utils/isEmpty";
import { grey, greywhite } from "../utils/ColorPicker";
import PasswordField from "../component/PasswordField";
import TextField from "../component/TextField";
import { Ionicons, Feather, Foundation } from "@expo/vector-icons";
import FadeInOut from "react-native-fade-in-out";
import CustomButton from "../component/CustomButton";
import Typography from "../component/Typography";
import { SectionBox, SectionDivider } from "../component/SectionComponents";

class SignInCreatePassword extends React.Component {
	state = {
		email: "",
		url: "app.storeup.io/set-password",
		isAppUsed: null,
		isRegister: false,
	};

	componentDidMount() {
		let data = {
			email: this.props.navigation.state.params.email,
			url: this.state.url,
		};
		this.props.forgotPassword(data);
	}

	componentDidUpdate(prevProps, prevState) {}

	componentWillUnmount() {
		this.props.setLoading(false);
	}

	render() {
		const { email, pass, isRegister } = this.state;

		return (
			<LayoutView>
				<View style={styles.container}>
					<Foundation name={"key"} size={86} color="#0D65FB" />
					<Typography type="largetitle" style={styles.pageTitle}>
						Create Your StoreUp ID Password
					</Typography>
					<Typography type="subtitle" style={styles.subtitle}>
						You will receive an email with a link to create a password to log in.
					</Typography>
					<View
						style={{
							flexDirection: "row",
							justifyContent: "space-between",
							alignItems: "center",
							position: "absolute",
							bottom: 50,
						}}
					>
						<TouchableRipple
							onPress={() => {
								this.props.navigation.navigate("SignIn", { isRegister: false });
							}}
						>
							<Text style={styles.buttonText}>Log In</Text>
						</TouchableRipple>
						<Text style={styles.button}>or</Text>
						<TouchableRipple
							onPress={() => {
								this.props.navigation.navigate("SignIn", { isRegister: true });
							}}
						>
							<Text style={styles.buttonText}>Register</Text>
						</TouchableRipple>
					</View>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		paddingBottom: 100,
		//backgroundColor: "red",
		justifyContent: "center",
		position: "relative",
	},
	pageTitle: {
		width: "100%",
		fontFamily: "bold",
		textAlign: "center",
		marginBottom: 50,
	},
	input: {
		width: "100%",
		// marginVertical: 25,
		marginBottom: 15,
	},
	buttonText: {
		color: "#0D65FB",
		// textAlign: "center",
		// width: "100%",
		fontSize: 16,
		fontFamily: "regular",
		marginTop: 30,
		marginHorizontal: 5,
	},
	button: {
		// textAlign: "center",
		// width: "100%",
		fontSize: 16,
		fontFamily: "regular",
		marginTop: 30,
	},
	googleLogo: {
		height: 21,
		width: 21,
	},
	signUpText: {
		fontFamily: "regular",
		paddingVertical: 5,
		fontSize: 16,
	},
	forgotText: {
		fontFamily: "light",
		//marginTop: 5,
		paddingVertical: 5,
		fontSize: 14,
		color: grey,
		alignSelf: "flex-start",
	},
	subtitle: {
		width: "100%",
		fontFamily: "light",
		textAlign: "center",
		marginBottom: 20,
		paddingHorizontal: 50,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	profile: state.profile,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	sendUserEmail,
	setLoading,
	customNoti,
	forgotPassword,
})(SignInCreatePassword);
