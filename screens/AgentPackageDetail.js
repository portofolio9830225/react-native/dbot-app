import React from "react";
import { connect } from "react-redux";
import isEmpty from "../utils/isEmpty";
import * as WebBrowser from "expo-web-browser";

import { black, greyblack, greylight, greywhite, transparent, whatsappColor } from "../utils/ColorPicker";

import { AntDesign, MaterialCommunityIcons } from "@expo/vector-icons";
import { Image, StyleSheet, Text, View, ScrollView, Platform } from "react-native";
import { LayoutView } from "../component/Layout";
import NavHeader from "../component/NavHeader";

import PageTitle from "../component/PageTitle";
import langSelector from "../utils/langSelector";
import { Button, Dialog, List, Portal, TouchableRipple } from "react-native-paper";
import { customNoti } from "../store/actions/alertAction";
import { createAgentOrder, getAgentItem, getSingleAgentPackage } from "../store/actions/agentAction";

class AgentPackageDetail extends React.Component {
	state = {
		payModal: false,
	};

	componentDidMount() {
		this.props.getSingleAgentPackage(this.props.navigation.state.params.package_id, this.props.auth.token);
	}
	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.alert.redirect) && this.props.alert.redirect === "createAgentOrder") {
			this.props.navigation.goBack();
		}
	}

	componentWillUnmount() {
		this.props.getSingleAgentPackage();
	}

	handlePage =
		(route, params = {}) =>
		() => {
			this.props.navigation.push(route, params);
		};

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handleOption = state => () => {
		this.setState({
			payModal: state,
		});
	};

	handleContact = () => {
		let itemArr = "";
		this.props.agent.packageDetail.items.map((e, i) => {
			itemArr += `*${i + 1}. ${e.item_name} (x${e.quantity})*\n`;
		});

		let text = `[StoreUp Agent - *${this.props.agent.packageDetail.package_name} Package*]\n\nHi ${this.props.agent.hqList[0].owner.business_name}, I'm ${this.props.profile.store.name}, your agent.\n\nI would like to puchase these products, under ${this.props.agent.packageDetail.package_name} Package:-\n${itemArr}\n\n And I would like to know more about it.`;

		WebBrowser.openBrowserAsync(
			`https://wa.me/60${this.props.agent.hqList[0].owner.phone}?text=${encodeURIComponent(text)}`
		);
	};

	handleSubmit = type => () => {
		this.handleOption(false)();
		let data = {
			type,
			package_id: this.props.navigation.state.params.package_id,
		};
		this.props.createAgentOrder(this.props.agent.hqList[0].business_id, data, this.props.auth.token);
	};

	render() {
		const { payModal } = this.state;
		const { packageDetail } = this.props.agent;
		const lang = langSelector.AgentPackageDetail.find(e => e.lang === this.props.preference.lang).data;

		const Divider = props => (
			<View
				style={{
					width: props.width ? props.width : "100%",
					borderBottomColor: greylight,
					borderBottomWidth: 0.5,
					alignSelf: "center",
				}}
			/>
		);

		const ActionList = props => (
			<List.Item
				rippleColor={transparent}
				title={props.title}
				description={props.description}
				style={{ width: "100%" }}
				titleStyle={{
					color: !isEmpty(props.color) ? props.color : black,
					fontFamily: "medium",
					fontSize: 16,
				}}
				descriptionStyle={{
					color: !isEmpty(props.color) ? props.color : black,
					fontFamily: "light",
				}}
				onPress={props.onPress}
			/>
		);

		const ItemBox = ({ id, name, img, count, price, total, isFirst }) => {
			return (
				<TouchableRipple
					rippleColor={transparent}
					onPress={this.handlePage("AgentItemPage", {
						id,
						isAgent: true,
						viewOnly: true,
					})}
				>
					<View style={styles.itemBox}>
						<View style={styles.itemImg}>
							<Image
								source={{ uri: img }}
								style={{
									width: "100%",
									height: undefined,
									aspectRatio: 1,
								}}
							/>
						</View>
						<View style={styles.itemBoxBreakdown}>
							<Text style={styles.itemName} ellipsizeMode="tail">
								{name}
							</Text>
							<View
								style={{
									flexDirection: "row",
									justifyContent: "space-between",
									marginTop: 10,
								}}
							>
								<Text style={styles.itemQuantity}>{`x ${count}`}</Text>
								<Text style={styles.itemPrice}>{`RM${price.toFixed(2)}`} </Text>
							</View>
						</View>
					</View>
				</TouchableRipple>
			);
		};

		const PriceList = ({ title, text, end }) => {
			return (
				<View style={styles.priceList}>
					<Text style={styles.priceListTitle}>{title}</Text>

					<Text
						style={[styles.priceListText, { fontFamily: end ? "bold" : "medium", fontSize: end ? 25 : 15 }]}
					>
						{text}
					</Text>
				</View>
			);
		};

		return (
			<LayoutView>
				<NavHeader onBack={this.handleBack} />

				<Portal>
					<Dialog visible={payModal} onDismiss={this.handleOption(false)}>
						<Dialog.Title>{lang.option}</Dialog.Title>
						<Divider width="100%" />
						<View>
							<ActionList
								title={lang.codTitle}
								description={lang.codDesc}
								onPress={this.handleSubmit(1)}
								color={greyblack}
							/>
							<ActionList
								title={lang.onlineTitle}
								description={lang.onlineDesc}
								onPress={this.handleSubmit(2)}
								color={greyblack}
							/>
						</View>
					</Dialog>
				</Portal>

				{!isEmpty(packageDetail) && (
					<View style={styles.root}>
						<ScrollView
							showsVerticalScrollIndicator={false}
							contentContainerStyle={{
								width: "100%",
							}}
						>
							<PageTitle>{packageDetail.package_name}</PageTitle>

							<View style={styles.orderContent}>
								<View style={styles.sectionBox}>
									<Text style={[styles.sectionTitle, { marginBottom: 13 }]}>{lang.product}</Text>

									{packageDetail.items.map((e, i) => {
										return (
											<ItemBox
												key={i}
												id={e.item_id}
												name={e.item_name}
												img={e.image_url}
												count={e.quantity}
												price={e.unit_price}
											/>
										);
									})}
								</View>
								<View style={styles.totalBox}>
									<View style={[styles.priceList, { marginTop: 25, marginBottom: 0 }]}>
										<Text
											style={[
												styles.priceListTitle,
												{
													fontFamily: "bold",
												},
											]}
										>
											{lang.total}
										</Text>

										<Text
											style={styles.priceListText}
											style={{
												fontFamily: "bold",
												fontSize: 30,
												//lineHeight: 33,
											}}
										>
											RM
											{packageDetail.package_price.toFixed(2)}
										</Text>
									</View>
								</View>
								{/* <PriceList
                  end
                  title={lang.total}
                  text={`RM${packageDetail.package_price.toFixed(2)}`}
                /> */}
							</View>
						</ScrollView>
						<View
							style={{
								flexDirection: "row",
								justifyContent: "space-between",
								alignItems: "center",
								width: "100%",
								alignSelf: "center",
								paddingTop: 10,
								paddingBottom: Platform.OS === "ios" ? 30 : 10,
							}}
						>
							<TouchableRipple
								onPress={this.handleContact}
								rippleColor="transparent"
								style={{
									alignItems: "center",
									justifyContent: "center",
									width: "18%",
									borderColor: whatsappColor,
									borderWidth: 0.5,
									height: 50,
									borderRadius: 7,
								}}
							>
								<MaterialCommunityIcons name="whatsapp" size={35} color={whatsappColor} />
							</TouchableRipple>
							{/* </Button> */}
							<Button
								mode="contained"
								labelStyle={{ color: "white" }}
								color={black}
								onPress={this.handleOption(true)}
								style={[styles.staticButton]}
								contentStyle={{
									height: 50,
								}}
							>
								{lang.submit}
							</Button>
						</View>
					</View>
				)}
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	root: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		position: "relative",
	},
	businessCard: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-evenly",
		alignItems: "center",
		marginVertical: 17,
		width: "100%",
	},

	businessLogo: {
		width: 80,
		height: 80,
		borderRadius: 40,
	},
	businessDetails: {
		display: "flex",
		flexDirection: "column",
	},
	businessName: {
		fontSize: 17,
		fontFamily: "bold",
		textTransform: "uppercase",
		marginBottom: 7,
	},
	businessRegName: {
		fontSize: 14,
	},
	businessEmail: {
		fontSize: 14,
		fontFamily: "regular",
	},
	businessPhone: {
		fontSize: 14,
		fontFamily: "regular",
	},
	orderContent: {
		//width: "100%",
		//paddingVertical: 25,
		marginTop: 17,
	},

	headerTitle: {
		fontSize: 12,
		fontFamily: "regular",
		marginBottom: 10,
	},
	iconTextText: {
		color: "black",
		fontSize: 14,
		fontFamily: "light",
	},

	sectionTitle: {
		fontSize: 15,
		fontFamily: "bold",
		textTransform: "uppercase",
	},
	sectionBox: {
		//width: "100%",
		display: "flex",
		flexDirection: "column",
		// borderBottomWidth: 1,
		// borderBottomColor: greywhite,
		paddingVertical: 14,
	},
	itemBox: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingVertical: 10,
		//width: "100%",
	},
	itemBoxHeader: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		width: "100%",
	},
	itemBoxBreakdown: {
		display: "flex",
		flexDirection: "column",
		width: "70%",
	},

	itemImg: {
		width: "25%",
		height: undefined,
		aspectRatio: 1,
		borderRadius: 10,
		borderWidth: 0.5,
		borderColor: greywhite,
		overflow: "hidden",
	},
	itemName: {
		fontSize: 16,
		fontFamily: "light",
	},
	itemPrice: {
		fontSize: 15,
		fontFamily: "regular",
		color: greyblack,
		lineHeight: 17,
	},
	itemQuantity: {
		fontSize: 15,
		fontFamily: "bold",
	},
	priceList: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "flex-start",
		marginBottom: 5,
	},
	priceListTitle: {
		// width: "53%",
		fontSize: 15,
		fontFamily: "light",
	},
	priceListText: {
		// width: "45%",
		textAlign: "right",
	},
	totalBox: {
		width: "100%",
		display: "flex",
		flexDirection: "column",
		marginTop: 14,
		paddingTop: 18,
	},
	viewText: {
		fontFamily: "medium",
		paddingTop: 5,
		paddingBottom: 3,
	},
	viewButton: {
		marginTop: 30,
	},
	staticButton: {
		width: "80%",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	profile: state.profile,
	agent: state.agent,
	alert: state.alert,
	loading: state.loading,
});

export default connect(mapStateToProps, {
	getAgentItem,
	getSingleAgentPackage,
	createAgentOrder,
	customNoti,
})(AgentPackageDetail);
