import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { connect } from "react-redux";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import NavHeader from "../component/NavHeader";
import { greywhite, grey, black, greyblack } from "../utils/ColorPicker";
import isEmpty from "../utils/isEmpty";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import handleCopyText from "../utils/handleCopyText";
import { SectionBox, SectionDivider } from "../component/SectionComponents";

class OrderCustomer extends React.Component {
	handleBack = () => {
		this.props.navigation.goBack();
	};

	render() {
		const { orderDetails, orderShipping } = this.props.order;
		const { customer } = orderDetails;

		const TitleDesc = ({ title, desc, enableCopy }) => {
			return (
				<TouchableWithoutFeedback onLongPress={enableCopy ? handleCopyText(desc, title) : null}>
					<View style={{ width: "100%" }}>
						<View
							style={{
								flexDirection: "row",
								justifyContent: "space-between",
								width: "100%",
								paddingVertical: 15,
							}}
						>
							<Text style={{ fontFamily: "regular", color: greyblack, width: "45%" }}>{title}</Text>
							<Text style={{ fontFamily: "regular", color: black, width: "50%", textAlign: "right" }}>
								{desc}
							</Text>
						</View>
					</View>
				</TouchableWithoutFeedback>
			);
		};

		return (
			<LayoutView>
				<NavHeader onBack={this.handleBack} title="Customer Details" />

				<View style={styles.container}>
					<SectionBox>
						<TitleDesc enableCopy title="Name" desc={customer.name} />
						<SectionDivider />
						<TitleDesc enableCopy title="Phone" desc={`+60${customer.phone}`} />
						<SectionDivider />
						<TitleDesc enableCopy title="Email" desc={customer.email} />
						{!isEmpty(orderShipping) ? (
							<>
								<SectionDivider />
								<TitleDesc
									enableCopy
									title="Address"
									desc={`${orderShipping.address_1} ${orderShipping.address_2 || ""} ${
										orderShipping.address_3 || ""
									} ${orderShipping.address_4 || ""}, ${orderShipping.postcode} ${
										orderShipping.city
									}, ${orderShipping.state}, ${orderShipping.country}`}
								/>
							</>
						) : null}
					</SectionBox>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		// alignItems: "center"
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	order: state.order,
	profile: state.profile,
	alert: state.alert,
});

export default connect(mapStateToProps, {})(OrderCustomer);
