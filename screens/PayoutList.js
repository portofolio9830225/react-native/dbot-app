import React from "react";
import { StyleSheet, View, RefreshControl, FlatList } from "react-native";
import { connect } from "react-redux";

import { setRefresh } from "../store/actions/loadingAction";
import { getPayoutList } from "../store/actions/payoutAction";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import NavHeader from "../component/NavHeader";
import PayoutCard from "../component/PayoutCard";

import { black, greyblack, transparent } from "../utils/ColorPicker";

import EmptyText from "../component/EmptyText";
import langSelector from "../utils/langSelector";
import Loading from "../component/Loading";
import isEmpty from "../utils/isEmpty";

class PayoutList extends React.Component {
	state = {
		active: 1,
		search: "",
		statusCard: {},
	};

	getPayoutList = () => {
		this.props.getPayoutList(this.props.auth.token);
	};

	componentDidMount() {
		this.getPayoutList();
	}

	componentDidUpdate(prevProps, prevState) {
		if (!prevProps.loading.refresh && this.props.loading.refresh) {
			setTimeout(() => {
				this.getPayoutList();
			}, 1500);
		}
	}

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handlePage =
		(page, params = {}) =>
		() => {
			this.props.navigation.push(page, params);
		};

	render() {
		const { list, isPayoutFetched } = this.props.payout;
		const lang = langSelector.PayoutList.find(e => e.lang === this.props.preference.lang).data;

		return (
			<LayoutView>
				<NavHeader onBack={this.handleBack} title="Payouts" />

				{isPayoutFetched ? (
					<FlatList
						refreshControl={
							<RefreshControl
								refreshing={this.props.loading.refresh}
								onRefresh={() => {
									this.props.setRefresh(true);
								}}
							/>
						}
						showsVerticalScrollIndicator={false}
						style={{ width: "100%", alignSelf: "center", position: "relative" }}
						contentContainerStyle={{
							width: "100%",
							paddingVertical: 25,
							flex: isEmpty(list) ? 1 : undefined,
						}}
						data={list}
						keyExtractor={e => e.id}
						ListEmptyComponent={
							<EmptyText
								title="No payouts"
								text="Payouts will be shown here when you make transactions via StoreUp Pay"
							/>
						}
						renderItem={({ item, index, separators }) => {
							return (
								<PayoutCard
									key={index}
									date={item.due_date}
									amount={item.total_payout_amount}
									onPress={this.handlePage("PayoutReport", item)}
								/>
							);
						}}
					/>
				) : (
					<Loading />
				)}
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
	rentalTabContainer: {
		width: "100%",
		flexDirection: "row",
		//justifyContent: "center",
		alignItems: "center",
	},
	rentalTabItem: {
		paddingTop: 10,
		//marginBottom: 15,
		justifyContent: "center",
		alignItems: "center",
		borderBottomWidth: 2,
	},
	rentalTabItemActive: {
		borderBottomColor: black,
	},
	rentalTabItemInactive: {
		borderBottomColor: transparent,
	},
	rentalTabTextContainer: {
		width: "100%",
		flexDirection: "column",
		alignItems: "center",
	},
	rentalTabText: {
		fontSize: 13,
	},
	indicator: {
		alignSelf: "flex-end",
		width: 8,
		height: undefined,
		aspectRatio: 1,
		borderRadius: 4,
		marginRight: 3,
	},
	rentalTabTextActive: {
		fontFamily: "bold",
	},
	rentalTabTextInactive: {
		fontFamily: "light",
		color: greyblack,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	payout: state.payout,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, { getPayoutList, setRefresh })(PayoutList);
