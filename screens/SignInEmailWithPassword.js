import { Text, View, StyleSheet, TextInput, TouchableWithoutFeedback } from "react-native";
import React from "react";

import { LayoutView } from "../component/Layout";
import { Ionicons } from "@expo/vector-icons";
import Typography from "../component/Typography";
import TextField from "../component/TextField";
import CustomButton from "../component/CustomButton";

export class SignInEmailWithPassword extends React.Component {
	state = {
		email: "",
		password: "",
	};

	componentDidMount() {
		console.log(this.props.navigation.state.params.email2);
		this.setState({
			// email: "saputraozhi@gmail.com",
			email: this.props.navigation.state.params.email2,
		});
	}

	handleChange = name => value => {
		this.setState({
			[name]: value,
		});
	};

	handlePageForgotPassword = () => {
		this.props.navigation.navigate("ForgotPassword");
	};

	render() {
		const { email, password } = this.state;

		return (
			<LayoutView>
				<View style={styles.container}>
					<Ionicons name="mail" size={86} color="#0D65FB" />
					<Typography type="largetitle" style={[styles.pageTitle, { marginBottom: 25 }]}>
						Login with Your StoreUp ID
					</Typography>
					<TextInput
						style={styles.input}
						onChangeText={this.handleChange("email")}
						value={email}
						placeholder="Email"
						keyboardType="email-address"
					/>
					<TextInput
						style={styles.input}
						onChangeText={this.handleChange("password")}
						value={password}
						placeholder="Password"
						keyboardType="default"
						secureTextEntry={true}
					/>
					<CustomButton style={styles.button}>Continue</CustomButton>
					<TouchableWithoutFeedback onPress={this.handlePageForgotPassword}>
						<Text style={[styles.buttonText, { marginBottom: 20 }]}>Forgot your password ?</Text>
					</TouchableWithoutFeedback>
					<View
						style={{
							width: "100%",
							justifyContent: "center",
							alignItems: "center",
							flexDirection: "row",
							fontSize: 16,
							// backgroundColor: "red",
						}}
					>
						<View>
							<Text style={{ fontSize: 16 }}>Don't have a StoreUp ID ? </Text>
						</View>
						<View>
							<TouchableWithoutFeedback>
								<Text style={styles.buttonText}>Register here</Text>
							</TouchableWithoutFeedback>
						</View>
					</View>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		paddingBottom: 100,
		//backgroundColor: "red",
		justifyContent: "center",
	},
	pageTitle: {
		width: "100%",
		fontFamily: "bold",
		textAlign: "center",
	},
	input: {
		width: "100%",
		backgroundColor: "#fff",
		paddingHorizontal: 16,
		height: 50,
		borderRadius: 10,
		marginBottom: 15,
		fontSize: 16,
	},
	button: {
		justifyContent: "center",
		width: "100%",
		marginBottom: 20,
	},
	buttonText: {
		color: "#0D67F1",
		textAlign: "center",
		width: "100%",
		fontSize: 16,
		fontFamily: "regular",
	},
});

export default SignInEmailWithPassword;
