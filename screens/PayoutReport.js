import React from "react";
import { StyleSheet, View, Text, Animated, Easing, RefreshControl, FlatList } from "react-native";
import { connect } from "react-redux";

import { setRefresh } from "../store/actions/loadingAction";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import NavHeader from "../component/NavHeader";
import DateConverter from "../utils/DateConverter";
import { black, grey, greyblack, greylight, transparent } from "../utils/ColorPicker";

import EmptyText from "../component/EmptyText";
import langSelector from "../utils/langSelector";
import { getPayoutList } from "../store/actions/payoutAction";
import PayoutOrderCard from "../component/PayoutOrderCard";

class PayoutList extends React.Component {
	constructor(props) {
		super(props);
		this.transValue = new Animated.Value(1);
	}

	state = {
		active: 1,
		search: "",
		statusCard: {},
	};

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handlePage =
		(page, params = {}) =>
		() => {
			this.props.navigation.push(page, params);
		};

	render() {
		const { orders, total_payout_amount, due_date, completed_at, total_sale_amount } =
			this.props.navigation.state.params;
		const lang = langSelector.PayoutReport.find(e => e.lang === this.props.preference.lang).data;

		const TotalHeader = ({ title, price }) => {
			return (
				<View style={{ width: "46%" }}>
					<Text style={{ fontFamily: "light", marginBottom: 3 }}>{title}</Text>
					<Text style={{ fontFamily: "medium", fontSize: 26 }}>RM{price.toFixed(2)}</Text>
				</View>
			);
		};

		return (
			<LayoutView>
				<NavHeader onBack={this.handleBack} />

				<FlatList
					showsVerticalScrollIndicator={false}
					style={{ width: "100%", alignSelf: "center", position: "relative" }}
					contentContainerStyle={{
						width: "100%",
						paddingVertical: 15,
						flex: 1,
					}}
					ListHeaderComponent={
						<View
							style={{
								width: "100%",
								alignItems: "flex-start",
								alignSelf: "center",
								marginBottom: 25,
							}}
						>
							<PageTitle>{lang.title}</PageTitle>
							<Text style={{ fontFamily: "light", color: greyblack, marginTop: 8 }}>
								{lang.payoutDate}: {completed_at}
							</Text>
							<View
								style={{
									width: "100%",
									alignItems: "center",
									marginTop: 30,
									marginBottom: 10,
									paddingVertical: 15,
									borderBottomColor: greylight,
									borderBottomWidth: 0.7,
									borderTopColor: greylight,
									borderTopWidth: 0.7,
									flexDirection: "row",
									justifyContent: "space-between",
								}}
							>
								<TotalHeader title={lang.salesAmount} price={total_sale_amount} />
								<View
									style={{
										borderRightColor: greylight,
										borderRightWidth: 1,
										height: "100%",
									}}
								/>
								<TotalHeader title={lang.payoutAmount} price={total_payout_amount} />
							</View>
						</View>
					}
					data={orders}
					keyExtractor={e => e.order_id}
					ListEmptyComponent={<EmptyText title="No payouts" />}
					renderItem={({ item, index, separators }) => {
						return (
							<PayoutOrderCard
								key={index}
								id={item.ref}
								date={item.paid_at}
								amount={item.sale_amount}
								onPress={this.handlePage("OrderDetail", {
									oID: item.ref,
								})}
							/>
						);
					}}
				/>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
	rentalTabContainer: {
		width: "100%",
		flexDirection: "row",
		//justifyContent: "center",
		alignItems: "center",
	},
	rentalTabItem: {
		paddingTop: 10,
		//marginBottom: 15,
		justifyContent: "center",
		alignItems: "center",
		borderBottomWidth: 2,
	},
	rentalTabItemActive: {
		borderBottomColor: black,
	},
	rentalTabItemInactive: {
		borderBottomColor: transparent,
	},
	rentalTabTextContainer: {
		width: "100%",
		flexDirection: "column",
		alignItems: "center",
	},
	rentalTabText: {
		fontSize: 13,
	},
	indicator: {
		alignSelf: "flex-end",
		width: 8,
		height: undefined,
		aspectRatio: 1,
		borderRadius: 4,
		marginRight: 3,
	},
	rentalTabTextActive: {
		fontFamily: "bold",
	},
	rentalTabTextInactive: {
		fontFamily: "light",
		color: greyblack,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	payout: state.payout,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, { getPayoutList, setRefresh })(PayoutList);
