import React from "react";
import { StyleSheet, View, Text, ScrollView, Platform } from "react-native";
import { connect } from "react-redux";

import { setLoading } from "../store/actions/loadingAction";
import { setLanguage } from "../store/actions/preferenceAction";

import { RadioButton, TouchableRipple, Button } from "react-native-paper";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import isEmpty from "../utils/isEmpty";
import NavHeader from "../component/NavHeader";
import { black, transparent } from "../utils/ColorPicker";
import langSelector from "../utils/langSelector";

class LanguageSet extends React.Component {
	state = {
		fromReg: this.props.navigation.state.params && !isEmpty(this.props.navigation.state.params.fromReg),
		language: this.props.preference.lang || "en",
	};

	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.alert.redirect) && this.props.alert.redirect === "LangSaved") {
			this.props.setLoading(false);
			this.handleBack();
		}
	}

	handleBack = () => {
		if (this.state.fromReg) {
			this.props.navigation.navigate("CompleteReg");
		} else {
			this.props.navigation.goBack();
		}
	};

	handleSelector = val => {
		this.setState({
			language: val,
		});
	};

	handleSelector2 = val => () => {
		this.setState({
			language: val,
		});
	};

	handleSubmit = () => {
		this.props.setLoading(true);
		this.props.setLanguage(this.state.language, !this.state.fromReg);
	};

	render() {
		const { language, fromReg } = this.state;
		const lang = langSelector.LanguageSet.find(e => e.lang === (this.props.preference.lang || "en")).data;

		const RadioButtonItem = props => {
			return (
				<TouchableRipple
					rippleColor={transparent}
					onPress={props.onPress}
					style={{
						width: "100%",
						flexDirection: "row",
						justifyContent: "space-between",
						alignItems: "center",
						paddingBottom: 5,
					}}
				>
					<>
						<Text
							style={{
								fontFamily: props.value === props.active ? "medium" : "regular",
								fontSize: 15,
								color: props.disabled ? "darkgrey" : "black",
							}}
						>
							{props.label}
						</Text>
						<RadioButton.Android value={props.value} color="black" disabled={props.disabled} />
					</>
				</TouchableRipple>
			);
		};

		return (
			<LayoutView>
				<NavHeader onBack={fromReg ? null : this.handleBack} />

				<View style={styles.container}>
					<ScrollView showsVerticalScrollIndicator={false} style={{ width: "100%" }}>
						<PageTitle>{lang.title}</PageTitle>
						<RadioButton.Group mode="android" onValueChange={this.handleSelector} value={language}>
							{lang.langChoice.map((e, i) => {
								return (
									<RadioButtonItem
										key={i}
										active={language}
										onPress={this.handleSelector2(e.value)}
										label={e.title}
										value={e.value}
									/>
								);
							})}
						</RadioButton.Group>
					</ScrollView>
				</View>

				<View
					style={{
						width: "100%",
						alignSelf: "center",
						paddingTop: 10,
						paddingBottom: Platform.OS === "ios" ? 30 : 10,
					}}
				>
					<Button
						mode="contained"
						color={black}
						onPress={this.handleSubmit}
						style={[styles.saveButton, { width: "100%" }]}
						contentStyle={{
							paddingHorizontal: 20,
							height: 50,
						}}
					>
						{lang.submitButton}
					</Button>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		// alignItems: "center"
	},

	saveButton: {
		marginTop: 20,
		alignSelf: "flex-end",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	alert: state.alert,
});

export default connect(mapStateToProps, {
	setLanguage,
	setLoading,
})(LanguageSet);
