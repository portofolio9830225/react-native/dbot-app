import React from "react";
import { StyleSheet, View, Text, RefreshControl, FlatList } from "react-native";
import { connect } from "react-redux";
import { setRefresh } from "../store/actions/loadingAction";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import EmptyText from "../component/EmptyText";
import NavHeader from "../component/NavHeader";

import { withNavigationFocus } from "react-navigation";
import langSelector from "../utils/langSelector";
import { MaterialCommunityIcons, MaterialIcons } from "@expo/vector-icons";
import { black, blue, green, greyblack, orange, red } from "../utils/ColorPicker";
import OrderBox from "../component/OrderBox";
import DateConverter from "../utils/DateConverter";
import { getAllAgentOrders } from "../store/actions/agentAction";
import Loading from "../component/Loading";

class AgentOrder extends React.Component {
	state = {
		order: [],
	};

	componentDidMount() {
		this.props.getAllAgentOrders(this.props.auth.token);
	}

	componentDidUpdate(prevProps, prevState) {
		if (!prevProps.loading.refresh && this.props.loading.refresh) {
			setTimeout(() => {
				this.props.getAllAgentOrders(this.props.auth.token);
			}, 1500);
		}
	}

	handleBack = () => {
		this.props.navigation.navigate("Settings");
	};

	handlePage =
		(route, params = {}) =>
		() => {
			this.props.navigation.push(route, params);
		};

	render() {
		const { orderList, isOrderFetched } = this.props.agent;
		const lang = langSelector.AgentOrder.find(e => e.lang === this.props.preference.lang).data;

		const StatusText = ({ status }) => {
			let color = greyblack;
			let text = lang.pending;
			let icon = "progress-clock";

			switch (status) {
				case 1:
					{
						text = lang.accepted;
						color = green;
						icon = "progress-check";
					}
					break;
				case 2:
					{
						text = lang.shipping;
						color = orange;
						icon = "truck-fast";
					}
					break;
				case 3:
					{
						text = lang.done;
						color = blue;
						icon = "check-circle";
					}
					break;
				case 8:
					{
						text = lang.canceled;
						color = red;
						icon = "block-helper";
					}
					break;
				case 9:
					{
						text = lang.rejected;
						color = red;
						icon = "close-circle-outline";
					}
					break;
			}

			return (
				<View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
					<MaterialCommunityIcons name={icon} color={color} />
					<Text
						style={{
							fontSize: 12,
							fontFamily: "medium",
							color: color,
							width: "100%",
							paddingLeft: 3,
						}}
					>
						{text}
					</Text>
				</View>
			);
		};

		return (
			<LayoutView>
				<NavHeader
					actions={[
						{
							onPress: this.handlePage("AgentOrderHistory"),
							icon: <MaterialIcons name="history" color={greyblack} size={30} />,
						},
					]}
				/>

				{isOrderFetched ? (
					<FlatList
						refreshControl={
							<RefreshControl
								refreshing={this.props.loading.refresh}
								onRefresh={() => {
									this.props.setRefresh(true);
								}}
							/>
						}
						showsVerticalScrollIndicator={false}
						style={{ width: "100%", alignSelf: "center", position: "relative" }}
						contentContainerStyle={{
							width: "100%",
							paddingVertical: 15,
						}}
						ListHeaderComponent={
							<View
								style={{
									width: "100%",
									alignItems: "flex-start",
									alignSelf: "center",
									marginBottom: 10,
								}}
							>
								<PageTitle>{lang.title}</PageTitle>
							</View>
						}
						data={orderList.filter(e => e.status < 3)}
						ListEmptyComponent={<EmptyText title={lang.emptyText} />}
						keyExtractor={e => e.order_id}
						renderItem={({ item, index, separators }) => {
							return (
								<OrderBox
									statusComponent={<StatusText status={item.status} />}
									statusType={2}
									onPress={this.handlePage("AgentOrderDetail", {
										oID: item.order_id,
									})}
									key={index}
									oTitle={`${lang.cardTitle}: ${item.order_id}`}
									id={item.id}
									nameIcon="archive-outline"
									name={item.package_name}
									date={DateConverter(item.created_ts)}
									price={item.total_price}

									// quantity={item.item_count}
									// status={item.status}
									// onPressStatus={() => {
									//   this.setState({
									//     statusCard: { id: item.order_id, status: item.status },
									//   });
									// }}
								/>
							);
						}}
					/>
				) : (
					<Loading />
				)}
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	agent: state.agent,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, { getAllAgentOrders, setRefresh })(withNavigationFocus(AgentOrder));
