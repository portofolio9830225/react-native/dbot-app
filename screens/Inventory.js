import React from "react";
import { StyleSheet, View, Text, Animated, Easing, RefreshControl, FlatList } from "react-native";

import { isTablet } from "react-native-device-detection";
import { connect } from "react-redux";

import { setRefresh } from "../store/actions/loadingAction";
import { getAllItems } from "../store/actions/itemAction";
import { editCategories, getAllCategory, getSingleCategory } from "../store/actions/categoryAction";

import { LayoutView } from "../component/Layout";
import NavHeader from "../component/NavHeader";
import PageTitle from "../component/PageTitle";
import ProductBox from "../component/ProductBox";
import EmptyText from "../component/EmptyText";
import { Button, TouchableRipple } from "react-native-paper";
import { MaterialCommunityIcons, Ionicons, Feather, MaterialIcons } from "@expo/vector-icons";
import {
	accent,
	black,
	greylight,
	green,
	greyblack,
	greywhite,
	orange,
	transparent,
	blue,
	greydark,
	mainBgColor,
	grey,
	white,
} from "../utils/ColorPicker";
import getWidth from "../utils/getWidth";
import langSelector from "../utils/langSelector";
import Loading from "../component/Loading";
import { NoProductIcon } from "../assets/VectorIcons";
import isEmpty from "../utils/isEmpty";
import SegmentedPicker from "../component/SegmentedPicker";
import CategoryBox from "../component/CategoryBox";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { SectionBox } from "../component/SectionComponents";
import InventoryFilter from "../component/InventoryFilter";
import Typography from "../component/Typography";
import EmptyList from "../component/EmptyText";
import DraggableFlatList, { ScaleDecorator, OpacityDecorator, ShadowDecorator } from "react-native-draggable-flatlist";

const PaddingH = 16;
class Inventory extends React.Component {
	constructor(props) {
		super(props);
		this.sortableViewRef = React.createRef();
	}

	state = {
		active: 1,
		isItemActive: false,
		filterArr: [],
		editMode: false,
		catList: [],
	};

	componentDidMount() {
		this.props.getAllItems(this.props.auth.token);
		this.props.getAllCategory(this.props.auth.token);
	}

	componentDidUpdate(prevProps, prevState) {
		// console.log(this.props.category);
		if (!prevProps.loading.refresh && this.props.loading.refresh) {
			setTimeout(() => {
				if (this.state.active === 1) {
					this.props.getAllItems(this.props.auth.token);
				} else if (this.state.active === 2) {
					this.props.getAllCategory(this.props.auth.token);
				}
			}, 1500);
		}
		if (this.props.category.isFetched && !prevProps.category.isFetched) {
			this.setState({
				catList: this.props.category.categories,
			});
		}
	}

	handleFilter = value => () => {
		this.handleItemActive(false)();
		this.setState({
			filterArr: value,
		});
	};

	handleActive = active => () => {
		this.setState({
			active,
		});
	};

	handleItemActive = active => () => {
		this.setState({
			isItemActive: active,
		});
	};

	handleItem =
		(id = null) =>
		() => {
			this.props.navigation.push("Product", { id });
		};

	handleCategory =
		(id = null) =>
		() => {
			this.props.navigation.push("Category", { id });
		};

	handleAdd =
		(id = null) =>
		() => {
			if (this.state.active === 1) {
				this.props.navigation.push("Product", { id });
			} else if (this.state.active === 2) {
				this.props.navigation.push("CategoryAdd", { id });
			}
		};

	filteredItem = (arr, key) => {
		let { filterArr } = this.state;
		if (!isEmpty(filterArr)) {
			let cArr = [];
			let condition = "";

			if (filterArr.findIndex(f => f === "Available") != -1) {
				cArr.push(`fil.${key} === true`);
			}
			if (filterArr.findIndex(f => f === "Unavailable") != -1) {
				cArr.push(`fil.${key} === false`);
			}

			cArr.map((e, i) => {
				let text = e;
				if (i !== 0) {
					text = ` || ${text}`;
				}
				condition += text;
			});

			return arr.filter(fil => eval(condition));
		} else {
			return arr;
		}
	};

	handleSort = state => () => {
		const prevEditMode = this.state.editMode;
		const { active } = this.state;
		const isProduct = active == 1;
		const isCategory = active == 2;

		if (isCategory) {
			if (prevEditMode && !this.state.prevEditMode) {
				this.setState({
					catList: this.props.category.categories,
				});
			}
		}

		this.setState({
			editMode: state,
		});
	};

	handleSubmitSortCategory = () => {
		let category = [];
		this.state.catList.map((c, i) => {
			category.push({
				category_id: c.id,
				number: i + 1,
			});
		});
		let data = {
			category,
		};
		this.props.editCategories(data, this.props.auth.token);
		this.setState({
			editMode: false,
		});
	};

	tabWidth = getWidth;

	render() {
		const { active, isItemActive, filterArr, editMode, catList } = this.state;
		const { categories } = this.props.category;
		const isProduct = active == 1;
		const isCategory = active == 2;
		const { userItem, isItemFetched } = this.props.item;
		const isListEmpty =
			(isProduct && isEmpty(this.filteredItem(userItem, "active"))) ||
			(isCategory && isEmpty(this.filteredItem(catList, "active")));
		const lang = langSelector.Inventory.find(e => e.lang === this.props.preference.lang).data;
		const numColumns = isProduct ? (isTablet ? 3 : 2) : 1;

		const EmptyTextContainer = props => {
			return (
				<View style={{ flex: 1 }}>
					<EmptyText {...props} />
				</View>
			);
		};

		return (
			<LayoutView noPadding noLoading>
				<View style={{ paddingHorizontal: PaddingH }}>
					<NavHeader
						onBack={active === 2 && editMode ? this.handleSort(false) : null}
						backText="Cancel"
						actionText={
							active === 2
								? !isEmpty(catList) && isEmpty(filterArr)
									? editMode
										? "Done"
										: "Edit"
									: ""
								: ""
						}
						onPress={
							active === 2
								? !isEmpty(catList) && isEmpty(filterArr)
									? editMode
										? this.handleSubmitSortCategory
										: this.handleSort(true)
									: null
								: null
						}
					/>
				</View>
				<View style={styles.container}>
					<View
						style={{
							width: "100%",
							alignItems: "flex-start",
							alignSelf: "center",
							paddingHorizontal: PaddingH,
						}}
					>
						<PageTitle>Catalogue</PageTitle>
						<SegmentedPicker
							active={this.state.active}
							style={{ marginTop: 8, marginBottom: 0 }}
							list={[
								{ text: "Products", value: 1, onPress: editMode ? null : this.handleActive(1) },
								{ text: "Categories", value: 2, onPress: editMode ? null : this.handleActive(2) },
							]}
						/>
					</View>
					{isListEmpty ? (
						<View style={{ flex: 1, paddingBottom: 100, paddingHorizontal: PaddingH }}>
							<EmptyTextContainer
								title={isProduct ? "No Products" : isCategory ? "No Categories" : null}
								text={`There is no ${
									isProduct ? "product" : isCategory ? "category" : null
								} related to the entered information`}
							/>
						</View>
					) : (
						<DraggableFlatList
							refreshControl={
								editMode ? null : (
									<RefreshControl
										refreshing={this.props.loading.refresh}
										onRefresh={() => {
											this.props.setRefresh(true);
										}}
									/>
								)
							}
							contentContainerStyle={[
								{
									width: "100%",
									paddingBottom: 150,
								},
								isCategory ? { paddingTop: 4 } : {},
								isProduct ? { paddingHorizontal: PaddingH } : {},
							]}
							showsVerticalScrollIndicator={false}
							showsHorizontalScrollIndicator={false}
							data={
								isProduct
									? this.filteredItem(userItem, "active")
									: isCategory
									? this.filteredItem(catList, "active")
									: []
							}
							onDragEnd={({ data }) => {
								if (isCategory) {
									this.setState({
										catList: data,
									});
								}
							}}
							key={isProduct ? "p" : isCategory ? "c" : undefined}
							keyExtractor={item => item.id}
							numColumns={numColumns}
							renderItem={({ item, index, drag, isActive }) => {
								if (isProduct) {
									return (
										<ProductBox
											key={index}
											index={index}
											isFirst={index === 0}
											image={isEmpty(item.images) ? null : item.images[0].url_m}
											imageKey={
												isEmpty(item.images) ? null : `${item.images[0].file.imagekit_id}-m`
											}
											title={item.name}
											price={item.price}
											another_price={item.another_price}
											onPress={this.handleItem(item.ref)}
										/>
									);
								}
								if (isCategory) {
									return (
										<ScaleDecorator key={index}>
											<CategoryBox
												isFirst={index === 0}
												editMode={editMode}
												name={item.name}
												onDrag={editMode ? drag : null}
												onPress={editMode ? null : this.handleCategory(item.id)}
												isSortActive={isActive}
											/>
										</ScaleDecorator>
									);
								}
							}}
						/>
					)}
				</View>

				{isItemActive ? (
					<InventoryFilter
						onCancel={this.handleItemActive(false)}
						onSubmit={this.handleFilter}
						selected={filterArr}
					/>
				) : null}

				{!editMode ? (
					<View
						style={{
							position: "absolute",
							bottom: 0,
							height: 50,
							borderTopColor: greywhite,
							borderTopWidth: 1,
							width: getWidth + 32,
							display: "flex",
							justifyContent: "space-between",
							flexDirection: "row",
							alignItems: "center",
							paddingHorizontal: 24,
							backgroundColor: mainBgColor,
						}}
					>
						<TouchableWithoutFeedback onPress={this.handleItemActive(true)}>
							<View style={styles.circleIcon}>
								<MaterialIcons name="filter-list" size={19} color={accent} />
							</View>
						</TouchableWithoutFeedback>
						{!isEmpty(filterArr) ? (
							<View
								style={{
									alignItems: "center",
									justifyContent: "center",
									flex: 1,
									paddingHorizontal: 12,
								}}
							>
								<Typography type="caption">Filter by:</Typography>
								<View style={{ flexDirection: "row" }}>
									{filterArr.map((f, i) => {
										let text = f;
										if (i !== 0) {
											text = `, ${text}`;
										}
										return (
											<Typography type="caption" style={{ color: accent }}>
												{text}
											</Typography>
										);
									})}
								</View>
							</View>
						) : null}
						<TouchableWithoutFeedback onPress={this.handleAdd()}>
							<View style={styles.circleIcon}>
								<Feather name="plus" size={19} color={accent} />
							</View>
						</TouchableWithoutFeedback>
					</View>
				) : null}
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		width: "100%",
		flex: 1,
	},
	circleIcon: {
		width: 24,
		height: 24,
		borderRadius: 12,
		borderColor: accent,
		borderWidth: 1,
		alignItems: "center",
		justifyContent: "center",
		// backgroundColor: "green",
	},
	orderStatusBox: {
		width: getWidth * 0.49,
		borderRadius: 15,
		alignItems: "center",
		justifyContent: "center",
		paddingVertical: 10,
	},
	orderStatusText: {
		fontSize: 11,
		textAlign: "center",
		textTransform: "capitalize",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	profile: state.profile,
	preference: state.preference,
	item: state.item,
	loading: state.loading,
	error: state.error,
	category: state.category,
});

export default connect(mapStateToProps, { getAllItems, setRefresh, getAllCategory, getSingleCategory, editCategories })(
	Inventory
);
