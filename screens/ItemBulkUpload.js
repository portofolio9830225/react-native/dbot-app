import React from "react";
import { StyleSheet, View, Text, ScrollView, Platform } from "react-native";
import { connect } from "react-redux";

import { requestItemBulk } from "../store/actions/itemAction";
import { setLoading } from "../store/actions/loadingAction";

import { Button } from "react-native-paper";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import isEmpty from "../utils/isEmpty";
import NavHeader from "../component/NavHeader";
import { black, greyblack, transparent } from "../utils/ColorPicker";
import langSelector from "../utils/langSelector";

class ItemBulkUpload extends React.Component {
	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.alert.redirect) && this.props.alert.redirect === "ItemBulkUpload") {
			this.props.setLoading(false);
			this.handleBack();
		}
	}

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handleSubmit = () => {
		this.props.setLoading(true);

		this.props.requestItemBulk(this.props.auth.token);
	};

	render() {
		const lang = langSelector.ItemBulkUpload.find(e => e.lang === (this.props.preference.lang || "en")).data;

		return (
			<LayoutView>
				<NavHeader onBack={this.handleBack} />

				<View style={styles.container}>
					<PageTitle>{lang.title}</PageTitle>
					<Text style={{ color: greyblack, fontFamily: "light", fontSize: 15 }}>{lang.desc}</Text>
				</View>

				<View
					style={{
						width: "100%",
						alignSelf: "center",
						paddingTop: 10,
						paddingBottom: Platform.OS === "ios" ? 30 : 10,
					}}
				>
					<Button
						mode="contained"
						color={black}
						onPress={this.handleSubmit}
						style={[styles.saveButton, { width: "100%" }]}
						contentStyle={{
							paddingHorizontal: 20,
							height: 50,
						}}
					>
						{lang.submitButton}
					</Button>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		// alignItems: "center"
	},

	saveButton: {
		marginTop: 20,
		alignSelf: "flex-end",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	alert: state.alert,
});

export default connect(mapStateToProps, {
	requestItemBulk,
	setLoading,
})(ItemBulkUpload);
