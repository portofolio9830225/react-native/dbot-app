import React from "react";
import { StyleSheet, View, Text, ScrollView, Platform, Image, FlatList } from "react-native";
import { connect } from "react-redux";
import FadeInOut from "react-native-fade-in-out";

import { setLoading } from "../store/actions/loadingAction";
import { RadioButton, TouchableRipple, Button, Checkbox } from "react-native-paper";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import isEmpty from "../utils/isEmpty";
import NavHeader from "../component/NavHeader";
import { black, grey, greyblack, greywhite, transparent } from "../utils/ColorPicker";
import langSelector from "../utils/langSelector";
import OrderBox from "../component/OrderBox";
import EmptyText from "../component/EmptyText";
import DateConverter from "../utils/DateConverter";
import {
	createShippingOrder,
	getNoWeightOrder,
	getShippingOrderReview,
	setMultipleOrderStatus,
} from "../store/actions/orderAction";
import { Entypo, MaterialIcons } from "@expo/vector-icons";
import shipCourierList from "../utils/shipCourierList";
import { NoOrderIcon } from "../assets/VectorIcons";

let Fade = FadeInOut;

class OrderBulkShipping extends React.Component {
	state = {
		step: 1,
		stepShow: 1,
		courier: "",
		selectedOrders: [],
		itemNoWeight: [],
		orderReview: [],
		isPassReview: false,
	};

	componentDidMount() {
		if (!isEmpty(this.props.navigation.state.params)) {
			let { isSetWeight, others } = this.props.navigation.state.params;
			this.setState(
				{
					itemNoWeight: [],
					selectedOrders: others.selectedOrders,
					courier: others.courier,
					isPassReview: isSetWeight ? true : false,
				},
				() => {
					if (!isSetWeight) {
						this.handleStep(1);
					}
				}
			);
		}
	}

	async componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.alert.redirect) && this.props.alert.redirect === "createShippingOrder") {
			this.handleStep(4);
		}
		if (isEmpty(prevProps.alert.redirect) && this.props.alert.redirect === "setMultipleOrderStatus") {
			this.handleBack();
		}
		if (isEmpty(prevState.itemNoWeight) && !isEmpty(this.state.itemNoWeight)) {
			this.props.navigation.replace("SetItemWeight", {
				route: "OrderBulkShipping",
				items: this.state.itemNoWeight,
				others: {
					courier: this.state.courier,
					selectedOrders: this.state.selectedOrders,
				},
			});
		}
		if (!prevState.isPassReview && this.state.isPassReview) {
			this.handleStep(2);
		}
		if (prevState.step < 3 && this.state.step === 3) {
			let str = "";
			this.state.selectedOrders.map((e, i) => {
				str += `${i === 0 ? "" : ","}${e}`;
			});

			let arr = await this.props.getShippingOrderReview(str, this.props.auth.token);

			this.setState({
				orderReview: arr,
			});
		}
		if (prevState.step === 3 && this.state.step < 3) {
			this.setState({
				orderReview: [],
			});
		}
		if (prevState.step === 2 && this.state.step < 2) {
			this.setState({
				itemNoWeight: [],
				isPassReview: false,
			});
		}
	}

	handleStep = step => {
		this.setState(
			{
				step,
			},
			() => {
				this.setState({
					stepShow: step,
				});
			}
		);
	};

	handleBack = () => {
		if (this.state.step <= 1 || this.state.step > 3) {
			this.props.navigation.pop();
		} else {
			this.handleStep(this.state.step - 1);
		}
	};

	handleSelector = val => {
		this.setState({
			courier: val,
		});
	};

	handleSelector2 = val => () => {
		this.setState({
			courier: val,
		});
	};

	handleOrderBox = item => () => {
		let orders = [...this.state.selectedOrders];
		let found = orders.findIndex(e => e === item.order_id);

		if (found === -1) {
			orders.push(item.order_id);
		} else {
			orders.splice(found, 1);
		}

		this.setState({
			selectedOrders: orders,
		});
	};

	handleSelectAll = isAll => () => {
		let arr = [];
		if (!isAll) {
			this.props.order.orderList.paid.map(e => {
				arr.push(e.order_id);
			});
		}
		this.setState({
			selectedOrders: arr,
		});
	};

	handleDisabledSubmit = step => {
		switch (step) {
			case 1:
				return isEmpty(this.state.selectedOrders);
			case 2:
				return isEmpty(this.state.courier);
		}
	};

	handleSubmit = async () => {
		let { step } = this.state;
		switch (step) {
			case 1:
				{
					let str = "";
					this.state.selectedOrders.map((e, i) => {
						str += `${i === 0 ? "" : ","}${e}`;
					});
					let arr = await this.props.getNoWeightOrder(str, this.props.auth.token);
					this.setState({
						itemNoWeight: arr,
						isPassReview: isEmpty(arr),
					});
				}
				break;
			case 2:
				{
					this.handleStep(3);
				}
				break;
			case 3:
				{
					let { courier, selectedOrders } = this.state;
					let data = {
						method: courier,
						orders: selectedOrders,
					};

					this.props.createShippingOrder(data, this.props.auth.token);
				}
				break;
			case 4: {
				let data = {
					status: 3,
					order_id: this.state.selectedOrders,
				};
				this.props.setMultipleOrderStatus(data, this.props.auth.token);
			}
		}
	};

	render() {
		const { step, stepShow, courier, selectedOrders, orderReview } = this.state;
		const { orderList } = this.props.order;
		const { paid } = orderList;
		const isAllSelected = selectedOrders.length === paid.length;
		const isMainFlow = step < 4;
		const lang = langSelector.OrderBulkShipping.find(e => e.lang === this.props.preference.lang).data;

		const RadioButtonItem = props => {
			let isSelected = props.value === props.active;
			return (
				<TouchableRipple
					rippleColor={transparent}
					onPress={props.disabled ? null : props.onPress}
					style={{
						width: "100%",
						flexDirection: "row",
						justifyContent: "space-between",
						alignItems: "center",
						paddingBottom: 10,
						marginBottom: 5,
					}}
				>
					<>
						<View style={{ flexDirection: "row", alignItems: "center" }}>
							<RadioButton.Android value={props.value} color="black" disabled={props.disabled} />
							<Text
								style={{
									fontSize: 15,
									paddingLeft: 5,
									fontFamily: isSelected ? "medium" : "regular",
									color: props.disabled ? "darkgrey" : "black",
								}}
							>
								{props.label}
							</Text>
						</View>

						<Image
							source={{ uri: props.img }}
							style={{
								opacity: isSelected ? 1 : props.disabled ? 0.2 : 0.5,
								width: 80,
								height: undefined,
								aspectRatio: 3,
							}}
						/>
					</>
				</TouchableRipple>
			);
		};

		const SelectOrderBox = props => {
			return (
				<TouchableRipple
					rippleColor={transparent}
					onPress={props.onPress}
					style={{
						width: "100%",
						alignSelf: "center",
					}}
				>
					<View
						style={{
							width: "100%",
							flexDirection: "row",
							alignItems: "center",
							justifyContent: "space-between",
						}}
					>
						<View style={{ marginBottom: 20 }}>
							<Checkbox.Android status={props.checked ? "checked" : "unchecked"} />
						</View>

						<OrderBox {...props} />
					</View>
				</TouchableRipple>
			);
		};

		const ItemBox = ({ name, img, count, weight }) => {
			return (
				<View style={styles.itemBox}>
					<View style={styles.itemImg}>
						<Image
							source={{ uri: img }}
							style={{
								width: "100%",
								height: undefined,
								aspectRatio: 1,
							}}
						/>
					</View>
					<View style={styles.itemBoxBreakdown}>
						<Text style={styles.itemName} ellipsizeMode="tail">
							{name}
						</Text>
						<View
							style={{
								flexDirection: "row",
								justifyContent: "space-between",
								marginTop: 10,
							}}
						>
							<Text style={styles.itemQuantity}>
								<Text style={{ fontFamily: "medium" }}>{`${count}x `}</Text>
								{`${weight}kg`}
							</Text>
						</View>
					</View>
				</View>
			);
		};

		return (
			<LayoutView>
				<NavHeader
					onBack={isMainFlow ? this.handleBack : null}
					actions={
						!isMainFlow
							? [
									{
										onPress: this.handleBack,
										icon: <MaterialIcons name="close" color={black} size={30} />,
									},
							  ]
							: []
					}
				/>

				<View style={styles.container}>
					{step === 1 && (
						<Fade visible={stepShow === 1}>
							<FlatList
								showsVerticalScrollIndicator={false}
								style={{ alignSelf: "center", width: "100%" }}
								contentContainerStyle={{
									width: "100%",
									flex: 1,
								}}
								ListHeaderComponent={
									<View
										style={{
											width: "100%",
											alignItems: "flex-start",
											alignSelf: "center",
										}}
									>
										<PageTitle>{lang.title1}</PageTitle>
										<Text style={styles.subtitle}>{lang.desc1}</Text>
										{!isEmpty(paid) && (
											<View
												style={{
													flexDirection: "row",
													alignItems: "center",
													marginBottom: 10,
												}}
											>
												<Checkbox.Android
													status={isAllSelected ? "checked" : "unchecked"}
													onPress={this.handleSelectAll(isAllSelected)}
												/>
												<Text style={{ fontFamily: "regular", marginLeft: 5 }}>
													{`${isAllSelected ? lang.deselectAll : lang.selectAll} (${
														paid.length
													})`}
												</Text>
											</View>
										)}
									</View>
								}
								data={paid}
								keyExtractor={e => e.order_id}
								ListEmptyComponent={
									<EmptyText image={<NoOrderIcon size={100} />} title={lang.emptyText} />
								}
								renderItem={({ item, index, separators }) => {
									return (
										<SelectOrderBox
											key={index}
											oTitle={`${lang.orderID}:`}
											id={item.order_id}
											name={item.customer_name}
											date={DateConverter(item.created_ts)}
											price={item.total_price}
											quantity={item.item_count}
											onPress={this.handleOrderBox(item)}
											checked={selectedOrders.findIndex(e => e === item.order_id) !== -1}
										/>
									);
								}}
							/>
						</Fade>
					)}
					{step === 2 && (
						<Fade visible={stepShow === 2}>
							<ScrollView
								showsVerticalScrollIndicator={false}
								style={{ width: "100%", alignSelf: "center" }}
							>
								<PageTitle>{lang.title2}</PageTitle>
								<Text style={styles.subtitle}>{lang.desc2}</Text>

								<RadioButton.Group mode="android" onValueChange={this.handleSelector} value={courier}>
									{shipCourierList.map((e, i) => {
										return (
											<RadioButtonItem
												key={i}
												active={courier}
												onPress={this.handleSelector2(e.val)}
												label={e.name}
												value={e.val}
												img={e.img}
											/>
										);
									})}
								</RadioButton.Group>
							</ScrollView>
						</Fade>
					)}

					{step === 3 && (
						<Fade visible={stepShow === 3}>
							<FlatList
								showsVerticalScrollIndicator={false}
								style={{ alignSelf: "center", width: "100%" }}
								contentContainerStyle={{
									width: "100%",
								}}
								ListHeaderComponent={
									<View
										style={{
											width: "100%",
											alignItems: "flex-start",
											alignSelf: "center",
										}}
									>
										<PageTitle>{lang.title3}</PageTitle>
									</View>
								}
								data={orderReview}
								keyExtractor={e => e.order_id}
								renderItem={({ item, index, separators }) => {
									let totalItem = item.items.length;
									return (
										<OrderBox
											key={index}
											oTitle={`${lang.orderID}:`}
											id={item.order_id}
											name={item.customer_name}
											date={DateConverter(item.created_ts)}
											price={item.total_price}
											expandHeader={
												<View
													style={{
														flexDirection: "row",
														alignItems: "center",
														alignSelf: "flex-end",
														marginBottom: 15,
													}}
												>
													<Text
														style={{
															fontFamily: "light",
															marginRight: 3,
															color: grey,
															fontSize: 12,
														}}
													>
														{totalItem} {lang.product}
														{this.props.preference.lang === "en" && totalItem > 1
															? "s"
															: ""}
													</Text>
													<Entypo name="chevron-thin-down" size={9} color={greyblack} />
												</View>
											}
											expandContent={
												<View>
													{item.items.map((e, i) => {
														return (
															<ItemBox
																key={i}
																name={e.item_name}
																img={e.item_image}
																count={e.quantity}
																weight={e.item_weight}
															/>
														);
													})}
												</View>
											}
											//onPress={this.handleOrderBox(item)}
										/>
									);
								}}
							/>
						</Fade>
					)}
					{step === 4 && (
						<Fade visible={stepShow === 4}>
							<View
								style={{
									width: "100%",
									alignSelf: "center",
									alignItems: "flex-start",
								}}
							>
								<PageTitle fontSize={29}>{lang.title4}</PageTitle>
								<Text style={[styles.subtitle, { fontSize: 17 }]}>{lang.desc4}</Text>
								<Button
									onPress={this.handleSubmit}
									mode="contained"
									color={black}
									style={styles.regButton}
									contentStyle={{ height: 50 }}
								>
									{lang.yes4}
								</Button>
								<Button
									onPress={this.handleBack}
									mode="outlined"
									color={black}
									style={styles.regButton}
									contentStyle={{ paddingHorizontal: 15, height: 50 }}
								>
									{lang.no4}
								</Button>
							</View>
						</Fade>
					)}
				</View>

				{isMainFlow && (
					<View
						style={{
							width: "100%",
							alignSelf: "center",
							paddingTop: 10,
							paddingBottom: Platform.OS === "ios" ? 30 : 10,
						}}
					>
						<Button
							mode="contained"
							disabled={this.handleDisabledSubmit(step)}
							color={black}
							onPress={this.handleSubmit}
							style={[styles.saveButton, { width: "100%" }]}
							contentStyle={{
								paddingHorizontal: 20,
								height: 50,
							}}
						>
							{step === 3 ? lang.request : lang.next}
						</Button>
					</View>
				)}
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		// alignItems: "center"
	},
	subtitle: {
		fontFamily: "regular",
		color: greyblack,
		marginBottom: 30,
	},
	itemBox: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingVertical: 10,
		//width: "100%",
	},
	itemBoxHeader: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		width: "100%",
	},
	itemBoxBreakdown: {
		display: "flex",
		flexDirection: "column",
		width: "70%",
	},

	itemImg: {
		width: "25%",
		height: undefined,
		aspectRatio: 1,
		borderRadius: 10,
		borderWidth: 0.5,
		borderColor: greywhite,
		overflow: "hidden",
	},
	itemName: {
		fontSize: 17,
		fontFamily: "bold",
		letterSpacing: 0.5,
	},
	itemPrice: {
		fontSize: 15,
		fontFamily: "regular",
		color: greyblack,
		lineHeight: 17,
	},
	itemQuantity: {
		color: greyblack,
		// fontSize: 15,
		fontFamily: "light",
	},
	regButton: {
		marginTop: 20,
	},
	saveButton: {
		marginTop: 20,
		alignSelf: "flex-end",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	order: state.order,
	alert: state.alert,
});

export default connect(mapStateToProps, {
	getNoWeightOrder,
	getShippingOrderReview,
	createShippingOrder,
	setMultipleOrderStatus,
	setLoading,
})(OrderBulkShipping);
