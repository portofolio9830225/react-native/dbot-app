import React from "react";
import { StyleSheet, View, Text, Platform, FlatList } from "react-native";
import { connect } from "react-redux";
import * as WebBrowser from "expo-web-browser";
import FadeInOut from "react-native-fade-in-out";
import { MaterialIcons } from "@expo/vector-icons";

import { TouchableRipple, Button, Checkbox } from "react-native-paper";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import isEmpty from "../utils/isEmpty";
import NavHeader from "../component/NavHeader";
import { black, greyblack, greywhite, transparent } from "../utils/ColorPicker";
import BankList from "../utils/BankList";
import langSelector from "../utils/langSelector";
import CheckBox from "../component/CheckBox";
import { SectionBox, SectionDivider } from "../component/SectionComponents";

let Fade = FadeInOut;

class OrderContact extends React.Component {
	state = {
		availableInfo: langSelector.OrderContact.find(e => e.lang === this.props.preference.lang).data.infoList,
		//	selectedInfo: [1, 2, 3],
		selectedInfo: [1, 2],
		isCustomer: null,
		paymentMethod: null,
	};

	componentDidMount() {
		let { orderPayment, orderDetails } = this.props.order;

		this.setState(
			{
				paymentMethod: !isEmpty(orderPayment) ? orderPayment.payment_method_id : this.state.paymentMethod,
				isCustomer: orderDetails.status === "paid" || orderDetails.status === "completed",
			},
			() => {
				let isShowDirectTransfer = !this.state.isCustomer && this.state.paymentMethod == 2;
				let pList = langSelector.OrderContact.find(e => e.lang === this.props.preference.lang).data.infoList;
				let noPayment = isShowDirectTransfer && this.props.profile.payout.verified != 1;

				this.setState({
					availableInfo: pList.filter(e => {
						if (noPayment) {
							return e.id !== 2;
						} else {
							return true;
						}
					}),
					//selectedInfo: noPayment ? [1, 3] : [1, 2, 3],
					selectedInfo: noPayment ? [1] : [1, 2],
				});
			}
		);
	}

	handleBack = () => {
		this.props.navigation.pop();
	};

	handleOrderBox = id => () => {
		let infos = [...this.state.selectedInfo];
		let found = infos.findIndex(e => e === id);

		if (found === -1) {
			infos.push(id);
		} else {
			infos.splice(found, 1);
		}

		this.setState({
			selectedInfo: infos,
		});
	};

	handleSelectAll = isAll => () => {
		let arr = [];
		if (!isAll) {
			this.state.availableInfo.map(e => {
				arr.push(e.id);
			});
		}
		this.setState({
			selectedInfo: arr,
		});
	};

	handleSubmit = () => {
		let { selectedInfo, isCustomer, paymentMethod } = this.state;
		let { orderDetails, orderShipping, orderItems } = this.props.order;
		let { payout } = this.props.profile;

		let isProduct = !isEmpty(selectedInfo.find(e => e === 1));
		let isPayment = !isEmpty(selectedInfo.find(e => e === 2));
		let isTracking = !isEmpty(selectedInfo.find(e => e === 3));

		let itemArr = "";
		if (isProduct) {
			orderItems.map((e, i) => {
				itemArr += `*${i + 1}. ${e.name} (x${e.quantity})*\n`;
			});
		}

		let text = `Hi ${orderDetails.customer.name}, thank you for ${isCustomer ? "choosing" : "visiting"} ${
			this.props.profile.store.name
		}\n${
			isProduct
				? `\n${
						isCustomer ? "We see you purchasing these products" : "We see you ordering these products"
				  }:-\n${itemArr}\n\n`
				: ""
		}${
			isTracking && !isEmpty(orderShipping)
				? `You can track the order via this link:\n${orderShipping.tracking_url}\n\n`
				: ""
		}${
			isPayment
				? `${
						isCustomer
							? `You can view the receipt here:`
							: `${"```"}If you’re still interested, you can proceed ${
									paymentMethod == 2 ? "with the payment to" : "thru this link"
							  }:${"```"}`
				  }\n${
						isCustomer
							? `https://receipt.storeup.io/${orderDetails.ref}`
							: paymentMethod == 2
							? `*${payout.acc_name}*\n*${BankList.find(f => f.code === payout.bank_code).name}*\n*${
									payout.bank_acc
							  }*`
							: `https://checkout.storeup.io/${orderDetails.ref}`
				  }${
						!isCustomer && paymentMethod == 2
							? `\n\nYou can view your order details here:\nhttps://receipt.storeup.io/${orderDetails.ref}`
							: ""
				  }`
				: ""
		}`;

		WebBrowser.openBrowserAsync(
			`https://wa.me/60${orderDetails.customer.phone}?text=${encodeURIComponent(text)}`
		).then(() => {
			this.handleBack();
		});
	};

	SelectorTitle = id => {
		switch (id) {
			case 1:
				return "List of products";
			case 2: {
				if (this.state.isCustomer) {
					return "Receipt link";
				} else {
					return "Payment link";
				}
			}
			case 3:
				return "Tracking number link";
		}
	};

	render() {
		const { selectedInfo, availableInfo, paymentMethod } = this.state;
		const isAllSelected = selectedInfo.length === availableInfo.length;
		const lang = langSelector.OrderContact.find(e => e.lang === this.props.preference.lang).data;

		const SelectInfoBox = props => {
			return (
				<TouchableRipple
					rippleColor={transparent}
					onPress={props.onPress}
					style={{
						width: "100%",
						alignSelf: "center",
					}}
				>
					<View
						style={{
							width: "100%",
							flexDirection: "row",
							alignItems: "center",
							justifyContent: "space-between",
							marginBottom: 15,
						}}
					>
						{/* <View style={{ marginBottom: 20 }}> */}
						<Checkbox.Android status={props.checked ? "checked" : "unchecked"} />
						{/* </View> */}
						<View style={{ width: "85%" }}>
							<Text
								style={{
									fontFamily: props.checked ? "medium" : "regular",
									fontSize: 17,
								}}
							>
								{props.title}
							</Text>
						</View>
					</View>
				</TouchableRipple>
			);
		};

		return (
			<LayoutView>
				<NavHeader
					onBack={this.handleBack}
					actionText="Send"
					onPress={this.handleSubmit}
					title="Choose info to send"
				/>

				<View style={styles.container}>
					<SectionBox>
						{availableInfo.map((e, i) => {
							return (
								<View>
									{i !== 0 ? <SectionDivider /> : null}
									<CheckBox
										key={i}
										label={this.SelectorTitle(e.id)}
										onChange={this.handleOrderBox(e.id)}
										checked={selectedInfo.findIndex(f => f === e.id) !== -1}
										style={{ paddingVertical: 12 }}
									/>
								</View>
							);
						})}
					</SectionBox>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		// alignItems: "center"
	},
	subtitle: {
		fontFamily: "regular",
		color: greyblack,
		marginBottom: 30,
	},
	itemBox: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingVertical: 10,
		//width: "100%",
	},
	itemBoxHeader: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		width: "100%",
	},
	itemBoxBreakdown: {
		display: "flex",
		flexDirection: "column",
		width: "70%",
	},

	itemImg: {
		width: "25%",
		height: undefined,
		aspectRatio: 1,
		borderRadius: 10,
		borderWidth: 0.5,
		borderColor: greywhite,
		overflow: "hidden",
	},
	itemName: {
		fontSize: 17,
		fontFamily: "bold",
		letterSpacing: 0.5,
	},
	itemPrice: {
		fontSize: 15,
		fontFamily: "regular",
		color: greyblack,
		lineHeight: 17,
	},
	itemQuantity: {
		color: greyblack,
		// fontSize: 15,
		fontFamily: "light",
	},
	regButton: {
		marginTop: 20,
	},
	saveButton: {
		marginTop: 20,
		alignSelf: "flex-end",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	order: state.order,
	profile: state.profile,
	alert: state.alert,
});

export default connect(mapStateToProps, {})(OrderContact);
