import React from "react";
import { StyleSheet, View, Text, Animated, Easing, RefreshControl, FlatList } from "react-native";
import { connect } from "react-redux";

import { getAllOrders } from "../store/actions/orderAction";
import { setRefresh } from "../store/actions/loadingAction";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import NavHeader from "../component/NavHeader";
import DateConverter from "../utils/DateConverter";
import { black, greyblack, transparent } from "../utils/ColorPicker";
import getWidth from "../utils/getWidth";
import { TouchableRipple } from "react-native-paper";

import OrderBox from "../component/OrderBox";
import EmptyText from "../component/EmptyText";
import langSelector from "../utils/langSelector";
import { MaterialIcons } from "@expo/vector-icons";
import StatusCard from "../component/StatusCard";
import isEmpty from "../utils/isEmpty";
import { NoOrderIcon } from "../assets/VectorIcons";

class OrderHistory extends React.Component {
	constructor(props) {
		super(props);
		this.transValue = new Animated.Value(1);
	}

	state = {
		active: 1,
		search: "",
		statusCard: {},
	};

	componentDidMount() {
		this.props.getAllOrders(this.props.auth.token);
	}

	componentDidUpdate(prevProps, prevState) {
		if (!prevProps.loading.refresh && this.props.loading.refresh) {
			setTimeout(() => {
				this.props.getAllOrders(this.props.auth.token);
			}, 1500);
		}
		if (prevState.search !== this.state.search && this.state.search.length >= 2) {
		}
	}

	handleActive = active => () => {
		Animated.timing(this.transValue, {
			toValue: active,
			duration: 200,
			easing: Easing.quad, // Easing is an additional import from react-native
			useNativeDriver: true, // To make use of native driver for performance
		}).start();
		this.setState({
			active,
		});
	};

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handlePage =
		(page, params = {}) =>
		() => {
			this.props.navigation.push(page, params);
		};

	handleItem = oID => () => {
		this.props.navigation.push("OrderDetail", { oID, isAction: true });
	};

	render() {
		const { active, statusCard } = this.state;
		const { orderList, isOrderFetched } = this.props.order;
		const { completed, rejected } = orderList;

		const lang = langSelector.OrderHistory.find(e => e.lang === this.props.preference.lang).data;

		const tabIndicatorMove = this.transValue.interpolate({
			inputRange: [1, 2],
			outputRange: [0, getWidth * 0.3],
		});

		const RentalTabBox = props => (
			<TouchableRipple
				onPress={this.handleActive(props.val)}
				rippleColor={transparent}
				style={[
					styles.rentalTabItem,
					props.active === props.val ? styles.rentalTabItemInactive : styles.rentalTabItemInactive,
					{
						width: "30%",
					},
				]}
			>
				<View style={styles.rentalTabTextContainer}>
					<Text
						style={[
							styles.rentalTabText,
							props.active === props.val ? styles.rentalTabTextActive : styles.rentalTabTextInactive,
						]}
					>
						{props.text}
					</Text>
				</View>
			</TouchableRipple>
		);

		const RentalTab = props => (
			<View style={{ width: "100%", paddingBottom: 15 }}>
				<View style={styles.rentalTabContainer}>
					<RentalTabBox active={props.active} val={1} text={lang.completed} />
					<RentalTabBox active={props.active} val={2} text={lang.rejected} />
				</View>
				<Animated.View
					style={{
						width: "30%",
						marginTop: 8,
						height: 2,
						borderRadius: 1,
						backgroundColor: black,
						transform: [{ translateX: tabIndicatorMove }],
					}}
				/>
			</View>
		);

		return (
			<LayoutView>
				<NavHeader
					onBack={this.handleBack}
					actions={[
						{
							onPress: this.handlePage("OrderSearch", { isHistory: true }),
							icon: <MaterialIcons name="search" color={greyblack} size={30} />,
						},
					]}
				/>
				<StatusCard
					orderID={statusCard.id}
					open={!isEmpty(statusCard)}
					status={statusCard.status}
					onClose={() => {
						this.setState({
							statusCard: {},
						});
					}}
				/>

				{isOrderFetched ? (
					<FlatList
						refreshControl={
							<RefreshControl
								refreshing={this.props.loading.refresh}
								onRefresh={() => {
									this.props.setRefresh(true);
								}}
							/>
						}
						showsVerticalScrollIndicator={false}
						style={{ width: "100%", alignSelf: "center" }}
						contentContainerStyle={{ width: "100%", flex: 1, paddingVertical: 15 }}
						ListHeaderComponent={
							<View
								style={{
									width: "100%",
									alignItems: "flex-start",
									alignSelf: "center",
								}}
							>
								<PageTitle>{lang.title}</PageTitle>
								<RentalTab active={active} />
							</View>
						}
						data={active === 2 ? rejected : active === 1 ? completed : []}
						keyExtractor={e => e.order_id}
						ListEmptyComponent={<EmptyText image={<NoOrderIcon size={100} />} title={lang.emptyText} />}
						renderItem={({ item, index, separators }) => {
							const canChangeStatus =
								item.status === 0 || item.status === 1 || item.status === 2 || item.status === 3;
							return (
								<OrderBox
									isUnread={!item.is_open}
									statusType={1}
									onPress={this.handleItem(item.order_id)}
									key={index}
									oTitle={`${lang.orderID}:`}
									id={item.order_id}
									name={item.customer_name}
									date={DateConverter(item.created_ts)}
									price={item.total_price}
									quantity={item.item_count}
									status={item.status}
									pMethod={item.payment.method_name}
									onPressStatus={
										canChangeStatus
											? () => {
													this.setState({
														statusCard: {
															id: item.order_id,
															status: item.status,
														},
													});
											  }
											: null
									}
								/>
							);
						}}
					/>
				) : (
					<Loading />
				)}
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
	rentalTabContainer: {
		width: "100%",
		flexDirection: "row",
		//justifyContent: "center",
		alignItems: "center",
	},
	rentalTabItem: {
		paddingTop: 10,
		//marginBottom: 15,
		justifyContent: "center",
		alignItems: "center",
		borderBottomWidth: 2,
	},
	rentalTabItemActive: {
		borderBottomColor: black,
	},
	rentalTabItemInactive: {
		borderBottomColor: transparent,
	},
	rentalTabTextContainer: {
		width: "100%",
		flexDirection: "column",
		alignItems: "center",
	},
	rentalTabText: {
		fontSize: 13,
	},
	indicator: {
		alignSelf: "flex-end",
		width: 8,
		height: undefined,
		aspectRatio: 1,
		borderRadius: 4,
		marginRight: 3,
	},
	rentalTabTextActive: {
		fontFamily: "bold",
	},
	rentalTabTextInactive: {
		fontFamily: "light",
		color: greyblack,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	order: state.order,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, { getAllOrders, setRefresh })(OrderHistory);
