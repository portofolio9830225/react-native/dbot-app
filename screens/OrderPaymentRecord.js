import React from "react";
import { StyleSheet, View, Text, Keyboard } from "react-native";
import { connect } from "react-redux";

import { clearNoti, customNoti } from "../store/actions/alertAction";
import { setOrderPayment } from "../store/actions/orderAction";

import { Button, TouchableRipple } from "react-native-paper";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import isEmpty from "../utils/isEmpty";

import { ScrollView, TouchableWithoutFeedback } from "react-native-gesture-handler";
import NavHeader from "../component/NavHeader";
import { black, greendark, greyblack, greydark, greylight, white } from "../utils/ColorPicker";

import CustomField from "../component/CustomField";
import TextField from "../component/TextField";
import { withNavigationFocus } from "react-navigation";
import langSelector from "../utils/langSelector";
import { imagekitUpload } from "../utils/imagekit";
import { setLoading } from "../store/actions/loadingAction";
import Typography from "../component/Typography";

class OrderPaymentRecord extends React.Component {
	state = {
		detail: "",
		file: null,
		fileName: "",
	};

	async componentDidMount() {}

	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.alert.status) && this.props.alert.status === "success") {
			this.handleBack();
		}
	}

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handleText = name => value => {
		this.setState({
			[name]: value,
		});
	};

	handleFile = (id, name, file, type) => {
		this.setState({
			file,
			fileName: name,
		});
	};

	handlePaymentType = val => () => {
		this.setState({
			paymentType: val,
		});
	};

	handleSubmit = async () => {
		// if (isEmpty(this.state.fileName)) {
		// 	this.props.customNoti("Receipt required", "error");
		// } else {
		let data = new FormData();
		data.append("file", this.state.file);
		data.append("fileName", `${this.props.order.orderDetails.ref}`);
		data.append("folder", `business/${this.props.profile.store.id}/receipt`);

		this.props.setLoading(true);
		imagekitUpload(data)
			.then(resImg => {
				let dataDB = {
					url: resImg.url,
					description: this.state.detail,
				};
				this.props.setOrderPayment(
					this.props.order.orderDetails.ref,
					dataDB,
					this.props.profile.store.id,
					this.props.auth.token
				);
			})
			.catch(imgErr => {
				this.props.setLoading(false);
				this.props.customNoti("Unknown error", "error");
			});
		// }
	};

	render() {
		const { detail } = this.state;
		const { orderDetails, orderPayment } = this.props.order;
		const lang = langSelector.OrderPaymentRecord.find(e => e.lang === this.props.preference.lang).data;

		return (
			<LayoutView>
				<NavHeader
					onBack={this.handleBack}
					onPress={this.handleSubmit}
					actionText="Submit"
					title="Record Payment"
				/>
				<View style={styles.container}>
					<ScrollView
						showsVerticalScrollIndicator={false}
						style={{ width: "100%" }}
						contentContainerStyle={{
							minHeight: "100%",
							position: "relative",
							paddingBottom: 100,
						}}
					>
						<View style={styles.paymentDetailBox}>
							<Typography style={{ color: greyblack }}>{lang.amount}</Typography>
							<Typography type="title1" style={{ fontFamily: "bold" }}>
								RM
								{orderPayment.amount.toFixed(2)}
							</Typography>
						</View>
						<CustomField id={1} type={1} title={lang.proofTitle} onChange={this.handleFile} />
						<TextField
							style={styles.textInput}
							type="multiline"
							numberOfLines={3}
							mode="outlined"
							label="Notes"
							placeholder="eg: date, payment type, approver, etc."
							value={detail}
							onChangeText={this.handleText("detail")}
							errorKey="description"
						/>
					</ScrollView>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		// alignItems: "center"
	},
	paymentDetailBox: {
		backgroundColor: white,
		width: "100%",
		marginVertical: 8,
		paddingVertical: 12,
		paddingHorizontal: 16,
		borderRadius: 10,
	},

	textInput: {
		marginVertical: 10,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	order: state.order,
	profile: state.profile,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	setOrderPayment,
	setLoading,
	clearNoti,
	customNoti,
})(withNavigationFocus(OrderPaymentRecord));
