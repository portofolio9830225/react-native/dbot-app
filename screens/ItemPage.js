import React from "react";
import {
	StyleSheet,
	ScrollView,
	View,
	Text,
	FlatList,
	Image,
	TouchableWithoutFeedback,
	Keyboard,
	Platform,
} from "react-native";
import CachedImage from "expo-cached-image";
import { connect } from "react-redux";
import * as ImagePicker from "expo-image-picker";

import { LayoutView } from "../component/Layout";
import NavHeader from "../component/NavHeader";
import {
	accent,
	black,
	green,
	grey,
	greyblack,
	greylight,
	greywhite,
	orange,
	red,
	transparent,
} from "../utils/ColorPicker";
import { MaterialCommunityIcons, MaterialIcons } from "@expo/vector-icons";
import TextField from "../component/TextField";
import getWidth from "../utils/getWidth";
import uriToFile from "../utils/uriToFile";
import { TouchableRipple } from "react-native-paper";
import isEmpty from "../utils/isEmpty";
import { getCameraPermissionAsync, getGalleryPermissionAsync } from "../utils/PermissionGetter";
import { isTablet } from "react-native-device-detection";
import { withNavigationFocus } from "react-navigation";
import {
	createItem,
	deleteItem,
	editItem,
	getImagePool,
	getSingleItem,
	setItemStatus,
} from "../store/actions/itemAction";
import { setLoading } from "../store/actions/loadingAction";
import langSelector from "../utils/langSelector";
import ShippingPriceRow from "../component/ShippingPriceRow";
import { getAgentItem } from "../store/actions/agentAction";
import { getItemShipping } from "../store/actions/catalogAction";
import ItemVariantRow from "../component/ItemVariantRow";
import { customNoti } from "../store/actions/alertAction";
import ImagePoolEditor from "../component/ImagePoolEditor";
import Typography from "../component/Typography";
import { SectionBox, SectionDivider } from "../component/SectionComponents";
import SwitcherWithLabel from "../component/SwitcherWithLabel";
import AlertDialog from "../component/AlertDialog";
import { createFirstCategory } from "../store/actions/categoryAction";
import CategorySelect from "../component/CategorySelect";

class ItemPage extends React.Component {
	state = {
		isEdit: false,
		viewOnly: false,
		isActive: true,
		name: "",
		desc: "",
		allPrice: "",
		discountedPrice: "",
		allStock: "",
		maxOrder: "",
		minOrder: "",
		weight: "",
		shipping: [],
		variants: [{ stock: "", variant_name: "", variant_number: 1, min_limit: "", max_limit: "" }],
		image: [""],
		imageFile: [],
		imageModal: null,
		imagePoolDialog: false,
		selectedImg: [""],
		option: false,
		deleteDialog: false,
		itemStatus: null,
		categoryName: "",
		categoryList: "",
		categoryIds: [],
		categorySelectDialog: false,
		filterArr: [],
	};

	fetchData = () => {
		if (!isEmpty(this.props.navigation.state.params.id)) {
			this.props.setLoading(true);
			this.setState(
				{
					isEdit: true,
					viewOnly: this.props.navigation.state.params.viewOnly,
				},
				() => {
					if (this.props.navigation.state.params.isAgent) {
						this.props.getAgentItem(this.props.navigation.state.params.id, this.props.auth.token);
					} else {
						this.props.getSingleItem(
							this.props.navigation.state.params.id,
							this.props.auth.token,
							this.props.navigation.state.params.bID
						);
					}
				}
			);
		} else {
			this.props.setLoading(false);
		}
	};

	componentDidMount() {
		this.props.setLoading(true);
		if (this.props.item.imagePoolFetched) {
			this.fetchData();
		} else {
			this.props.getImagePool(this.props.auth.token);
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (!prevProps.item.imagePoolFetched && this.props.item.imagePoolFetched) {
			this.fetchData();
		}
		if (isEmpty(prevProps.catalog.shipping) && !isEmpty(this.props.catalog.shipping)) {
			this.setState({
				shipping: this.props.catalog.shipping.shipping.product,
			});
		}
		if (isEmpty(prevProps.item.itemPage.details) && !isEmpty(this.props.item.itemPage.details)) {
			let { details, images, shipping, stock, variants, categories } = this.props.item.itemPage;

			let imgArr = [];
			images.map(e => {
				imgArr.push(e.file_id);
			});

			let varArr = [];

			variants.map((v, i) => {
				varArr.push({
					...v,
					variant_number: i + 1,
					stock: `${v.stock}`,
					min_limit: !isEmpty(v.min_limit) ? `${v.min_limit}` : "",
					max_limit: !isEmpty(v.max_limit) ? `${v.max_limit}` : "",
				});
			});

			let catIds = [];
			let catList = "";

			categories.map((c, i) => {
				let text = c.name;
				if (i !== 0) {
					text = `, ${text}`;
				}

				catIds.push(c.id);
				catList += text;
			});

			this.setState({
				name: details.name,
				desc: details.description,
				// allPrice: `${details.price.toFixed(2)}`,
				allPrice: isEmpty(details.another_price)
					? `${details.price.toFixed(2)}`
					: `${details.another_price.toFixed(2)}`,
				discountedPrice: isEmpty(details.another_price) ? "" : `${details.price.toFixed(2)}`,
				allStock: `${details.total_stock}`,
				//minOrder: !isEmpty(details.min_limit) ? `${details.min_limit}` : "",
				//maxOrder: !isEmpty(details.max_limit) ? `${details.max_limit}` : "",
				//shipping: shipping,
				variants: !isEmpty(variants) ? varArr : this.state.variants,
				selectedImg: !isEmpty(imgArr) ? imgArr : [""],
				image: !isEmpty(imgArr) ? [...imgArr] : [""],
				imageFile: !isEmpty(imgArr) ? imgArr : [],
				isActive: details.active,
				weight: `${details.weight}`,
				categoryList: catList,
				categoryIds: catIds,
			});
		}
		if (!prevProps.isFocused && this.props.isFocused) {
			if (!isEmpty(this.props.navigation.state.params)) {
				if (this.props.navigation.state.params.variants) {
					this.setState({
						variants: this.props.navigation.state.params.variants,
					});
				}
				if (
					this.props.navigation.state.params.fromPage &&
					this.props.navigation.state.params.fromPage === "CategoryAdd"
				) {
					this.setState({
						categorySelectDialog: true,
					});
				}
			}
		}
		if (prevState.categoryIds !== this.state.categoryIds) {
			let catList = "";
			this.state.categoryIds.map((c, i) => {
				let cat = this.props.category.categories.find(f => f.id === c);

				let text = cat.name;
				if (i !== 0) {
					text = `, ${text}`;
				}
				catList += text;
			});

			this.setState({
				categoryList: catList,
			});
		}

		if (isEmpty(prevProps.alert.status) && this.props.alert.status === "success") {
			if (
				this.props.alert.redirect === "editItem" ||
				this.props.alert.redirect === "deleteItem" ||
				this.props.alert.redirect === "createItem"
			) {
				this.handleBack();
			}

			if (this.props.alert.redirect === "createCategory") {
				this.handleCategorySelectDialog(true)();
			}
		}
	}

	componentWillUnmount() {
		this.props.getSingleItem();
	}

	handleBack = () => {
		this.props.navigation.pop();
	};

	handleDeleteDialog = state => () => {
		this.setState({
			deleteDialog: state,
		});
	};

	handleText = name => value => {
		this.setState({
			[name]: value,
		});
	};

	handleDeleteProduct = () => {
		this.setState(
			{
				deleteDialog: false,
			},
			() => {
				this.props.deleteItem(this.props.item.itemPage.details.ref, this.props.auth.token);
			}
		);
	};

	handleImagePool = state => () => {
		this.setState({
			imagePoolDialog: state,
		});
	};

	handleImageItem = data => () => {
		this.setState({
			imagePoolDialog: false,
			selectedImg: !isEmpty(data) ? data : [""],
		});
		//console.log(data);
	};

	handleImageModal = index => () => {
		this.setState({
			imageModal: index,
		});
	};

	handleImage = index => () => {
		this.setState({
			imageModal: index,
		});
	};

	handleImageArr = (img, index) => {
		let arr = [...this.state.image];
		let arrFile = [...this.state.imageFile];
		arr[index] = img;
		arrFile[index] = uriToFile(img);
		if (arr.length <= arrFile.length) {
			arr.push("");
		}

		this.setState({
			imageModal: null,
			image: arr,
			imageFile: arrFile,
		});
	};

	pickImageFromGallery = index => async () => {
		let status = await getGalleryPermissionAsync();
		if (status === "granted") {
			this.setState({
				choose: false,
			});

			let result = await ImagePicker.launchImageLibraryAsync({
				mediaTypes: ImagePicker.MediaTypeOptions.Images,
				allowsEditing: true,
				aspect: [1, 1],
			});
			if (!result.cancelled) {
				this.handleImageArr(result.uri, index);
			}
		}
	};

	pickImageFromCamera = index => async () => {
		let status1 = await getCameraPermissionAsync();
		let status2 = await getGalleryPermissionAsync();

		if (status1 === "granted" && status2 === "granted") {
			this.setState({
				choose: false,
			});
			let result = await ImagePicker.launchCameraAsync({
				mediaTypes: ImagePicker.MediaTypeOptions.Images,
				allowsEditing: true,
				aspect: [1, 1],
			});

			if (!result.cancelled) {
				this.handleImageArr(result.uri, index);
			}
		}
	};

	delImage = () => {
		this.props.setLoading(true);
		let pos = this.state.imageModal;

		this.setState(
			{
				imageModal: null,
			},
			() => {
				let arr = [...this.state.imageFile];
				arr.splice(pos, 1);
				this.setState({
					image: [...arr, ""],
					imageFile: arr,
				});

				this.props.setLoading(false);
			}
		);
	};

	setMainImage = () => {
		this.props.setLoading(true);
		let pos = this.state.imageModal;
		let arr = [...this.state.imageFile];

		this.setState(
			{
				imageModal: null,
			},
			() => {
				let nMain = arr[pos];
				let oMain = arr[0];
				arr.splice(0, 1, nMain);
				arr.splice(pos, 1, oMain);
				this.setState({
					image: [...arr, ""],
					imageFile: arr,
				});
				this.props.setLoading(false);
			}
		);
	};

	handleChangeVariant = (index, data) => {
		let arr = [...this.state.variants];

		let foundIndex = arr.findIndex(f => f.variant_number === index);
		arr[foundIndex] = {
			...data,
		};

		this.setState({
			variants: arr,
		});
	};

	handleVariant = () => {
		this.props.navigation.navigate("ItemVariant", {
			variants: this.state.variants,
		});
	};

	handleAddCategory = () => {
		this.props.navigation.push("CategoryAdd", { fromPage: "ItemPage" });
		setTimeout(() => {
			this.handleCategorySelectDialog(false)();
		}, 500);
	};

	handleCategorySelectDialog = state => () => {
		this.setState({
			categorySelectDialog: state,
		});
	};

	handleCategory = data => () => {
		this.setState({
			categorySelectDialog: false,
			categoryIds: data,
		});
	};

	handleFilter = value => {
		this.setState({
			filterArr: value,
		});
	};

	handleSubmit = async () => {
		Keyboard.dismiss();

		const {
			name,
			desc,
			allPrice,
			discountedPrice,
			isActive,
			variants,
			selectedImg,
			categoryIds,
			minOrder,
			maxOrder,
			shipping,
			imageFile,
		} = this.state;

		if (!isEmpty(discountedPrice)) {
			let aPrice = parseFloat(allPrice);
			let dPrice = parseFloat(discountedPrice);

			if (dPrice < 0 || dPrice >= aPrice) {
				this.props.customNoti("Invalid discounted price", "error");
				return;
			}
		}

		let variantSent = [];
		variants.map((v, i) => {
			let p = {
				...v,
				min_limit: !isEmpty(v.min_limit) ? v.min_limit : null,
				max_limit: !isEmpty(v.max_limit) ? v.max_limit : null,
				variant_number: i + 1,
			};
			variantSent.push(p);
		});

		let imageSent = [];
		selectedImg.map((m, i) => {
			let img = {
				file_id: m,
				number: i + 1,
			};
			imageSent.push(img);
		});
		let data = {
			name,
			description: desc,
			price: isEmpty(discountedPrice) ? allPrice : discountedPrice,
			another_price: isEmpty(discountedPrice) ? null : allPrice,
			active: isActive,
			published: true,
			variants: variantSent,
			weight: !isEmpty(this.state.weight) ? this.state.weight : 0,
			images: imageSent,
			category_ids: categoryIds,
		};

		if (isEmpty(this.props.category.categories) && !isEmpty(this.state.categoryName)) {
			let newCategory = await this.props.createFirstCategory(
				{ name: this.state.categoryName, description: "", active: true },
				this.props.auth.token
			);
			if (!isEmpty(newCategory) && newCategory.id) {
				data = { ...data, category_ids: [newCategory.id] };
				this.handleSubmitItemDetails(data);
			}
		} else {
			this.handleSubmitItemDetails(data);
		}
	};

	handleSubmitItemDetails = data => {
		if (!this.state.isEdit) {
			this.props.createItem(data, this.props.auth.token);
		} else {
			this.props.setLoading(true);
			this.props.editItem(this.props.item.itemPage.details.ref, data, this.props.auth.token);
		}
	};

	render() {
		const {
			isEdit,
			viewOnly,
			isActive,
			name,
			desc,
			allPrice,
			discountedPrice,
			deleteDialog,
			weight,
			variants,
			imagePoolDialog,
			selectedImg,
			categoryName,
			categoryList,
			categoryIds,
			categorySelectDialog,
			filterArr,
		} = this.state;
		const businessCategories = this.props.category.categories;
		const { imagePool } = this.props.item;
		const { isAgentItem } = this.props.item.itemPage;

		const isDone =
			!isEmpty(name) &&
			!isEmpty(allPrice) &&
			!isEmpty(selectedImg) &&
			!isEmpty(variants) &&
			!isEmpty(variants[0].stock);
		const lang = langSelector.ItemPage.find(e => e.lang === this.props.preference.lang).data;

		const ImageBox = ({ index, isFirst, isLast, isSingle, img, cacheKey }) => {
			return (
				<TouchableWithoutFeedback onPress={this.handleImagePool(true)}>
					<View
						style={{
							alignItems: "center",
							justifyContent: "center",
							backgroundColor: "#f0f0f3",
							borderRadius: 16,
							width: isTablet ? getWidth * 0.6 : getWidth * 0.95,
							height: isTablet ? getWidth * 0.6 : getWidth * 0.95,
							marginHorizontal: 10,
							marginLeft: isFirst ? 16 : 10,
							marginRight: isLast ? 16 : 10,
							marginVertical: 16,
						}}
					>
						{!isEmpty(img) ? (
							<CachedImage
								style={[
									styles.image,
									{
										borderRadius: 16,
									},
								]}
								source={{ uri: img }}
								cacheKey={cacheKey}
							/>
						) : (
							<>
								<MaterialCommunityIcons
									name="camera-plus-outline"
									size={getWidth / 7}
									color={greylight}
								/>
								<Text
									style={{
										color: grey,
										fontFamily: "regular",
										marginTop: 5,
									}}
								>
									{lang.addImage}
								</Text>
							</>
						)}
					</View>
				</TouchableWithoutFeedback>
			);
		};

		return (
			<LayoutView noPadding>
				<View style={{ paddingHorizontal: 16 }}>
					<NavHeader
						onBack={this.handleBack}
						onPress={isDone ? this.handleSubmit : null}
						actionText={viewOnly ? "" : isEdit ? "Done" : "Publish"}
						title={isEdit ? "Product Details" : "New Product"}
					/>
				</View>
				{imagePoolDialog && (
					<ImagePoolEditor
						onCancel={this.handleImagePool(false)}
						onSubmit={this.handleImageItem}
						data={selectedImg}
					/>
				)}

				{categorySelectDialog && (
					<CategorySelect
						onCancel={this.handleCategorySelectDialog(false)}
						onSubmit={this.handleCategory}
						selected={categoryIds}
						handleAddCategory={this.handleAddCategory}
						filterArr={filterArr}
						handleFilter={this.handleFilter}
					/>
				)}
				<AlertDialog
					active={deleteDialog}
					title="Confirm delete product?"
					desc="This cannot be undone"
					onDismiss={this.handleDeleteDialog(null)}
					actions={[
						{ title: "Yes", onPress: this.handleDeleteProduct },
						{ title: "Cancel", onPress: this.handleDeleteDialog(null) },
					]}
				/>

				<ScrollView contentContainerStyle={{ width: "100%", paddingBottom: 30 }}>
					<View style={styles.container}>
						<FlatList
							horizontal
							showsHorizontalScrollIndicator={false}
							contentContainerStyle={styles.imageSection}
							//data={!viewOnly && !isAgentItem ? image : image.slice(0, -1)}
							data={selectedImg}
							keyExtractor={(e, i) => `${i}`}
							renderItem={({ item, index, separators }) => {
								let d = imagePool.find(f => f.id === item);

								if (isEmpty(item)) {
									return <ImageBox key={index} />;
								} else {
									return (
										<ImageBox
											key={index}
											index={d.id}
											isFirst={index === 0}
											isLast={index === selectedImg.length - 1}
											isSingle={selectedImg.length === 1}
											img={d.url_l}
											cacheKey={`${d.imagekit_id}-l`}
										/>
									);
								}
							}}
						/>
						<View
							style={{
								width: "100%",
								alignSelf: "center",
								paddingHorizontal: 16,
							}}
						>
							<SwitcherWithLabel
								label="Availability"
								enabled={isActive}
								onValueChange={() => {
									this.setState({
										isActive: !this.state.isActive,
									});
								}}
							/>
							<SectionBox title="Details">
								<TextField
									editable={!viewOnly && !isAgentItem}
									inputStyle={{ paddingHorizontal: 0 }}
									style={styles.textField}
									label="Name"
									value={name}
									onChangeText={this.handleText("name")}
								/>
								<SectionDivider />
								<TextField
									editable={!viewOnly && !isAgentItem}
									inputStyle={{ paddingHorizontal: 0 }}
									style={styles.textField}
									label="Description"
									value={desc}
									onChangeText={this.handleText("desc")}
									numberOfLines={5}
									type="multiline"
									errorKey="item_description"
								/>
							</SectionBox>
							<SectionBox title="Category">
								{!isEmpty(businessCategories) ? (
									<TextField
										editable={!viewOnly && !isAgentItem}
										onPressInput={this.handleCategorySelectDialog(true)}
										inputStyle={{ paddingHorizontal: 0 }}
										style={styles.textField}
										label="Category"
										value={categoryList}
										type="multiline"
									/>
								) : (
									<TextField
										editable={!viewOnly && !isAgentItem}
										inputStyle={{ paddingHorizontal: 0 }}
										style={styles.textField}
										label="Category"
										value={categoryName}
										onChangeText={this.handleText("categoryName")}
									/>
								)}
							</SectionBox>
							<SectionBox title="Price">
								<TextField
									editable={!viewOnly && !isAgentItem}
									inputStyle={{ paddingHorizontal: 0 }}
									style={styles.textField}
									label="Price"
									value={allPrice}
									keyboardType="decimal-pad"
									errorKey="item_price"
									onChangeText={this.handleText("allPrice")}
									startAdornment="RM"
								/>
								<SectionDivider />
								<TextField
									editable={!viewOnly && !isAgentItem}
									style={styles.textField}
									inputStyle={{ paddingHorizontal: 0 }}
									label="Promo price"
									value={discountedPrice}
									keyboardType="decimal-pad"
									errorKey="item_price"
									onChangeText={this.handleText("discountedPrice")}
									startAdornment="RM"
									// endAdornment="kg"
									endAdornment={
										!isEmpty(allPrice) && !isEmpty(discountedPrice) ? (
											<View
												style={{
													borderWidth: 1,
													borderColor: orange,
													paddingHorizontal: 5,
													borderRadius: 12,
												}}
											>
												<Typography type="subheadline" style={{ color: orange }}>
													{parseFloat(allPrice) <= parseFloat(discountedPrice)
														? "0"
														: parseInt(((allPrice - discountedPrice) / allPrice) * 100)}
													% OFF
												</Typography>
											</View>
										) : undefined
									}
								/>
							</SectionBox>
							<SectionBox title="Weight">
								<TextField
									editable={!viewOnly}
									inputStyle={{ paddingHorizontal: 0 }}
									style={styles.textField}
									label="Weight"
									//placeholder="Product weight"
									value={weight}
									onChangeText={this.handleText("weight")}
									keyboardType="decimal-pad"
									errorKey="item_weight"
									endAdornment="kg"
								/>
							</SectionBox>
							<SectionBox
								title={variants.length <= 1 ? "Stock" : "Variants"}
								rightHeader={
									<TouchableRipple rippleColor="transparent" onPress={this.handleVariant}>
										<Typography type="headline" style={{ fontFamily: "regular", color: accent }}>
											{variants.length <= 1 ? "Add variant" : "Edit"}
										</Typography>
									</TouchableRipple>
								}
							>
								{variants.length <= 1 ? (
									<>
										<TextField
											editable={!viewOnly && !isAgentItem}
											style={styles.textField}
											inputStyle={{ paddingHorizontal: 0 }}
											label="Stock"
											keyboardType="number-pad"
											value={variants[0].stock}
											onChangeText={val => {
												let d = { ...variants[0], stock: val };
												this.handleChangeVariant(1, d);
											}}
										/>
										<SectionDivider />
										<TextField
											editable={!viewOnly && !isAgentItem}
											style={styles.textField}
											inputStyle={{ paddingHorizontal: 0 }}
											label="Min order"
											keyboardType="number-pad"
											value={variants[0].min_limit}
											onChangeText={val => {
												let d = { ...variants[0], min_limit: val };
												this.handleChangeVariant(1, d);
											}}
										/>
										<SectionDivider />
										<TextField
											editable={!viewOnly && !isAgentItem}
											style={styles.textField}
											inputStyle={{ paddingHorizontal: 0 }}
											label="Max order"
											keyboardType="number-pad"
											value={variants[0].max_limit}
											onChangeText={val => {
												let d = { ...variants[0], max_limit: val };
												this.handleChangeVariant(1, d);
											}}
										/>
									</>
								) : (
									<View style={{ paddingVertical: 5 }}>
										{variants.map((e, i) => {
											return (
												<TouchableRipple rippleColor="transparent" key={i}>
													<View>
														{i !== 0 ? <SectionDivider /> : null}
														<View
															style={{
																flexDirection: "row",
																justifyContent: "space-between",
																alignItems: "center",
																paddingVertical: 10,
															}}
														>
															<Text style={{ fontFamily: "regular" }}>{e.name}</Text>
															<Text style={{ fontFamily: "regular", color: grey }}>
																{e.stock} units
															</Text>
														</View>
													</View>
												</TouchableRipple>
											);
										})}
									</View>
								)}
							</SectionBox>
							{!viewOnly && isEdit ? (
								<TouchableRipple onPress={this.handleDeleteDialog(true)} rippleColor="transparent">
									<SectionBox
										innerStyle={{ height: 55, justifyContent: "center", alignItems: "center" }}
									>
										<Typography
											type="headline"
											style={{
												color: red,
												textAlign: "center",
												fontFamily: "semibold",
											}}
										>
											Delete product
										</Typography>
									</SectionBox>
								</TouchableRipple>
							) : null}
						</View>
					</View>
				</ScrollView>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
	sectionBox: {
		//width: "100%",
		display: "flex",
		flexDirection: "column",
		// borderBottomWidth: 1,
		// borderBottomColor: greywhite,
		paddingVertical: 14,
	},
	sectionTitle: {
		fontFamily: "regular",
		paddingLeft: 12,
		textTransform: "uppercase",
		marginBottom: 10,
	},
	sectionContent: {
		width: "100%",
		backgroundColor: "white",
		borderRadius: 10,
		paddingHorizontal: 20,
	},
	orderBox: {
		overflow: "hidden",
		flexDirection: "row",
		justifyContent: "space-between",
		marginBottom: 25,
	},
	orderBoxLeft: {
		width: "35%",
		height: "100%",
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "#f7f7ff",
	},
	orderBoxRight: {
		width: "60%",
		height: "100%",
		justifyContent: "center",
		paddingRight: 10,
	},
	orderName: {
		fontFamily: "medium",
		fontSize: 16,
		lineHeight: 20,
	},
	orderPrice: {
		fontFamily: "bold",
		fontSize: 15,
		lineHeight: 20,
	},
	orderStock: {
		fontFamily: "light",
		fontSize: 12,
		color: grey,
		marginTop: 8,
	},

	rentalTabContainer: {
		width: "100%",
		flexDirection: "row",
		//justifyContent: "center",
		alignItems: "center",
	},
	rentalTabItem: {
		paddingTop: 10,
		//marginBottom: 15,
		justifyContent: "center",
		alignItems: "center",
		borderBottomWidth: 2,
	},
	rentalTabItemActive: {
		borderBottomColor: black,
	},
	rentalTabItemInactive: {
		borderBottomColor: transparent,
	},
	rentalTabTextContainer: {
		width: "100%",
		flexDirection: "column",
		alignItems: "center",
	},
	rentalTabText: {
		fontSize: 13,
	},
	indicator: {
		alignSelf: "flex-end",
		width: 8,
		height: undefined,
		aspectRatio: 1,
		borderRadius: 4,
		marginRight: 3,
	},
	rentalTabTextActive: {
		fontFamily: "bold",
	},
	rentalTabTextInactive: {
		fontFamily: "light",
		color: greyblack,
	},
	textField: {
		marginBottom: 0,
	},
	halfRow: {
		flexDirection: "row",
		justifyContent: "space-between",
	},
	image: {
		width: "100%",
		height: undefined,
		aspectRatio: 1,
	},
	imageSection: {
		flexDirection: "row",
		justifyContent: "flex-start",
		marginBottom: 5,
		minWidth: getWidth + 32,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	item: state.item,
	category: state.category,
	profile: state.profile,
	catalog: state.catalog,
	loading: state.loading,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	getAgentItem,
	createItem,
	getSingleItem,
	editItem,
	setItemStatus,
	deleteItem,
	getItemShipping,
	getImagePool,
	createFirstCategory,
	setLoading,
	customNoti,
})(withNavigationFocus(ItemPage));
