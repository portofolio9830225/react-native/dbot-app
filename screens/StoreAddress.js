import React from "react";
import { StyleSheet, View, Text, Keyboard, Image, ScrollView, Platform, Dimensions } from "react-native";
import { connect } from "react-redux";
import isEmpty from "../utils/isEmpty";

import { editStoreAddress } from "../store/actions/profileAction";
import { customNoti } from "../store/actions/alertAction";

import { Button } from "react-native-paper";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";

import NavHeader from "../component/NavHeader";
import { greyblack, black, greywhite } from "../utils/ColorPicker";

import TextField from "../component/TextField";
import stateList from "../utils/stateList";
import langSelector from "../utils/langSelector";
import { SectionBox, SectionDivider } from "../component/SectionComponents";

class StoreAddress extends React.Component {
	state = {
		address1: "",
		address2: "",
		address3: "",
		isAddress3: false,
		address4: "",
		isAddress4: false,
		postcode: "",
		ciName: "",
		staName: "",
	};

	componentDidMount() {
		if (!isEmpty(this.props.profile.store.address)) {
			let { address_1, address_2, address_3, address_4, city, postcode, state } =
				this.props.profile.store.address;
			this.setState({
				address1: address_1,
				address2: address_2,
				address3: address_3,
				address4: address_4,
				postcode,
				ciName: city,
				staName: state,
			});
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.alert.redirect) && this.props.alert.redirect === "StoreAddressSaved") {
			this.handleBack();
		}
	}

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handleText =
		(name, limit = 999) =>
		value => {
			if (value.length <= limit) {
				this.setState({
					[name]: value,
				});
			}
		};

	handleStaName = (name, code) => {
		this.setState({
			staName: code,
		});
	};

	handleSubmit = () => {
		Keyboard.dismiss();
		let { address1, address2, address3, address4, postcode, ciName, staName } = this.state;
		let data = {
			address_1: address1,
			address_2: address2,
			address_3: address3,
			address_4: address4,
			postcode,
			city: ciName,
			state: staName,
			country: "MY",
		};

		this.props.editStoreAddress(this.props.profile.store.id, data, this.props.auth.token);
	};

	render() {
		const { address1, address2, address3, address4, postcode, ciName, staName } = this.state;
		const isDone = !isEmpty(address1) && !isEmpty(postcode) && !isEmpty(ciName) && !isEmpty(staName);

		const lang = langSelector.StoreAddress.find(e => e.lang === this.props.preference.lang).data;

		return (
			<LayoutView>
				<NavHeader
					onBack={this.handleBack}
					actionText="Done"
					onPress={isDone ? this.handleSubmit : null}
					title="Address"
				/>

				<View style={styles.container}>
					<ScrollView
						style={{ width: "100%" }}
						contentContainerStyle={{ height: Dimensions.get("window").height }}
						showsVerticalScrollIndicator={false}
					>
						<SectionBox>
							<TextField
								onChangeText={this.handleText("address1", 35)}
								style={styles.textInput}
								inputStyle={{ paddingHorizontal: 0 }}
								mode="outlined"
								label="Address"
								placeholder="Line 1"
								value={address1}
								errorKey="address_1"
							/>
							<SectionDivider />
							<TextField
								onChangeText={this.handleText("address2", 35)}
								inputStyle={{ paddingHorizontal: 0 }}
								style={styles.textInput}
								mode="outlined"
								label="Address"
								placeholder="Line 2 (Optional)"
								value={address2}
								errorKey="address_2"
							/>
							<SectionDivider />
							<TextField
								onChangeText={this.handleText("address3", 35)}
								inputStyle={{ paddingHorizontal: 0 }}
								style={styles.textInput}
								mode="outlined"
								label="Address"
								placeholder="Line 3 (Optional)"
								value={address3}
								errorKey="address_3"
							/>
							<SectionDivider />
							{/* </Collapsible>
							<Collapsible collapsed={isEmpty(address3)}> */}
							<TextField
								onChangeText={this.handleText("address4", 35)}
								inputStyle={{ paddingHorizontal: 0 }}
								style={styles.textInput}
								mode="outlined"
								label="Address"
								placeholder="Line 4 (Optional)"
								value={address4}
								errorKey="address_4"
							/>
							{/* </Collapsible> */}
							<SectionDivider />
							<TextField
								onChangeText={this.handleText("postcode")}
								inputStyle={{ paddingHorizontal: 0 }}
								keyboardType="number-pad"
								style={[styles.textInput]}
								mode="outlined"
								label={lang.postcode}
								value={postcode}
								errorKey="postcode"
							/>
							<SectionDivider />
							<TextField
								onChangeText={this.handleText("ciName")}
								inputStyle={{ paddingHorizontal: 0 }}
								style={[styles.textInput]}
								mode="outlined"
								label={lang.city}
								value={ciName}
								errorKey="city"
							/>
							<SectionDivider />
							<TextField
								type="select"
								style={styles.textInput}
								inputStyle={{ paddingHorizontal: 0 }}
								selectorList={stateList}
								dialogTitle={lang.state}
								label={lang.state}
								onSelect={this.handleStaName}
								value={!isEmpty(staName) ? staName : lang.selectState}
								errorKey="state"
							/>
						</SectionBox>
					</ScrollView>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		paddingTop: 15,
		//	position: "relative",
	},
	contentCenter: {
		marginTop: 15,
		width: "100%",
		alignSelf: "center",
		backgroundColor: "white",
		borderRadius: 10,
		paddingHorizontal: 20,
	},
	textInput: {
		marginVertical: 0,
	},
	dialogActionButton: {
		minWidth: 70,
	},
	sectionBox: {
		//width: "100%",
		display: "flex",
		flexDirection: "column",
		// borderBottomWidth: 1,
		// borderBottomColor: greywhite,
		paddingVertical: 14,
	},
	sectionTitle: {
		fontFamily: "regular",
		textTransform: "uppercase",
		color: greyblack,
		paddingLeft: 12,
		marginBottom: 15,
	},
	halfRow: {
		flexDirection: "row",
		justifyContent: "space-between",
	},
	saveButton: {
		marginTop: 20,
		alignSelf: "flex-end",
		width: "100%",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	profile: state.profile,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	editStoreAddress,
	customNoti,
})(StoreAddress);
