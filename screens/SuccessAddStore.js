import { Ionicons } from "@expo/vector-icons";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Button } from "react-native-paper";
import { connect } from "react-redux";

import { LayoutView } from "../component/Layout";

import NavHeader from "../component/NavHeader";
import { black, accent, transparent } from "../utils/ColorPicker";
import getWidth from "../utils/getWidth";
import { setLoading } from "../store/actions/loadingAction";
import CustomButton from "../component/CustomButton";

class StoreSuccess extends React.Component {
	componentDidMount() {
		this.props.setLoading(false);
	}
	handleBack = () => {
		this.props.navigation.navigate("ListOfBusiness");
	};

	render() {
		return (
			<LayoutView>
				<NavHeader />

				<View style={styles.container}>
					<View style={styles.successBox}>
						<Ionicons name="ios-checkmark-circle" size={getWidth * 0.44} color={accent} />
						<Text style={styles.successTitle}>Success</Text>
						<Text style={styles.successDesc}>Store successfully created</Text>
					</View>
					<CustomButton style={styles.button} onPress={this.handleBack}>
						Return to store list
					</CustomButton>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "space-between",
	},
	successBox: {
		alignItems: "center",
	},
	successTitle: {
		marginTop: 10,
		fontFamily: "medium",
		fontSize: 30,
	},
	successDesc: {
		fontFamily: "thin",
		fontSize: 18,
		textAlign: "center",
	},
	button: {
		width: "100%",
		marginBottom: 20,
		borderWidth: 1,
		borderColor: black,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	book: state.book,
	item: state.item,
	alert: state.alert,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, { setLoading })(StoreSuccess);
