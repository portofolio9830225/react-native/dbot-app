import { Text, View, StyleSheet, TextInput } from "react-native";
import React from "react";

import { LayoutView } from "../component/Layout";
import { Foundation } from "@expo/vector-icons";
import Typography from "../component/Typography";
import { accent } from "../utils/ColorPicker";
import CustomButton from "../component/CustomButton";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { forgotPassword } from "../store/actions/authAction";
import { connect } from "react-redux";
import isEmpty from "../utils/isEmpty";

export class ForgotPassword extends React.Component {
	state = {
		email: "",
	};

	componentDidMount() {
		if (!isEmpty(this.props.navigation.state.params)) {
			this.setState({
				email: this.props.navigation.state.params.email,
			});
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevProps.alert.redirect !== this.props.alert.redirect) {
			if (this.props.alert.redirect === "forgotPassword") {
				this.handlePage("SignIn", { isRegister: false, email: this.state.email })();
			}
		}
	}

	handleChange = name => value => {
		this.setState({
			[name]: value,
		});
	};

	handlePage = (page, params) => () => {
		this.props.navigation.navigate(page, params);
	};

	handleSubmit = () => {
		this.props.forgotPassword({ email: this.state.email });
	};

	render() {
		const { email } = this.state;

		return (
			<LayoutView>
				<View style={styles.container}>
					<Foundation name="key" size={86} color={accent} />
					<Typography type="largetitle" style={[styles.pageTitle, { marginBottom: 57 }]}>
						Reset Your StoreUp ID Password
					</Typography>
					<Typography type="headline" style={[styles.pageText, { marginBottom: 42 }]}>
						Tell us your email address and we'll send you an email with a link to reset your password.
					</Typography>
					<TextInput
						style={styles.input}
						onChangeText={this.handleChange("email")}
						value={email}
						placeholder="Email"
						keyboardType="email-address"
					/>
					<CustomButton onPress={this.handleSubmit} style={styles.button}>
						Continue
					</CustomButton>
					<View
						style={{
							flexDirection: "row",
							position: "absolute",
							bottom: 40,
							alignItems: "center",
						}}
					>
						{/* <View style={{ flexDirection: "row", width: "100%", justifyContent: "center" }}> */}
						<View>
							<TouchableWithoutFeedback onPress={this.handlePage("SignIn", { isRegister: false, email })}>
								<Text style={styles.buttonText}>Log In</Text>
							</TouchableWithoutFeedback>
						</View>
						<Text style={{ fontSize: 16 }}> or </Text>
						<View>
							<TouchableWithoutFeedback onPress={this.handlePage("SignIn", { isRegister: true, email })}>
								<Text style={styles.buttonText}>Register</Text>
							</TouchableWithoutFeedback>
						</View>
					</View>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		paddingBottom: 100,
		//backgroundColor: "red",
		justifyContent: "center",
		position: "relative",
	},
	pageTitle: {
		width: "100%",
		fontFamily: "bold",
		textAlign: "center",
	},
	pageText: {
		width: "100%",
		textAlign: "center",
	},
	input: {
		width: "100%",
		backgroundColor: "#fff",
		paddingHorizontal: 16,
		height: 50,
		borderRadius: 10,
		marginBottom: 15,
		fontSize: 16,
	},
	button: {
		justifyContent: "center",
		width: "100%",
		// marginBottom: 100,
	},
	buttonText: {
		color: "#0D67F1",
		fontSize: 16,
		fontFamily: "regular",
	},
});

const mapStateToProps = state => ({
	alert: state.alert,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, { forgotPassword })(ForgotPassword);
