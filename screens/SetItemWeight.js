import React from "react";
import { StyleSheet, View, Keyboard, ScrollView, Platform } from "react-native";
import { connect } from "react-redux";
import { black } from "../utils/ColorPicker";

import { clearNoti, customNoti } from "../store/actions/alertAction";
import { Button } from "react-native-paper";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import isEmpty from "../utils/isEmpty";
import NavHeader from "../component/NavHeader";
import ItemWeightBox from "../component/ItemWeightBox";
import langSelector from "../utils/langSelector";
import { setItemWeight } from "../store/actions/itemAction";

class ChangeEmail extends React.Component {
	state = {
		items: [],
	};

	componentDidMount() {
		this.setState({
			items: this.props.navigation.state.params.items,
		});
	}

	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.alert.redirect) && this.props.alert.redirect === "setItemWeight") {
			this.props.navigation.replace(this.props.navigation.state.params.route, {
				isSetWeight: true,
				others: this.props.navigation.state.params.others,
			});
		}
	}

	handleBack = () => {
		this.props.navigation.replace(this.props.navigation.state.params.route, {
			isSetWeight: false,
			others: this.props.navigation.state.params.others,
		});
	};

	handleText = name => value => {
		this.setState({
			[name]: value,
		});
	};

	handleWeight = (id, weight) => {
		let arr = [...this.state.items];
		let itm = arr.findIndex(e => e.item_id === id);
		arr[itm] = { ...arr[itm], item_weight: weight };
		this.setState({
			items: arr,
		});
	};

	handleSubmit = () => {
		Keyboard.dismiss();
		let isFound = this.state.items.find(e => parseFloat(e.item_weight) <= 0);
		if (isFound) {
			this.props.customNoti("Weight must be more than 0kg", "error");
		} else {
			this.props.setItemWeight({ arr: this.state.items }, this.props.auth.token);
		}
	};

	render() {
		const { items } = this.state;
		const lang = langSelector.SetItemWeight.find(e => e.lang === this.props.preference.lang).data;

		return (
			<LayoutView>
				<NavHeader onBack={this.handleBack} actionText="Done" onPress={this.handleSubmit} />
				<ScrollView
					showsVerticalScrollIndicator={false}
					contentContainerStyle={{ width: "100%", paddingBottom: 30 }}
				>
					<View style={styles.container}>
						<PageTitle fontSize={19}>{lang.title}</PageTitle>
						{items.map((e, i) => {
							return (
								<ItemWeightBox
									key={i}
									name={e.item_name}
									img={e.image_url}
									id={e.item_id}
									val={e.item_weight}
									onChange={this.handleWeight}
								/>
							);
						})}
					</View>
				</ScrollView>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		// alignItems: "center"
	},

	button: {
		width: "100%",
		marginTop: 20,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	profile: state.profile,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	setItemWeight,
	clearNoti,
	customNoti,
})(ChangeEmail);
