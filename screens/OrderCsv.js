import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { connect } from "react-redux";
import * as WebBrowser from "expo-web-browser";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import NavHeader from "../component/NavHeader";

import { accent, grey, mainBgColor } from "../utils/ColorPicker";
import { Feather, MaterialIcons } from "@expo/vector-icons";
import { Button } from "react-native-paper";
import { getOrdersCsv } from "../store/actions/orderAction";
import isEmpty from "../utils/isEmpty";
import { Calendar, CalendarList } from "react-native-calendars";
import getDateRange from "../utils/getDateRange";
import langSelector from "../utils/langSelector";
import { XANO_API_LINK } from "../utils/key";

class OrdersCsv extends React.Component {
	state = {
		sDate: null,
		eDate: null,
		csvModal: false,
		marked: {},
		maxDate: "",
	};

	componentDidMount() {
		let today = new Date();
		let max = new Date(today.getFullYear(), today.getMonth(), today.getDate());
		this.setState({
			maxDate: max.getTime(),
		});
	}
	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.alert.status) && this.props.alert.status === "success") {
			this.handleBack();
		}
	}

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handleDate = day => {
		if (isEmpty(this.state.sDate)) {
			this.setState({
				sDate: day.dateString,
				marked: {
					[day.dateString]: {
						selected: true,
						selectedColor: accent,
					},
				},
			});
		} else {
			let s = new Date(this.state.sDate);
			let selected = new Date(day.dateString);
			if (selected <= s || !isEmpty(this.state.eDate)) {
				this.setState({
					sDate: day.dateString,
					eDate: null,
					marked: {
						[day.dateString]: {
							selected: true,
							selectedColor: accent,
						},
					},
				});
			} else {
				let dList = getDateRange(s, selected);
				let mList = {};
				dList.map(e => {
					mList = {
						...mList,
						[e]: {
							selected: true,
							selectedColor: accent,
						},
					};
				});
				this.setState({
					eDate: day.dateString,
					marked: mList,
				});
			}
		}
	};

	handleCsvModal = state => () => {
		this.setState({
			csvModal: state,
		});
	};

	handleDownload = () => {
		let data = {
			start: this.state.sDate,
			end: this.state.eDate || this.state.sDate,
			business_id: this.props.profile.store.id,
		};
		WebBrowser.openBrowserAsync(
			`${XANO_API_LINK}/csv/order?start_date=${data.start}&end_date=${data.end}&business_id=${data.business_id}`
		);
	};

	render() {
		const { marked, sDate, eDate, maxDate } = this.state;
		const lang = langSelector.OrderCsv.find(e => e.lang === this.props.preference.lang).data;

		return (
			<LayoutView>
				<NavHeader
					onBack={this.handleBack}
					actionText="Done"
					onPress={this.handleDownload}
					title="Select Date"
				/>

				<View style={styles.container}>
					{/* <Text
						style={{
							fontFamily: "light",
							width: "100%",
							marginBottom: 10,
							marginTop: 8,
						}}
					>
						{lang.desc}
					</Text> */}
					{!isEmpty(maxDate) && (
						<View style={{ width: "100%", flex: 1, justifyContent: "space-between" }}>
							<CalendarList
								futureScrollRange={0}
								theme={{
									calendarBackground: mainBgColor,
									todayTextColor: accent,
									textDayFontFamily: "regular",
									textDayHeaderFontFamily: "regular",
									textMonthFontFamily: "bold",
								}}
								maxDate={maxDate}
								markedDates={marked}
								onDayPress={this.handleDate}
								// renderArrow={dir => {
								// 	if (dir == "left") {
								// 		return <Feather name="chevron-left" size={20} color={grey} />;
								// 	}

								// 	if (dir == "right") {
								// 		return <Feather name="chevron-right" size={20} color={grey} />;
								// 	}
								// }}
							/>
						</View>
					)}
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		height: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	profile: state.profile,
	preference: state.preference,
	alert: state.alert,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, { getOrdersCsv })(OrdersCsv);
