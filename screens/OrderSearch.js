import React from "react";
import { View, FlatList, TextInput } from "react-native";

import { LayoutView } from "../component/Layout";
import NavHeader from "../component/NavHeader";

import DateConverter from "../utils/DateConverter";
import { greywhite } from "../utils/ColorPicker";

import langSelector from "../utils/langSelector";
import { connect } from "react-redux";
import OrderBox from "../component/OrderBox";

class OrderSearch extends React.Component {
	state = {
		statusCard: {},
		search: "",
		orderList: [
			...this.props.order.orderList.unpaid,
			...this.props.order.orderList.paid,
			// ...this.props.order.orderList.shipped,
			...this.props.order.orderList.completed,
			...this.props.order.orderList.rejected,
		],
	};

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handleText = name => val => {
		this.setState({
			[name]: val,
		});
	};

	handlePage = page => () => {
		this.props.navigation.push(page);
	};

	handleItem = oID => () => {
		this.props.navigation.replace("OrderDetail", { oID, isAction: true });
	};

	render() {
		const { search, orderList, statusCard } = this.state;
		const isSearch = search.length >= 2;
		const lang = langSelector.OrderSearch.find(e => e.lang === this.props.preference.lang).data;

		return (
			<LayoutView>
				<NavHeader onBack={this.handleBack} />

				<View
					style={{
						width: "100%",
						alignItems: "flex-start",
						alignSelf: "center",
					}}
				>
					<TextInput
						style={{
							//height: 50,
							width: "100%",
							borderBottomColor: greywhite,
							borderBottomWidth: 1,
							paddingHorizontal: 10,
							paddingVertical: 5,
							fontFamily: "medium",
							fontSize: 33,
						}}
						placeholder={lang.searchText}
						onChangeText={this.handleText("search")}
						value={search}
					/>
				</View>

				<FlatList
					showsVerticalScrollIndicator={false}
					style={{ width: "100%", alignSelf: "center" }}
					contentContainerStyle={{ width: "100%", paddingVertical: 25 }}
					data={isSearch ? orderList.filter(p => p.order_id.includes(search)) : []}
					keyExtractor={e => e.order_id}
					renderItem={({ item, index, separators }) => {
						return (
							<OrderBox
								isUnread={!item.is_open}
								statusType={1}
								key={index}
								oTitle={`${lang.orderID}:`}
								id={item.order_id}
								name={item.customer_name}
								date={DateConverter(item.created_ts)}
								price={item.total_price}
								quantity={item.item_count}
								status={item.status}
								onPress={this.handleItem(item.order_id)}
								onPressStatus={this.handleItem(item.order_id)}
								pMethod={item.payment.method_name}
								// onPressStatus={() => {
								//   this.setState({
								//     statusCard: { id: item.order_id, status: item.status },
								//   });
								// }}
							/>
						);
					}}
				/>
			</LayoutView>
		);
	}
}

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	order: state.order,
	alert: state.alert,
});

export default connect(mapStateToProps)(OrderSearch);
