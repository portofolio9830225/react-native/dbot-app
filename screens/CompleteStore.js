import React from "react";

import { StyleSheet, Text, View, Keyboard } from "react-native";
import * as WebBrowser from "expo-web-browser";

import { withNavigationFocus } from "react-navigation";
import { connect } from "react-redux";
import { setLoading } from "../store/actions/loadingAction";
import { customNoti } from "../store/actions/alertAction";

import { Button, TouchableRipple } from "react-native-paper";

import { LayoutView } from "../component/Layout";

import isEmpty from "../utils/isEmpty";
import { accent, grey, mainBgColor } from "../utils/ColorPicker";
import TextField from "../component/TextField";
import { Ionicons } from "@expo/vector-icons";
import { editStoreDetails } from "../store/actions/profileAction";
import Timeline from "react-native-timeline-flatlist";
import MainBottomBar from "../AppNavigator/common/MainBottomBar";
import CustomButton from "../component/CustomButton";

class CompleteStore extends React.Component {
	state = {
		activeStep: 2,
	};

	updateDataToPage = () => {
		if (!isEmpty(this.props.item.userItem)) {
			this.setState({
				activeStep: !isEmpty(this.props.profile.payout.bank_acc) ? 4 : 3,
			});
		}
	};

	componentDidMount() {
		this.updateDataToPage();
	}

	componentDidUpdate(prevProps, prevState) {
		if (
			(!prevProps.isFocused && this.props.isFocused) ||
			(!isEmpty(this.props.item.userItem) && isEmpty(prevProps.item.userItem)) ||
			(isEmpty(prevProps.profile.payout) &&
				!isEmpty(this.props.profile.payout) &&
				!isEmpty(this.props.profile.payout.bank_acc))
		) {
			this.updateDataToPage();
		}
		// if (
		// 	isEmpty(prevProps.profile.payout) &&
		// 	!isEmpty(this.props.profile.payout) &&
		// 	!isEmpty(this.props.profile.payout.bank_acc)
		// ) {
		// 	this.props.navigation.navigate("Private");
		// }
	}

	handleChange = name => value => {
		this.setState({
			[name]: value,
		});
	};

	handleProduct = () => {
		this.props.navigation.push("ProductStarter", { id: null, firstProduct: true });
	};

	handlePayout = () => {
		this.props.navigation.push("BankDetailsStarter");
	};

	handleSubmit = () => {
		Keyboard.dismiss();
		if (this.state.isCompleted) {
			this.props.navigation.navigate("Private");
		} else {
			let { store } = this.props.profile;
			let data = {
				name: this.state.name,
				phone: this.state.phone,
				subdomain: store.subdomain.toLowerCase(),
				logo: store.logo,
				registered_name: store.registered_name,
				ssm_no: store.ssm_no,
				email: store.email,
				description: store.description,
			};

			this.props.editStoreDetails(store.id, data, this.props.auth.token);
		}
	};

	render() {
		const { activeStep } = this.state;

		const RegisterStep = props => {
			let { data, active } = props;

			return (
				<View style={styles.stepContainer}>
					{data.map((d, i) => {
						let isDone = i + 1 < active;
						let isActive = i + 1 === active;
						return (
							<View
								style={[
									styles.stepBox,
									{ borderLeftColor: i + 1 >= data.length ? "transparent" : accent },
								]}
								key={i}
							>
								<View style={[styles.stepIconBox, { backgroundColor: isDone ? accent : mainBgColor }]}>
									{i + 1 < active ? (
										<Ionicons name="checkmark-sharp" size={30} color={mainBgColor} />
									) : (
										<Text style={{ fontFamily: "regular", color: accent, fontSize: 21 }}>
											{i + 1}
										</Text>
									)}
								</View>
								<View style={styles.stepContent}>
									<Text style={styles.stepContentTitle}>
										{isActive ? d.secondaryTitle : isDone ? d.doneTitle : d.title}
									</Text>
									{isActive || d.contentFix ? (
										<View style={{ width: "100%" }}>{d.content}</View>
									) : null}
								</View>
							</View>
						);
					})}
				</View>
			);
		};

		return (
			<LayoutView>
				<View style={styles.container}>
					<Ionicons name="color-wand" size={66} color="#0D65FB" style={{ alignSelf: "flex-start" }} />
					<Text style={styles.pageTitle}>Complete your store setup...</Text>
					<Text style={styles.pageDesc}>
						Sellers who add more than 25 products are likely to get more orders. Add all your products to
						your store now.
					</Text>
					<RegisterStep
						active={activeStep}
						data={[
							{
								title: "Your online store is ready!",
								doneTitle: "Your online store is ready!",
								content: (
									<TouchableRipple
										onPress={() => {
											WebBrowser.openBrowserAsync(
												`https://${this.props.profile.store.subdomain}.storeup.site`
											);
										}}
									>
										<Text style={{ color: accent, textDecorationLine: "underline" }}>
											Visit your online store
										</Text>
									</TouchableRipple>
								),
								contentFix: true,
							},
							{
								title: "Add your first product",
								secondaryTitle: "Add your first product",
								doneTitle: "First product added",
								content: (
									<CustomButton style={styles.stepContentButton} onPress={this.handleProduct}>
										Add a product
									</CustomButton>
								),
							},
							{
								title: "Payout details",
								secondaryTitle: "Set your payout details",
								doneTitle: "Payout details submitted",
								content: (
									<CustomButton style={styles.stepContentButton} onPress={this.handlePayout}>
										Set payout details
									</CustomButton>
								),
							},
						]}
					/>
					{activeStep >= 4 ? (
						<CustomButton
							onPress={() => {
								this.props.navigation.navigate("Private");
							}}
							style={{ alignSelf: "flex-start" }}
						>
							Go to dashboard
						</CustomButton>
					) : null}
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		paddingBottom: 100,
		//backgroundColor: "red",
		justifyContent: "center",
	},
	pageTitle: {
		width: "100%",
		fontSize: 32,
		fontFamily: "medium",
		marginVertical: 5,
		//textAlign: "center",
	},
	pageDesc: {
		width: "100%",
		fontSize: 16,
		fontFamily: "regular",
		//textAlign: "center",
	},
	stepContainer: {
		marginTop: 30,

		width: "100%",
		flexDirection: "column",
		justifyContent: "flex-start",
		alignItems: "flex-start",
	},
	stepBox: {
		position: "relative",
		width: "100%",
		borderLeftWidth: 1.5,
		minHeight: 100,
		paddingBottom: 40,
		marginLeft: 20,
		paddingLeft: 36,
	},
	stepIconBox: {
		position: "absolute",
		top: -5,
		left: -20,
		zIndex: 100,
		width: 40,
		height: 40,
		borderRadius: 20,
		alignItems: "center",
		justifyContent: "center",
		borderWidth: 1.5,
		borderColor: accent,
		borderStyle: "solid",
	},
	stepContent: {
		width: "100%",
		alignSelf: "flex-end",
	},
	stepContentTitle: {
		fontSize: 15,
		fontFamily: "medium",
		marginBottom: 10,
	},
	stepContentButton: {
		justifyContent: "center",
		marginTop: 10,
		alignSelf: "flex-start",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	profile: state.profile,
	item: state.item,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	editStoreDetails,
	setLoading,
	customNoti,
})(withNavigationFocus(CompleteStore));
