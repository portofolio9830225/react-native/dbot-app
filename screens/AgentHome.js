import React from "react";
import { StyleSheet, View, Text, RefreshControl, FlatList } from "react-native";
import { connect } from "react-redux";
import { setRefresh } from "../store/actions/loadingAction";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import NavHeader from "../component/NavHeader";
import { withNavigationFocus } from "react-navigation";
import langSelector from "../utils/langSelector";
import { Entypo } from "@expo/vector-icons";
import { grey, greyblack, greylight, transparent } from "../utils/ColorPicker";
import { Card, TouchableRipple } from "react-native-paper";
import { getAllAgentPackages } from "../store/actions/agentAction";

class AgentHome extends React.Component {
	state = {
		package: [
			{
				name: "Test1",
				price: 30.5,
			},
			{
				name: "Test2",
				price: 100,
			},
		],
	};

	componentDidMount() {
		this.props.getAllAgentPackages(this.props.agent.hqList[0].business_id, this.props.auth.token);
	}

	handleBack = () => {
		this.props.navigation.navigate("Settings");
	};

	handlePage =
		(route, params = {}) =>
		() => {
			this.props.navigation.push(route, params);
		};

	render() {
		const { packageList } = this.props.agent;
		const lang = langSelector.AgentHome.find(e => e.lang === this.props.preference.lang).data;

		const PackageBox = ({ name, price, onPress }) => {
			return (
				<TouchableRipple
					rippleColor={transparent}
					onPress={onPress}
					style={{ marginVertical: 15, width: "100%", alignSelf: "center" }}
				>
					<Card elevation={3}>
						<View
							style={{
								padding: 15,
							}}
						>
							<View
								style={{
									flexDirection: "row",
									alignItems: "center",
									justifyContent: "space-between",
								}}
							>
								<Text
									style={{
										fontFamily: "medium",
										fontSize: 17,
										color: greyblack,
									}}
								>
									{name}
								</Text>
								<Text style={{ fontFamily: "medium", fontSize: 17 }}>RM{price.toFixed(2)}</Text>
							</View>
							<View
								style={{
									flexDirection: "row",
									alignItems: "center",
									justifyContent: "flex-end",
									marginTop: 10,
								}}
							>
								<Text
									style={{
										fontSize: 11,
										fontFamily: "regular",
										color: grey,
									}}
								>
									{lang.seeMore}
								</Text>
								<Entypo name="chevron-right" color={greylight} />
							</View>
						</View>
					</Card>
				</TouchableRipple>
			);
		};

		return (
			<LayoutView>
				<NavHeader />
				<FlatList
					refreshControl={
						<RefreshControl
							refreshing={this.props.loading.refresh}
							onRefresh={() => {
								this.props.setRefresh(true);
							}}
						/>
					}
					showsVerticalScrollIndicator={false}
					style={{ width: "100%", alignSelf: "center", position: "relative" }}
					contentContainerStyle={{
						width: "100%",
						paddingVertical: 15,
					}}
					ListHeaderComponent={
						<View
							style={{
								width: "100%",
								alignItems: "flex-start",
								alignSelf: "center",
								marginBottom: 10,
							}}
						>
							<PageTitle>{lang.title}</PageTitle>
						</View>
					}
					data={packageList}
					keyExtractor={e => e.package_id}
					renderItem={({ item, index, separators }) => {
						return (
							<PackageBox
								key={index}
								name={item.package_name}
								price={item.package_price}
								onPress={this.handlePage("AgentPackageDetail", item)}
							/>
						);
					}}
				/>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	agent: state.agent,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, { getAllAgentPackages, setRefresh })(withNavigationFocus(AgentHome));
