import React from "react";

import AsyncStorage from "@react-native-async-storage/async-storage";
import { StyleSheet, Text, View, Keyboard } from "react-native";
import * as WebBrowser from "expo-web-browser";

import { connect } from "react-redux";
import { loginUser, getBusinessList, getBusinessToken, logoutUser } from "../store/actions/authAction";
import { setLoading } from "../store/actions/loadingAction";
import { customNoti } from "../store/actions/alertAction";
import { getRegProgress } from "../store/actions/profileAction";

import { Button } from "react-native-paper";

import { LayoutView } from "../component/Layout";

import PageTitle from "../component/PageTitle";
import registerPushNoti from "../utils/registerPushNoti";
import NavHeader from "../component/NavHeader";
import isEmpty from "../utils/isEmpty";
import { grey } from "../utils/ColorPicker";
import PasswordField from "../component/PasswordField";
import TextField from "../component/TextField";
import CustomButton from "../component/CustomButton";

class SignIn extends React.Component {
	state = {
		email: "",
		pass: "",
		isAppUsed: null,
	};

	async componentDidMount() {
		await AsyncStorage.getItem("@dbot:language").then(data => {
			this.setState({
				isAppUsed: !isEmpty(data),
			});
		});

		if (!isEmpty(this.props.navigation.state.params)) {
			this.setState({
				email: this.props.navigation.state.params.email,
			});
		}
	}

	// async componentWillReceiveProps(nextProps) {

	// }

	async componentDidUpdate(prevProps, prevState) {
		// if (this.props.auth.isAuthenticated && !prevProps.auth.isAuthenticated) {
		//   this.props.getBusinessList(this.props.auth.userToken);
		// }
		// if (
		//   !isEmpty(this.props.auth.businessList) &&
		//   isEmpty(prevProps.auth.businessList)
		// ) {
		//   let data = {
		//     business_id: this.props.auth.businessList[0].business_id,
		//   };
		//   this.props.getBusinessToken(data, this.props.auth.userToken);
		// }
		if (!isEmpty(this.props.auth.token) && isEmpty(prevProps.auth.token)) {
			AsyncStorage.setItem("@dbot:businessToken", this.props.auth.token);
			AsyncStorage.setItem("@dbot:refreshToken", this.props.auth.refreshToken);

			registerPushNoti(this.props.auth.token);
		}
		if (this.props.auth.regNotiTry && !prevProps.auth.regNotiTry) {
			this.props.getRegProgress(this.props.auth.token);
		}

		if (!isEmpty(this.props.profile.regStatus) && isEmpty(prevProps.profile.regStatus)) {
			if (!isEmpty(this.props.alert.notiScreen)) {
				this.props.navigation.navigate(this.props.alert.notiScreen, this.props.alert.notiParams);
			} else {
				let { regStatus } = this.props.profile;
				// if (!regStatus.store_completed || !regStatus.payout_completed) {
				if (!regStatus.store_completed) {
					this.props.navigation.navigate("CompleteReg");
				} else {
					this.props.navigation.navigate("Private");
				}
			}
		}
		if (!isEmpty(prevProps.auth.token) && isEmpty(this.props.auth.token)) {
			this.props.navigation.navigate("SignIn");
		}
	}

	componentWillUnmount() {
		this.props.setLoading(false);
	}

	handleChange = name => value => {
		this.setState({
			[name]: value,
		});
	};

	handlePage = name => () => {
		let params = {
			email: this.state.email.toLowerCase(),
		};
		this.props.navigation.navigate(name, params);
	};

	handleURL = url => () => {
		WebBrowser.openBrowserAsync(url);
		// Linking.openURL(url);
	};

	handleSubmit = () => {
		Keyboard.dismiss();
		let data = {
			email: this.state.email.toLowerCase(),
			password: this.state.pass,
		};
		this.props.loginUser(data);
	};

	handleGoogle = async () => {
		// let config={}
		// let { type, accessToken, user } = await Google.logInAsync(config);
		// try {
		//   await GoogleSignIn.askForPlayServicesAsync();
		//   const { type, user } = await GoogleSignIn.signInAsync();
		// } catch ({ message }) {
		//   alert("login: Error:" + message);
		// }
	};

	render() {
		const { email, pass, isAppUsed } = this.state;

		return (
			<LayoutView>
				<NavHeader
					actions={[
						{
							onPress: this.handleURL("https://app.storeup.io/registration"),
							icon: <Text style={styles.signUpText}>Sign Up</Text>,
						},
					]}
				/>

				<View style={styles.container}>
					{!isEmpty(isAppUsed) && <PageTitle>{!isAppUsed ? "Sign In" : "Welcome back"} </PageTitle>}

					<TextField
						keyboardType="email-address"
						mode="outlined"
						style={styles.input}
						label="Email"
						value={email}
						onChangeText={this.handleChange("email")}
						autoCapitalize="none"
					/>

					<PasswordField
						mode="outlined"
						style={styles.input}
						label="Password"
						value={pass}
						onChangeText={this.handleChange("pass")}
					/>

					<CustomButton style={styles.button} onPress={this.handleSubmit}>
						Sign in
					</CustomButton>

					<Text style={styles.forgotText} onPress={this.handleURL("https://app.storeup.io/forgot-password")}>
						Forgot password?
					</Text>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		marginTop: 25,
		//justifyContent: "center"
	},
	input: {
		width: "100%",
		marginVertical: 10,
	},
	button: {
		justifyContent: "center",
		width: "100%",
		marginTop: 15,
		marginBottom: 10,
	},
	googleLogo: {
		height: 21,
		width: 21,
	},
	signUpText: {
		fontFamily: "regular",
		paddingVertical: 5,
		fontSize: 16,
	},
	forgotText: {
		fontFamily: "light",
		//marginTop: 5,
		paddingVertical: 5,
		fontSize: 14,
		color: grey,
		alignSelf: "flex-start",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	profile: state.profile,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	getRegProgress,
	loginUser,
	getBusinessList,
	getBusinessToken,
	logoutUser,
	setLoading,
	customNoti,
})(SignIn);
