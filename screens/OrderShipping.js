import React from "react";
import { StyleSheet, View, Text, Keyboard, Platform } from "react-native";
import { connect } from "react-redux";

import { getCourierList, setOrderCourier } from "../store/actions/orderAction";
import { customNoti } from "../store/actions/alertAction";
import { setLoading } from "../store/actions/loadingAction";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import isEmpty from "../utils/isEmpty";
import NavHeader from "../component/NavHeader";
import { black, grey, greyblack } from "../utils/ColorPicker";
import TextField from "../component/TextField";
import langSelector from "../utils/langSelector";

class OrderShipping extends React.Component {
	state = {
		courierName: "",
		courierCode: "",
		trackNum: "",
		isSetShipped: false,
		isEdit: null,
	};

	componentDidMount() {
		this.props.setLoading(true);
		this.props.getCourierList();
		let { params } = this.props.navigation.state;
		this.setState({
			courierName: params.courier_name || "",
			courierCode: params.courier_code || "",
			trackNum: params.tracking_no || "",
			isEdit: !isEmpty(params),
		});
	}

	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.alert.redirect) && !isEmpty(this.props.alert.redirect)) {
			if (this.props.alert.redirect === "setCourier") {
				this.handleBack();
			}
		}
	}

	handleBack = () => {
		this.props.navigation.pop();
	};

	handleText = name => value => {
		this.setState({
			[name]: value,
		});
	};

	handleCourier = (name, code) => {
		this.setState({
			courierName: name,
			courierCode: code,
		});
	};

	handleSetShipped = () => {
		this.setState({
			isSetShipped: !this.state.isSetShipped,
		});
	};

	handleSubmit = () => {
		Keyboard.dismiss();

		let data = {
			courier_code: this.state.courierCode,
			tracking_no: this.state.trackNum,
		};

		this.props.setOrderCourier(
			this.props.order.orderDetails.order_id,
			data,
			this.props.auth.token,
			this.state.isSetShipped
		);
	};

	render() {
		const { courierName, trackNum, isEdit, isSetShipped } = this.state;
		const { courierList } = this.props.order;
		const lang = langSelector.OrderShipping.find(e => e.lang === this.props.preference.lang).data;

		return (
			<LayoutView>
				<NavHeader onBack={this.handleBack} onPress={this.handleSubmit} actionText="Done" />

				<View style={styles.container}>
					<PageTitle>{lang.title}</PageTitle>

					<TextField
						type="select"
						style={styles.textInput}
						selectorList={courierList}
						dialogTitle={lang.courierDesc}
						label={lang.courierTitle}
						onSelect={this.handleCourier}
						value={!isEmpty(courierName) ? courierName : lang.courierDesc}
						errorKey="courier_code"
					/>
					<TextField
						onChangeText={this.handleText("trackNum")}
						style={styles.textInput}
						mode="outlined"
						label={lang.trackTitle}
						placeholder={lang.trackTitle}
						value={trackNum}
						errorKey="tracking_no"
					/>
					{/* {!isEdit && (
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginBottom: 10,
              }}
            >
              <Checkbox.Android
                status={isSetShipped ? "checked" : "unchecked"}
                onPress={this.handleSetShipped}
              />
              <Text style={{ fontFamily: "regular", marginLeft: 5 }}>
                {lang.setShipped}
              </Text>
            </View>
          )} */}
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		// alignItems: "center"
	},
	textInput: {
		marginVertical: 10,
	},
	sectionTitle: {
		fontFamily: "light",
		marginTop: 25,
		color: greyblack,
		textTransform: "uppercase",
	},
	saveButton: {
		marginTop: 20,
		alignSelf: "flex-end",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	order: state.order,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	getCourierList,
	setOrderCourier,
	customNoti,
	setLoading,
})(OrderShipping);
