import React from "react";
import { StyleSheet, View, Text, Keyboard, Platform } from "react-native";
import { connect } from "react-redux";

import { getUser, editUser } from "../store/actions/profileAction";
import { customNoti } from "../store/actions/alertAction";

import { TextInput, Button } from "react-native-paper";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import isEmpty from "../utils/isEmpty";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import NavHeader from "../component/NavHeader";
import TextField from "../component/TextField";
import { greyblack, black, green, grey, greywhite } from "../utils/ColorPicker";
import langSelector from "../utils/langSelector";
import { verifyEmail } from "../store/actions/profileAction";
import { Ionicons } from "@expo/vector-icons";
import { SectionBox, SectionDivider } from "../component/SectionComponents";
import AlertDialog from "../component/AlertDialog";

class PersonalDetails extends React.Component {
	state = {
		name: "",
		phone: "",
		email: "",
		password: "",
		password_dumy: "InonityStoreUp2021",
		isPasswordChange: false,
		oldPass: "",
		dialogPassword: "",
		dialogSuccessPassword: "",
	};

	updateData = () => {
		let { name, phone, email } = this.props.profile.user;

		this.setState({
			name: name,
			phone: phone,
			email: email,
		});
	};

	componentDidMount() {
		this.updateData();
	}

	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.profile.user) && !isEmpty(this.props.profile.user)) {
			this.updateData();
		}
		if (isEmpty(prevProps.alert.redirect) && !isEmpty(this.props.alert.redirect)) {
			if (this.props.alert.redirect === "UserSaved") {
				this.handleBack();
			}
		}
	}

	handleBack = () => {
		this.props.navigation.pop();
	};

	handlePage = page => () => {
		this.props.navigation.push(page);
	};

	handleText = name => value => {
		this.setState({
			[name]: value,
		});
	};

	handleTextPass = name => value => {
		// this.setState({
		// 	isPasswordChange: true,
		// });

		this.setState({
			[name]: value,
		});
	};

	handleVerifyEmail = () => {
		this.props.verifyEmail(this.props.auth.token);
	};

	handleSubmit = () => {
		this.setState(
			{
				dialogPassword: false,
			},
			() => {
				Keyboard.dismiss();
				if (isEmpty(this.state.name)) {
					this.props.customNoti("Name must be included", "error");
				} else if (isEmpty(this.state.email)) {
					this.props.customNoti("Email must be included", "error");
				} else {
					let data = {
						name: this.state.name,
						phone: this.state.phone,
						email: this.state.email.toLowerCase(),
						old_password: this.state.oldPass,
						new_password: this.state.isPasswordChange ? this.state.password : null,
					};

					this.props.editUser(this.props.profile.user.id, data, this.props.auth.token);
					this.setState({
						oldPass: "",
					});
				}
			}
		);
	};

	handleDialog = state => () => {
		Keyboard.dismiss();
		this.setState({
			dialogPassword: state,
		});
	};

	handleDialogSuccessPassword = state => () => {
		this.setState({
			dialogPassword: false,
		});

		this.setState({
			dialogSuccessPassword: state,
		});
	};

	handlePassword = () => () => {
		// console.log(123);

		this.setState({
			isPasswordChange: true,
		});
	};

	render() {
		const {
			name,
			phone,
			email,
			dialogPassword,
			password,
			dialogSuccessPassword,
			password_dumy,
			oldPass,
			isPasswordChange,
		} = this.state;
		const lang = langSelector.PersonalDetails.find(e => e.lang === this.props.preference.lang).data;
		const isDone = !isEmpty(email) && !isEmpty(name);

		return (
			<LayoutView>
				<NavHeader
					title="Account Details"
					onBack={this.handleBack}
					// actionText="Done"
					// onPress={isDone ? this.handleSubmit : null}
					actionText="Done"
					onPress={this.handleDialog(true)}
				/>

				<AlertDialog
					actions={[
						{
							title: "Cancel",
							onPress: this.handleDialog(false),
						},
						{
							title: "OK",
							onPress: this.handleSubmit,
						},
					]}
					title="StoreUp ID Password"
					desc={`Enter your current StoreUp ID password for ${this.props.profile.user.email} to proceed`}
					active={dialogPassword}
					onDismiss={this.handleDialog(false)}
					inputPassword={true}
					passVal={oldPass}
					onPassChange={val => {
						this.setState({ oldPass: val });
					}}
				/>

				<AlertDialog
					actions={[
						{
							title: "OK",
							onPress: this.handleDialogSuccessPassword(false),
						},
					]}
					title="Susccessful"
					desc="Your account details updated."
					active={dialogSuccessPassword}
					onDismiss={this.handleDialogSuccessPassword(false)}
				/>

				<View style={styles.container}>
					<SectionBox>
						<TextField
							style={styles.textInput}
							inputStyle={{ paddingHorizontal: 0 }}
							mode="outlined"
							label="Name"
							value={name}
							onChangeText={this.handleText("name")}
							errorKey="name"
						/>
						<SectionDivider />
						<TextField
							keyboardType="email-address"
							inputStyle={{ paddingHorizontal: 0 }}
							style={styles.textInput}
							mode="outlined"
							label="Email"
							value={email}
							onChangeText={this.handleText("email")}
						/>
						<SectionDivider />
						<TextField
							inputStyle={{ paddingHorizontal: 0 }}
							style={styles.textInput}
							mode="outlined"
							label="Password"
							type={isPasswordChange ? undefined : "dummy"}
							value={isPasswordChange ? password : password_dumy}
							secureTextEntry
							onChangeText={isPasswordChange ? this.handleTextPass("password") : null}
							onPressInput={!isPasswordChange ? this.handlePassword() : null}
						/>
					</SectionBox>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		paddingTop: 15,
		// alignItems: "center"
	},
	sectionBox: {
		marginTop: 15,
		width: "100%",
		backgroundColor: "white",
		borderRadius: 10,
		paddingHorizontal: 16,
	},
	textInput: {
		//marginBottom: 15,
		marginVertical: 0,
	},
	dialogActionButton: {
		marginHorizontal: 10,
	},
	note: {
		fontFamily: "light",
		fontSize: 15,
		color: greyblack,
		textAlign: "justify",
		marginBottom: 10,
	},
	saveButton: {
		marginTop: 20,
		alignSelf: "flex-end",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	profile: state.profile,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	getUser,
	editUser,
	verifyEmail,
	customNoti,
})(PersonalDetails);
