import React from "react";
import { StyleSheet, View, Text, Keyboard, Image, ScrollView, Platform } from "react-native";
import { connect } from "react-redux";
import * as ImagePicker from "expo-image-picker";

import { getStoreDetails, editStoreDetails, getSocialMediaList } from "../store/actions/profileAction";
import { customNoti } from "../store/actions/alertAction";

import { Portal, Dialog, Button, List, TouchableRipple, Checkbox, Card } from "react-native-paper";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import isEmpty from "../utils/isEmpty";
import NavHeader from "../component/NavHeader";
import {
	grey,
	greyblack,
	greywhite,
	transparent,
	white,
	black,
	red,
	greylight,
	green,
	accent,
	mainBgColor,
	blackTransparent,
} from "../utils/ColorPicker";

import uriToFile from "../utils/uriToFile";
import handleCopyText from "../utils/handleCopyText";
import TextField from "../component/TextField";

import { getCameraPermissionAsync, getGalleryPermissionAsync } from "../utils/PermissionGetter";
import langSelector from "../utils/langSelector";
import { Entypo, Feather, MaterialCommunityIcons } from "@expo/vector-icons";
import getWidth from "../utils/getWidth";
import { FacebookIcon, InstagramIcon, TiktokIcon, TwitterIcon, YoutubeIcon } from "../assets/VectorIcons";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { imagekitDelete, imagekitUpload } from "../utils/imagekit";
import { setLoading } from "../store/actions/loadingAction";
import Animated, { EasingNode } from "react-native-reanimated";
import PortalSafeView from "../component/PortalSafeView";
import Typography from "../component/Typography";
import { SectionBox, SectionDivider } from "../component/SectionComponents";

const CARD_HEIGHT = 200;

class StoreDetails extends React.Component {
	constructor(props) {
		super(props);
		this.transValue = new Animated.Value(1);
	}

	state = {
		isReady: false,
		isLogoChanged: false,
		logo: null,
		choose: null,
		storeName: "",
		storeDesc: "",
		storeUrl: "",
		phone: "",
		address: "",
		regName: "",
		ssm: "",
		email: "",
		showSold: false,
		fromRegProg: false,
		socMed: [],
		socMedID: null,
		socMedLink: "",
		socMedSelectorName: "",
		socMedSelectorID: 0,
	};

	updateData = () => {
		let {
			logo,
			name,
			phone,
			description,
			subdomain,
			address,
			reg_name,
			ssm_no,
			show_number_sold,
			social_media,
			email,
		} = this.props.profile.store;

		let socMed = [];
		social_media.map(s => {
			let found = this.props.profile.socialMediaList.find(f => f.name.toLowerCase() === s.type.toLowerCase());
			socMed.push({ link: s.link, type: found.name, typeID: found.code, name: found.name });
		});

		this.setState({
			isReady: true,
			logo,
			storeName: name,
			storeDesc: description || "",
			storeUrl: subdomain || "",
			phone: phone || "",
			address: address || "",
			regName: reg_name || "",
			ssm: ssm_no || "",
			showSold: show_number_sold !== 0,
			email,
			socMed,
		});
	};

	componentDidMount() {
		//this.props.getSocialMediaList();
		if (!isEmpty(this.props.profile.store)) {
			this.updateData();
		} else {
			this.props.getStoreDetails(this.props.auth.token);
		}
		if (this.props.navigation.state.params) {
			this.setState({
				fromRegProg: this.props.navigation.state.params.fromRegProg,
			});
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.profile.store) && !isEmpty(this.props.profile.store)) {
			this.updateData();
		}
		if (prevState.choose !== this.state.choose) {
			Animated.timing(this.transValue, {
				toValue: this.state.choose ? 2 : 1,
				duration: 250,
				easing: EasingNode.in, // Easing is an additional import from react-native
				useNativeDriver: true, // To make use of native driver for performance
			}).start();
		}
		if (isEmpty(prevState.socMedID) && !isEmpty(this.state.socMedID)) {
			let findID = this.state.socMedID;
			let found = this.state.socMed.find((e, i) => i + 1 == findID);

			if (!isEmpty(found)) {
				this.setState({
					socMedLink: found.link,
					socMedSelectorName: found.name,
					socMedSelectorID: found.typeID,
				});
			}
		}
		if (!isEmpty(prevState.socMedID) && isEmpty(this.state.socMedID)) {
			this.setState({
				socMedLink: "",
				socMedSelectorName: "",
				socMedSelectorID: 0,
			});
		}
		if (isEmpty(prevProps.alert.redirect) && this.props.alert.redirect === "StoreSaved") {
			this.props.navigation.pop();
		}
	}

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handleText = name => value => {
		this.setState({
			[name]: value,
		});
	};

	handleImageDialog = state => () => {
		Keyboard.dismiss();
		this.setState({
			choose: state,
		});
	};

	handleLogo = newImg => {
		this.setState({
			logo: newImg.uri,
			isLogoChanged: true,
		});
	};

	handleCheckbox = (name, state) => () => {
		this.setState({
			[name]: state,
		});
	};

	pickImageFromGallery = async () => {
		let modal = this.state.choose;
		let status = await getGalleryPermissionAsync();
		if (status === "granted") {
			this.setState({
				choose: null,
			});

			let result = await ImagePicker.launchImageLibraryAsync({
				mediaTypes: ImagePicker.MediaTypeOptions.Images,
				allowsEditing: true,
				aspect: modal === "header" ? [3, 1] : [1, 1],
			});

			if (!result.cancelled) {
				this.handleLogo(result);
			}
		}
	};

	pickImageFromCamera = async () => {
		let status1 = await getCameraPermissionAsync();
		let status2 = await getGalleryPermissionAsync();
		let modal = this.state.choose;

		if (status1 === "granted" && status2 === "granted") {
			this.setState({
				choose: null,
			});

			let result = await ImagePicker.launchCameraAsync({
				mediaTypes: ImagePicker.MediaTypeOptions.Images,
				allowsEditing: true,
				aspect: modal === "header" ? [3, 1] : [1, 1],
			});

			if (!result.cancelled) {
				this.handleLogo(result);
			}
		}
	};

	handleSocMedDialog =
		(id = null) =>
		() => {
			this.setState({
				socMedID: id,
			});
		};

	handleSocMedSelector = (name, code) => {
		this.setState({
			socMedSelectorName: name,
			socMedSelectorID: code,
		});
	};

	handleDeleteSocMed = () => {
		let pos = this.state.socMedID;

		this.setState(
			{
				socMedID: null,
			},
			() => {
				let arr = [...this.state.socMed];
				arr.splice(pos - 1, 1);
				this.setState({
					socMed: arr,
				});
			}
		);
	};

	handleSubmitSocMed = () => {
		let name = this.state.socMedSelectorName;
		let type = this.state.socMedSelectorName.toLowerCase();
		let typeID = this.state.socMedSelectorID;
		let link = this.state.socMedLink;
		let id = this.state.socMedID;
		this.setState(
			{
				socMedID: null,
			},
			() => {
				let list = [...this.state.socMed];

				let fIndex = list.findIndex((e, i) => i + 1 === id);
				let d = { link, type, typeID, name };
				if (fIndex !== -1) {
					list[fIndex] = d;
				} else {
					list.push(d);
				}

				this.setState({
					socMedLink: "",
					socMedSelectorID: 0,
					socMedSelectorName: "",
					socMed: list,
				});
			}
		);
	};

	handleSubmit = async () => {
		Keyboard.dismiss();
		this.props.setLoading(true);
		let {
			isLogoChanged,
			logo,
			storeName,
			storeDesc,
			storeUrl,
			phone,
			regName,
			ssm,
			showSold,
			socMed,
			email,
			socMedSelectorName,
		} = this.state;

		let logoUrl = this.props.profile.store.logo;
		let logoID = this.props.profile.store.imagekit_id;
		if (isLogoChanged) {
			if (!isEmpty(logoID)) {
				imagekitDelete(logoID)
					.then(resImg => {})
					.catch(imgErr => {
						this.props.customNoti("Unknown error (Imagekit delete)", "error");
						this.props.setLoading(false);
					});
			}
			let data = new FormData();
			data.append("file", uriToFile(logo));
			data.append("fileName", `logo_${this.props.profile.store.id}`);
			data.append("folder", `business/${this.props.profile.store.id}`);
			await imagekitUpload(data)
				.then(resImg => {
					logoUrl = resImg.data.url;
					logoID = resImg.data.fileId;
				})
				.catch(imgErr => {
					this.props.setLoading(false);
					this.props.customNoti("Unknown error (Imagekit upload)", "error");
				});
		}

		let socMedSent = [];
		socMed.map((s, i) => {
			socMedSent.push({ link: s.link, number: i + 1, type: s.type.toLowerCase() });
		});

		let dataDB = {
			name: storeName,
			subdomain: storeUrl.toLowerCase(),
			logo: logoUrl,
			imagekit_id: logoID,
			registered_name: regName,
			ssm_no: ssm,
			email: !isEmpty(email) ? email.toLowerCase() : "",
			phone: phone,
			description: storeDesc,
			social_media: socMedSent,
		};

		this.props.editStoreDetails(this.props.profile.store.id, dataDB, this.props.auth.token);
	};

	socialMediaPlatformURL = type => () => {
		switch (type) {
			case 1:
				return "https://facebook.com";

			case 2:
				return "https://twitter.com";

			case 3:
				return "https://www.instagram.com";

			case 4:
				return "https://www.youtube.com/channel";

			case 5:
				return "https://www.tiktok.com";
			default:
				return "";
		}
	};

	render() {
		const {
			choose,
			logo,
			storeName,
			storeDesc,
			phone,
			storeUrl,
			address,
			regName,
			ssm,
			fromRegProg,
			showSold,
			socMed,
			socMedID,
			socMedLink,
			socMedSelectorID,
			socMedSelectorName,
		} = this.state;
		const { regStatus, socialMediaList, store } = this.props.profile;
		const isDone = !isEmpty(storeName) && !isEmpty(storeUrl);
		const lang = langSelector.StoreDetails.find(e => e.lang === this.props.preference.lang).data;

		const CardMove = this.transValue.interpolate({
			inputRange: [1, 2],
			outputRange: [CARD_HEIGHT, 0],
		});

		const DialogButton = ({ text, color, onPress }) => {
			return (
				<TouchableRipple
					rippleColor="transparent"
					style={{
						backgroundColor: greywhite,
						width: "100%",
						borderRadius: 10,
						height: 40,
						alignItems: "center",
						justifyContent: "center",
					}}
					onPress={onPress}
				>
					<Text style={{ fontFamily: "regular", color, textAlign: "center", fontSize: 16 }}>{text}</Text>
				</TouchableRipple>
			);
		};

		const ImageHolder = props => {
			return (
				<TouchableRipple
					rippleColor={transparent}
					onPress={props.onPressLogo}
					style={{
						height: 76,
						width: undefined,
						aspectRatio: 1,
						position: "relative",
						overflow: "hidden",
						borderRadius: 38,
						borderWidth: 1,
						borderColor: greywhite,
						zIndex: 1,
						alignSelf: "center",
					}}
				>
					<>
						<Image
							source={
								!isEmpty(props.logo)
									? { uri: props.logo }
									: require("../assets/image/noBusinessLogo.png")
							}
							style={{
								height: "100%",
								width: undefined,
								aspectRatio: 1,
							}}
						/>
					</>
				</TouchableRipple>
			);
		};

		const SelectInfoBox = props => {
			return (
				<TouchableRipple
					rippleColor={transparent}
					onPress={props.onPress}
					style={{
						width: "100%",
						alignSelf: "center",
					}}
				>
					<View
						style={{
							width: "100%",
							flexDirection: "row",
							alignItems: "center",
							justifyContent: "space-between",
							marginBottom: 15,
						}}
					>
						{/* <View style={{ marginBottom: 20 }}> */}
						<Checkbox.Android status={props.checked ? "checked" : "unchecked"} />
						{/* </View> */}
						<View style={{ width: "85%" }}>
							<Text
								style={{
									fontFamily: props.checked ? "medium" : "regular",
									fontSize: 16,
								}}
							>
								{props.title}
							</Text>
						</View>
					</View>
				</TouchableRipple>
			);
		};

		const SocMedIcon = ({ type }) => {
			switch (type) {
				case 1:
					return <FacebookIcon size={25} color="#4267B2" />;
				case 2:
					return <TwitterIcon size={25} color="#1DA1F2" />;
				case 3:
					return <InstagramIcon size={25} color="#C13584" />;
				case 4:
					return <YoutubeIcon size={25} color="#FF0000" />;
				case 5:
					return <TiktokIcon size={25} color="#25F4EE" />;
			}
		};

		const SocialMediaBox = ({ id, type, typeID, name, link }) => {
			let empty = isEmpty(type);
			return (
				<View style={[styles.socialMediaBox]}>
					<TouchableRipple rippleColor={transparent} onPress={this.handleSocMedDialog(id)}>
						<View style={styles.socialMediaBoxContainer}>
							{empty ? (
								<View
									style={{
										width: "100%",
										flexDirection: "row",
										alignItems: "center",
										justifyContent: "flex-start",
									}}
								>
									<MaterialCommunityIcons name="plus" size={23} color={accent} />
									<Typography
										type="headline"
										style={{
											color: accent,
											fontFamily: "regular",
											marginLeft: 12,
										}}
									>
										Add
									</Typography>
								</View>
							) : (
								<View
									style={{
										width: "100%",
										flexDirection: "row",
										justifyContent: "space-between",
										alignItems: "center",
									}}
								>
									<View style={{ flexDirection: "row", alignItems: "center" }}>
										<SocMedIcon type={typeID} />
										<View style={{ marginLeft: 20 }}>
											<Text style={styles.socialMediaBoxName}>{type}</Text>
											<Text style={styles.socialMediaBoxText}>{link}</Text>
										</View>
									</View>
									<Entypo name="chevron-thin-right" color={greyblack} size={20} />
								</View>
							)}
						</View>
					</TouchableRipple>
				</View>
			);
		};

		return (
			<LayoutView>
				<NavHeader
					title="Store Details"
					onBack={this.handleBack}
					actionText="Done"
					onPress={isDone ? this.handleSubmit : null}
				/>
				<PortalSafeView>
					{!isEmpty(socMedID) && (
						<View
							style={{
								width: "100%",
								height: "100%",
								backgroundColor: mainBgColor,
								paddingHorizontal: 16,
							}}
						>
							<NavHeader
								onBack={this.handleSocMedDialog()}
								actionText="Done"
								onPress={this.handleSubmitSocMed}
								title="Social Media"
							/>
							<View style={styles.container}>
								<SectionBox style={{ paddingVertical: 0 }}>
									<TextField
										type="select"
										style={styles.textInput}
										inputStyle={{ paddingHorizontal: 0 }}
										selectorList={socialMediaList}
										label="Type"
										onSelect={this.handleSocMedSelector}
										value={
											!isEmpty(socMedSelectorName) ? socMedSelectorName : "Select social media"
										}
										errorKey="state"
									/>
									<SectionDivider />
									<TextField
										style={styles.textInput}
										inputStyle={{ paddingHorizontal: 0 }}
										mode="outlined"
										label="Link"
										onChangeText={this.handleText("socMedLink")}
										value={socMedLink}
										autoCapitalize="none"
									/>
								</SectionBox>

								{socMedID <= socMed.length && (
									<TouchableRipple onPress={this.handleDeleteSocMed} rippleColor="transparent">
										<View
											style={{
												width: "100%",
												backgroundColor: "white",
												borderRadius: 10,
												height: 50,
												alignItems: "center",
												justifyContent: "center",
												marginTop: 15,
											}}
										>
											<Text
												style={{
													color: red,
													fontFamily: "medium",
													textAlign: "center",
													fontSize: 16,
												}}
											>
												Delete
											</Text>
										</View>
									</TouchableRipple>
								)}
							</View>
						</View>
					)}
				</PortalSafeView>

				<View style={styles.container}>
					<ScrollView
						style={{ width: "100%" }}
						// contentContainerStyle={{ paddingBottom: 250 }}
						showsVerticalScrollIndicator={false}
					>
						<ImageHolder onPressLogo={this.handleImageDialog("logo")} logo={logo} />
						<Text
							style={{
								width: "100%",
								alignSelf: "center",
								textAlign: "center",
								fontFamily: "regular",
								fontSize: 24,
								// paddingTop: 16,
								// paddingBottom: 12,
								//backgroundColor: "blue",
							}}
						>
							{store.name}
						</Text>
						{store.subdomain && (
							<TouchableWithoutFeedback
								onPress={handleCopyText(`https://${store.subdomain.toLowerCase()}.storeup.site`)}
							>
								<View
									style={{
										flexDirection: "row",
										alignItems: "center",
										justifyContent: "center",
										paddingBottom: 20,
										//	backgroundColor: "red",
									}}
								>
									<Feather name="copy" color={transparent} size={15} style={{ paddingRight: 5 }} />
									<Text
										style={{
											alignSelf: "center",
											textAlign: "center",
											fontFamily: "regular",
											color: greyblack,
										}}
									>
										https://{store.subdomain}.storeup.site
									</Text>
									<Feather name="copy" color={greylight} size={15} style={{ paddingLeft: 5 }} />
								</View>
							</TouchableWithoutFeedback>
						)}

						<View style={styles.contentCenter}>
							<SectionBox title="Details">
								<TextField
									onChangeText={this.handleText("storeName")}
									style={styles.textInput}
									inputStyle={{ paddingHorizontal: 0 }}
									label="Name"
									value={storeName}
									errorKey="business_name"
								/>
								<SectionDivider />
								<TextField
									type="multiline"
									maxLimit={250}
									placeholder="Describe your store"
									onChangeText={this.handleText("storeDesc")}
									style={styles.textInput}
									inputStyle={{ paddingHorizontal: 0 }}
									label="Description"
									value={storeDesc}
									errorKey="description"
								/>
								<SectionDivider />
								<TextField
									keyboardType="phone-pad"
									inputStyle={{ paddingHorizontal: 0 }}
									style={styles.textInput}
									mode="outlined"
									label="Phone"
									placeholder="Business phone number"
									startAdornment="+60"
									value={phone}
									onChangeText={this.handleText("phone")}
									errorKey="phone"
								/>
								<SectionDivider />
								<TextField
									onChangeText={this.handleText("storeUrl")}
									style={styles.textInput}
									inputStyle={{ paddingHorizontal: 0 }}
									label="Subdomain"
									value={storeUrl}
									errorKey="subdomain"
									autoCapitalize="none"
								/>
							</SectionBox>
							<SectionBox title="Social media">
								{socMed.map((e, i) => {
									return (
										<View key={i}>
											{i !== 0 ? <SectionDivider /> : null}
											<SocialMediaBox
												id={i + 1}
												name={e.name}
												type={e.type}
												typeID={e.typeID}
												link={e.link}
											/>
										</View>
									);
								})}
								<SectionDivider />
								{socMed.length < 8 && <SocialMediaBox id={socMed.length + 1} name="" type="" link="" />}
							</SectionBox>
						</View>
					</ScrollView>
					<PortalSafeView>
						{!isEmpty(choose) ? (
							<TouchableRipple
								style={{
									position: "absolute",
									zIndex: 10,
									bottom: 0,
									backgroundColor: blackTransparent,
									width: "100%",
									height: "100%",
								}}
								rippleColor="transparent"
								onPress={this.handleImageDialog(null)}
							>
								<View />
							</TouchableRipple>
						) : null}
						<Animated.View
							style={{
								alignSelf: "center",
								position: "absolute",
								zIndex: 100,
								bottom: 0,
								width: "90%",
								height: CARD_HEIGHT,
								backgroundColor: "transparent",
								alignItems: "center",
								justifyContent: "flex-end",
								paddingBottom: 20,
								transform: [{ translateY: CardMove }],
							}}
						>
							<View
								style={{
									backgroundColor: greywhite,
									borderRadius: 10,
									marginBottom: 15,
									width: "100%",
								}}
							>
								<DialogButton
									text="Pick from gallery"
									color={accent}
									onPress={this.pickImageFromGallery}
								/>
								<DialogButton text="Open camera" color={accent} onPress={this.pickImageFromCamera} />
							</View>
							<DialogButton text="Cancel" color={red} onPress={this.handleImageDialog(null)} />
						</Animated.View>
					</PortalSafeView>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		paddingTop: 20,
	},
	contentCenter: {
		width: "100%",
		alignSelf: "center",
	},
	textInput: {
		marginVertical: 0,
	},
	dialogActionButton: {
		minWidth: 70,
	},
	sectionBox: {
		//width: "100%",
		display: "flex",
		flexDirection: "column",
		// borderBottomWidth: 1,
		// borderBottomColor: greywhite,
		paddingVertical: 16,
	},
	sectionTitle: {
		fontFamily: "light",
		paddingLeft: 12,
		textTransform: "uppercase",
		marginBottom: 10,
	},
	sectionContent: {
		width: "100%",
		backgroundColor: "white",
		borderRadius: 10,
		paddingHorizontal: 20,
	},
	addressBox: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		borderColor: greywhite,
		borderWidth: 0.5,
		borderRadius: 10,
		paddingHorizontal: 10,
		paddingVertical: 15,
	},
	addressText: {
		fontFamily: "regular",
		fontSize: 15,
		color: greyblack,
	},
	socialMediaBox: {
		width: "100%",
		backgroundColor: white,
		//marginBottom: 10,
		borderRadius: 10,
	},
	socialMediaBoxContainer: {
		flexDirection: "row",
		width: "100%",
		//paddingHorizontal: 10,
		paddingVertical: 15,
		alignItems: "center",
	},
	socialMediaBoxName: {
		fontSize: 15,
		fontFamily: "regular",
		color: black,
		textTransform: "capitalize",
	},
	socialMediaBoxText: {
		fontSize: 13,
		fontFamily: "regular",
		color: greyblack,
	},
	saveButton: {
		marginTop: 20,
		alignSelf: "flex-end",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	profile: state.profile,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	getSocialMediaList,
	getStoreDetails,
	editStoreDetails,
	customNoti,
	setLoading,
})(StoreDetails);
