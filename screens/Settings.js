import React from "react";
import { StyleSheet, ScrollView, View, Text, Image } from "react-native";
import { connect } from "react-redux";

import { logoutUser } from "../store/actions/authAction";
import * as WebBrowser from "expo-web-browser";
import { LayoutView } from "../component/Layout";
import NavHeader from "../component/NavHeader";
import { TouchableRipple } from "react-native-paper";
import {
	black,
	grey,
	greyblack,
	greywhite,
	mainBgColor,
	orange,
	orangepurple,
	pink,
	pinkpurple,
	purple,
	purpleorange,
	purplepink,
	red,
	transparent,
} from "../utils/ColorPicker";
import isEmpty from "../utils/isEmpty";
import { Ionicons, MaterialCommunityIcons, Entypo } from "@expo/vector-icons";
import langSelector from "../utils/langSelector";
import { setLoading } from "../store/actions/loadingAction";
import PageTitle from "../component/PageTitle";
import AlertDialog from "../component/AlertDialog";
import Typography from "../component/Typography";
import { SectionBox, SectionDivider } from "../component/SectionComponents";

class Settings extends React.Component {
	state = {
		logoutModal: false,
		isAgent: false,
	};

	componentDidMount() {
		this.setState({
			isAgent: this.props.navigation.state.routeName === "AgentSettings",
		});
	}

	componentDidUpdate(prevProps, prevState) {
		if (!isEmpty(prevProps.auth.token) && isEmpty(this.props.auth.token)) {
			this.props.navigation.navigate("SignIn");
		}
	}

	componentWillUnmount() {
		this.props.setLoading(false);
	}

	handlePage = page => () => {
		this.props.navigation.push(page);
	};

	handleBrowser = page => () => {
		if (page.includes("http")) {
			WebBrowser.openBrowserAsync(page);
		} else {
			if (page === "help") {
				WebBrowser.openBrowserAsync("https://storeup.tawk.help/");
			} else {
				WebBrowser.openBrowserAsync(`https://storeup.io/${page}`);
			}
		}
	};

	handleLogoutDialog = state => () => {
		this.setState({
			logoutModal: state,
		});
	};

	handleLogout = () => {
		this.props.setLoading(true);
		this.setState(
			{
				logoutModal: false,
			},
			() => {
				this.props.logoutUser();
			}
		);
	};

	render() {
		const { isAgent } = this.state;
		const { store, user } = this.props.profile;
		const { hqList } = this.props.auth;
		const lang = langSelector.Settings.find(e => e.lang === this.props.preference.lang).data;

		const List = ({ iconType: Icon, ...props }) => {
			let color = props.color || black;

			return (
				<TouchableRipple rippleColor={transparent} style={{ width: "100%" }} onPress={props.onPress}>
					<View style={{ width: "100%" }}>
						{!props.isFirst ? <SectionDivider /> : null}
						<View style={styles.listContainer}>
							<View style={{ flexDirection: "row", alignItems: "center" }}>
								<View>
									<Icon name={props.iconName} size={32} style={styles.icon} color={color} />
								</View>
								<Text style={[styles.listText]}>{props.children}</Text>
							</View>
							<Entypo name="chevron-thin-right" color={grey} size={16} />
						</View>
					</View>
				</TouchableRipple>
			);
		};

		const SectionList = props => {
			return (
				<SectionBox title={props.title}>
					{props.list.map((e, i) => {
						let isExist = true;
						if (!isEmpty(e.isExist)) {
							isExist = e.isExist;
						}

						if (isExist) {
							return (
								<List
									key={i}
									isFirst={i === 0}
									iconType={e.iconType}
									iconName={e.iconName}
									onPress={e.onPress}
									dotColor={e.dotColor}
									color={e.color}
								>
									{e.title}
								</List>
							);
						} else {
							return null;
						}
					})}
				</SectionBox>
			);
		};
		return (
			<LayoutView>
				<NavHeader />
				<AlertDialog
					actions={[
						{
							title: "Sign out",
							onPress: this.handleLogout,
						},
						{
							title: "No",
							onPress: this.handleLogoutDialog(false),
						},
					]}
					title="Sign out account?"
					active={this.state.logoutModal}
					onDismiss={this.handleLogoutDialog(false)}
				/>

				<ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ width: "100%" }}>
					<View style={styles.container}>
						<PageTitle>Account</PageTitle>

						<TouchableRipple onPress={this.handlePage("PersonalDetails")} rippleColor="transparent">
							<View
								style={{
									width: "100%",
									alignSelf: "center",
									flexDirection: "row",
									justifyContent: "space-between",
									alignItems: "center",
									paddingVertical: 12,
									paddingHorizontal: 16,
									backgroundColor: "white",
									borderRadius: 10,
									marginVertical: 16,
								}}
							>
								<Image
									style={[
										{
											width: 56,
											height: undefined,
											aspectRatio: 1,
											borderRadius: 28,
											borderWidth: 0.5,
											borderColor: greywhite,
										},
									]}
									source={
										!isEmpty(store.logo)
											? { uri: store.logo }
											: require("../assets/image/noBusinessLogo.png")
									}
								/>

								<View
									style={{
										//flex: isTablet ? 4 : 3,
										flex: 1,
										paddingLeft: 16,
									}}
								>
									<Typography
										type="title3"
										numberOfLines={1}
										ellipsizeMode="tail"
										style={{
											fontFamily: "medium",
										}}
									>
										{user.name}
									</Typography>

									<Text
										numberOfLines={1}
										ellipsizeMode="tail"
										style={{
											fontFamily: "regular",
											fontSize: 12,
											color: greyblack,
										}}
									>
										{user.email}
									</Text>
								</View>
								<Entypo name="chevron-thin-right" color={grey} size={17} />
							</View>
						</TouchableRipple>
						<View style={{ width: "100%" }}>
							<SectionList
								title="About store"
								list={[
									{
										title: "Store Details",
										iconType: MaterialCommunityIcons,
										iconName: "storefront",
										onPress: this.handlePage("StoreDetails"),
										color: orange,
									},
									{
										title: "Address",
										iconType: MaterialCommunityIcons,
										iconName: "map-marker-radius",
										onPress: this.handlePage("StoreAddress"),
										color: orangepurple,
									},
								]}
							/>
							<SectionList
								title="StoreUp Pay"
								list={[
									{
										title: "Bank Details",
										iconType: Ionicons,
										iconName: "card",
										onPress: this.handlePage("BankDetails"),
										color: purpleorange,
									},
								]}
							/>

							<SectionList
								title="Support"
								list={[
									{
										title: "Help Center",
										iconType: MaterialCommunityIcons,
										iconName: "help-circle",
										onPress: this.handleBrowser("help"),
										color: purple,
									},
									{
										title: "StoreUp VIP Club",
										iconType: MaterialCommunityIcons,
										iconName: "account-group",
										onPress: this.handleBrowser("https://www.facebook.com/groups/storeupcommunity"),
										color: purplepink,
									},
								]}
							/>
							<SectionList
								title="Legal"
								list={[
									{
										title: "Terms of Services",
										iconType: MaterialCommunityIcons,
										iconName: "file-document",
										onPress: this.handleBrowser("terms"),
										color: pinkpurple,
									},
									{
										title: "Privacy Policy",
										iconType: MaterialCommunityIcons,
										iconName: "file-document",
										onPress: this.handleBrowser("privacy"),
										color: pink,
									},
								]}
							/>

							<TouchableRipple onPress={this.handleLogoutDialog(true)} rippleColor="transparent">
								<SectionBox innerStyle={{ height: 55, justifyContent: "center", alignItems: "center" }}>
									<Typography
										type="headline"
										style={{
											color: red,
											textAlign: "center",
											fontFamily: "semibold",
										}}
									>
										Sign out
									</Typography>
								</SectionBox>
							</TouchableRipple>
						</View>
					</View>
				</ScrollView>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		marginBottom: 30,
	},
	sectionTitle: {
		fontFamily: "regular",
		textTransform: "uppercase",
		color: black,
		paddingLeft: 12,
		marginBottom: 10,
	},
	listContainer: {
		width: "100%",

		paddingVertical: 12,
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
	},
	listText: {
		fontFamily: "semibold",
		fontSize: 16,
		paddingLeft: 16,
	},
	indicator: {
		position: "absolute",
		borderColor: mainBgColor,
		borderWidth: 2,
		zIndex: 1,
		width: 12,
		height: undefined,
		aspectRatio: 1,
		borderRadius: 6,
		right: -4,
		top: -4,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	profile: state.profile,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, { setLoading, logoutUser })(Settings);
