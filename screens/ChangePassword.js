import React from "react";
import { StyleSheet, View, Text, Keyboard } from "react-native";
import { connect } from "react-redux";

import { changePassword } from "../store/actions/profileAction";
import { customNoti } from "../store/actions/alertAction";

import { Button } from "react-native-paper";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import isEmpty from "../utils/isEmpty";
import NavHeader from "../component/NavHeader";
import PasswordField from "../component/PasswordField";
import { black } from "../utils/ColorPicker";
import langSelector from "../utils/langSelector";

class ChangePassword extends React.Component {
	state = {
		opass: "",
		pass: "",
	};

	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.alert.status) && this.props.alert.status === "success") {
			this.handleBack();
		}
	}

	handleBack = () => {
		this.props.navigation.pop();
	};

	handleText = name => value => {
		this.setState({
			[name]: value,
		});
	};

	handleSubmit = () => {
		Keyboard.dismiss();
		let { opass, pass } = this.state;
		if (isEmpty(pass)) {
			this.props.customNoti("Empty password field");
		} else {
			let data = {
				password: opass,
				new_password: pass,
			};
			this.props.changePassword(data, this.props.auth.token);
		}
	};

	render() {
		const { opass, pass } = this.state;
		const lang = langSelector.ChangePassword.find(e => e.lang === this.props.preference.lang).data;

		return (
			<LayoutView>
				<NavHeader onBack={this.handleBack} />

				<View style={styles.container}>
					<PageTitle>{lang.title}</PageTitle>

					<PasswordField
						style={styles.textInput}
						mode="outlined"
						label={lang.password}
						value={opass}
						onChangeText={this.handleText("opass")}
					/>

					<PasswordField
						mode="outlined"
						style={styles.input}
						label={lang.newPassword}
						value={pass}
						onChangeText={this.handleText("pass")}
					/>
				</View>
				<View
					style={{
						paddingVertical: 10,
						width: "100%",
						alignSelf: "center",
						alignItems: "center",
						justifyContent: "center",
					}}
				>
					<Button
						onPress={this.handleSubmit}
						mode="contained"
						color={black}
						style={styles.button}
						contentStyle={{ height: 50 }}
					>
						{lang.submitButton}
					</Button>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		// alignItems: "center"
	},
	textInput: {
		marginVertical: 10,
	},
	button: {
		width: "100%",
		marginTop: 20,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	profile: state.profile,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	changePassword,
	customNoti,
})(ChangePassword);
