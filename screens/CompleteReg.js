import React from "react";

import { StyleSheet, Text, View, Keyboard } from "react-native";

import { connect } from "react-redux";
import { setLoading } from "../store/actions/loadingAction";
import { customNoti } from "../store/actions/alertAction";

import { Button, TouchableRipple } from "react-native-paper";

import { LayoutView } from "../component/Layout";

import isEmpty from "../utils/isEmpty";
import { grey } from "../utils/ColorPicker";
import TextField from "../component/TextField";
import { Ionicons } from "@expo/vector-icons";
import { editStoreDetails } from "../store/actions/profileAction";
import CustomButton from "../component/CustomButton";

class CompleteReg extends React.Component {
	state = {
		isCompleted: false,
		name: "",
		phone: "",
	};

	componentDidUpdate(prevProps, prevState) {
		if (!isEmpty(this.props.alert.redirect) && isEmpty(prevProps.alert.redirect)) {
			if (this.props.alert.redirect === "StoreSaved") {
				this.setState({
					isCompleted: true,
				});
			} else {
				this.props.navigation.navigate(this.props.alert.redirect);
			}
		}
	}

	handleChange = name => value => {
		this.setState({
			[name]: value,
		});
	};

	handleSubmit = () => {
		Keyboard.dismiss();
		if (this.state.isCompleted) {
			this.props.navigation.navigate("CompleteStore");
		} else {
			let { store } = this.props.profile;
			let data = {
				name: this.state.name,
				phone: this.state.phone,
				subdomain: store.subdomain.toLowerCase(),
				logo: store.logo,
				registered_name: store.registered_name,
				ssm_no: store.ssm_no,
				email: store.email,
				description: store.description,
			};

			this.props.editStoreDetails(store.id, data, this.props.auth.token);
		}
	};

	render() {
		const { name, phone, isCompleted } = this.state;

		return (
			<LayoutView>
				<View style={styles.container}>
					<Ionicons name={isCompleted ? "checkmark-circle" : "business"} size={86} color="#0D65FB" />
					<Text style={styles.pageTitle}>
						{isCompleted ? "StoreUp Account Created" : "Enter Business Details"}
					</Text>
					{isCompleted ? null : (
						<>
							<TextField
								style={styles.input}
								label="Name"
								value={name}
								onChangeText={this.handleChange("name")}
							/>
							<TextField
								keyboardType="numeric"
								style={styles.input}
								label="Phone"
								value={phone}
								startAdornment="+60"
								onChangeText={this.handleChange("phone")}
							/>
						</>
					)}
					<CustomButton style={styles.button} onPress={this.handleSubmit}>
						Continue
					</CustomButton>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		paddingBottom: 100,
		//backgroundColor: "red",
		justifyContent: "center",
	},
	pageTitle: {
		width: "100%",
		fontSize: 40,
		fontFamily: "medium",
		textAlign: "center",
	},
	input: {
		width: "100%",
		marginVertical: 10,
	},
	buttonText: {
		color: "#0D65FB",
		textAlign: "center",
		width: "100%",
		fontSize: 16,
		fontFamily: "regular",
	},
	button: {
		justifyContent: "center",
		width: "100%",
		marginVertical: 30,
		alignSelf: "flex-end",
	},
	googleLogo: {
		height: 21,
		width: 21,
	},
	signUpText: {
		fontFamily: "regular",
		paddingVertical: 5,
		fontSize: 16,
	},
	forgotText: {
		fontFamily: "light",
		//marginTop: 5,
		paddingVertical: 5,
		fontSize: 14,
		color: grey,
		alignSelf: "flex-start",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	profile: state.profile,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	editStoreDetails,
	setLoading,
	customNoti,
})(CompleteReg);
