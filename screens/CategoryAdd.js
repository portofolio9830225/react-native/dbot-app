import React from "react";
import {
	StyleSheet,
	ScrollView,
	View,
	Text,
	FlatList,
	Image,
	TouchableWithoutFeedback,
	Keyboard,
	Platform,
} from "react-native";
import { connect } from "react-redux";

import { LayoutView } from "../component/Layout";
import NavHeader from "../component/NavHeader";
import {
	accent,
	black,
	green,
	grey,
	greyblack,
	greylight,
	greywhite,
	orange,
	red,
	transparent,
} from "../utils/ColorPicker";
import TextField from "../component/TextField";
import getWidth from "../utils/getWidth";
import isEmpty from "../utils/isEmpty";
import { isTablet } from "react-native-device-detection";
import { withNavigationFocus } from "react-navigation";
import { createCategory, editCategory, getSingleCategory } from "../store/actions/categoryAction";
import { setLoading } from "../store/actions/loadingAction";
import langSelector from "../utils/langSelector";
import { getAgentItem } from "../store/actions/agentAction";
import { customNoti } from "../store/actions/alertAction";
import ImagePoolEditor from "../component/ImagePoolEditor";
import Typography from "../component/Typography";
import { SectionBox, SectionDivider } from "../component/SectionComponents";
import SwitcherWithLabel from "../component/SwitcherWithLabel";
import AlertDialog from "../component/AlertDialog";

class CategoryAdd extends React.Component {
	state = {
		isEdit: false,
		isActive: true,
		name: "",
		desc: "",
		active: true,
		number: 1,
		deleteDialog: false,
	};

	fetchData = () => {
		if (!isEmpty(this.props.category.singleCategory)) {
			this.props.setLoading(true);
			let { singleCategory } = this.props.category;

			this.setState({
				isEdit: true,
				name: singleCategory.name,
				active: singleCategory.active,
			});
		}
		this.props.setLoading(false);
	};

	componentDidMount() {
		this.props.setLoading(true);
		this.fetchData();
	}

	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.alert.status) && this.props.alert.status === "success") {
			if (
				this.props.alert.redirect === "editCategory" ||
				// this.props.alert.redirect === "deleteCategory" ||
				this.props.alert.redirect === "createCategory"
			) {
				this.handleBack();
			}
		}
	}

	componentWillUnmount() {
		if (this.props.navigation.state.params && this.props.navigation.state.params.fromPage) {
			if (this.props.navigation.state.params.fromPage !== "CategoryDetail") {
				this.props.getSingleCategory();
			}
		}
	}

	handleBack = () => {
		if (this.props.navigation.state.params && this.props.navigation.state.params.fromPage) {
			this.props.navigation.navigate(this.props.navigation.state.params.fromPage, { fromPage: "CategoryAdd" });
		} else {
			this.props.navigation.goBack();
		}
	};

	handleText = name => value => {
		this.setState({
			[name]: value,
		});
	};

	handleSubmit = () => {
		Keyboard.dismiss();

		const { name, desc, active, number } = this.state;

		let data = {
			name,
			description: desc,
			active: active,
			number,
		};

		if (!this.state.isEdit) {
			this.props.createCategory(data, this.props.auth.token);
		} else {
			this.props.setLoading(true);
			this.props.editCategory(this.props.category.singleCategory.id, data, this.props.auth.token);
		}
	};

	render() {
		const { isEdit, active, name } = this.state;

		const isDone = !isEmpty(name);

		// const lang = langSelector.CategoryAdd.find(e => e.lang === this.props.preference.lang).data;

		return (
			<LayoutView noPadding>
				<View style={{ paddingHorizontal: 16 }}>
					<NavHeader
						onBack={this.handleBack}
						onPress={isDone ? this.handleSubmit : null}
						actionText="Done"
						title={isEdit ? "Edit Category" : "Add Category"}
					/>
				</View>

				<ScrollView contentContainerStyle={{ width: "100%", paddingBottom: 30 }}>
					<View style={styles.container}>
						<View
							style={{
								width: "100%",
								alignSelf: "center",
								paddingHorizontal: 16,
							}}
						>
							<SectionBox>
								<TextField
									inputStyle={{ paddingHorizontal: 0 }}
									style={styles.textField}
									label="Name"
									value={name}
									onChangeText={this.handleText("name")}
								/>
								<SectionDivider />
								<SwitcherWithLabel
									label="Availability"
									enabled={active}
									onValueChange={() => {
										this.setState({
											active: !this.state.active,
										});
									}}
									cardStyle={{ paddingHorizontal: 0 }}
								/>
							</SectionBox>
						</View>

						{/* <TouchableRipple onPress={this.handleDeleteDialog(true)} rippleColor="transparent">
								<SectionBox innerStyle={{ height: 55, justifyContent: "center", alignItems: "center" }}>
									<Typography
										type="headline"
										style={{
											color: red,
											textAlign: "center",
											fontFamily: "semibold",
										}}
									>
										Delete Category
									</Typography>
								</SectionBox>
							</TouchableRipple> */}
					</View>
				</ScrollView>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
	sectionBox: {
		//width: "100%",
		display: "flex",
		flexDirection: "column",
		// borderBottomWidth: 1,
		// borderBottomColor: greywhite,
		paddingVertical: 14,
	},
	sectionTitle: {
		fontFamily: "regular",
		paddingLeft: 12,
		textTransform: "uppercase",
		marginBottom: 10,
	},
	sectionContent: {
		width: "100%",
		backgroundColor: "white",
		borderRadius: 10,
		paddingHorizontal: 20,
	},
	orderBox: {
		overflow: "hidden",
		flexDirection: "row",
		justifyContent: "space-between",
		marginBottom: 25,
	},
	orderBoxLeft: {
		width: "35%",
		height: "100%",
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "#f7f7ff",
	},
	orderBoxRight: {
		width: "60%",
		height: "100%",
		justifyContent: "center",
		paddingRight: 10,
	},
	orderName: {
		fontFamily: "medium",
		fontSize: 16,
		lineHeight: 20,
	},
	orderPrice: {
		fontFamily: "bold",
		fontSize: 15,
		lineHeight: 20,
	},
	orderStock: {
		fontFamily: "light",
		fontSize: 12,
		color: grey,
		marginTop: 8,
	},

	rentalTabContainer: {
		width: "100%",
		flexDirection: "row",
		//justifyContent: "center",
		alignItems: "center",
	},
	rentalTabItem: {
		paddingTop: 10,
		//marginBottom: 15,
		justifyContent: "center",
		alignItems: "center",
		borderBottomWidth: 2,
	},
	rentalTabItemActive: {
		borderBottomColor: black,
	},
	rentalTabItemInactive: {
		borderBottomColor: transparent,
	},
	rentalTabTextContainer: {
		width: "100%",
		flexDirection: "column",
		alignItems: "center",
	},
	rentalTabText: {
		fontSize: 13,
	},
	indicator: {
		alignSelf: "flex-end",
		width: 8,
		height: undefined,
		aspectRatio: 1,
		borderRadius: 4,
		marginRight: 3,
	},
	rentalTabTextActive: {
		fontFamily: "bold",
	},
	rentalTabTextInactive: {
		fontFamily: "light",
		color: greyblack,
	},
	textField: {
		marginBottom: 0,
	},
	halfRow: {
		flexDirection: "row",
		justifyContent: "space-between",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	item: state.item,
	profile: state.profile,
	category: state.category,
	loading: state.loading,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	getAgentItem,
	createCategory,
	getSingleCategory,
	editCategory,
	// setItemCategory,
	// deleteCategory,
	setLoading,
	customNoti,
})(withNavigationFocus(CategoryAdd));
