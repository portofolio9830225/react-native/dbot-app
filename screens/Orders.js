import React from "react";
import { StyleSheet, View, Image, Animated, Easing, RefreshControl, ScrollView, TextInput } from "react-native";
import { connect } from "react-redux";

import { getAllOrders } from "../store/actions/orderAction";
import { setLoading, setRefresh } from "../store/actions/loadingAction";

import { LayoutView } from "../component/Layout";
import PageTitle from "../component/PageTitle";
import NavHeader from "../component/NavHeader";
import DateConverter from "../utils/DateConverter";
import { black, greyblack, greywhite, transparent } from "../utils/ColorPicker";
import { Ionicons } from "@expo/vector-icons";

import { TouchableRipple } from "react-native-paper";
import getWidth from "../utils/getWidth";
import EmptyText from "../component/EmptyText";
import langSelector from "../utils/langSelector";
import StatusCard from "../component/StatusCard";
import isEmpty from "../utils/isEmpty";
import Loading from "../component/Loading";
import OrderBoxSelect from "../component/OrderBoxSelect";
import { withNavigationFocus } from "react-navigation";
import SegmentedPicker from "../component/SegmentedPicker";
import { FlatList } from "react-native-gesture-handler";

const SEARCH_MIN_LIMIT = 3;
class Orders extends React.Component {
	constructor(props) {
		super(props);
		this.transValue = new Animated.Value(1);
	}

	state = {
		active: "",
		list: [],
		searchText: "",
		searchList: [],
		statusCard: false,
		selectMode: false,
		orderSelected: [],
	};

	getOrderList =
		(status = null) =>
		() => {
			if (!isEmpty(status)) {
				if (status !== this.state.active || this.props.loading.refresh) {
					this.props.setLoading(true);
					this.props.getAllOrders(this.props.auth.token, status, 1);
				} else {
					this.props.getAllOrders(this.props.auth.token, status, this.props.order.pagination.next);
				}
				this.setState(
					{
						active: status,
						searchText: "",
					},
					() => {}
				);
			}
			// if (!isEmpty(status) && (status !== this.state.active || this.props.loading.refresh)) {
			// 	this.props.setLoading(true);
			// 	this.setState(
			// 		{
			// 			active: status,
			// 			searchText: "",
			// 		},
			// 		() => {
			// 			this.props.getAllOrders(this.props.auth.token, status);
			// 		}
			// 	);
			// }
		};

	componentDidMount() {
		this.getOrderList("paid")();
	}

	componentDidUpdate(prevProps, prevState) {
		if (!prevProps.isFocused && this.props.isFocused) {
			this.props.setLoading(true);
			this.props.getAllOrders(this.props.auth.token, this.state.active, 1);
		}
		if (prevState.searchText !== this.state.searchText) {
			let { searchText } = this.state;

			this.setState({
				searchList:
					searchText.length >= SEARCH_MIN_LIMIT
						? this.state.list.filter(f => {
								let lcSearchText = searchText.toLowerCase();
								let lcCustName = !isEmpty(f.customer) ? f.customer.name.toLowerCase() : "customer";

								return (
									lcCustName.includes(lcSearchText) ||
									f.ref.includes(lcSearchText) ||
									`60${f.customer?.phone}`.includes(lcSearchText)
								);
						  })
						: [],
			});
		}
		if (this.props.order.isOrderFetched && !prevProps.order.isOrderFetched) {
			const { paid, unpaid, completed, rejected } = this.props.order.orderList;
			let arr = [];
			switch (this.state.active) {
				case "unpaid":
					arr = unpaid;
					break;
				case "paid":
					arr = paid;
					break;
				case "completed":
					arr = completed;
					break;
				case "rejected":
					arr = rejected;
			}

			this.setState(
				{
					list: arr,
				},
				() => {
					this.props.setLoading(false);
				}
			);
		}
		if (isEmpty(prevProps.alert.redirect) && !isEmpty(this.props.alert.redirect)) {
			if (this.props.alert.redirect === "setMultipleOrderStatus") {
				this.setState({
					selectMode: false,
				});
			}
		}
		if (!prevProps.loading.refresh && this.props.loading.refresh) {
			setTimeout(() => {
				this.getOrderList(this.state.active)();
			}, 1500);
		}
	}

	handleText = name => val => {
		this.setState({
			[name]: val,
		});
	};

	handlePage =
		(page, params = {}) =>
		() => {
			this.props.navigation.push(page, params);
		};

	handleOrderBox = item => () => {
		if (this.state.selectMode) {
			let orders = [...this.state.orderSelected];
			let found = orders.findIndex(e => e === item.order_id);

			if (found === -1) {
				orders.push(item.order_id);
			} else {
				orders.splice(found, 1);
			}

			this.setState({
				orderSelected: orders,
			});
		} else {
			this.props.navigation.push("OrderDetail", {
				oID: item.ref,
				isAction: true,
			});
		}
	};

	handleLongOrderBox = item => () => {
		if (isEmpty(this.state.orderSelected)) {
			let orders = [];
			orders.push(item.order_id);
			this.setState({
				orderSelected: orders,
			});
		}
	};

	handleOrderCsv = () => {
		this.props.navigation.navigate("OrderCsv");
	};

	render() {
		const { active, list, statusCard, selectMode, orderSelected, searchText, searchList } = this.state;
		const { store } = this.props.profile;
		const { orderList, isOrderFetched, pagination } = this.props.order;
		const { paid, unpaid } = orderList;
		const { isLoading } = this.props.loading;

		const lang = langSelector.Orders.find(e => e.lang === this.props.preference.lang).data;

		const EmptyTextContainer = props => {
			return (
				<View
					style={{
						flex: 1,
						marginBottom: 70,
					}}
				>
					<EmptyText {...props} />
				</View>
			);
		};

		return (
			<LayoutView noLoading>
				<StatusCard
					orderID={orderSelected}
					open={statusCard}
					status={active === 2 ? 0 : active === 1 ? 1 : null}
					onClose={() => {
						this.setState({
							statusCard: false,
						});
					}}
				/>
				<NavHeader
					actions={[
						{
							title: "Download statement",
							iconType: Ionicons,
							iconName: "download-outline",
							onPress: this.handleOrderCsv,
						},
					]}
				/>
				<View style={styles.container}>
					<FlatList
						style={{ width: "100%" }}
						contentContainerStyle={{
							flex:
								(!isEmpty(searchText) && !isEmpty(searchList)) ||
								(isEmpty(searchText) && !isEmpty(list) && !isLoading)
									? undefined
									: 1,
							paddingBottom: 60,
						}}
						showsVerticalScrollIndicator={false}
						refreshControl={
							<RefreshControl
								refreshing={this.props.loading.refresh}
								onRefresh={() => {
									this.props.setRefresh(true);
								}}
							/>
						}
						onEndReached={() => {
							if (!isLoading) {
								this.getOrderList(active)();
							}
						}}
						onEndReachedThreshold={0.3}
						ListHeaderComponent={
							<>
								<PageTitle>{lang.title}</PageTitle>
								<View style={styles.searchBar}>
									<Ionicons name="search" size={18} color={greyblack} />
									<TextInput
										style={{ fontFamily: "regular", fontSize: 13, flex: 1, paddingHorizontal: 7 }}
										placeholder="Order ID, name, phone number"
										value={searchText}
										onChangeText={val => {
											this.setState({ searchText: val });
										}}
									/>
									{!isEmpty(searchText) ? (
										<TouchableRipple
											rippleColor="transparent"
											onPress={() => {
												this.setState({
													searchText: "",
												});
											}}
										>
											<Ionicons
												name="ios-close-circle"
												size={18}
												color={greyblack}
												style={{ alignSelf: "flex-end" }}
											/>
										</TouchableRipple>
									) : null}
								</View>
							</>
						}
						ListEmptyComponent={
							!isLoading ? (
								!isEmpty(searchText) ? (
									searchText.length >= SEARCH_MIN_LIMIT ? (
										<EmptyTextContainer
											title="No Orders"
											text="There is no order related to the entered information"
										/>
									) : (
										<EmptyTextContainer
											title="Not Enough Characters"
											text={`Enter at least ${SEARCH_MIN_LIMIT} characters to search for your order`}
											noImage
										/>
									)
								) : (
									<EmptyTextContainer
										title="No Orders"
										text="There is no order related in this status"
										noImage
									/>
								)
							) : (
								<Loading />
							)
						}
						ListFooterComponent={
							!isLoading && !isEmpty(pagination.next) ? (
								<View
									style={{
										alignItems: "center",
										justifyContent: "center",
										alignSelf: "center",
										paddingTop: 3,
									}}
								>
									<Image
										source={require("../assets/LoadingCircle.gif")}
										style={{ width: 30, height: undefined, aspectRatio: 1 }}
									/>
								</View>
							) : null
						}
						data={!isLoading ? (!isEmpty(searchList) ? searchList : !isEmpty(list) ? list : []) : []}
						keyExtractor={(item, index) => {
							return index;
						}}
						renderItem={({ item, index, separators }) => {
							let totalPrice = 0;
							item.products.map(p => {
								totalPrice += p.price * p.quantity;
							});

							if (!isEmpty(item.shipping)) {
								totalPrice += item.shipping.price;
							}

							return (
								<OrderBoxSelect
									statusType={1}
									onPress={this.handleOrderBox(item)}
									isHideCheck={!selectMode}
									isUnread={!item.opened}
									key={index}
									oTitle={`${lang.orderID}:`}
									id={item.order_id}
									name={!isEmpty(item.customer) ? item.customer.name : "Customer"}
									date={DateConverter(item.created_ts)}
									price={totalPrice}
									quantity={item.item_count}
									status={item.status}
									checked={orderSelected.findIndex(e => e === item.order_id) !== -1}
									pMethod={!isEmpty(item.payment) ? item.payment.payment_method : "Leads"}
								/>
							);
						}}
					/>
					{selectMode ? null : (
						<SegmentedPicker
							rounded
							style={{ position: "absolute", bottom: 10 }}
							active={this.state.active}
							list={[
								{ text: "Paid", value: "paid", onPress: this.getOrderList("paid") },
								{ text: "Unpaid", value: "unpaid", onPress: this.getOrderList("unpaid") },
								{
									text: "Completed",
									value: "completed",
									onPress: this.getOrderList("completed"),
								},
								{ text: "Rejected", value: "rejected", onPress: this.getOrderList("rejected") },
							]}
						/>
					)}
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
	orderStatusBox: {
		width: getWidth * 0.24,
		borderRadius: 15,
		alignItems: "center",
		justifyContent: "center",
		paddingVertical: 10,
	},
	orderStatusText: {
		fontSize: 11,
		textAlign: "center",
		textTransform: "capitalize",
	},
	fab: {
		position: "absolute",
		margin: 16,
		right: 0,
		bottom: 0,
	},
	searchBar: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		width: "100%",
		borderRadius: 7,
		backgroundColor: greywhite,
		height: 36,
		borderRadius: 18,
		paddingHorizontal: 10,
		marginBottom: 5,
		marginTop: 8,
	},
	searchBarText: {
		fontFamily: "light",
		color: greyblack,
		paddingLeft: 5,
	},
	rentalTabContainer: {
		width: "100%",
		flexDirection: "row",
		//justifyContent: "center",
		alignItems: "center",
	},
	rentalTabItem: {
		paddingTop: 10,
		//marginBottom: 15,
		justifyContent: "center",
		alignItems: "center",
		borderBottomWidth: 2,
	},
	rentalTabItemActive: {
		borderBottomColor: black,
	},
	rentalTabItemInactive: {
		borderBottomColor: transparent,
	},
	rentalTabTextContainer: {
		width: "100%",
		flexDirection: "column",
		alignItems: "center",
	},
	rentalTabText: {
		fontSize: 13,
	},
	indicator: {
		alignSelf: "flex-end",
		width: 8,
		height: undefined,
		aspectRatio: 1,
		borderRadius: 4,
		marginRight: 3,
	},
	rentalTabTextActive: {
		fontFamily: "bold",
	},
	rentalTabTextInactive: {
		fontFamily: "light",
		color: greyblack,
	},
	navbarText: {
		fontFamily: "regular",
		fontSize: 16,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	order: state.order,
	profile: state.profile,
	alert: state.alert,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, { getAllOrders, setRefresh, setLoading })(withNavigationFocus(Orders));
