import { StyleSheet, TouchableWithoutFeedback, View } from "react-native";
import React from "react";
import { connect } from "react-redux";
import { withNavigationFocus } from "react-navigation";
import { LayoutView } from "../component/Layout";
import NavHeader from "../component/NavHeader";
import SegmentedPicker from "../component/SegmentedPicker";
import EmptyList from "../component/EmptyText";
import { white, greydark } from "../utils/ColorPicker";
import { Entypo } from "@expo/vector-icons";
import Typography from "../component/Typography";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { getAllPromo } from "../store/actions/promoAction";
import isEmpty from "../utils/isEmpty";
import { ScrollView } from "react-native-gesture-handler";

export class Promotion extends React.Component {
	state = {
		active: "active",
		arrPromo: [],
	};

	componentDidMount() {
		this.props.getAllPromo(this.props.auth.token);
	}

	componentDidUpdate(prevProps, prevState) {
		if (this.props.promo.isPromoFetched && !prevProps.promo.isPromoFetched) {
			if (!isEmpty(this.state.active)) {
				this.getPromoActive(this.state.active);
			} else {
				this.setState({
					active: "active",
				});
			}
		}
		if (this.state.active !== prevState.active) {
			this.getPromoActive(this.state.active);
		}
	}

	handleBack = () => {
		this.props.navigation.pop();
	};

	getActive =
		(status = null) =>
		() => {
			this.setState({
				active: status,
			});
		};

	getPromoActive = (status = null) => {
		const now = new Date().getTime();

		let data = [...this.props.promo.promos];
		let arr = [];

		switch (status) {
			case "incoming":
				arr = data.filter(item => {
					let start = new Date(item.start_date);
					start.setHours(0, 0, 0);
					start = start.getTime();

					let end = null;
					if (!isEmpty(item.end_date)) {
						end = new Date(item.end_date);
						end.setHours(23, 59, 59);
						end = end.getTime();
					}

					if (!isEmpty(end)) {
						return now < start && now < end;
					} else {
						return now < start;
					}
				});
				break;
			case "expired":
				arr = data.filter(item => {
					let start = new Date(item.start_date);
					start.setHours(0, 0, 0);
					start = start.getTime();

					let end = null;
					if (!isEmpty(item.end_date)) {
						end = new Date(item.end_date);
						end.setHours(23, 59, 59);
						end = end.getTime();
					}

					if (!isEmpty(end)) {
						return now > start && now > end;
					} else {
						return false;
					}
				});
				break;
			default:
				arr = data.filter(item => {
					let start = new Date(item.start_date);
					start.setHours(0, 0, 0);
					start = start.getTime();

					let end = null;
					if (!isEmpty(item.end_date)) {
						end = new Date(item.end_date);
						end.setHours(23, 59, 59);
						end = end.getTime();
					}

					if (!isEmpty(end)) {
						return start <= now && end >= now;
					} else {
						return now >= start;
					}
				});
		}

		this.setState({
			arrPromo: arr,
		});
	};

	handlePromo =
		(id = null) =>
		() => {
			this.props.navigation.navigate("PromotionAdd", { id });
		};

	render() {
		const { arrPromo } = this.state;
		// console.log(arrPromo);

		const Box = ({ code, end, start, type, value, onPress }) => {
			const Time = ({ time }) => {
				const date = new Date(time);
				const m = date.getMonth() + 1;

				const tgl = date.getDate() > 10 ? date.getDate() : "0" + date.getDate();
				const bln = m > 10 ? m : "0" + m;

				return `${tgl}/${bln}/${date.getFullYear()}`;
			};

			return (
				<TouchableWithoutFeedback onPress={onPress}>
					<View style={styles.box}>
						<View style={[styles.infoBox, { alignItems: "center" }]}>
							<View style={{ flexDirection: "row", alignItems: "center" }}>
								<MaterialCommunityIcons name="timer-sand" size={20} color="#0D67F1" />
								<Typography
									type="body"
									style={{ color: "#0D67F1", fontFamily: "semibold", marginLeft: 3 }}
								>
									<Time time={start} /> - {!isEmpty(end) ? <Time time={end} /> : "No expiry"}
								</Typography>
							</View>
							<View>
								<Entypo name="chevron-thin-right" size={13} color={greydark} />
							</View>
						</View>
						<View style={[styles.infoBox, { marginTop: 5, alignItems: "flex-end" }]}>
							<View>
								<Typography type="largeTitle" numberOfLines={1} style={{ fontFamily: "semibold" }}>
									{code}
								</Typography>
							</View>
							<View>
								<Typography type="body" style={{ color: greydark, fontFamily: "bold" }}>
									{type == "percent" ? value + "%" : "RM" + value} OFF
								</Typography>
							</View>
						</View>
					</View>
				</TouchableWithoutFeedback>
			);
		};

		return (
			<LayoutView>
				<NavHeader onBack={this.handleBack} title="Promotions" actionText="Add" onPress={this.handlePromo()} />
				<View style={{ width: "100%", paddingTop: 12, flex: 1, position: "relative", paddingBottom: 50 }}>
					{isEmpty(arrPromo) ? (
						<View style={{ width: "100%", height: "100%" }}>
							<EmptyList title="No Promotions" text="There is no available promotions now" />
						</View>
					) : (
						<ScrollView style={{ width: "100%" }} showsVerticalScrollIndicator={false}>
							{arrPromo.map((item, index) => {
								return (
									<Box
										key={index}
										code={item.code}
										end={item.end_date}
										start={item.start_date}
										type={item.type}
										value={item.type == "percent" ? item.value_percent : item.value_fixed}
										onPress={this.handlePromo(item.id)}
									/>
								);
							})}
						</ScrollView>
					)}
					<SegmentedPicker
						rounded
						style={{ position: "absolute", bottom: 25 }}
						active={this.state.active}
						list={[
							{ text: "Active", value: "active", onPress: this.getActive("active") },
							{ text: "Incoming", value: "incoming", onPress: this.getActive("incoming") },
							{ text: "Expired", value: "expired", onPress: this.getActive("expired") },
						]}
					/>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	box: {
		width: "100%",
		padding: 10,
		backgroundColor: white,
		borderRadius: 10,
		marginBottom: 10,
	},
	infoBox: {
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-between",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	profile: state.profile,
	loading: state.loading,
	error: state.error,
	promo: state.promo,
});

export default connect(mapStateToProps, { getAllPromo })(withNavigationFocus(Promotion));
