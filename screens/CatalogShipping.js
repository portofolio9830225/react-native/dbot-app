import React from "react";
import { connect } from "react-redux";
import { StyleSheet, View, Text, Keyboard, Platform } from "react-native";

import { LayoutView } from "../component/Layout";
import NavHeader from "../component/NavHeader";

import { Entypo, MaterialCommunityIcons } from "@expo/vector-icons";
import { clearNoti, customNoti } from "../store/actions/alertAction";
import { Button, Card, TouchableRipple, RadioButton, Dialog, Portal } from "react-native-paper";
import PageTitle from "../component/PageTitle";
import isEmpty from "../utils/isEmpty";
import { Foundation, Ionicons } from "@expo/vector-icons";
import {
	accent,
	accentDark,
	black,
	green,
	grey,
	greyblack,
	greywhite,
	mainBgColor,
	transparent,
} from "../utils/ColorPicker";
import { ScrollView, TouchableWithoutFeedback } from "react-native-gesture-handler";
import langSelector from "../utils/langSelector";
import Collapsible from "react-native-collapsible";
import EmptyText from "../component/EmptyText";
import { editBusinessShipping, getBusinessShipping } from "../store/actions/catalogAction";
import { setLoading } from "../store/actions/loadingAction";
import { withNavigationFocus } from "react-navigation";
import { setInputError } from "../store/actions/errorAction";
import TextField from "../component/TextField";
import { editLogisticInfo, getLogisticInfo } from "../store/actions/logisticAction";
import PortalSafeView from "../component/PortalSafeView";
import Typography from "../component/Typography";
import { SectionBox, SectionDivider } from "../component/SectionComponents";
import { FontAwesome } from "@expo/vector-icons";
import AlertDialog from "../component/AlertDialog";

class CatalogShipping extends React.Component {
	state = {
		type: null,
		shipping: [],
		EPBalance: "0.00",
		EPkey: "",
		addRate: "0.00",
		selectorID: null,
		selectorData: null,
		isPortalThirdParty: false,
		isValThirdParty: null,
		apiKeyParcel: "",
		extraChargeParcel: "",
		shippingParcel: 1,
		apiKeyDelyva: "",
		extraChargeDelyva: "",
		companyID: "",
		customerID: "",
		shippingDelyva: 1,
		isProvider: "",
		isEditProvider: false,
		dialogEditProvider: false,
	};

	componentDidMount() {
		//this.props.setLoading(true);
		this.props.getBusinessShipping(this.props.auth.token);
		// this.setState({
		// 	type: this.props.profile.store.shipping_type,
		// });
	}

	componentDidUpdate(prevProps, prevState) {
		if (
			isEmpty(prevProps.alert.redirect) &&
			(this.props.alert.redirect === "editBusinessShipping" || this.props.alert.redirect === "editLogisticInfo")
		) {
			this.props.navigation.goBack();
		}
		if (isEmpty(prevProps.catalog.shipping) && !isEmpty(this.props.catalog.shipping)) {
			let { shipping_custom, shipping_service, type } = this.props.catalog.shipping;

			let ship_custom = [];
			shipping_custom.map(s => {
				ship_custom.push({ name: s.name, price: `${s.price}` });
			});
			this.setState({
				type,
				shipping: !isEmpty(shipping_custom) ? ship_custom : [],
				EPkey: !isEmpty(shipping_service) ? shipping_service.api_key : "",
				addRate: !isEmpty(shipping_service) ? `${shipping_service.additional_rate}` : "",
			});
		}
	}

	handleBack = () => {
		this.props.navigation.goBack();
	};

	handleText = name => val => {
		this.setState({
			[name]: val,
		});
	};

	handleType = val => {
		this.setState({
			type: val,
		});
	};

	handleType2 = val => () => {
		this.handleType(val);
	};

	handleSubmitRate = () => {
		let data = this.state.selectorData;
		let arr = [...this.state.shipping];
		if (this.state.selectorID >= arr.length) {
			arr.push(data);
		} else {
			arr[this.state.selectorID] = data;
		}

		this.setState({
			shipping: arr,
			selectorData: null,
			selectorID: null,
		});
	};

	handleDeleteRate = () => {
		let arr = [...this.state.shipping];
		if (arr.length <= 1) {
			this.props.customNoti("Need to set at least one rate", "error");
		} else {
			arr.splice(this.state.selectorID, 1);

			this.setState({
				shipping: arr,
				selectorData: null,
				selectorID: null,
			});
		}
	};

	handleChangeRate = (data, index) => () => {
		this.setState({
			selectorID: index,
			selectorData: data,
		});
	};

	handleAddRate = () => {
		this.setState({
			selectorID: this.state.shipping.length,
			selectorData: {
				name: "",
				price: "",
			},
		});
	};

	handleSubmit = () => {
		//this.handleSaveDialog(false)();
		this.props.setInputError();
		this.props.clearNoti();
		let { shipping, type, EPkey, addRate } = this.state;
		let data = {
			type,
			api_key: type == 2 ? EPkey : undefined,
			additional_rate: type == 2 ? addRate : undefined,
			custom_shipping: type == 1 ? shipping : undefined,
		};

		this.props.editBusinessShipping(data, this.props.auth.token);
	};

	handleThirdParty = val => () => {
		// console.log(name);
		this.setState({
			isPortalThirdParty: true,
			isValThirdParty: val,
		});
	};

	handleDoneProvider = val => () => {
		if (!isEmpty(this.state.isProvider)) {
			if (this.state.isProvider == val) {
				this.setState({
					isPortalThirdParty: false,
					isProvider: val,
					isEditProvider: true,
				});
			} else {
				// console.log("ada beda");
				this.setState({
					dialogEditProvider: true,
				});
			}
		} else {
			this.setState({
				isPortalThirdParty: false,
				isProvider: val,
				isEditProvider: true,
			});
		}
	};

	handleDialog = action => () => {
		this.setState({
			dialogEditProvider: action,
		});
	};

	render() {
		const {
			type,
			shipping,
			EPkey,
			addRate,
			selectorID,
			selectorData,
			isPortalThirdParty,
			isValThirdParty,
			apiKeyParcel,
			extraChargeParcel,
			shippingParcel,
			apiKeyDelyva,
			extraChargeDelyva,
			companyID,
			customerID,
			shippingDelyva,
			isProvider,
			isEditProvider,
			dialogEditProvider,
		} = this.state;
		const lang = langSelector.CatalogShipping.find(e => e.lang === this.props.preference.lang).data;

		const ItemSelector = ({ title, desc, val }) => {
			let isSelected = val == type;
			return (
				<TouchableRipple rippleColor="transparent" onPress={this.handleType2(val)}>
					<View
						style={{
							width: "100%",
							flexDirection: "row",
							justifyContent: "space-between",
							alignItems: "center",
							paddingVertical: 10,
						}}
					>
						<View style={{ flex: 1 }}>
							<Text style={{ fontSize: 15, fontFamily: "regular" }}>{title}</Text>
							{desc ? (
								<Text style={{ fontSize: 12, fontFamily: "regular", color: greyblack }}>{desc}</Text>
							) : null}
						</View>
						<View
							style={{
								width: 22,
								height: 22,
								borderRadius: 11,
								alignItems: "center",
								justifyContent: "center",
								borderWidth: 1,
								borderColor: isSelected ? accent : greywhite,
								backgroundColor: isSelected ? accent : "transparent",
							}}
						>
							{isSelected ? <Ionicons color="white" size={16} name="ios-checkmark" /> : null}
						</View>
					</View>
				</TouchableRipple>
			);
		};

		const ShippingPriceRow = ({ name, price, onChange }) => {
			return (
				<TouchableRipple rippleColor="transparent" onPress={onChange}>
					<View>
						<View
							style={{
								flexDirection: "row",
								justifyContent: "space-between",
								alignItems: "center",
								paddingVertical: 12,
								//paddingHorizontal: 15,
							}}
						>
							<Text style={{ fontFamily: "regular" }}>{name}</Text>
							<View style={{ flexDirection: "row", alignItems: "center" }}>
								<Text style={{ fontFamily: "regular", color: greyblack }}>
									RM{parseFloat(price).toFixed(2)}
								</Text>
								<Entypo name="chevron-thin-right" size={18} color={grey} />
							</View>
						</View>
						<SectionDivider />
					</View>
				</TouchableRipple>
			);
		};

		const ItemThirdParty = ({ name, onPress, isCheck }) => {
			return (
				<TouchableWithoutFeedback onPress={onPress}>
					<View
						style={{
							justifyContent: "space-between",
							width: "100%",
							paddingVertical: 12,
							flexDirection: "row",
						}}
					>
						<View>
							<Typography type="body">{name}</Typography>
						</View>
						<View style={{ flexDirection: "row" }}>
							{isCheck && <Entypo name="check" size={18} color={accent} />}
							<Entypo name="chevron-thin-right" size={18} color={grey} />
						</View>
					</View>
				</TouchableWithoutFeedback>
			);
		};

		return (
			<LayoutView>
				<NavHeader onBack={this.handleBack} title="Shipments" actionText="Done" onPress={this.handleSubmit} />

				<PortalSafeView>
					{!isEmpty(selectorID) ? (
						<View style={{ flex: 1, width: "100%", backgroundColor: mainBgColor, paddingHorizontal: 16 }}>
							<NavHeader
								onBack={() => {
									this.setState({
										selectorData: null,
										selectorID: null,
									});
								}}
								title="Rate"
								actionText="Done"
								onPress={this.handleSubmitRate}
							/>
							<SectionBox>
								<TextField
									inputStyle={{ paddingHorizontal: 0 }}
									label="Name"
									value={selectorData.name}
									onChangeText={val => {
										this.setState({
											selectorData: { ...this.state.selectorData, name: val },
										});
									}}
								/>
								<SectionDivider />
								<TextField
									inputStyle={{ paddingHorizontal: 0 }}
									keyboardType="decimal-pad"
									label="Price"
									value={selectorData.price}
									onChangeText={val => {
										this.setState({
											selectorData: { ...this.state.selectorData, price: val },
										});
									}}
									startAdornment="RM"
								/>
							</SectionBox>
						</View>
					) : null}
				</PortalSafeView>

				{isPortalThirdParty && (
					<PortalSafeView>
						<AlertDialog
							actions={[
								{
									title: "Confirm",
									onPress: () => {
										if (isProvider == "Delyva") {
											this.setState({
												isPortalThirdParty: false,
												isEditProvider: true,
												isProvider: "EasyParcel",
												dialogEditProvider: false,
											});
										} else {
											this.setState({
												isPortalThirdParty: false,
												isEditProvider: true,
												isProvider: "Delyva",
												dialogEditProvider: false,
											});
										}
									},
								},
								{
									title: "Cancel",
									onPress: this.handleDialog(false),
								},
							]}
							title="Change 3PL Provider?"
							desc={`This action will remove the existing configuration setting.`}
							active={dialogEditProvider}
							onDismiss={this.handleDialog(false)}
						/>

						<View style={{ flex: 1, width: "100%", backgroundColor: mainBgColor, paddingHorizontal: 16 }}>
							{isValThirdParty == 1 ? (
								<View>
									<NavHeader
										onBack={() => {
											this.setState({
												isPortalThirdParty: false,
											});
										}}
										title={"EasyParcel"}
										actionText="Done"
										onPress={this.handleDoneProvider("EasyParcel")}
									/>
									<View style={{ marginTop: 30 }}>
										<SectionBox title="EASY PARCEL INTEGRATION" style={{ marginBottom: 10 }}>
											<TextField
												inputStyle={{ paddingHorizontal: 0 }}
												label="API KEY"
												value={apiKeyParcel}
												onChangeText={val => {
													this.setState({
														apiKeyParcel: val,
													});
												}}
											/>
											<SectionDivider />
											<TextField
												inputStyle={{ paddingHorizontal: 0 }}
												keyboardType="decimal-pad"
												label="Extra charges"
												value={extraChargeParcel}
												startAdornment="RM"
												onChangeText={val => {
													this.setState({
														extraChargeParcel: val,
													});
												}}
											/>
										</SectionBox>
										<SectionBox title="SHIPMENTS METHOD">
											<TouchableWithoutFeedback
												onPress={() => {
													this.setState({
														shippingParcel: 1,
													});
												}}
											>
												<View
													style={{
														justifyContent: "space-between",
														width: "100%",
														paddingVertical: 12,
														flexDirection: "row",
													}}
												>
													<View>
														<Typography type="body">Drop-Off</Typography>
													</View>
													{shippingParcel == 1 && (
														<View>
															<Entypo name="check" size={18} color={accent} />
														</View>
													)}
												</View>
											</TouchableWithoutFeedback>
											<SectionDivider />
											<TouchableWithoutFeedback
												onPress={() => {
													this.setState({
														shippingParcel: 2,
													});
												}}
											>
												<View
													style={{
														justifyContent: "space-between",
														width: "100%",
														paddingVertical: 12,
														flexDirection: "row",
													}}
												>
													<View>
														<Typography type="body">Pick-Up by Courier</Typography>
													</View>
													{shippingParcel == 2 && (
														<View>
															<Entypo name="check" size={18} color={accent} />
														</View>
													)}
												</View>
											</TouchableWithoutFeedback>
										</SectionBox>
									</View>
								</View>
							) : (
								<View>
									<NavHeader
										onBack={() => {
											this.setState({
												isPortalThirdParty: false,
											});
										}}
										title={"Delyva"}
										actionText="Done"
										onPress={this.handleDoneProvider("Delyva")}
									/>
									<View style={{ marginTop: 30 }}>
										<SectionBox title="DELYVA INTEGRATION" style={{ marginBottom: 10 }}>
											<TextField
												inputStyle={{ paddingHorizontal: 0 }}
												label="API KEY"
												value={apiKeyDelyva}
												onChangeText={val => {
													this.setState({
														apiKeyDelyva: val,
													});
												}}
											/>
											<SectionDivider />
											<TextField
												inputStyle={{ paddingHorizontal: 0 }}
												label="Customer ID"
												value={customerID}
												onChangeText={val => {
													this.setState({
														customerID: val,
													});
												}}
											/>
											<SectionDivider />
											<TextField
												inputStyle={{ paddingHorizontal: 0 }}
												label="Company ID"
												value={companyID}
												onChangeText={val => {
													this.setState({
														companyID: val,
													});
												}}
											/>
											<SectionDivider />
											<TextField
												inputStyle={{ paddingHorizontal: 0 }}
												keyboardType="decimal-pad"
												label="Extra charges"
												value={extraChargeDelyva}
												startAdornment="RM"
												onChangeText={val => {
													this.setState({
														extraChargeDelyva: val,
													});
												}}
											/>
										</SectionBox>
										<SectionBox title="SHIPMENTS METHOD">
											<TouchableWithoutFeedback
												onPress={() => {
													this.setState({
														shippingDelyva: 1,
													});
												}}
											>
												<View
													style={{
														justifyContent: "space-between",
														width: "100%",
														paddingVertical: 12,
														flexDirection: "row",
													}}
												>
													<View>
														<Typography type="body">Drop-Off</Typography>
													</View>
													{shippingDelyva == 1 && (
														<View>
															<Entypo name="check" size={18} color={accent} />
														</View>
													)}
												</View>
											</TouchableWithoutFeedback>
											<SectionDivider />
											<TouchableWithoutFeedback
												onPress={() => {
													this.setState({
														shippingDelyva: 2,
													});
												}}
											>
												<View
													style={{
														justifyContent: "space-between",
														width: "100%",
														paddingVertical: 12,
														flexDirection: "row",
													}}
												>
													<View>
														<Typography type="body">Pick-Up by Courier</Typography>
													</View>
													{shippingDelyva == 2 && (
														<View>
															<Entypo name="check" size={18} color={accent} />
														</View>
													)}
												</View>
											</TouchableWithoutFeedback>
										</SectionBox>
									</View>
								</View>
							)}
						</View>
					</PortalSafeView>
				)}

				<View style={styles.container}>
					<ScrollView
						showsVerticalScrollIndicator={false}
						style={{ width: "100%" }}
						contentContainerStyle={styles.priceSection}
					>
						<SectionBox title="Type">
							<ItemSelector
								title="No Shipping"
								desc="Address will not be collected during checkout"
								val={0}
							/>
							<SectionDivider />
							<ItemSelector title="Flat-Rate" val={1} />
							<SectionDivider />
							<ItemSelector title="Third Party Logistics (3PL)" val={2} />
						</SectionBox>

						<Collapsible collapsed={type != 1}>
							<SectionBox title="Flat-rate">
								{!isEmpty(shipping) &&
									shipping.map((e, i) => {
										return (
											<ShippingPriceRow
												key={i}
												name={e.name}
												price={e.price}
												onChange={this.handleChangeRate(e, i)}
											/>
										);
									})}
								<TouchableRipple rippleColor="transparent" onPress={this.handleAddRate}>
									<View style={{ flexDirection: "row", alignItems: "center", height: 52 }}>
										<MaterialCommunityIcons name="plus" size={23} color={accent} />
										<Typography
											type="headline"
											style={{
												color: accent,
												fontFamily: "regular",
												marginLeft: 12,
											}}
										>
											Add
										</Typography>
									</View>
								</TouchableRipple>
							</SectionBox>
						</Collapsible>
						<Collapsible collapsed={type != 2}>
							{isEditProvider ? (
								<SectionBox
									title="SELECT 3PL PROVIDER"
									isEdit
									onEdit={() => this.setState({ isEditProvider: false })}
								>
									<ItemThirdParty
										name={isProvider}
										onPress={
											isProvider == "EasyParcel"
												? this.handleThirdParty(1)
												: this.handleThirdParty(2)
										}
									/>
								</SectionBox>
							) : (
								<SectionBox title="SELECT 3PL PROVIDER">
									<ItemThirdParty
										name="EasyParcel"
										onPress={this.handleThirdParty(1)}
										isCheck={isProvider == "EasyParcel" ? true : false}
									/>
									<SectionDivider />
									<ItemThirdParty
										name="Delyva"
										onPress={this.handleThirdParty(2)}
										isCheck={isProvider == "Delyva" ? true : false}
									/>
								</SectionBox>
							)}
						</Collapsible>
						{/* <Collapsible collapsed={type != 2}>
							<SectionBox title="EasyParcel Integration">
								<TextField
									inputStyle={{ paddingHorizontal: 0 }}
									mode="outlined"
									label="API key"
									value={EPkey}
									onChangeText={this.handleText("EPkey")}
								/>
								<SectionDivider />
								<TextField
									inputStyle={{ paddingHorizontal: 0 }}
									keyboardType="decimal-pad"
									mode="outlined"
									label="Extra charges"
									value={addRate}
									startAdornment="RM"
									onChangeText={this.handleText("addRate")}
								/>
							</SectionBox>
						</Collapsible> */}
					</ScrollView>
				</View>
			</LayoutView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		paddingBottom: 30,
	},
	priceSection: {
		alignSelf: "center",
		width: "100%",
	},
	cardContainer: {
		width: "99%",
		alignSelf: "center",
		paddingHorizontal: 10,
		paddingVertical: 20,
		marginVertical: 20,
	},
	sectionBox: {
		//width: "100%",
		display: "flex",
		flexDirection: "column",
		// borderBottomWidth: 1,
		// borderBottomColor: greywhite,
		paddingVertical: 14,
	},
	sectionTitle: {
		fontFamily: "regular",
		textTransform: "uppercase",
		marginBottom: 5,
		paddingLeft: 12,
	},
	sectionContent: {
		width: "100%",
		backgroundColor: "white",
		borderRadius: 10,
		paddingHorizontal: 20,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	profile: state.profile,
	preference: state.preference,
	catalog: state.catalog,
	item: state.item,
	logistic: state.logistic,
	loading: state.loading,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	getBusinessShipping,
	editBusinessShipping,
	getLogisticInfo,
	editLogisticInfo,
	setLoading,
	customNoti,
	clearNoti,
	setInputError,
})(withNavigationFocus(CatalogShipping));
