import React from "react";

import * as Font from "expo-font";
import * as SplashScreen from "expo-splash-screen";
import * as Updates from "expo-updates";
import * as ScreenOrientation from "expo-screen-orientation";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { isTestAppLoading } from "../utils/key";

import { StyleSheet, View } from "react-native";

import { connect } from "react-redux";
import { checkToken, logoutUser } from "../store/actions/authAction";
import { setLoading } from "../store/actions/loadingAction";
import { customNoti } from "../store/actions/alertAction";
import { getRegProgress } from "../store/actions/profileAction";

import { LayoutView } from "../component/Layout";

import isEmpty from "../utils/isEmpty";
import registerPushNoti from "../utils/registerPushNoti";
import { SkypeIndicator } from "react-native-indicators";
import { greyblack } from "../utils/ColorPicker";
import { NoNetworkIcon } from "../assets/VectorIcons";
import getWidth from "../utils/getWidth";
import CustomButton from "../component/CustomButton";
import Typography from "../component/Typography";
import Loading from "../component/Loading";

class EntryPoint extends React.Component {
	state = {
		isReady: false,
		isUpdated: 0,
		isTokenMissing: false,
		isErrorRetry: false,
		dot: [],
	};

	async componentDidMount() {
		await Font.loadAsync({
			bold: require("../assets/fonts/Bold.ttf"),
			semibold: require("../assets/fonts/SemiBold.ttf"),
			medium: require("../assets/fonts/Medium.ttf"),
			regular: require("../assets/fonts/Regular.ttf"),
			light: require("../assets/fonts/Light.ttf"),
			thin: require("../assets/fonts/Thin.ttf"),
		});

		this.setState({
			isReady: true,
		});
	}

	// async getResourcesAsync() {

	// }

	// onFinish = async () => {

	// };

	onError = err => {
		console.log(err);
	};

	errorRetry = () => {
		// this.props.setLoading(true);
		this.setState({
			isErrorRetry: true,
		});
	};

	async componentDidUpdate(prevProps, prevState) {
		const isUpdated = this.state.isUpdated;

		if (!prevState.isReady && this.state.isReady) {
			if (process.env.NODE_ENV === "production") {
				const update = await Updates.checkForUpdateAsync();

				this.setState({
					isUpdated: update.isAvailable ? 1 : 9,
				});
			} else {
				this.setState({
					isUpdated: isTestAppLoading ? 1 : 9,
				});
			}
		}
		if ((prevState.isUpdated === 0 && isUpdated === 9) || (!prevState.isErrorRetry && this.state.isErrorRetry)) {
			//console.log("masuk dah");
			this.setState({
				isErrorRetry: false,
			});
			const login1 = await AsyncStorage.getItem("@dbot:login1");

			if (isEmpty(login1)) {
				SplashScreen.hideAsync();
				AsyncStorage.setItem("@dbot:login1", "true");
				this.props.logoutUser();
				this.props.navigation.navigate("SignIn");
			} else {
				this.props.setLoading(true);
				const businessToken = AsyncStorage.getItem("@dbot:businessToken");
				const refreshToken = AsyncStorage.getItem("@dbot:refreshToken");

				Promise.all([businessToken, refreshToken])
					.then(async res => {
						let bToken = res[0];
						let rToken = res[1];
						this.props.checkToken(bToken, rToken);
					})
					.catch(err => {
						this.setState({
							isTokenMissing: true,
						});
					});
			}
		}

		if (!isEmpty(this.props.auth.token) && isEmpty(prevProps.auth.token)) {
			AsyncStorage.setItem("@dbot:businessToken", this.props.auth.token);
			AsyncStorage.setItem("@dbot:refreshToken", this.props.auth.refreshToken);
		}
		if (!isEmpty(this.props.profile.store) && isEmpty(prevProps.profile.store)) {
			registerPushNoti(this.props.auth.token);
		}
		if (this.props.auth.regNotiTry && !prevProps.auth.regNotiTry) {
			if (!isEmpty(this.props.alert.notiScreen)) {
				this.props.navigation.navigate(this.props.alert.notiScreen, this.props.alert.notiParams);
			} else {
				this.props.navigation.navigate("Registration");
			}
		}
		if (prevState.isUpdated !== isUpdated && isUpdated === 1) {
			SplashScreen.hideAsync();
			setTimeout(() => {
				this.setState({
					dot: ["."],
				});
			}, 1000);
			setTimeout(async () => {
				let update = await Updates.fetchUpdateAsync();
				if (update.isNew) {
					Updates.reloadAsync();
				}
			}, 3000);
		}
		if (prevState.dot.length !== this.state.dot.length) {
			setTimeout(() => {
				let arr = [];
				let num = this.state.dot.length % 3;

				for (i = 1; i <= num + 1; i++) {
					arr.push(".");
				}

				this.setState({
					dot: arr,
				});
			}, 800);
		}
		if (
			(!isEmpty(prevProps.auth.token) && isEmpty(this.props.auth.token)) ||
			(!prevState.isTokenMissing && this.state.isTokenMissing)
		) {
			this.props.logoutUser();
			this.props.navigation.navigate("SignIn");
		}
	}

	componentWillUnmount() {
		SplashScreen.hideAsync();
		this.props.setLoading(false);
	}

	render() {
		const { isReady, isUpdated } = this.state;
		const { isLoading } = this.props.loading;
		const { isAuthenticated } = this.props.auth;

		if (isReady) {
			if (!isLoading) {
				return (
					<LayoutView>
						{!isAuthenticated && isUpdated === 9 ? (
							<View style={styles.container}>
								<NoNetworkIcon size={getWidth * 0.4} />

								<Typography type="title3" style={styles.title}>
									No network
								</Typography>
								<Typography style={styles.text}>Cannot connect to StoreUp.</Typography>
								<CustomButton style={styles.button} onPress={this.errorRetry} loading={isLoading}>
									Retry
								</CustomButton>
							</View>
						) : (
							<View style={styles.container}>
								<Loading />
								{/* <SkypeIndicator
									animating={true}
									count={5}
									animationDuration={1000}
									size={100}
									color={black}
								/> */}
								<Typography type="headline" style={styles.loadingText}>
									Downloading latest version
								</Typography>
							</View>
						)}
					</LayoutView>
				);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
}

const styles = StyleSheet.create({
	container: {
		alignSelf: "center",
		flex: 1,
		width: "100%",
		justifyContent: "center",
		alignItems: "center",
	},
	image: {
		width: "30%",
		height: undefined,
		aspectRatio: 1,
	},
	loadingText: {
		width: "100%",
		textAlign: "center",
		marginBottom: 50,
		color: greyblack,
		fontFamily: "semibold",
	},
	title: {
		color: greyblack,
		textAlign: "center",
		marginVertical: 20,
		fontFamily: "bold",
	},
	text: {
		textAlign: "center",
		marginBottom: 30,
	},
	button: {
		width: "96%",
		paddingVertical: 3,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	profile: state.profile,
	loading: state.loading,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {
	getRegProgress,
	checkToken,
	logoutUser,
	customNoti,
	setLoading,
})(EntryPoint);
