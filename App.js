import React, { Component } from "react";
import { GestureHandlerRootView } from "react-native-gesture-handler";
import { Provider } from "react-redux";
import { Provider as UIProvider } from "react-native-paper";

import store from "./store";
import theme from "./theme";
import Layout from "./component/Layout";
import AppNavigator from "./AppNavigator";

class App extends Component {
	render() {
		return (
			<GestureHandlerRootView style={{ flex: 1 }}>
				<Provider store={store}>
					<UIProvider theme={theme}>
						<Layout>
							<AppNavigator />
						</Layout>
					</UIProvider>
				</Provider>
			</GestureHandlerRootView>
		);
	}
}

export default App;
