import { Text, StyleSheet, View, ScrollView, Platform } from "react-native";
import { TextInput, Portal, Dialog, Divider, List, TouchableRipple } from "react-native-paper";
import React from "react";
import getWidth from "../utils/getWidth";
import { SectionBox, SectionDivider } from "../component/SectionComponents";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { Feather, Ionicons, MaterialIcons } from "@expo/vector-icons";
import { accent, greywhite, mainBgColor } from "../utils/ColorPicker";
import NavHeader from "./NavHeader";
import Typography from "./Typography";
import { connect } from "react-redux";
import isEmpty from "../utils/isEmpty";
import { withNavigation } from "react-navigation";
import InventoryFilter from "./InventoryFilter";
import EmptyText from "./EmptyText";

class CategorySelect extends React.Component {
	state = {
		arr: [],
		filterArr: [],
		filterDialog: false,
	};

	componentDidMount() {
		this.setState({
			arr: this.props.selected,
			filterArr: this.props.filterArr,
		});
	}

	handleFilterActive = value => () => {
		let array = [];
		let checked = this.state.arr.findIndex(f => f === value) != -1;

		if (!checked) {
			array = [...this.state.arr, value];
		} else {
			array = this.state.arr.filter(e => e !== value);
		}

		this.setState({
			arr: array,
		});
	};

	handleFilterDialog = state => () => {
		this.setState({
			filterDialog: state,
		});
	};

	handleFilter = value => () => {
		this.handleFilterDialog(false)();
		this.setState(
			{
				filterArr: value,
			},
			() => {
				this.props.handleFilter(value);
			}
		);
	};

	filteredItem = (arr, key) => {
		let { filterArr } = this.state;

		if (!isEmpty(filterArr)) {
			let cArr = [];
			let condition = "";

			if (filterArr.findIndex(f => f === "Available") != -1) {
				cArr.push(`fil.${key} === true`);
			}
			if (filterArr.findIndex(f => f === "Unavailable") != -1) {
				cArr.push(`fil.${key} === false`);
			}

			cArr.map((e, i) => {
				let text = e;
				if (i !== 0) {
					text = ` || ${text}`;
				}
				condition += text;
			});

			return arr.filter(fil => eval(condition));
		} else {
			return arr;
		}
	};

	render() {
		const { filterArr, filterDialog } = this.state;
		const { categories } = this.props.category;

		const FilterList = props => {
			return (
				<TouchableWithoutFeedback onPress={this.handleFilterActive(props.val)}>
					<View
						style={{
							height: 44,
							alignItems: "center",
							justifyContent: "space-between",
							flexDirection: "row",
						}}
					>
						<Text>{props.text}</Text>
						{this.state.arr.findIndex(f => f === props.val) != -1 ? (
							<Ionicons name="checkmark-circle" size={20} color={accent} />
						) : null}
					</View>
				</TouchableWithoutFeedback>
			);
		};

		return (
			<Portal>
				{filterDialog ? (
					<InventoryFilter
						onCancel={this.handleFilterDialog(false)}
						onSubmit={this.handleFilter}
						selected={filterArr}
					/>
				) : null}
				<View
					style={{
						width: getWidth + 32,
						height: "100%",
						bottom: 0,
						position: "absolute",
						zIndex: 100,
						backgroundColor: mainBgColor,
						// borderTopLeftRadius: 40,
						// borderTopRightRadius: 40,
						paddingHorizontal: 20,
					}}
				>
					<View
						style={{
							justifyContent: "space-between",
							width: "100%",
							marginTop: 40,
							flexDirection: "row",
						}}
					>
						<NavHeader
							onBack={this.props.onCancel}
							backText="Cancel"
							actionText="Done"
							onPress={this.props.onSubmit(this.state.arr)}
						/>
					</View>
					{!isEmpty(this.filteredItem(categories, "active")) ? (
						<ScrollView showsVerticalScrollIndicator={false}>
							<SectionBox title="Select">
								{this.filteredItem(categories, "active").map((c, i) => {
									return (
										<View key={i}>
											{i !== 0 ? <SectionDivider /> : null}
											<FilterList val={c.id} text={c.name} />
										</View>
									);
								})}
							</SectionBox>
						</ScrollView>
					) : (
						<View style={{ flex: 1, padding: 50 }}>
							<EmptyText
								title="No Categories"
								text="There is no category related to the entered information"
							/>
						</View>
					)}
					<View
						style={{
							position: "absolute",
							bottom: 0,
							height: 50,
							borderTopColor: greywhite,
							borderTopWidth: 1,
							width: getWidth + 32,
							display: "flex",
							justifyContent: "space-between",
							flexDirection: "row",
							alignItems: "center",
							paddingHorizontal: 24,
							backgroundColor: mainBgColor,
							marginBottom: Platform.OS === "ios" ? 16 : 0,
						}}
					>
						<TouchableWithoutFeedback onPress={this.handleFilterDialog(true)}>
							<View style={styles.circleIcon}>
								<MaterialIcons name="filter-list" size={19} color={accent} />
							</View>
						</TouchableWithoutFeedback>
						{!isEmpty(filterArr) ? (
							<View
								style={{
									alignItems: "center",
									justifyContent: "center",
									flex: 1,
									paddingHorizontal: 12,
								}}
							>
								<Typography type="caption">Filter by:</Typography>
								<View style={{ flexDirection: "row" }}>
									{filterArr.map((f, i) => {
										let text = f;
										if (i !== 0) {
											text = `, ${text}`;
										}
										return (
											<Typography type="caption" style={{ color: accent }}>
												{text}
											</Typography>
										);
									})}
								</View>
							</View>
						) : null}
						<TouchableWithoutFeedback onPress={this.props.handleAddCategory}>
							<View style={styles.circleIcon}>
								<Feather name="plus" size={19} color={accent} />
							</View>
						</TouchableWithoutFeedback>
					</View>
				</View>
			</Portal>
		);
	}
}

const styles = StyleSheet.create({
	circleIcon: {
		width: 24,
		height: 24,
		borderRadius: 12,
		borderColor: accent,
		borderWidth: 1,
		alignItems: "center",
		justifyContent: "center",
		// backgroundColor: "green",
	},
});

const mapStateToProps = state => ({
	category: state.category,
	loading: state.loading,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, {})(withNavigation(CategorySelect));
