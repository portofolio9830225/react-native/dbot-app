import React, { createRef } from "react";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { Keyboard, View } from "react-native";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import TimeConverter from "../utils/TimeConverter";
import TextField from "./TextField";

class TimeField extends React.Component {
	state = {
		date: null,
		open: false,
	};

	componentDidMount() {
		if (this.props.value) {
			let val = this.props.value.split(":");
			let newDate = new Date();
			newDate.setHours(parseInt(val[0]));
			newDate.setMinutes(parseInt(val[1]));
			this.setState({
				rawDate: newDate,
				date: TimeConverter(this.props.value),
			});
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (this.props.value && prevProps.value !== this.props.value) {
			let val = this.props.value.split(":");
			let newDate = new Date();
			newDate.setHours(parseInt(val[0]));
			newDate.setMinutes(parseInt(val[1]));
			this.setState({
				rawDate: newDate,
				date: TimeConverter(this.props.value),
			});
		}
	}

	handleDialog = state => () => {
		this.setState({
			open: state,
		});
	};

	handleConfirm = date => {
		let newDate = new Date(date);
		this.handleDialog(false)();
		let hour = newDate.getHours();
		let minute = newDate.getMinutes();
		if (hour < 10) {
			hour = `0${hour}`;
		}
		if (minute < 10) {
			minute = `0${minute}`;
		}
		let time = `${hour}:${minute}:00`;

		this.setState(
			{
				date: TimeConverter(time),
			},
			() => {
				this.props.onChange(time);
			}
		);
	};

	render() {
		const { rawDate, date, open } = this.state;

		return (
			<View>
				<TouchableWithoutFeedback onPress={this.handleDialog(true)}>
					<View pointerEvents="none">
						<TextField {...this.props} editable={false} value={date} />
					</View>
				</TouchableWithoutFeedback>
				<DateTimePickerModal
					mode="time"
					isVisible={open}
					onConfirm={this.handleConfirm}
					onCancel={this.handleDialog(false)}
					is24Hour={false}
					date={rawDate}
				/>
			</View>
		);
	}
}

export default TimeField;
