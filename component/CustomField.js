import React from "react";
import { StyleSheet, Text, View } from "react-native";
import * as DocumentPicker from "expo-document-picker";

import { greyblack, greywhite } from "../utils/ColorPicker";
import { Button, Dialog, List, Portal } from "react-native-paper";
import TextField from "./TextField";
import isEmpty from "../utils/isEmpty";
import uriToFile from "../utils/uriToFile";
import { connect } from "react-redux";
import AlertDialog from "./AlertDialog";
import Typography from "./Typography";

class CustomField extends React.Component {
	state = {
		fileName: "",
		choose: false,
		langSelector: [
			{
				lang: "en",
				data: {
					fileType: "File type",
					upload: "upload",
					change: "change",
					image: "Image",
				},
			},
			{
				lang: "ms",
				data: {
					fileType: "Jenis fail",
					upload: "muatnaik",
					change: "tukar",
					image: "Gambar",
				},
			},
		],
	};

	handleChoose = state => () => {
		this.setState({
			choose: state,
		});
	};

	handleText = e => {
		this.setState(
			{
				fileName: e,
			},
			() => {
				this.props.onChange(
					`customField-${this.props.id}`,
					this.props.title,
					this.state.fileName,
					this.props.type
				);
			}
		);
	};

	fileUpload = type => async () => {
		// TYPE
		// 1: pdf
		// 2: image
		this.handleChoose(false)();
		let doc = await DocumentPicker.getDocumentAsync({
			type: "image/*",
		});

		if (doc.type === "success") {
			this.props.onChange(`customField-${this.props.id}`, this.props.title, uriToFile(doc.uri), this.props.type);
			this.setState({
				fileName: doc.name,
			});
		}
	};

	render() {
		const { title, id, type } = this.props;
		const { fileName, choose, langSelector } = this.state;
		const lang = langSelector.find(e => e.lang === this.props.preference.lang).data;

		if (type === 1) {
			return (
				<View style={styles.root}>
					<AlertDialog
						active={choose}
						title={lang.fileType}
						onDismiss={this.handleChoose(false)}
						actions={[
							{ title: "PDF", onPress: this.fileUpload(1) },
							{ title: "Image", onPress: this.fileUpload(2) },
						]}
					/>
					<View style={styles.left}>
						<Typography type="headline" style={styles.title}>
							{title}
						</Typography>
						<Typography style={styles.desc}>
							{!isEmpty(fileName) ? fileName : `${lang.fileType}: jpg, jpeg, png`}
						</Typography>
					</View>

					<Button mode="outlined" onPress={this.fileUpload()}>
						{isEmpty(fileName) ? lang.upload : lang.change}
					</Button>
				</View>
			);
		} else if (type === 2) {
			return (
				<TextField
					style={styles.textField}
					mode="outlined"
					label={title}
					value={fileName}
					onChangeText={this.handleText}
				/>
			);
		} else {
			return <View />;
		}
	}
}
const styles = StyleSheet.create({
	root: {
		display: "flex",
		width: "100%",
		borderRadius: 7,
		marginVertical: 10,
		paddingHorizontal: 15,
		paddingVertical: 10,
		backgroundColor: "white",
	},
	left: {
		display: "flex",
		flexDirection: "column",
		marginBottom: 20,
	},
	title: {
		fontFamily: "semibold",
		marginBottom: 5,
	},
	desc: {
		color: greyblack,
	},
	uploadIcon: {
		fontSize: 25,
		marginRight: 5,
	},
	textField: {
		alignSelf: "center",
		width: "100%",
		marginVertical: 10,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
});

export default connect(mapStateToProps)(CustomField);
