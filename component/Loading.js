import React from "react";
import { View, Image, Text } from "react-native";
import { UIActivityIndicator } from "react-native-indicators";
import { greyblack } from "../utils/ColorPicker";
import Typography from "./Typography";

class Loading extends React.Component {
	render() {
		return (
			<View
				style={{
					flex: 1,
					alignSelf: "center",
					justifyContent: "center",
					alignItems: "center",
				}}
			>
				<Image
					source={require("../assets/LoadingCircle.gif")}
					style={{ width: 50, height: undefined, aspectRatio: 1 }}
				/>
				<Typography type="caption" style={{ color: greyblack }}>
					LOADING
				</Typography>
				{/* <UIActivityIndicator
					color={greyblack}
					count={8}
					size={60}
					// style={{
					//   flex: 2,
					// }}
				/> */}
			</View>
		);
	}
}

export default Loading;
