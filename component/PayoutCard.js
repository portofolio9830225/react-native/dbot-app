import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { black, greyblack, mainBgColor, transparent, white } from "../utils/ColorPicker";
import { AntDesign, Entypo, Feather } from "@expo/vector-icons";
import { TouchableRipple } from "react-native-paper";
import langSelector from "../utils/langSelector";
import { connect } from "react-redux";
import DateConverter from "../utils/DateConverter";
import Typography from "./Typography";

class PayoutCard extends React.Component {
	render() {
		const { id, date, amount, onPress } = this.props;
		const lang = langSelector.PayoutList.find(e => e.lang === this.props.preference.lang).data;

		const InfoBox = ({ title, desc }) => {
			return (
				<View style={{ width: "100%" }}>
					<Typography type="subheadline">{title}</Typography>
					<Typography type="headline" style={{ fontFamily: "semibold" }}>
						{desc}
					</Typography>
				</View>
			);
		};

		return (
			<TouchableRipple onPress={onPress} rippleColor={transparent} style={{ width: "100%" }}>
				<View
					style={[
						styles.payoutBox,
						{
							borderRadius: 15,
							backgroundColor: white,
							// shadowColor: "#000",
							// shadowOffset: {
							// 	width: 0,
							// 	height: 1,
							// },
							// shadowOpacity: 0.22,
							// shadowRadius: 2.22,
							// elevation: 3,
						},
					]}
				>
					<View style={styles.payoutBoxLeft}>
						<InfoBox title={lang.cardTitle} desc={DateConverter(date)} />
					</View>
					<View style={styles.payoutBoxRight}>
						<View style={{ width: "80%" }}>
							<InfoBox title={lang.cardAmount} desc={`RM${amount.toFixed(2)}`} />
						</View>
						<Entypo name="chevron-thin-right" color={greyblack} size={16} />
					</View>
				</View>
			</TouchableRipple>
		);
	}
}

const styles = StyleSheet.create({
	payoutBox: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		alignSelf: "center",
		marginBottom: 12,
		paddingHorizontal: 15,
		width: "100%",
		height: 72,
	},
	payoutBoxLeft: {
		width: "60%",
		height: "100%",
		//alignItems: "center",
		justifyContent: "space-evenly",
	},
	payoutBoxRight: {
		width: "40%",
		height: "100%",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "flex-end",
		// alignItems: "flex-end",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
});

export default connect(mapStateToProps)(PayoutCard);
