import React from "react";

import { StyleSheet, View, Text, Image, ScrollView } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import Animated, { EasingNode } from "react-native-reanimated";
import CachedImage from "expo-cached-image";

import { TextInput, Portal, Dialog, Divider, List, TouchableRipple } from "react-native-paper";

import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import * as ImagePicker from "expo-image-picker";
import {
	accent,
	blackTransparent,
	grey,
	greydark,
	greylight,
	greywhite,
	mainBgColor,
	red,
	transparent,
	white,
} from "../utils/ColorPicker";
import isEmpty from "../utils/isEmpty";
import NavHeader from "./NavHeader";
import getWidth from "../utils/getWidth";
import { getCameraPermissionAsync, getGalleryPermissionAsync } from "../utils/PermissionGetter";
import { connect } from "react-redux";
import { addImagePool } from "../store/actions/itemAction";
import uriToFile from "../utils/uriToFile";
import Loading from "./Loading";
import Typography from "./Typography";

const CARD_HEIGHT = 200;

class ImagePoolEditor extends React.Component {
	constructor(props) {
		super(props);
		this.transValue = new Animated.Value(1);
	}

	state = {
		dialog: false,
		selected: [],
	};

	componentDidMount() {
		this.setState({
			selected: this.props.data,
		});
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevState.dialog !== this.state.dialog) {
			Animated.timing(this.transValue, {
				toValue: this.state.dialog ? 2 : 1,
				duration: 250,
				easing: EasingNode.in, // Easing is an additional import from react-native
				useNativeDriver: true, // To make use of native driver for performance
			}).start();
		}
	}

	handleAddDialog = state => () => {
		this.setState({
			dialog: state,
		});
	};

	pickImageFromGallery = async () => {
		let status = await getGalleryPermissionAsync();
		if (status === "granted") {
			let result = await ImagePicker.launchImageLibraryAsync({
				mediaTypes: ImagePicker.MediaTypeOptions.Images,
				allowsEditing: true,
				aspect: [1, 1],
			});
			if (!result.cancelled) {
				this.handleAddImage(result.uri);
			}
		}
	};

	pickImageFromCamera = async () => {
		let status1 = await getCameraPermissionAsync();
		let status2 = await getGalleryPermissionAsync();

		if (status1 === "granted" && status2 === "granted") {
			let result = await ImagePicker.launchCameraAsync({
				mediaTypes: ImagePicker.MediaTypeOptions.Images,
				allowsEditing: true,
				aspect: [1, 1],
			});

			if (!result.cancelled) {
				this.handleAddImage(result.uri);
			}
		}
	};

	handleAddImage = fileURI => {
		this.setState({
			dialog: false,
		});
		let data = new FormData();
		let file = uriToFile(fileURI);
		data.append("file", file);
		data.append("fileName", `imagepool_${this.props.profile.store.id}`);
		data.append("folder", `business/${this.props.profile.store.id}/imagepool`);

		this.props.addImagePool(data, this.props.auth.token);
	};

	handleImgArr = data => () => {
		let arr = [...this.state.selected];

		let foundI = arr.findIndex(f => f === data);

		if (foundI === -1) {
			arr.push(data);
		} else {
			arr.splice(foundI, 1);
		}
		this.setState({
			selected: arr.filter(f => !isEmpty(f)),
		});
	};

	render() {
		const { visible, selectedImgs, onCancel, onSubmit } = this.props;
		const { selected, dialog } = this.state;
		const { imagePool } = this.props.item;
		const { isLoading } = this.props.loading;

		const CardMove = this.transValue.interpolate({
			inputRange: [1, 2],
			outputRange: [CARD_HEIGHT, 0],
		});

		const ImageBox = ({ id, img, selected, selectedNum, cacheKey }) => {
			let boxWidth = (getWidth + 32) / 3;

			return (
				<TouchableWithoutFeedback onPress={!isEmpty(img) ? this.handleImgArr(id) : this.handleAddDialog(true)}>
					<View
						style={[
							{
								alignItems: "center",
								justifyContent: "center",
								height: undefined,
								width: boxWidth,
								aspectRatio: 1,
								position: "relative",
							},
						]}
					>
						{selected ? (
							<View
								style={{
									position: "absolute",
									width: "100%",
									height: "100%",
									backgroundColor: blackTransparent,
									zIndex: 10,
								}}
							>
								<View
									style={{
										position: "absolute",
										width: "20%",
										height: undefined,
										aspectRatio: 1,
										borderRadius: boxWidth / 5 / 2,
										backgroundColor: accent,
										alignItems: "center",
										justifyContent: "center",
										bottom: 5,
										right: 5,
									}}
								>
									<Typography type="caption" style={{ color: white, fontFamily: "medium" }}>
										{selectedNum}
									</Typography>
								</View>
							</View>
						) : null}
						{!isEmpty(img) ? (
							<CachedImage
								style={{ width: "100%", height: "100%" }}
								source={{ uri: img }}
								cacheKey={cacheKey}
							/>
						) : (
							<>
								<MaterialCommunityIcons name="camera-plus-outline" size={25} color={grey} />
								<Typography
									style={{
										color: greydark,
										marginTop: 5,
									}}
								>
									New image
								</Typography>
							</>
						)}
					</View>
				</TouchableWithoutFeedback>
			);
		};

		const DialogButton = ({ text, color, onPress }) => {
			return (
				<TouchableRipple
					rippleColor="transparent"
					style={{
						backgroundColor: greywhite,
						width: "100%",
						borderRadius: 10,
						height: 40,
						alignItems: "center",
						justifyContent: "center",
					}}
					onPress={onPress}
				>
					<Text style={{ fontFamily: "regular", color, textAlign: "center", fontSize: 16 }}>{text}</Text>
				</TouchableRipple>
			);
		};

		return (
			<Portal>
				{isLoading ? (
					<View style={{ flex: 1, zIndex: 100 }} />
				) : (
					<View
						style={{
							width: "100%",
							height: "100%",
							position: "relative",
							flexDirection: "row",
							// backgroundColor: "red",
						}}
					>
						<TouchableRipple
							style={{
								position: "absolute",
								zIndex: 10,
								bottom: 0,
								backgroundColor: blackTransparent,
								width: "100%",
								height: "100%",
							}}
							rippleColor="transparent"
							onPress={this.handleAddDialog(false)}
						>
							<View />
						</TouchableRipple>
						<View
							style={{
								zIndex: 11,
								alignSelf: "flex-end",
								width: "100%",
								height: "90%",
								backgroundColor: mainBgColor,
								borderRadius: 16,
							}}
						>
							<View
								style={{
									paddingHorizontal: 16,
								}}
							>
								<NavHeader
									onBack={onCancel}
									backText="Cancel"
									onPress={onSubmit(selected)}
									actionText="Done"
								/>
							</View>
							<ScrollView showsVerticalScrollIndicator={false}>
								<View style={styles.imageContainer}>
									<ImageBox />
									{imagePool.map((e, i) => {
										let active = selected.findIndex(f => f === e.id);

										return (
											<ImageBox
												key={i}
												id={e.id}
												img={e.url_m}
												cacheKey={`${e.imagekit_id}-m`}
												selected={active !== -1}
												selectedNum={active + 1}
											/>
										);
									})}
								</View>
							</ScrollView>
						</View>
					</View>
				)}
				<Portal>
					{dialog ? (
						<TouchableRipple
							style={{
								position: "absolute",
								zIndex: 10,
								bottom: 0,
								backgroundColor: blackTransparent,
								width: "100%",
								height: "100%",
							}}
							rippleColor="transparent"
							onPress={this.handleAddDialog(false)}
						>
							<View />
						</TouchableRipple>
					) : null}
					<Animated.View
						style={{
							alignSelf: "center",
							position: "absolute",
							zIndex: 100,
							bottom: 0,
							width: "90%",
							height: CARD_HEIGHT,
							backgroundColor: "transparent",
							alignItems: "center",
							justifyContent: "flex-end",
							paddingBottom: 20,
							transform: [{ translateY: CardMove }],
						}}
					>
						<View style={{ backgroundColor: greywhite, borderRadius: 10, marginBottom: 15, width: "100%" }}>
							<DialogButton text="Pick from gallery" color={accent} onPress={this.pickImageFromGallery} />
							<DialogButton text="Open camera" color={accent} onPress={this.pickImageFromCamera} />
						</View>
						<DialogButton text="Cancel" color={red} onPress={this.handleAddDialog(false)} />
					</Animated.View>
				</Portal>
			</Portal>
		);
	}
}

const styles = StyleSheet.create({
	imageContainer: {
		//	paddingTop: 50,
		flex: 1,
		flexDirection: "row",
		flexWrap: "wrap",
		width: "100%",
		alignSelf: "center",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	item: state.item,
	profile: state.profile,
	loading: state.loading,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps, { addImagePool })(ImagePoolEditor);
