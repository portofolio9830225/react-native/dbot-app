import React from "react";
import { StyleSheet, View } from "react-native";
import { Button, Portal, TouchableRipple } from "react-native-paper";
import Animated, { EasingNode } from "react-native-reanimated";
import { connect } from "react-redux";
import { setMultipleOrderStatus, setOrderStatusRejected } from "../store/actions/orderAction";
import {
	blackTransparent,
	blue,
	green,
	greyblack,
	greylight,
	purple,
	red,
	transparent,
	white,
} from "../utils/ColorPicker";
import langSelector from "../utils/langSelector";
import orderColor from "../utils/orderColor";
import CustomButton from "./CustomButton";
import PageTitle from "./PageTitle";
import Typography from "./Typography";

const STATUS_CARD_HEIGHT = 250;

class StatusCard extends React.Component {
	constructor(props) {
		super(props);
		this.transValue = new Animated.Value(1);
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevProps.open !== this.props.open) {
			Animated.timing(this.transValue, {
				toValue: this.props.open ? 2 : 1,
				duration: 250,
				easing: EasingNode.in, // Easing is an additional import from react-native
				useNativeDriver: true, // To make use of native driver for performance
			}).start();
		}
	}

	handleConfirm = status => () => {
		this.props.onClose();

		if (this.props.status !== status) {
			let data = {
				status,
				order_refs: [this.props.orderID],
			};

			this.props.setMultipleOrderStatus(data, this.props.auth.token);
		}
	};

	render() {
		const { open, status, onClose, orderID } = this.props;
		const lang = langSelector.OrderDetail.find(e => e.lang === this.props.preference.lang).data;

		const StatusCardMove = this.transValue.interpolate({
			inputRange: [1, 2],
			outputRange: [STATUS_CARD_HEIGHT, 0],
		});

		const StatusButton = ({ value }) => {
			let valToSend = value.toLowerCase();
			return (
				<CustomButton
					onPress={this.handleConfirm(valToSend)}
					color={orderColor(valToSend)}
					style={styles.statusButton}
				>
					{value}
				</CustomButton>
			);
		};

		return (
			<Portal>
				{open && (
					<TouchableRipple
						style={{
							position: "absolute",
							zIndex: 10,
							bottom: 0,
							backgroundColor: blackTransparent,
							width: "100%",
							height: "100%",
						}}
						rippleColor={transparent}
						onPress={onClose}
					>
						<View />
					</TouchableRipple>
				)}
				<Animated.View
					style={{
						alignSelf: "center",
						position: "absolute",
						zIndex: 100,
						bottom: 0,
						width: "100%",
						paddingHorizontal: 16,
						height: STATUS_CARD_HEIGHT,
						backgroundColor: "white",
						borderTopLeftRadius: 20,
						borderTopRightRadius: 20,
						transform: [{ translateY: StatusCardMove }],
					}}
				>
					<TouchableRipple
						rippleColor={transparent}
						onPress={onClose}
						style={{ justifyContent: "center", alignItems: "center" }}
					>
						<View
							style={{
								width: "30%",
								height: 6,
								borderRadius: 3,
								backgroundColor: greylight,
								marginTop: 10,
								marginBottom: 15,
								alignSelf: "center",
							}}
						/>
					</TouchableRipple>
					<View
						style={{
							width: "100%",
							height: STATUS_CARD_HEIGHT - 31,
							alignSelf: "center",
							justifyContent: "space-evenly",
						}}
					>
						<Typography type="title2" style={{ fontFamily: "semibold", textAlign: "center" }}>
							{lang.setStatusTitle}
						</Typography>

						{status === "unpaid" && (
							<View>
								<StatusButton value="Unpaid" />
								<StatusButton value="Rejected" />
							</View>
						)}
						{(status === "paid" || status === "completed") && (
							<View>
								<StatusButton value="Paid" />
								<StatusButton value="Completed" />
							</View>
						)}
					</View>
				</Animated.View>
			</Portal>
		);
	}
}

const styles = StyleSheet.create({
	statusButton: {
		width: "100%",
		marginVertical: 5,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
});

export default connect(mapStateToProps, {
	setMultipleOrderStatus,
	setOrderStatusRejected,
})(StatusCard);
