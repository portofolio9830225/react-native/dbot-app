import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { accent } from "../utils/ColorPicker";
import Typography from "./Typography";

const PageTitle = props => {
	return (
		<View style={styles.titleContainer}>
			<Typography
				type={props.small ? "title3" : "title1"}
				style={{
					// marginTop: props.marginTop >= 0 ? props.marginTop : 8,
					// marginBottom: props.marginBottom >= 0 ? props.marginBottom : 8,
					marginTop: 0,
					marginBottom: 0,
					width: !props.secondary ? "100%" : "auto",
					fontFamily: "bold",
				}}
			>
				{props.children}
			</Typography>
			{props.secondary}
		</View>
	);
};

const styles = StyleSheet.create({
	titleContainer: {
		alignSelf: "center",
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		alignSelf: "flex-start",
		width: "100%",
	},
});

export default PageTitle;
