import React from "react";
import { TouchableWithoutFeedback, View } from "react-native";
import Typography from "./Typography";
import { accent, greywhite, mainBgColor, white } from "../utils/ColorPicker";
import { TouchableRipple } from "react-native-paper";

export const SectionBox = ({ title, rightHeader, children, style, innerStyle, isEdit, onEdit }) => {
	return (
		<View style={[{ display: "flex", flexDirection: "column", paddingVertical: 14 }, style]}>
			{title || rightHeader ? (
				<View
					style={{
						flexDirection: "row",
						justifyContent: "space-between",
						alignItems: "flex-end",
						width: "100%",
						marginBottom: 10,
						paddingHorizontal: 12,
					}}
				>
					<Typography type="subheadline" style={{ fontFamily: "regular", textTransform: "uppercase" }}>
						{title}
					</Typography>
					{rightHeader}
					{isEdit && (
						<TouchableWithoutFeedback onPress={onEdit}>
							<Typography type="body" style={{ fontFamily: "regular", color: accent }}>
								Edit
							</Typography>
						</TouchableWithoutFeedback>
					)}
				</View>
			) : null}
			<View
				style={[{ width: "100%", backgroundColor: white, borderRadius: 10, paddingHorizontal: 20 }, innerStyle]}
			>
				{children}
			</View>
		</View>
	);
};

export const SectionDivider = ({ color }) => {
	return <View style={{ width: "100%", borderBottomColor: color || mainBgColor, borderBottomWidth: 1 }} />;
};
