import React from "react";
import { Platform, Switch } from "react-native";
import { green, greywhite, mainBgColor, white } from "../utils/ColorPicker";

class Switcher extends React.Component {
	render() {
		return (
			<Switch
				{...this.props}
				value={this.props.enabled}
				trackColor={{ false: mainBgColor, true: green }}
				thumbColor={Platform.OS === "ios" ? undefined : white}
				ios_backgroundColor={this.props.value ? green : mainBgColor}
			/>
		);
	}
}

export default Switcher;
