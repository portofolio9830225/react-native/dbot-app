import React from "react";
import { StyleSheet, View, Text } from "react-native";
import {
	blue,
	green,
	greyblack,
	greylight,
	mainBgColor,
	red,
	purple,
	transparent,
	black,
	greywhite,
	white,
	grey,
	accent,
	pink,
} from "../utils/ColorPicker";
import { AntDesign, Entypo, Feather, Ionicons } from "@expo/vector-icons";
import { TouchableRipple } from "react-native-paper";
import isEmpty from "../utils/isEmpty";
import Collapsible from "react-native-collapsible";
import orderColor from "../utils/orderColor";
import Typography from "./Typography";

class OrderBox extends React.Component {
	state = {
		expand: false,
	};

	handleExpand = () => {
		this.setState({
			expand: !this.state.expand,
		});
	};
	render() {
		const {
			id,
			oTitle,
			nameIcon,
			name,
			date,
			price,
			status,
			pMethod,
			onPress,
			onPressStatus,
			expandContent,
			expandHeader,
			statusType,
			statusComponent,
			isUnread,
		} = this.props;
		const { expand } = this.state;
		const noStatus = status === undefined;
		const noPayStatus = pMethod === undefined;

		const InfoBox = ({ iconType: Icon, iconName, color = greyblack, text, isUnread }) => {
			return (
				<View style={styles.infoBox}>
					<Icon name={iconName} size={13} color={color} style={styles.infoBoxIcon} />
					<Typography
						type="headline"
						numberOfLines={1}
						ellipsizeMode="tail"
						style={{ color: color || greyblack, fontSize: 13, fontFamily: "semibold" }}
					>
						{text}
					</Typography>
				</View>
			);
		};

		return (
			<View
				style={[
					styles.orderBox,
					{
						borderRadius: 15,
						backgroundColor: white,

						// shadowColor: "#000",
						// shadowOffset: {
						//   width: 0,
						//   height: 1,
						// },
						// shadowOpacity: 0.22,
						// shadowRadius: 2.22,
						// elevation: 3,
					},
				]}
			>
				{statusType === 2 ? statusComponent : null}

				<View style={[styles.orderBoxContent]}>
					<TouchableRipple
						onPress={!isEmpty(expandContent) ? this.handleExpand : onPress}
						rippleColor={transparent}
						style={{ width: "100%" }}
					>
						<View
							style={{
								justifyContent: "center",
								flexDirection: "row",
							}}
						>
							{isUnread ? (
								<View
									style={{
										width: 10,
										height: 10,
										borderRadius: 5,
										backgroundColor: accent,
										alignSelf: "flex-start",
										marginTop: 5,
									}}
								/>
							) : null}
							<View
								style={{
									width: "100%",
									paddingLeft: 8,
								}}
							>
								<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
									<InfoBox
										iconType={Ionicons}
										iconName="person"
										text={name}
										color={orderColor(status)}
									/>
									<Entypo name="chevron-thin-right" size={17} color={greyblack} />
								</View>
								<View
									style={{
										flexDirection: "row",
										justifyContent: "space-between",
										marginTop: 15,
									}}
								>
									{!isEmpty(price) && (
										<Typography type="title1" style={styles.orderPrice}>
											{price.toFixed(2)}
											<Typography style={{ fontSize: 12, paddingLeft: 5, color: grey }}>
												MYR
											</Typography>
										</Typography>
									)}
									<View style={{ flexDirection: "row", alignItems: "center", alignSelf: "flex-end" }}>
										<Ionicons name="card-outline" size={14} color={grey} />
										<Typography type="subheadline" style={{ color: grey, marginLeft: 5 }}>
											{pMethod}
										</Typography>
									</View>
								</View>
							</View>
						</View>
					</TouchableRipple>
				</View>

				{!isEmpty(expandContent) && (
					<>
						<TouchableRipple
							rippleColor={transparent}
							onPress={this.handleExpand}
							style={{ width: "100%" }}
						>
							{expandHeader}
						</TouchableRipple>
						<Collapsible collapsed={!expand}>{expandContent}</Collapsible>
					</>
				)}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	orderBox: {
		justifyContent: "space-between",
		alignItems: "center",
		alignSelf: "center",
		marginVertical: 5,
		paddingHorizontal: 16,
		width: "100%",
	},
	orderBoxContent: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "flex-start",
		alignSelf: "center",
		width: "100%",
		paddingVertical: 12,
	},
	orderBoxLeft: {
		width: "70%",
		//height: "100%",
		justifyContent: "space-evenly",
	},
	orderBoxRight: {
		width: "30%",
		//	height: "100%",
		justifyContent: "space-evenly",
		alignItems: "flex-end",
	},
	orderID: {
		fontFamily: "bold",
		fontSize: 16,
		// marginBottom: 10,
	},
	orderPrice: {
		lineHeight: 38,
		fontFamily: "bold",
	},
	infoBox: {
		flexDirection: "row",
		alignItems: "center",
	},

	infoBoxIcon: {
		// color: greyblack,
		marginRight: 8,
		//marginBottom: 4,
	},
});

export default OrderBox;
