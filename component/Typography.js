import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { accent } from "../utils/ColorPicker";

class Typography extends React.Component {
	textStyle = (type = "body") => {
		switch (type.toLowerCase()) {
			case "caption":
				return styles.caption;
			case "subheadline":
				return styles.subheadline;
			case "body":
				return styles.body;
			case "headline":
				return styles.headline;
			case "title3":
				return styles.title3;
			case "title2":
				return styles.title2;
			case "title1":
				return styles.title1;
			case "largetitle":
				return styles.largeTitle;
		}
	};

	render() {
		const { style, type, children, ...rest } = this.props;

		return (
			<Text {...rest} style={[this.textStyle(type), { fontFamily: "regular" }, style]}>
				{children}
			</Text>
		);
	}
}

const styles = StyleSheet.create({
	caption: {
		fontSize: 11,
	},
	subheadline: {
		fontSize: 12,
	},
	body: {
		fontSize: 14,
	},
	headline: {
		fontSize: 16,
	},
	title3: {
		fontSize: 20,
	},
	title2: {
		fontSize: 24,
	},
	title1: {
		fontSize: 32,
	},
	largeTitle: {
		fontSize: 38,
	},
});

export default Typography;
