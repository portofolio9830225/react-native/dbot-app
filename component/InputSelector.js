import React from "react";

import { StyleSheet, View, Text, ScrollView, Keyboard } from "react-native";
import WheelPicker from "react-native-wheely";
import Animated, { EasingNode } from "react-native-reanimated";

import { TextInput, Portal, Dialog, Divider, List, TouchableRipple } from "react-native-paper";

import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { accent, grey, greylight, red, transparent, white } from "../utils/ColorPicker";
import TextField from "./TextField";
import isEmpty from "../utils/isEmpty";
import PortalSafeView from "./PortalSafeView";

const CARD_HEIGHT = 180;

class InputSelector extends React.Component {
	constructor(props) {
		super(props);
		this.transValue = new Animated.Value(1);
	}

	state = {
		dialog: false,
		selected: 0,
	};

	updateVal = () => {
		if (!isEmpty(this.props.list)) {
			let foundI = this.props.list.findIndex((f, i) => f.name === this.props.value);

			this.setState({
				selected: foundI !== -1 ? foundI : 0,
			});
		}
	};

	componentDidMount() {
		this.updateVal();
	}

	componentDidUpdate(prevProps, prevState) {
		if (this.props.value !== prevProps.value) {
			this.updateVal();
		}
		if (prevState.dialog !== this.state.dialog) {
			Animated.timing(this.transValue, {
				toValue: this.state.dialog ? 2 : 1,
				duration: 250,
				easing: EasingNode.in, // Easing is an additional import from react-native
				useNativeDriver: true, // To make use of native driver for performance
			}).start();
		}
	}

	handleSelectorDialog = state => () => {
		Keyboard.dismiss();

		this.setState({
			dialog: state,
		});
	};

	handleChangeSelector = index => {
		this.setState({
			selected: index,
		});
	};

	handleSubmit = () => {
		this.setState(
			{
				dialog: false,
			},
			() => {
				let { name, code } = this.props.list[this.state.selected];
				this.props.onSelect(name, code);
			}
		);
	};

	render() {
		const { dialog } = this.state;
		const { list, disabled, label, style, value, dialogTitle, errorKey, onSelect, inputStyle } = this.props;

		const CardMove = this.transValue.interpolate({
			inputRange: [1, 2],
			outputRange: [CARD_HEIGHT, 0],
		});

		return (
			<>
				<PortalSafeView>
					{dialog && (
						<TouchableRipple
							style={{
								position: "absolute",
								zIndex: 10,
								bottom: 0,
								backgroundColor: "rgba(0,0,0,0.4)",
								width: "100%",
								height: "100%",
							}}
							rippleColor="transparent"
							onPress={this.handleSelectorDialog(false)}
						>
							<View />
						</TouchableRipple>
					)}

					<Animated.View
						style={{
							alignSelf: "center",
							position: "absolute",
							zIndex: 100,
							bottom: 0,
							width: "100%",
							height: CARD_HEIGHT,
							backgroundColor: "white",
							borderTopLeftRadius: 10,
							borderTopRightRadius: 10,
							transform: [{ translateY: CardMove }],
						}}
					>
						<View
							style={{
								flexDirection: "row",
								justifyContent: "space-between",
								alignItems: "center",
							}}
						>
							<TouchableRipple rippleColor="transparent" onPress={this.handleSelectorDialog(false)}>
								<Text style={styles.textButton}>Cancel</Text>
							</TouchableRipple>
							<Text style={{ fontFamily: "medium", fontSize: 15 }}>{label}</Text>
							<TouchableRipple rippleColor="transparent" onPress={this.handleSubmit}>
								<Text style={styles.textButton}>Done</Text>
							</TouchableRipple>
						</View>
						<View
							style={{
								width: "90%",
								height: CARD_HEIGHT,
								alignSelf: "center",
								justifyContent: "space-evenly",
							}}
						>
							<WheelPicker
								selectedIndex={this.state.selected}
								options={list.map(e => e.name)}
								visibleRest={2}
								onChange={this.handleChangeSelector}
								itemTextStyle={styles.itemTextStyle}
								decelerationRate="normal"
							/>
						</View>
					</Animated.View>
				</PortalSafeView>
				<TouchableWithoutFeedback onPress={!disabled ? this.handleSelectorDialog(true) : null}>
					<View pointerEvents="none">
						<TextField
							inputStyle={inputStyle}
							type="select"
							disabled={disabled}
							style={style}
							mode="outlined"
							label={label}
							value={value}
							errorKey={errorKey}
						/>
					</View>
				</TouchableWithoutFeedback>
			</>
		);
	}
}

const styles = StyleSheet.create({
	textButton: {
		fontFamily: "regular",
		//fontSize: 15,
		color: accent,
		paddingHorizontal: 20,
		paddingVertical: 15,
		//backgroundColor: "red",
	},
	itemTextStyle: {
		fontFamily: "regular",
		textAlign: "center",
	},
});

export default InputSelector;
