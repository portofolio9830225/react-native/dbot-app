import { Text, StyleSheet, View } from "react-native";
import { TextInput, Portal, Dialog, Divider, List, TouchableRipple } from "react-native-paper";
import React from "react";
import getWidth from "../utils/getWidth";
import { SectionBox, SectionDivider } from "./SectionComponents";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { Ionicons } from "@expo/vector-icons";
import { accent, greywhite, mainBgColor } from "../utils/ColorPicker";
import NavHeader from "./NavHeader";
import Typography from "./Typography";

export default class InventoryFilter extends React.Component {
	state = {
		arr: [],
	};

	componentDidMount() {
		this.setState({
			arr: this.props.selected,
		});
	}

	handleFilterActive = value => () => {
		let array = [];
		let checked = this.state.arr.findIndex(f => f === value) != -1;

		if (!checked) {
			array = [...this.state.arr, value];
		} else {
			array = this.state.arr.filter(e => e !== value);
		}

		this.setState({
			arr: array,
		});
	};

	render() {
		const FilterList = props => {
			return (
				<TouchableWithoutFeedback onPress={this.handleFilterActive(props.val)}>
					<View
						style={{
							height: 44,
							alignItems: "center",
							justifyContent: "space-between",
							flexDirection: "row",
						}}
					>
						<Text>{props.val}</Text>
						{this.state.arr.findIndex(f => f === props.val) != -1 ? (
							<Ionicons name="checkmark-circle" size={20} color={accent} />
						) : null}
					</View>
				</TouchableWithoutFeedback>
			);
		};

		return (
			<Portal>
				<View
					style={{
						width: getWidth + 32,
						height: "100%",
						bottom: 0,
						position: "absolute",
						zIndex: 100,
						backgroundColor: mainBgColor,
						// borderTopLeftRadius: 40,
						// borderTopRightRadius: 40,
						paddingHorizontal: 20,
					}}
				>
					<View
						style={{
							justifyContent: "space-between",
							width: "100%",
							marginTop: 40,
							flexDirection: "row",
						}}
					>
						<NavHeader
							onBack={this.props.onCancel}
							backText="Cancel"
							actionText="Done"
							onPress={this.props.onSubmit(this.state.arr)}
						/>
					</View>
					<SectionBox title="Include:">
						<FilterList val="Available" />
						<SectionDivider />
						<FilterList val="Unavailable" />
					</SectionBox>
				</View>
			</Portal>
		);
	}
}

const styles = StyleSheet.create({});
