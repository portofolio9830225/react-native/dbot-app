import React from "react";
import { Button } from "react-native-paper";
import { accent, transparent, white } from "../utils/ColorPicker";

class CustomButton extends React.Component {
	render() {
		const { onPress, loading, style, color, children } = this.props;
		return (
			<Button
				onPress={onPress}
				loading={loading}
				style={style}
				color={color || accent}
				contentStyle={{ height: 56 }}
				labelStyle={{ fontFamily: "medium", fontSize: 16, color: white }}
				rippleColor={transparent}
				mode="contained"
				uppercase={false}
			>
				{children}
			</Button>
		);
	}
}

export default CustomButton;
