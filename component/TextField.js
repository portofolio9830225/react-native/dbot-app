import React from "react";
import { connect } from "react-redux";

import isEmpty from "../utils/isEmpty";
import Collapsible from "react-native-collapsible";
import { Text, View, TextInput, StyleSheet } from "react-native";
import { Portal, TouchableRipple } from "react-native-paper";
import {
	accent,
	accentDark,
	black,
	grey,
	greyblack,
	greydark,
	greylight,
	greywhite,
	mainBgColor,
	red,
	white,
} from "../utils/ColorPicker";
import WheelPicker from "react-native-wheely";
import Animated, { EasingNode } from "react-native-reanimated";
import NavHeader from "./NavHeader";
import { Entypo, Feather } from "@expo/vector-icons";
import PortalSafeView from "./PortalSafeView";
import Typography from "./Typography";
import { Calendar } from "react-native-calendars";

const CARD_HEIGHT = 180;

class TextField extends React.Component {
	constructor(props) {
		super(props);
		this.transValue = new Animated.Value(1);
	}
	state = {
		errorKey: this.props.errorKey || "",
		errorText: "",
		multilineDialog: false,
		multilineInput: "",
		selectorDialog: false,
		selectorIndex: 0,
		calendarDate: null,
		calendarMarked: {},
		renderCalendar: true,
	};

	componentDidMount() {
		if (!isEmpty(this.props.error.errInput)) {
			let err = this.props.error.errInput.find(e => e.param === this.state.errorKey);

			this.setState({
				errorText: !isEmpty(err) ? err.error : "",
			});
		}

		if (this.props.type === "select") {
			this.updateSelectorVal();
		}
		if (this.props.type === "calendar") {
			this.updateCalendarMarked(this.props.date);
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevProps.error.errInput !== this.props.error.errInput && !isEmpty(this.props.error.errInput)) {
			let err = this.props.error.errInput.find(e => e.param === this.state.errorKey);

			this.setState({
				errorText: !isEmpty(err) ? err.error : "",
			});
		}
		if (prevProps.value !== this.props.value) {
			this.setState({
				errorText: "",
			});
			if (this.props.type === "select") {
				this.updateSelectorVal();
			}
			if (this.props.type === "calendar") {
				this.updateCalendarMarked(this.props.date);
			}
		}
		if (!prevState.multilineDialog && this.state.multilineDialog) {
			this.setState({
				multilineInput: this.props.value,
			});
			setTimeout(() => {
				this.multilineInput.focus();
			}, 100);
		}
		if (prevState.multilineDialog && !this.state.multilineDialog) {
			setTimeout(() => {
				this.setState({
					multilineInput: "",
				});
			}, 100);
			this.multilineInput.blur();
		}
		if (prevState.selectorDialog !== this.state.selectorDialog) {
			Animated.timing(this.transValue, {
				toValue: this.state.selectorDialog ? 2 : 1,
				duration: 250,
				easing: EasingNode.in, // Easing is an additional import from react-native
				useNativeDriver: true, // To make use of native driver for performance
			}).start();
		}
		if (this.props.minDate !== prevProps.minDate || this.props.maxDate !== prevProps.maxDate) {
			//to re-render newly updated max/min date
			this.setState(
				{
					renderCalendar: false,
				},
				() => {
					this.setState({
						renderCalendar: true,
					});
				}
			);
		}
		if (prevProps.type !== this.props.type && prevProps.type === "dummy") {
			this.textInput.focus();
		}
	}

	handlePressInput = () => {
		switch (this.props.type) {
			case "multiline":
				this.handleMultilineDialog(true)();
				break;
			case "select":
				this.handleSelectorDialog(true)();
				break;
			case "calendar":
				this.props.handleExpand(this.props.expandValue);
				break;
			default:
				this.textInput.focus();
		}
	};

	handleMultilineDialog = state => () => {
		this.setState({
			multilineDialog: state,
		});
	};

	handleSubmitMultiline = () => {
		this.handleMultilineDialog(false)();
		this.props.onChangeText(this.state.multilineInput);
	};

	updateSelectorVal = () => {
		if (!isEmpty(this.props.selectorList)) {
			let foundI = this.props.selectorList.findIndex((f, i) => f.name === this.props.value);

			this.setState({
				selectorIndex: foundI !== -1 ? foundI : 0,
			});
		}
	};

	handleSelectorDialog = state => () => {
		this.setState({
			selectorDialog: state,
		});
	};

	handleChangeSelector = index => {
		this.setState({
			selectorIndex: index,
		});
	};

	handleSubmitSelector = () => {
		this.setState(
			{
				selectorDialog: false,
			},
			() => {
				let { name, code } = this.props.selectorList[this.state.selectorIndex];
				this.props.onSelect(name, code);
			}
		);
	};

	updateCalendarMarked = date => {
		this.setState({
			calendarMarked: {
				[date]: {
					selected: true,
					selectedColor: accent,
				},
			},
		});
	};

	handleDate = day => {
		this.setState(
			{
				calendarDate: day.dateString,
			},
			() => {
				this.props.onChangeText(this.state.calendarDate);
			}
		);
	};

	render() {
		const { errorText, multilineDialog, selectorDialog, selectorIndex, calendarMarked, renderCalendar } =
			this.state;
		const {
			style,
			inputStyle,
			disabled,
			label,
			textAlign,
			startAdornment,
			endAdornment,
			children,
			type,
			onPressInput,
			selectorList,
			calendarDate,
			expandActive,
			minDate,
			maxDate,
			...rest
		} = this.props;

		const multiline = type === "multiline";
		const select = type === "select";
		const calendar = type === "calendar";
		const dummy = type === "dummy";

		const isTextInput = !select && !multiline && !calendar && !dummy;
		const multilineCharCount = this.props.maxLimit - this.state.multilineInput.length;

		const CardMove = this.transValue.interpolate({
			inputRange: [1, 2],
			outputRange: [CARD_HEIGHT, 0],
		});

		return (
			<View style={[style]}>
				<PortalSafeView>
					{multilineDialog && (
						<View
							style={{
								width: "100%",
								height: "100%",
								backgroundColor: mainBgColor,
								paddingHorizontal: 16,
							}}
						>
							<NavHeader
								onBack={this.handleMultilineDialog(false)}
								actionText="Done"
								onPress={this.handleSubmitMultiline}
								title={label}
							/>
							<View style={{ width: "100%", alignSelf: "center", paddingTop: 20 }}>
								<View
									style={{ width: "100%", backgroundColor: "white", padding: 10, borderRadius: 10 }}
								>
									<TextInput
										multiline
										style={{ fontSize: 15, fontFamily: "regular", textAlignVertical: "top" }}
										ref={input => {
											this.multilineInput = input;
										}}
										onChangeText={val => {
											this.setState({
												multilineInput: this.props.maxLimit
													? val.length > this.props.maxLimit
														? this.state.multilineInput
														: val
													: val,
											});
										}}
										value={this.state.multilineInput}
										placeholder={rest.placeholder || label}
										numberOfLines={5}
									/>
								</View>
								{this.props.maxLimit ? (
									<Typography
										type="caption"
										style={{
											color: multilineCharCount < 11 ? red : greyblack,
											padding: 5,
										}}
									>
										{multilineCharCount} character{multilineCharCount > 1 ? "s" : ""} remaining
									</Typography>
								) : null}
							</View>
						</View>
					)}
					{select && (
						<>
							{selectorDialog && (
								<TouchableRipple
									style={{
										position: "absolute",
										zIndex: 10,
										bottom: 0,
										backgroundColor: "rgba(0,0,0,0.4)",
										width: "100%",
										height: "100%",
									}}
									rippleColor="transparent"
									onPress={this.handleSelectorDialog(false)}
								>
									<View />
								</TouchableRipple>
							)}

							<Animated.View
								style={{
									alignSelf: "center",
									position: "absolute",
									zIndex: 100,
									bottom: 0,
									width: "100%",
									height: CARD_HEIGHT,
									backgroundColor: "white",
									borderTopLeftRadius: 10,
									borderTopRightRadius: 10,
									transform: [{ translateY: CardMove }],
								}}
							>
								<View
									style={{
										flexDirection: "row",
										justifyContent: "space-between",
										alignItems: "center",
									}}
								>
									<TouchableRipple
										rippleColor="transparent"
										onPress={this.handleSelectorDialog(false)}
									>
										<Text style={styles.textButton}>Cancel</Text>
									</TouchableRipple>
									<Text style={{ fontFamily: "medium", fontSize: 15 }}>{label}</Text>
									<TouchableRipple rippleColor="transparent" onPress={this.handleSubmitSelector}>
										<Text style={styles.textButton}>Done</Text>
									</TouchableRipple>
								</View>
								<View
									style={{
										width: "90%",
										height: CARD_HEIGHT,
										alignSelf: "center",
										justifyContent: "space-evenly",
									}}
								>
									<WheelPicker
										selectedIndex={selectorIndex}
										options={selectorList.map(e => e.name)}
										visibleRest={2}
										onChange={this.handleChangeSelector}
										itemTextStyle={styles.itemTextStyle}
										decelerationRate="normal"
									/>
								</View>
							</Animated.View>
						</>
					)}
				</PortalSafeView>

				<View style={[styles.inputBox, inputStyle]}>
					<TouchableRipple
						rippleColor="transparent"
						onPress={disabled ? null : onPressInput || this.handlePressInput}
						style={{ width: "100%" }}
					>
						<View style={styles.innerInput}>
							{!isEmpty(label) ? (
								<View style={{ width: "34%" }}>
									<Typography ellipsizeMode="tail">
										{label}
										{rest.required ? (
											<Typography type="headline" style={{ color: red, fontFamily: "semibold" }}>
												*
											</Typography>
										) : null}
									</Typography>
								</View>
							) : null}

							<View
								style={{
									width: !isEmpty(label) ? "66%" : "100%",
									flexDirection: "row",
									alignItems: "center",
									justifyContent: "space-between",
								}}
							>
								{isTextInput && startAdornment ? (
									<Typography style={styles.adornmentText}>{startAdornment}</Typography>
								) : null}
								{isTextInput ? (
									<TextInput
										editable={!disabled}
										onChangeText={rest.onChangeText}
										value={rest.value}
										keyboardType={rest.keyboardType}
										placeholder={rest.placeholder}
										secureTextEntry={rest.secureTextEntry}
										//	autoFocus={rest.autoFocus}
										ref={input => {
											this.textInput = input;
										}}
										style={{
											flex: 1,
											fontFamily: "regular",
											color: disabled ? greylight : black,
										}}
									/>
								) : dummy ? (
									<View pointerEvents="none" style={{ flex: 1 }}>
										<TextInput
											placeholder={rest.placeholder}
											secureTextEntry={rest.secureTextEntry}
											value={rest.value}
											style={{
												flex: 1,
												fontFamily: "regular",
											}}
										/>
									</View>
								) : (
									<Typography
										style={{
											width: "93%",
											color: disabled ? greylight : black,
										}}
										ellipsizeMode="tail"
										numberOfLines={1}
									>
										{rest.value}
									</Typography>
								)}
								{isTextInput ? (
									endAdornment ? (
										typeof endAdornment == "string" ? (
											<Text style={[styles.adornmentText, { textAlign: "right" }]}>
												{endAdornment}
											</Text>
										) : (
											endAdornment
										)
									) : null
								) : multiline ? (
									<Entypo name="chevron-thin-right" color={greyblack} size={17} />
								) : select ? (
									<Entypo name="chevron-thin-down" color={greyblack} size={17} />
								) : calendar ? (
									<Entypo
										name={`chevron-thin-${expandActive ? "up" : "down"}`}
										color={greyblack}
										size={17}
									/>
								) : null}
							</View>
						</View>
					</TouchableRipple>
					{calendar && (
						<Collapsible collapsed={!expandActive}>
							{renderCalendar ? (
								<View style={styles.calendarContainer}>
									<Calendar
										current={
											!isEmpty(calendarDate)
												? calendarDate
												: !isEmpty(minDate)
												? minDate
												: undefined
										}
										minDate={minDate || undefined}
										maxDate={maxDate || undefined}
										theme={{
											backgroundColor: white,
											todayTextColor: accentDark,
											textDayFontFamily: "regular",
											textDayHeaderFontFamily: "regular",
											textMonthFontFamily: "bold",
										}}
										markedDates={calendarMarked}
										onDayPress={this.handleDate}
										renderArrow={dir => {
											if (dir == "left") {
												return <Feather name="chevron-left" size={20} color={greyblack} />;
											}

											if (dir == "right") {
												return <Feather name="chevron-right" size={20} color={greyblack} />;
											}
										}}
									/>
								</View>
							) : (
								<View />
							)}
						</Collapsible>
					)}
				</View>

				{rest.desc && (
					<Collapsible collapsed={rest.isDescCollapsed ? isEmpty(rest.value) : false}>
						<Text
							style={{
								fontFamily: "light",
								color: !isEmpty(errorText) ? red : black,
								paddingTop: 3,
								paddingLeft: 5,
							}}
						>
							{rest.desc}
						</Text>
					</Collapsible>
				)}
				<Collapsible collapsed={isEmpty(errorText)}>
					<Text
						style={{
							fontFamily: "light",
							fontSize: 11,
							color: red,
							paddingLeft: 5,
							paddingTop: 1,
						}}
					>
						{errorText}
					</Text>
				</Collapsible>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	inputBox: {
		width: "100%",
		flexDirection: "column",
		backgroundColor: "#fff",
		paddingHorizontal: 16,
		borderRadius: 10,
	},
	innerInput: {
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		height: 50,
		// overflow: "hidden",
	},
	labelText: {
		fontFamily: "regular",
		//textAlign: "center",
	},
	adornmentText: {
		width: "15%",
		color: greydark,
	},
	textButton: {
		fontFamily: "regular",
		//fontSize: 15,
		color: accent,
		paddingHorizontal: 20,
		paddingVertical: 15,
		//backgroundColor: "red",
	},
	itemTextStyle: {
		fontFamily: "regular",
		textAlign: "center",
	},
	calendarContainer: {
		paddingVertical: 10,
	},
});

const mapStateToProps = state => ({
	error: state.error,
});

export default connect(mapStateToProps)(TextField);
