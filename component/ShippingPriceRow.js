import React from "react";
import { StyleSheet, View, Text, TouchableOpacity, Keyboard } from "react-native";
import { Button, Dialog, Portal, TextInput } from "react-native-paper";
import { Ionicons } from "@expo/vector-icons";
import { black, grey, greyblack, red } from "../utils/ColorPicker";
import TextField from "./TextField";
import { connect } from "react-redux";
import langSelector from "../utils/langSelector";
import isEmpty from "../utils/isEmpty";
import CustomButton from "./CustomButton";
import AlertDialog from "./AlertDialog";
import Typography from "./Typography";

class SetPriceRow extends React.Component {
	state = {
		name: "",
		min: "",
		max: "",
		price: "0",
		delDialog: false,
	};

	componentDidMount() {
		let { name, price, min, max } = this.props;
		this.setState({
			name,
			price: `${price}`,
			min: `${!isEmpty(min) ? min : ""}`,
			max: `${!isEmpty(max) ? max : ""}`,
		});
	}

	componentDidUpdate(prevProps, prevState) {
		if (
			prevProps.min !== this.props.min ||
			prevProps.max !== this.props.max ||
			prevProps.name !== this.props.name
		) {
			let { min, max, name, price } = this.props;
			this.setState({
				name,
				price: `${price}`,
				min: `${!isEmpty(min) ? min : ""}`,
				max: `${!isEmpty(max) ? max : ""}`,
			});
		}
	}

	handleChange = () => {
		this.props.onChange(this.props.id, {
			name: this.state.name,
			min: this.state.min,
			max: this.state.max,
			price: this.state.price,
		});
	};

	handleText = name => val => {
		this.setState(
			{
				[name]: val,
			},
			() => {
				this.handleChange();
			}
		);
	};

	handleDelDialog = state => () => {
		this.setState({
			delDialog: state,
		});
	};

	handleDelete = () => {
		this.handleDelDialog(false)();
		this.props.onDelete(this.props.id);
	};

	render() {
		const { name, price, min, max, delDialog } = this.state;
		const { id, noDeleteIcon, type, view } = this.props;
		const lang = langSelector.CatalogShipping.find(e => e.lang === this.props.preference.lang).data;

		return (
			<View
				style={{
					width: "100%",
					marginVertical: 10,
				}}
			>
				<AlertDialog
					title={`${lang.modalTitle} ${name}?`}
					active={delDialog}
					onDismiss={this.handleDelDialog(false)}
					actions={[
						{ title: lang.modalNo, onPress: this.handleDelDialog(false) },
						{ title: lang.modalYes, onPress: this.handleDelete },
					]}
				/>

				{noDeleteIcon ? null : (
					<TouchableOpacity
						style={{
							alignSelf: "flex-end",
						}}
						onPress={this.handleDelDialog(true)}
					>
						<Ionicons name="md-remove-circle" size={18} color={grey} />
					</TouchableOpacity>
				)}

				<View
					style={{
						width: "100%",
						flexDirection: "row",
						justifyContent: "space-between",
					}}
				>
					<View
						style={{
							flexDirection: "row",
							alignItems: "flex-start",
							justifyContent: "space-between",
							width: type === 4 ? "46%" : "66%",
						}}
					>
						{type === 2 ? (
							<TextField
								disabled={view}
								style={{ width: "100%" }}
								mode="outlined"
								label={lang.name}
								type="multiline"
								value={name}
								onChangeText={this.handleText("name")}
								errorKey={`zone_name_${id}`}
							/>
						) : type === 3 ? (
							<>
								<TextField
									style={{ width: "48%" }}
									keyboardType="decimal-pad"
									disabled={view || this.props.id === 0}
									mode="outlined"
									label={lang.min}
									value={min}
									errorKey={`min_weight_${id}`}
									right={
										<TextInput.Affix
											text="kg"
											contentStyle={{
												marginTop: 1,
												fontFamily: "regular",
											}}
										/>
									}
									onChangeText={this.handleText("min")}
								/>
								<TextField
									disabled={view}
									keyboardType="decimal-pad"
									style={{ width: "48%" }}
									mode="outlined"
									label={lang.max}
									value={max}
									errorKey={`max_weight_${id}`}
									right={
										<TextInput.Affix
											text="kg"
											contentStyle={{
												marginTop: 1,
												fontFamily: "regular",
											}}
										/>
									}
									onChangeText={this.handleText("max")}
								/>
							</>
						) : type === 4 ? (
							<Typography
								type="headline"
								numberOfLines={1}
								ellipsizeMode="tail"
								style={{
									marginBottom: 5,
									marginLeft: 2,
									color: black,
									alignSelf: "center",
									fontFamily: "semibold",
								}}
							>
								{name}
							</Typography>
						) : null}
					</View>
					<TextField
						disabled={view}
						style={{ width: type === 4 ? "48%" : "30%" }}
						keyboardType="decimal-pad"
						errorKey={`shipping_price_${id}`}
						left={
							<TextInput.Affix
								text="RM "
								contentStyle={{
									marginTop: 1,
									fontFamily: "regular",
								}}
							/>
						}
						mode="outlined"
						label={lang.price}
						value={price}
						onChangeText={this.handleText("price")}
					/>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	slideButton: {
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
	},

	dialogActionButton: {
		marginHorizontal: 10,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
});

export default connect(mapStateToProps)(SetPriceRow);
