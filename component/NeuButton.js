import React from "react";
import { View, TouchableWithoutFeedback } from "react-native";

import Typography from "./Typography";

class NeuButton extends React.Component {
	state = {
		active: false,
	};

	handleActive = state => () => {
		this.setState({
			active: state,
		});
	};
	render() {
		const { label, disabled, onPress, textStyle } = this.props;
		const { active } = this.state;
		return (
			<TouchableWithoutFeedback
				onPressIn={!disabled ? this.handleActive(true) : null}
				onPressOut={!disabled ? this.handleActive(false) : null}
				onPress={!disabled ? onPress : null}
			>
				<View
					style={{
						width: "100%",
						height: "100%",
						alignItems: "center",
						justifyContent: "center",
					}}
				>
					<Typography style={[{ textAlign: "center" }, textStyle]}>{label}</Typography>
				</View>
			</TouchableWithoutFeedback>
		);
	}
}

export default NeuButton;
