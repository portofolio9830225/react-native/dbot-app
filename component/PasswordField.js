import React from "react";

import { TextInput, withTheme } from "react-native-paper";
import TextField from "./TextField";

class PasswordField extends React.Component {
  state = {
    showPass: false,
  };

  render() {
    const { showPass } = this.state;
    const { theme } = this.props;

    return (
      <TextField
        {...this.props}
        secureTextEntry={!showPass}
        right={
          <TextInput.Icon
            style={{ opacity: 0.7 }}
            animated={true}
            name={showPass ? "eye-off-outline" : "eye-outline"}
            color={theme.colors.text}
            onPress={() => {
              this.setState({
                showPass: !this.state.showPass,
              });
            }}
          />
        }
      />
    );
  }
}

export default withTheme(PasswordField);
