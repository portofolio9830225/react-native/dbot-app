import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { green, greyblack, mainBgColor, transparent, white } from "../utils/ColorPicker";
import { TouchableRipple } from "react-native-paper";
import langSelector from "../utils/langSelector";
import { connect } from "react-redux";
import DateConverter from "../utils/DateConverter";
import Typography from "./Typography";

class PayoutOrderCard extends React.Component {
	render() {
		const { id, date, amount, onPress } = this.props;
		const lang = langSelector.PayoutReport.find(e => e.lang === this.props.preference.lang).data;

		const InfoBox = ({ title, desc, descColor = greyblack, textAlign = "left" }) => {
			return (
				<View style={{ width: "100%" }}>
					<Typography
						type="headline"
						style={{
							marginBottom: 7,
							textAlign: textAlign,
							fontFamily: "semibold",
						}}
					>
						{title}
					</Typography>
					<Typography
						type="caption"
						style={{
							color: descColor,
							textAlign: textAlign,
						}}
					>
						{desc}
					</Typography>
				</View>
			);
		};

		return (
			<TouchableRipple onPress={onPress} rippleColor={transparent} style={{ width: "100%" }}>
				<View
					style={[
						styles.payoutBox,
						{
							borderRadius: 15,
							backgroundColor: white,
							// shadowColor: "#000",
							// shadowOffset: {
							// 	width: 0,
							// 	height: 1,
							// },
							// shadowOpacity: 0.22,
							// shadowRadius: 2.22,
							// elevation: 3,
						},
					]}
				>
					<View style={styles.payoutBoxLeft}>
						<InfoBox title={`${lang.cardTitle}: ${id}`} desc={`${lang.cardDesc}: ${date}`} />
					</View>
					<View style={styles.payoutBoxRight}>
						<InfoBox
							title={`+ RM${amount.toFixed(2)}`}
							desc={lang.completed}
							descColor={green}
							textAlign="right"
						/>
					</View>
				</View>
			</TouchableRipple>
		);
	}
}

const styles = StyleSheet.create({
	payoutBox: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		alignSelf: "center",
		marginBottom: 20,
		paddingHorizontal: 15,
		width: "100%",

		height: 72,
	},
	payoutBoxLeft: {
		width: "70%",
		height: "100%",
		//alignItems: "center",
		justifyContent: "space-evenly",
	},
	payoutBoxRight: {
		width: "30%",
		height: "100%",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "flex-end",
		// alignItems: "flex-end",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
});

export default connect(mapStateToProps)(PayoutOrderCard);
