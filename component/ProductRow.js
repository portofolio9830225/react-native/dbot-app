import React from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import getWidth from "../utils/getWidth";
import isEmpty from "../utils/isEmpty";
import Typography from "./Typography";
import { black, greydark, mainBgColor, grey, greyblack, greywhite } from "../utils/ColorPicker";
import CachedImage from "expo-cached-image";

export default function ProductRow(props) {
	return (
		// <View style={{ width: getWidth, height: 70 }}>
		<TouchableWithoutFeedback onPress={props.onPress}>
			<View style={[styles.card, !props.isFirst ? { borderTopColor: greywhite, borderTopWidth: 1 } : {}]}>
				<CachedImage
					style={styles.image}
					source={!isEmpty(props.image) ? { uri: props.image } : require("../assets/image/noImage.png")}
					cacheKey={!isEmpty(props.image) && props.imageKey ? props.imageKey : undefined}
				/>
				<View style={{ flex: 1 }}>
					<Typography type="headline" numberOfLines={1} ellipsizeMode="tail" style={styles.title}>
						{props.title}
					</Typography>
					<View style={{ display: "flex", flexDirection: "row", flexWrap: "wrap" }}>
						<Typography type="caption" style={styles.price}>
							{!isEmpty(props.price) ? `RM${props.price.toFixed(2)}` : "No price yet"}
						</Typography>

						{!isEmpty(props.price) && !isEmpty(props.another_price) ? (
							<Typography type="caption" style={styles.another_price}>
								{" "}
								RM{props.another_price.toFixed(2)}{" "}
							</Typography>
						) : null}
					</View>
				</View>
			</View>
		</TouchableWithoutFeedback>
	);
}

const styles = StyleSheet.create({
	card: {
		flexDirection: "row",
		alignItems: "center",
		height: 70,
		// width: getWidth,
	},
	image: {
		width: 56,
		height: 56,
		borderRadius: 10,
		marginRight: 16,
		borderWidth: 0.5,
		borderColor: greywhite,
	},
	title: {
		fontFamily: "regular",
		color: black,
	},
	price: {
		color: greydark,
		fontFamily: "regular",
	},
	another_price: {
		color: greydark,
		fontFamily: "regular",
		textDecorationLine: "line-through",
		marginLeft: 5,
	},
});
