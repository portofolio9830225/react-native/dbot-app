import React from "react";
import { View } from "react-native";
import { TouchableRipple, Checkbox } from "react-native-paper";
import { transparent } from "../utils/ColorPicker";
import OrderBox from "./OrderBox";

class OrderBoxSelect extends React.Component {
  render() {
    return (
      <TouchableRipple
        rippleColor={transparent}
        onPress={this.props.onPress}
        onLongPress={this.props.onLongPress}
        style={{
          display: "flex",
          //backgroundColor: "red",
          width: !this.props.isHideCheck ? "95%" : "100%",
          alignSelf: "center",
        }}
      >
        <View
          style={{
            width: "100%",
            flexDirection: !this.props.isHideCheck ? "row" : "column",
            alignItems: "center",
            justifyContent: "space-between",
            alignSelf: "center",
          }}
        >
          {!this.props.isHideCheck ? (
            <View style={{ marginBottom: 20 }}>
              <Checkbox.Android
                status={this.props.checked ? "checked" : "unchecked"}
              />
            </View>
          ) : null}

          <OrderBox {...this.props} onPress={null} />
        </View>
      </TouchableRipple>
    );
  }
}

export default OrderBoxSelect;
