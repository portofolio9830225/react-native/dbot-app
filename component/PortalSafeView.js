import React from "react";
import { View } from "react-native";
import { Portal } from "react-native-paper";
import Constants from "expo-constants";

const PortalSafeView = props => {
	return (
		<Portal>
			<View style={{ paddingTop: Constants.statusBarHeight }} />
			{props.children}
		</Portal>
	);
};

export default PortalSafeView;
