import { StyleSheet, Text, View, TouchableWithoutFeedback } from "react-native";
import React from "react";
import { greyblack, mainBgColor, white } from "../utils/ColorPicker";
import { Entypo } from "@expo/vector-icons";

export default function CategoryBox(props) {
	return (
		<TouchableWithoutFeedback
			onPress={props.onPress}
			onLongPress={props.onLongPress}
			onPressOut={props.onPressOut}
			onPressIn={props.onPressIn}
			disabled={props.disabled}
		>
			<View style={styles.outerCard}>
				<View
					style={[
						styles.card,
						props.isSortActive
							? {
									borderTopColor: mainBgColor,
									borderTopWidth: 1,
									borderBottomColor: mainBgColor,
									borderBottomWidth: 1,
							  }
							: !props.isFirst
							? { borderTopColor: mainBgColor, borderTopWidth: 1 }
							: {},
					]}
				>
					<Text>{props.name}</Text>
					{props.editMode ? null : <Entypo name="chevron-thin-right" size={18} color={greyblack} />}
				</View>
				{props.editMode ? (
					<TouchableWithoutFeedback onPressIn={props.onDrag}>
						<Entypo name="menu" size={23} color={greyblack} style={{ paddingLeft: 16 }} />
					</TouchableWithoutFeedback>
				) : null}
			</View>
		</TouchableWithoutFeedback>
	);
}

const styles = StyleSheet.create({
	outerCard: {
		flexDirection: "row",
		width: "100%",
		height: 60,
		marginVertical: 8,
		alignItems: "center",
		paddingHorizontal: 16,
	},
	card: {
		flex: 1,
		height: "100%",
		borderRadius: 10,
		backgroundColor: white,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		paddingHorizontal: 16,
	},
});
