import { StyleSheet, View } from "react-native";
import React from "react";
import { Card, TouchableRipple } from "react-native-paper";
import getWidth from "../utils/getWidth";
import { black, greydark, greylight, greywhite, transparent, white } from "../utils/ColorPicker";
import Typography from "./Typography";
import isEmpty from "../utils/isEmpty";

class SegmentedPicker extends React.Component {
	state = {
		index: 0,
	};

	getTabWidth = () => {
		switch (this.props.list.length) {
			case 2:
				return getWidth * 0.48;
			case 3:
				return getWidth * 0.32;
			case 4:
				return getWidth * 0.24;
		}
	};

	render() {
		const { rounded, style, selectedBgColor, selectedColor, defaultColor } = this.props;

		const Tab = props => {
			const isActive = props.value === this.props.active;
			let delBorder =
				props.index === this.state.index || props.index === this.state.index + 1 || props.index === 0;

			return (
				<TouchableRipple rippleColor={transparent} onPress={props.onPress}>
					<View style={[{ flexDirection: "row", height: 36, alignItems: "center" }]}>
						<View
							style={{
								height: "50%",
								borderLeftWidth: 0.8,
								borderLeftColor: !delBorder ? greydark : transparent,
							}}
						/>
						<View
							style={[
								styles.orderStatusBox,
								{
									backgroundColor: isActive ? white : transparent,
									width: this.getTabWidth(),
									borderRadius: rounded ? 18 : 8,
								},

								isActive
									? {
											shadowColor: black,
											shadowOffset: {
												width: 0,
												height: 1,
											},
											shadowOpacity: 0.22,
											shadowRadius: 2.22,

											elevation: 3,
									  }
									: {},
							]}
						>
							<Typography type="subheadline" style={[styles.orderStatusText]}>
								{props.text}
							</Typography>
						</View>
					</View>
				</TouchableRipple>
			);
		};

		return (
			<View style={[style, styles.container, { borderRadius: rounded ? 20 : 8 }]}>
				{this.props.list.map((l, i) => {
					return (
						<Tab
							key={i}
							index={i}
							text={l.text}
							onPress={
								isEmpty(l.onPress)
									? null
									: () => {
											this.setState({ index: i });
											l.onPress();
									  }
							}
							value={l.value}
						/>
					);
				})}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		height: 40,
		paddingHorizontal: 2,
		width: "100%",
		//alignSelf: "center",
		backgroundColor: greywhite,
	},
	orderStatusBox: {
		alignItems: "center",
		justifyContent: "center",
		height: "100%",
	},
	orderStatusText: {
		textAlign: "center",
		fontFamily: "semibold",
		textTransform: "capitalize",
	},
});

export default SegmentedPicker;
