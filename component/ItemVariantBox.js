import React from "react";

import { StyleSheet, View, Text } from "react-native";

import TextField from "./TextField";
import isEmpty from "../utils/isEmpty";
import { accent, greywhite, red } from "../utils/ColorPicker";
import { TouchableRipple } from "react-native-paper";
import Typography from "./Typography";
import { SectionBox, SectionDivider } from "./SectionComponents";

class ItemVariantBox extends React.Component {
	state = {
		name: "",
		stock: "",
		min_limit: "",
		max_limit: "",
		variant_number: "",
	};

	updateVal = () => {
		let { data } = this.props;

		this.setState({
			name: data.name,
			stock: data.stock,
			min_limit: data.min_limit,
			max_limit: data.max_limit,
			variant_number: data.variant_number,
		});
	};

	componentDidMount() {
		this.updateVal();
	}

	componentDidUpdate(prevProps, prevState) {}

	handleText = name => value => {
		this.setState(
			{
				[name]: value,
			},
			() => {
				this.props.onChange(this.state);
			}
		);
	};

	render() {
		const { name, stock, min_limit, max_limit, variant_number } = this.state;

		return (
			<SectionBox
				title={`Variant ${variant_number}`}
				rightHeader={
					this.props.canDelete ? (
						<TouchableRipple rippleColor="transparent" onPress={this.props.onDelete}>
							<Typography style={{ color: red }}>Delete</Typography>
						</TouchableRipple>
					) : null
				}
			>
				<TextField
					style={styles.textField}
					inputStyle={{ paddingHorizontal: 0 }}
					label="Name"
					placeholder="Required"
					value={name}
					onChangeText={this.handleText("name")}
				/>
				<SectionDivider />
				<TextField
					inputStyle={{ paddingHorizontal: 0 }}
					style={styles.textField}
					label="Stock"
					placeholder="Required"
					value={stock}
					onChangeText={this.handleText("stock")}
					keyboardType="number-pad"
				/>
				<SectionDivider />
				<TextField
					inputStyle={{ paddingHorizontal: 0 }}
					style={styles.textField}
					label="Min order"
					placeholder="Optional"
					value={min_limit}
					onChangeText={this.handleText("min_limit")}
					keyboardType="number-pad"
				/>
				<SectionDivider />
				<TextField
					inputStyle={{ paddingHorizontal: 0 }}
					style={styles.textField}
					label="Max order"
					placeholder="Optional"
					value={max_limit}
					onChangeText={this.handleText("max_limit")}
					keyboardType="number-pad"
				/>
			</SectionBox>
		);
	}
}

const styles = StyleSheet.create({
	sectionBox: {
		//width: "100%",
		display: "flex",
		flexDirection: "column",
		// borderBottomWidth: 1,
		// borderBottomColor: greywhite,
		paddingVertical: 14,
	},
	sectionTitle: {
		fontFamily: "regular",
		textTransform: "uppercase",
		paddingLeft: 12,
	},
	sectionContent: {
		width: "100%",
		backgroundColor: "white",
		borderRadius: 10,
		paddingHorizontal: 20,
	},
	textField: {
		margin: 0,
	},
});

export default ItemVariantBox;
