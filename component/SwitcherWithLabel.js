import React from "react";
import { View, Text } from "react-native";
import { TouchableRipple } from "react-native-paper";
import { greyblack, white } from "../utils/ColorPicker";
import Switcher from "./Switcher";
import Typography from "./Typography";

class SwitcherWithLabel extends React.Component {
	render() {
		const { style, cardStyle, labelStyle, descStyle, label, desc, value, enabled, onValueChange } = this.props;
		const paddingH = 16;
		return (
			<View style={[{ width: "100%" }, style]}>
				<TouchableRipple rippleColor="transparent" onPress={onValueChange}>
					<View
						style={[
							{
								height: 45,
								width: "100%",
								flexDirection: "row",
								justifyContent: "space-between",
								alignItems: "center",
								backgroundColor: white,
								paddingHorizontal: paddingH,
								// paddingLeft: paddingH,
								// paddingRight: paddingH,
								borderRadius: 10,
							},
							cardStyle,
						]}
					>
						<Typography type="body" style={labelStyle}>
							{label}
						</Typography>
						<Switcher enabled={enabled} onValueChange={onValueChange} />
					</View>
				</TouchableRipple>
				{desc ? (
					<View style={{ paddingHorizontal: paddingH, paddingVertical: 6 }}>
						<Typography type="subheadline" style={[{ color: greyblack }, descStyle]}>
							{desc}
						</Typography>
					</View>
				) : null}
			</View>
		);
	}
}

export default SwitcherWithLabel;
