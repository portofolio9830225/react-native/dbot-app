import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";

import { greyblack, greywhite } from "../utils/ColorPicker";
import TextField from "./TextField";
import langSelector from "../utils/langSelector";
import { connect } from "react-redux";
import { TextInput } from "react-native-paper";
import Typography from "./Typography";

class ItemWeightBox extends React.Component {
	state = {
		weight: "",
	};

	handleText = name => value => {
		this.setState(
			{
				[name]: value,
			},
			() => {
				this.props.onChange(this.props.id, this.state.weight);
			}
		);
	};

	render() {
		const { id, name, img } = this.props;
		const { weight } = this.state;
		const lang = langSelector.SetItemWeight.find(e => e.lang === this.props.preference.lang).data;

		return (
			<View style={styles.itemBox}>
				<View style={styles.itemImg}>
					<Image
						source={{ uri: img }}
						style={{
							width: "100%",
							height: undefined,
							aspectRatio: 1,
						}}
					/>
				</View>
				<View style={styles.itemBoxBreakdown}>
					<Typography type="headline" ellipsizeMode="tail">
						{name}
					</Typography>
					<TextField
						style={{ width: "100%" }}
						keyboardType="decimal-pad"
						mode="outlined"
						label={lang.weight}
						value={weight}
						onChangeText={this.handleText("weight")}
						right={
							<TextInput.Affix
								text="kg"
								contentStyle={{
									marginTop: 1,
									fontFamily: "regular",
								}}
							/>
						}
					/>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	itemBox: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingVertical: 10,
		width: "100%",
	},
	itemBoxHeader: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		width: "100%",
	},
	itemBoxBreakdown: {
		display: "flex",
		flexDirection: "column",
		width: "70%",
	},

	itemImg: {
		width: "25%",
		height: undefined,
		aspectRatio: 1,
		borderRadius: 10,
		borderWidth: 0.5,
		borderColor: greywhite,
		overflow: "hidden",
	},

	itemPrice: {
		fontSize: 15,
		fontFamily: "light",
		color: greyblack,
		lineHeight: 17,
	},
	itemQuantity: {
		fontSize: 15,
		fontFamily: "bold",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	profile: state.profile,
	preference: state.preference,
	alert: state.alert,
	error: state.error,
});

export default connect(mapStateToProps)(ItemWeightBox);
