import React from "react";
import { StyleSheet, View, Text, TouchableOpacity, Keyboard } from "react-native";
import { Button, Dialog, Portal, TextInput } from "react-native-paper";
import { Ionicons } from "@expo/vector-icons";
import { black, grey, greyblack, red } from "../utils/ColorPicker";
import TextField from "./TextField";
import { connect } from "react-redux";
import langSelector from "../utils/langSelector";
import isEmpty from "../utils/isEmpty";
import AlertDialog from "./AlertDialog";

class ItemVariantRow extends React.Component {
	state = {
		name: "",
		stock: "",
		delDialog: false,
	};

	componentDidMount() {
		let { name, stock, price, min, max } = this.props;
		this.setState({
			name,
			stock: `${stock}`,
			// stock: `${price}`,
		});
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevProps.name !== this.props.name || prevProps.stock !== this.props.stock) {
			let { name, stock } = this.props;
			this.setState({
				name,
				stock: `${stock}`,
				// price: `${price}`,
			});
		}
	}

	handleChange = () => {
		this.props.onChange(this.props.num, {
			variant_name: this.state.name,
			stock: this.state.stock,
			variant_number: this.props.num,
			//stock
		});
	};

	handleText = name => val => {
		if (this.props.editable) {
			this.setState(
				{
					[name]: val,
				},
				() => {
					this.handleChange();
				}
			);
		}
	};

	handleDelDialog = state => () => {
		this.setState({
			delDialog: state,
		});
	};

	handleDelete = () => {
		this.handleDelDialog(false)();
		this.props.onDelete(this.props.num);
	};

	render() {
		const { name, stock, delDialog } = this.state;
		const { id, noDeleteIcon, type, view, editable } = this.props;
		const lang = langSelector.ItemPage.find(e => e.lang === this.props.preference.lang).data;

		return (
			<View
				style={{
					width: "100%",
					marginVertical: 10,
				}}
			>
				<AlertDialog
					active={delDialog}
					title={`${lang.variantModalTitle} ${name}?`}
					actions={[
						{
							text: lang.noModal,
							onPress: this.handleDelDialog(false),
						},
						{
							text: lang.yesModal,
							onPress: this.handleDelete,
						},
					]}
					onDismiss={this.handleDelDialog(false)}
				/>

				{noDeleteIcon || !editable ? null : (
					<TouchableOpacity
						style={{
							alignSelf: "flex-end",
						}}
						onPress={this.handleDelDialog(true)}
					>
						<Ionicons name="md-remove-circle" size={18} color={grey} />
					</TouchableOpacity>
				)}

				<View
					style={{
						width: "100%",
						flexDirection: "row",
						justifyContent: "space-between",
					}}
				>
					<View
						style={{
							flexDirection: "row",
							alignItems: "flex-start",
							justifyContent: "space-between",
							width: "66%",
						}}
					>
						<TextField
							disabled={view}
							style={{ width: "100%" }}
							mode="outlined"
							label={lang.variantName}
							type="multiline"
							value={name}
							onChangeText={this.handleText("name")}
							//errorKey={`zone_name_${id}`}
						/>
					</View>
					<TextField
						disabled={view}
						style={{ width: "30%" }}
						keyboardType="number-pad"
						//errorKey={`shipping_price_${id}`}

						mode="outlined"
						label={lang.stock}
						value={stock}
						onChangeText={this.handleText("stock")}
					/>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	slideButton: {
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
	},

	dialogActionButton: {
		marginHorizontal: 10,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
});

export default connect(mapStateToProps)(ItemVariantRow);
