import React from "react";
import { connect } from "react-redux";

import { StyleSheet, View, StatusBar, KeyboardAvoidingView, Keyboard, Platform } from "react-native";
import * as SplashScreen from "expo-splash-screen";
import * as Notifications from "expo-notifications";
import * as ScreenOrientation from "expo-screen-orientation";

import { clearNoti, notiRedirect } from "../store/actions/alertAction";

import Loading from "./Loading";
import isEmpty from "../utils/isEmpty";
import { greywhite, transparent, red, mainBgColor, green, black } from "../utils/ColorPicker";
import { SafeAreaView } from "react-native-safe-area-context";
import NavigationService from "../AppNavigator/NavigationService";
import langSelector from "../utils/langSelector";
import AlertDialog from "./AlertDialog";
import { Portal } from "react-native-paper";

class LayoutContainer extends React.Component {
	render() {
		const { isLoading } = this.props.loading;
		const { children, noPadding, noLoading } = this.props;

		return (
			<View
				style={{
					flex: 1,
					backgroundColor: mainBgColor,
					position: "relative",
					paddingHorizontal: noPadding ? 0 : 16,
				}}
			>
				{!noLoading && isLoading ? (
					<View style={{ flex: 1 }}>
						<Loading />
					</View>
				) : (
					children
				)}
			</View>
		);
	}
}

class Layout extends React.Component {
	state = {
		keyboardOffset: false,
		isShowLoading: true,
	};

	async componentDidMount() {
		//SplashScreen.hideAsync();
		await SplashScreen.preventAutoHideAsync();
		await ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT_UP);
		Notifications.addNotificationResponseReceivedListener(res => {
			const data = res.notification.request.content.data;
			if (!isEmpty(data)) {
				if (!isEmpty(data.is_test) && data.is_test) {
					this.props.notiRedirect("Settings");
				}
				if (!isEmpty(data.order_id)) {
					this.props.notiRedirect("OrderDetail", { oID: data.order_id });
				}
			}
		});
		Keyboard.addListener("keyboardDidHide", this.handleOffsetKeyboard(false));
		Keyboard.addListener("keyboardDidShow", this.handleOffsetKeyboard(true));
	}

	componentDidUpdate(prevProps, prevState) {
		if (
			this.props.auth.regNotiTry &&
			isEmpty(prevProps.alert.notiScreen) &&
			!isEmpty(this.props.alert.notiScreen)
		) {
			NavigationService.navigate(this.props.alert.notiScreen, this.props.alert.notiParams);
		}
	}

	componentWillUnmount() {
		Keyboard.removeAllListeners("keyboardDidHide");
		Keyboard.removeAllListeners("keyboardDidShow");
	}

	handleOffsetKeyboard = state => () => {
		this.setState({
			keyboardOffset: state,
		});
	};

	render() {
		const { keyboardOffset, isShowLoading } = this.state;
		const { children } = this.props;
		const { isLoading } = this.props.loading;
		const { message, status } = this.props.alert;
		const isIOS = Platform.OS === "ios";

		return (
			<SafeAreaView style={{ flex: 1, backgroundColor: mainBgColor, paddingTop: isIOS ? 33 : 0 }} edges={["top"]}>
				<KeyboardAvoidingView
					style={styles.container}
					behavior={isIOS ? "padding" : "height"}
					keyboardVerticalOffset={isIOS ? 0 : 50}
				>
					<View style={[styles.inner]}>{children}</View>
					<AlertDialog
						actions={[
							{
								title: "OK",
								onPress: () => {
									this.props.clearNoti();
								},
							},
						]}
						title={status === "error" ? "Error" : status === "success" ? "Success" : ""}
						desc={message}
						active={!isEmpty(message)}
					/>
				</KeyboardAvoidingView>
			</SafeAreaView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		width: "100%",
		display: "flex",
		flexDirection: "column",
		backgroundColor: mainBgColor,
	},
	inner: {
		flex: 1,
		justifyContent: "space-around",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	preference: state.preference,
	profile: state.profile,
	alert: state.alert,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, { clearNoti, notiRedirect })(Layout);
export const LayoutView = connect(mapStateToProps)(LayoutContainer);
