import React from "react";

import isEmpty from "../utils/isEmpty";
import { Checkbox, Switch, TouchableRipple } from "react-native-paper";
import { accent, black, grey, greyblack, white } from "../utils/ColorPicker";
import { Text, View } from "react-native";
import Switcher from "./Switcher";
import Typography from "./Typography";
import { Ionicons } from "@expo/vector-icons";

class CheckBox extends React.Component {
	render() {
		const { checked, label, desc, style, disabled, onChange } = this.props;

		return (
			<TouchableRipple rippleColor="transparent" onPress={onChange}>
				<View
					style={[
						{
							width: "100%",
							flexDirection: "row",
							justifyContent: "space-between",
							alignItems: "center",
						},
						style,
					]}
				>
					<View
						style={{
							width: 22,
							height: 22,
							borderRadius: 11,
							alignItems: "center",
							justifyContent: "center",
							borderWidth: 1,
							borderColor: checked ? accent : grey,
							backgroundColor: checked ? accent : "transparent",
						}}
					>
						{checked ? <Ionicons color={white} size={16} name="ios-checkmark" /> : null}
					</View>
					<View style={{ flex: 1, paddingLeft: 16 }}>
						<Typography type="headline">{label}</Typography>
					</View>

					{/* <Switcher value={checked} onValueChange={this.handleChange} /> */}
				</View>
			</TouchableRipple>
			// <Checkbox.Item
			// 	disabled={disabled}
			// 	mode="android"
			// 	label={label}
			// 	status={checked ? "checked" : "unchecked"}
			// 	onPress={!disabled ? this.handleChange : null}
			// 	color={black}
			// />
		);
	}
}

export default CheckBox;
