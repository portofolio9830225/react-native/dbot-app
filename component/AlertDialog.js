import React from "react";
import { View, TextInput } from "react-native";
import { Portal, TouchableRipple } from "react-native-paper";
import { accent, blackTransparent, grey, greylight, greywhite, transparent } from "../utils/ColorPicker";
import Typography from "./Typography";

class AlertDialog extends React.Component {
	state = {
		newPassword: "",
	};

	handleText = name => value => {
		this.setState({
			[name]: value,
		});
	};

	render() {
		const { newPassword } = this.state;
		const { title, desc, actions, active, onDismiss, inputPassword, passVal, onPassChange } = this.props;

		const ActionButton = ({ onPress, text, color, bold, second, width, ...props }) => {
			return (
				<TouchableRipple style={{ width }} rippleColor={transparent} onPress={onPress}>
					<View
						style={[
							{
								paddingHorizontal: 16,
								paddingVertical: 12,
								justifyContent: "center",
								alignItems: "center",
								width: "100%",
							},
							second ? { borderLeftColor: grey, borderLeftWidth: 1 } : {},
						]}
					>
						<Typography
							type="headline"
							style={{
								fontFamily: bold ? "semibold" : "regular",
								textAlign: "center",
								color: color || accent,
							}}
						>
							{text}
						</Typography>
					</View>
				</TouchableRipple>
			);
		};

		return (
			<Portal>
				{active ? (
					<View
						style={{
							width: "100%",
							height: "100%",
							position: "relative",
							justifyContent: "center",
							alignItems: "center",
							order: 99999,
						}}
					>
						<TouchableRipple
							style={{
								position: "absolute",
								zIndex: 10,
								bottom: 0,
								backgroundColor: blackTransparent,
								width: "100%",
								height: "100%",
							}}
							rippleColor="transparent"
							onPress={onDismiss || null}
						>
							<View />
						</TouchableRipple>

						<View
							style={{
								backgroundColor: greywhite,
								minWidth: 150,
								width: "70%",
								alignItems: "center",
								borderRadius: 10,
								overflow: "hidden",
								opacity: 0.95,
								zIndex: 100,
							}}
						>
							<View
								style={{
									padding: 16,
									borderBottomColor: greylight,
									borderBottomWidth: 1,
									width: "100%",
									alignItems: "center",
								}}
							>
								<Typography type="headline" style={{ textAlign: "center", fontFamily: "semibold" }}>
									{title}
								</Typography>
								{desc ? (
									<Typography
										style={{
											textAlign: "center",
											paddingTop: 4,
											marginBottom: inputPassword ? 10 : 0,
										}}
									>
										{desc}
									</Typography>
								) : null}
								{inputPassword && (
									<TextInput
										onChangeText={val => {
											onPassChange(val);
										}}
										value={passVal}
										placeholder="Password"
										keyboardType="default"
										secureTextEntry={true}
										style={{
											width: "100%",
											backgroundColor: "#fff",
											paddingHorizontal: 6,
											paddingVertical: 5,
											borderRadius: 4,
										}}
									/>
								)}
							</View>
							<View style={{ flexDirection: "row", justifyContent: "space-between", width: "100%" }}>
								{actions.map((a, i) => {
									return (
										<ActionButton
											key={i}
											text={a.title}
											onPress={a.onPress}
											bold={actions.length <= 1 || i === 1}
											second={i >= 1}
											color={a.color}
											width={actions.length <= 1 ? "100%" : "50%"}
										/>
									);
								})}
							</View>
						</View>
					</View>
				) : null}
			</Portal>
		);
	}
}

export default AlertDialog;
