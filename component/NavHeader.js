import React from "react";
import { Dimensions, StyleSheet, Text, View } from "react-native";
import isEmpty from "../utils/isEmpty";
import { connect } from "react-redux";
import { Ionicons, Feather } from "@expo/vector-icons";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { white, black, greywhite, accent, blackTransparent, greydark } from "../utils/ColorPicker";
import { Portal, TouchableRipple } from "react-native-paper";
import Typography from "./Typography";

class NavHeader extends React.Component {
	state = {
		actionsDialog: false,
		actionsLogoX: 0,
		actionsLogoY: 0,
	};

	handleActionsIconLocation = () => {
		this.actionsIcon.measure((fx, fy, width, height, px, py) => {
			this.setState({
				actionsLogoY: py + 36,
				actionsLogoX: Dimensions.get("window").width - px - 28,
			});
		});
	};

	handleActionsDialog = state => () => {
		this.setState({
			actionsDialog: state,
		});
	};

	render() {
		const { actionsDialog, actionsLogoX, actionsLogoY } = this.state;
		const {
			onBack,
			backText,
			inverted,
			title,
			secondActionText,
			secondOnPress,
			actionText,
			onPress,
			actions,
			customContentRight,
		} = this.props;

		const ActionsList = ({ iconType: Icon, ...props }) => {
			return (
				<TouchableRipple
					rippleColor="transparent"
					onPress={() => {
						this.handleActionsDialog(false)();
						props.onPress();
					}}
				>
					<View
						style={{
							width: "100%",
							flexDirection: "row",
							justifyContent: "space-between",
							alignItems: "center",
							padding: 12,
						}}
					>
						<Typography type="headline" style={{ fontFamily: "regular", color: black }}>
							{props.title}
						</Typography>
						<Icon name={props.iconName} size={20} color={black} />
					</View>
				</TouchableRipple>
			);
		};

		return (
			<View style={styles.header}>
				{actionsDialog ? (
					<Portal>
						<View
							style={{
								width: "100%",
								height: "100%",
								//backgroundColor: blackTransparent,
								position: "relative",
								zIndex: 5,
							}}
						>
							<TouchableRipple rippleColor="transparent" onPress={this.handleActionsDialog(false)}>
								<View
									style={{
										width: "100%",
										height: "100%",
										backgroundColor: blackTransparent,
									}}
								/>
							</TouchableRipple>
							<View
								style={{
									//width: "70%",
									width: 236,
									backgroundColor: greywhite,
									position: "absolute",
									right: actionsLogoX,
									top: actionsLogoY,
									borderRadius: 10,
									overflow: "hidden",
									zIndex: 100,
								}}
							>
								{actions.map((a, i) => {
									return <ActionsList key={i} {...a} />;
								})}
							</View>
						</View>
					</Portal>
				) : null}
				<View style={{ zIndex: 5 }}>
					{onBack ? (
						<TouchableRipple rippleColor="transparent" onPress={onBack}>
							{!isEmpty(backText) ? (
								<Typography style={styles.actionText}>{backText}</Typography>
							) : (
								<View style={{ flexDirection: "row", alignItems: "center" }}>
									<Feather name="chevron-left" size={30} color={inverted ? white : accent} />
									<Typography style={[styles.actionText]}>Back</Typography>
								</View>
							)}
						</TouchableRipple>
					) : (
						<View />
					)}
				</View>
				{!isEmpty(title) ? (
					<Typography
						type="headline"
						style={{
							color: black,
							textAlign: "center",
							fontFamily: "semibold",
							position: "absolute",
							width: "100%",
							zIndex: 1,
						}}
					>
						{title}
					</Typography>
				) : null}
				<View
					style={{
						display: "flex",
						flexDirection: "row",
						alignItems: "center",
						justifyContent: "flex-end",
						zIndex: 5,
					}}
				>
					{!isEmpty(secondActionText) && secondActionText ? (
						<TouchableWithoutFeedback
							centered
							style={styles.textButton}
							onPress={secondOnPress}
							disabled={!secondOnPress}
						>
							<Typography
								style={[
									styles.actionText,
									{
										color: !isEmpty(secondOnPress) ? accent : inverted ? white : greydark,
									},
								]}
							>
								{secondActionText}
							</Typography>
						</TouchableWithoutFeedback>
					) : null}

					{!isEmpty(actionText) && actionText ? (
						<TouchableWithoutFeedback
							centered
							style={styles.textButton}
							onPress={onPress}
							disabled={!onPress}
						>
							<Typography
								style={[
									styles.actionText,
									{
										color: !isEmpty(onPress) ? accent : inverted ? white : greydark,
									},
								]}
							>
								{actionText}
							</Typography>
						</TouchableWithoutFeedback>
					) : actions ? (
						<TouchableRipple rippleColor="transparent" onPress={this.handleActionsDialog(true)}>
							<View
								onLayout={event => {
									setTimeout(() => {
										this.handleActionsIconLocation(event);
									}, 300);
								}}
								ref={view => {
									this.actionsIcon = view;
								}}
							>
								<Ionicons name="ellipsis-horizontal-circle" size={28} color={accent} />
							</View>
						</TouchableRipple>
					) : customContentRight ? (
						customContentRight
					) : null}
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	header: {
		height: 40,
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		width: "100%",
		alignSelf: "center",
	},
	textButton: {
		height: "100%",
		paddingLeft: 15,
		marginLeft: 5,
		alignSelf: "flex-end",
		justifyContent: "center",
	},
	actionText: {
		fontSize: 16,
		color: accent,
	},
	arrowButton: {
		// paddingRight: 10,
		// paddingLeft: -10,
		paddingVertical: 10,
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	error: state.error,
});

export default connect(mapStateToProps)(NavHeader);
