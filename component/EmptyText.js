import React from "react";
import { Text, View } from "react-native";
import { Button } from "react-native-paper";
import { EmptyListImage } from "../assets/VectorIcons";
import { black, greyblack, greylight } from "../utils/ColorPicker";
import getWidth from "../utils/getWidth";
import CustomButton from "./CustomButton";
import Typography from "./Typography";

const EmptyList = ({ style, title, text, image, button, onPress, noImage = false }) => {
	return (
		<View
			style={[
				{
					flex: 1,
					width: "100%",
					alignSelf: "center",
					alignItems: "center",
					justifyContent: "center",
				},
				style,
			]}
		>
			{/* {noImage ? null : image ? image : <EmptyListImage size={getWidth * 0.5} />} */}
			<Typography
				type="title3"
				style={{
					fontFamily: "semibold",
					color: greyblack,
					textAlign: "center",
				}}
			>
				{title}
			</Typography>
			{text && (
				<Typography
					style={{
						color: greyblack,
						textAlign: "center",
					}}
				>
					{text}
				</Typography>
			)}

			{button && (
				<CustomButton
					style={{
						marginTop: 30,
					}}
					onPress={onPress}
				>
					{button}
				</CustomButton>
			)}
		</View>
	);
};

export default EmptyList;
