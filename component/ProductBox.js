import React from "react";
import { StyleSheet, Text, View, Image, TouchableWithoutFeedback } from "react-native";
import { isTablet } from "react-native-device-detection";
import CachedImage from "expo-cached-image";

import isEmpty from "../utils/isEmpty";
import { black, greydark } from "../utils/ColorPicker";
import { LinearGradient } from "expo-linear-gradient";
import Typography from "./Typography";
import getWidth from "../utils/getWidth";

const ProductBox = props => {
	let containerWidth = getWidth;
	let padding = 8;
	return (
		<View
			style={[
				styles.root,
				isTablet
					? {
							width: containerWidth * 0.33,
							paddingHorizontal: padding / 2,
							//   paddingRight:
							//     props.index % 3 === 2 ? 0 : containerWidth * (1.5 / 100),
							//   paddingLeft:
							//     props.index % 3 === 0 ? 0 : containerWidth * (1.5 / 100),
					  }
					: {
							width: containerWidth * 0.5,
							paddingLeft: props.index % 2 === 0 ? 0 : padding,
							paddingRight: props.index % 2 === 0 ? padding : 0,
					  },
			]}
		>
			<TouchableWithoutFeedback onPress={props.onPress}>
				<View style={styles.container}>
					<View style={styles.imageContainer}>
						{isEmpty(props.image) ? (
							<Image style={styles.image} source={require("../assets/image/noImage.png")} />
						) : (
							<CachedImage
								style={styles.image}
								source={{ uri: props.image }}
								cacheKey={props.imageKey ? props.imageKey : undefined}
							/>
						)}

						<LinearGradient
							style={{
								position: "absolute",
								left: 0,
								right: 0,
								bottom: 0,
								height: "50%",
								maxHeight: 150,
							}}
							colors={["transparent", "rgba(0,0,0,0.2)", "rgba(0,0,0,0.6)"]}
						/>
						<View style={styles.infoContainer}></View>
					</View>

					<View
						style={{
							marginVertical: isTablet ? 10 : 5,
						}}
					>
						<Typography type="headline" style={styles.title} numberOfLines={1} ellipsizeMode="tail">
							{props.title}
						</Typography>
						<View style={{ display: "flex", flexDirection: "row", flexWrap: "wrap" }}>
							<Typography style={styles.price}>
								{!isEmpty(props.price) ? `RM${props.price.toFixed(2)}` : "No price yet"}
							</Typography>

							{!isEmpty(props.price) && !isEmpty(props.another_price) ? (
								<Typography style={styles.another_price}>
									{" "}
									RM{props.another_price.toFixed(2)}{" "}
								</Typography>
							) : null}
						</View>
					</View>
				</View>
			</TouchableWithoutFeedback>
		</View>
	);
};

const styles = StyleSheet.create({
	root: {
		alignSelf: "flex-start",
	},
	container: {
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "flex-start",
		justifyContent: "flex-start",
		borderRadius: 10,
		marginVertical: 10,
	},
	imageContainer: {
		position: "relative",
		width: "100%",
		height: undefined,
		aspectRatio: 1,
		overflow: "hidden",
		borderRadius: 5,
	},
	image: {
		width: "100%",
		height: undefined,
		aspectRatio: 1,
		marginBottom: 10,
	},
	infoContainer: {
		position: "absolute",
		width: "100%",
		bottom: 0,
		paddingHorizontal: 10,
		paddingBottom: 5,
	},

	title: {
		fontFamily: "regular",
		color: black,
	},
	price: {
		color: greydark,
		fontFamily: "regular",
		marginRight: 7,
	},
	another_price: {
		color: greydark,
		fontFamily: "regular",
		textDecorationLine: "line-through",
	},
	stocks: {
		fontFamily: "light",
		fontSize: 12,
	},
});

export default ProductBox;
