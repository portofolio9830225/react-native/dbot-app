import axios from "axios";
import * as SplashScreen from "expo-splash-screen";
import isEmpty from "../../utils/isEmpty";

import {
	SET_CURRENT_USER,
	DEVICE_TOKEN,
	GET_BUSINESS_LIST,
	GET_HQ_LIST,
	SET_CURRENT_BUSINESS,
	ALERT,
	LOGOUT,
	CHANGE_BUSINESS_ACC,
	CHECK_EMAIL,
} from "./index";
import { activateAccLink, API_LINK, setPassLink, XANO_API_LINK } from "../../utils/key";

import { setLoading, setRefresh } from "./loadingAction";
import { clearNoti, customNoti } from "./alertAction";
import { getError, retryWithRefresh } from "./errorAction";
import { getUser, getStoreDetails, getBankDetails } from "./profileAction";
import AsyncStorage from "@react-native-async-storage/async-storage";
import getPushNotiToken from "../../utils/getPushNotiToken";
import NavigationService from "../../AppNavigator/NavigationService";
import { setLanguage } from "./preferenceAction";

export const getNewAccessToken = (data, token) => async dispatch => {
	let val = {};

	await axios
		.post(`${XANO_API_LINK}/auth/refresh`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			console.log(res.data);
			val.refresh_token = res.data.refresh_token;
			val.token = res.data.access_token;
		})
		.catch(err => {
			if (err.response) {
				console.log(err.response.data);
			} else {
				console.log("No network detected (getNewAccessToken)");
			}
		});

	return val;
};

export const checkToken = (token, rToken) => dispatch => {
	axios
		.post(`${XANO_API_LINK}/auth/auto`, null, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(setCurrentBusiness(res.data.business_id, res.data.user_id, token, rToken));
		})
		.catch(async err => {
			if (err.response && err.response.status === 401) {
				let tokenData = await dispatch(getNewAccessToken({ refresh_token: rToken }, token));
				if (!isEmpty(tokenData)) {
					dispatch(checkToken(tokenData.token, tokenData.refresh_token));
				} else {
					SplashScreen.hideAsync();
					dispatch(setLoading(false));
					NavigationService.navigate("SignIn");
				}
			} else {
				SplashScreen.hideAsync();
				dispatch(setLoading(false));
				//NavigationService.navigate("SignIn");
			}
		});
};

export const sendUserEmail = data => dispatch => {
	dispatch(clearNoti());
	dispatch(setLoading(true));
	axios
		.post(`${XANO_API_LINK}/auth`, data)
		.then(res => {
			dispatch(setLoading(false));
			dispatch(customNoti(res.data.message, "success", "SignInPasscode"));
		})
		.catch(async err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const sendUserPasscode = data => dispatch => {
	dispatch(setLoading(true));
	axios
		.post(`${XANO_API_LINK}/auth/passcode`, data)
		.then(async res => {
			dispatch(getBusinessList(res.data.user_id, res.data.user_token));
		})
		.catch(async err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const getBusinessList =
	(id = null, token = null) =>
	async dispatch => {
		let arr = [];
		if (isEmpty(token)) {
			dispatch({
				type: GET_BUSINESS_LIST,
				payload: [],
			});
		} else {
			dispatch(setLoading(true));
			await axios
				.get(`${XANO_API_LINK}/user/${id}/business`, {
					headers: { Authorization: token },
				})
				.then(res => {
					arr = res.data.business;
					if (arr.length === 0) {
						dispatch({
							type: ALERT,
							message: "No business registered",
							status: "error",
						});
						dispatch(logoutUser());
					} else {
						if (arr.length === 1) {
							let data = {
								business_id: arr[0].id,
							};
							dispatch(getBusinessToken(data, token));
						} else {
							dispatch(setLoading(false));
						}
						dispatch(setRefresh(false));
						dispatch({
							type: GET_BUSINESS_LIST,
							payload: arr,
						});
					}
				})
				.catch(err => {
					dispatch(setLoading(false));
					dispatch(getError(err));
				});
		}
		return arr;
	};

export const getBusinessToken = (data, token) => dispatch => {
	dispatch(setLoading(true));
	axios
		.post(`${XANO_API_LINK}/auth/business`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(
				setCurrentBusiness(data.business_id, res.data.user_id, res.data.access_token, res.data.refresh_token)
			);
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const sendDeviceToken = (data, token) => dispatch => {
	if (isEmpty(data.device_token)) {
		console.log("noti not registered");
		dispatch(setLoading(false));
		dispatch({
			type: DEVICE_TOKEN,
			payload: data.device_token,
			isSignIn: true,
		});
	} else {
		axios
			.post(`${XANO_API_LINK}/device-token`, data, {
				headers: { Authorization: token },
			})
			.then(res => {
				dispatch({
					type: DEVICE_TOKEN,
					payload: data.device_token,
					isSignIn: true,
				});
			})
			.catch(async err => {
				let retryData = await dispatch(retryWithRefresh(err));

				if (!isEmpty(retryData)) {
					dispatch(sendDeviceToken(data, retryData.token));
				} else {
					dispatch(setLoading(false));
				}
			});
	}
};

export const deleteDeviceToken = (data, token) => dispatch => {
	if (isEmpty(data)) {
		console.log("noti token empty");

		dispatch({
			type: DEVICE_TOKEN,
			payload: "",
			isSignIn: false,
		});
	} else {
		axios
			.delete(`${XANO_API_LINK}/device-token/${data}`, {
				headers: { Authorization: token },
			})
			.then(res => {
				dispatch({
					type: DEVICE_TOKEN,
					payload: "",
					isSignIn: false,
				});
			})
			.catch(async err => {
				let retryData = await dispatch(retryWithRefresh(err));

				if (!isEmpty(retryData)) {
					dispatch(deleteDeviceToken(data, retryData.token));
				}
			});
	}
};

export const logoutUser = () => async dispatch => {
	let dToken = await getPushNotiToken();

	let access_token = "";
	await AsyncStorage.getItem("@dbot:businessToken")
		.then(token => {
			access_token = token;
		})
		.catch(err => {
			console.log("getAccessToken", err);
		});

	if (!isEmpty(dToken) && !isEmpty(access_token)) {
		await dispatch(deleteDeviceToken(dToken, access_token));
	}

	dispatch(setCurrentUser());
	dispatch(setCurrentBusiness());
	dispatch({
		type: LOGOUT,
	});
};

export const logoutBusiness = () => dispatch => {
	dispatch(setCurrentBusiness());
	dispatch({
		type: CHANGE_BUSINESS_ACC,
	});
};

export const setCurrentUser = data => dispatch => {
	dispatch({
		type: SET_CURRENT_USER,
		payload: data,
	});
	// dispatch(getUser(data));
};

export const setBusinessToken =
	(data = null, refresh = null) =>
	dispatch => {
		if (isEmpty(data)) {
			AsyncStorage.removeItem("@dbot:businessToken");
		} else {
			AsyncStorage.setItem("@dbot:businessToken", data);
		}
		if (isEmpty(refresh)) {
			AsyncStorage.removeItem("@dbot:refreshToken");
		} else {
			AsyncStorage.setItem("@dbot:refreshToken", refresh);
		}

		dispatch({
			type: SET_CURRENT_BUSINESS,
			token: data,
			refreshToken: refresh,
		});
	};

export const setCurrentBusiness =
	(bid = null, uid = null, data = null, refresh = null) =>
	dispatch => {
		dispatch(setBusinessToken(data, refresh));

		if (!isEmpty(data)) {
			dispatch(getStoreDetails(bid, data));
			dispatch(getUser(uid, data));
			//dispatch(getBankDetails(id, data));
			//dispatch(getAllItems(id, data));

			AsyncStorage.getItem("@dbot:language")
				.then(lang => {
					//dispatch(setLanguage(lang, false));
					dispatch(setLanguage("en", false));
				})
				.catch(err => {
					console.log("langErr", err);
				});
		} else {
			dispatch(getStoreDetails());
		}
	};

export const checkEmail = email => async dispatch => {
	dispatch(setLoading(true));
	let result = {};
	await axios
		.post(`${XANO_API_LINK}/auth/check`, email)
		.then(res => {
			dispatch(setLoading(false));
			result = res.data;
			// console.log(res.data.user_exist);
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
	return result;
};

export const forgotPassword = data => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNoti());
	axios
		.post(`${XANO_API_LINK}/auth/forgot-password`, { ...data, url: setPassLink })
		.then(res => {
			dispatch(customNoti(res.data.message, "success", "forgotPassword"));
			dispatch(setLoading(false));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const registerUser = data => dispatch => {
	dispatch(setLoading(true));
	axios
		.post(`${XANO_API_LINK}/auth/register`, { ...data, url: activateAccLink })
		.then(res => {
			dispatch(setLoading(false));
			dispatch(loginUser(data));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const loginUser = data => dispatch => {
	dispatch(setLoading(true));
	axios
		.post(`${XANO_API_LINK}/auth/login`, data)
		.then(res => {
			dispatch(setLoading(false));
			dispatch(getBusinessList(res.data.user_id, res.data.user_token));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};
