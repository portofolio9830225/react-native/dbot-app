import { setInputError } from "./errorAction";
import { CLEAR_ALERT, ALERT, NOTI_REDIRECT } from "./index";

export const customNoti =
  (message, status = "error", redirect = "") =>
  (dispatch) => {
    dispatch({
      type: ALERT,
      message,
      status,
      redirect,
    });
  };

export const clearNoti = () => (dispatch) => {
  dispatch(setInputError());
  dispatch({
    type: CLEAR_ALERT,
  });
};

export const notiRedirect = (screen, params) => (dispatch) => {
  dispatch({
    type: NOTI_REDIRECT,
    screen: "",
    params: {},
  });
  dispatch({
    type: NOTI_REDIRECT,
    screen,
    params,
  });
};
