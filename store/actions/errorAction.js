import isEmpty from "../../utils/isEmpty";
import { GET_ERROR, SET_INPUT_ERROR } from "./index";
import { customNoti } from "./alertAction";
import { getNewAccessToken, setBusinessToken, logoutUser } from "./authAction";
import store from "../";

export const retryWithRefresh = (err) => async (dispatch) => {
  let data = {};
  let authStore = store.getState().auth;

  if (err.response && err.response.status === 401) {
    let tokenData = await dispatch(
      getNewAccessToken(
        { refresh_token: authStore.refreshToken },
        authStore.token
      )
    );
    if (!isEmpty(tokenData)) {
      data = tokenData;
      dispatch(setBusinessToken(tokenData.token, tokenData.refresh_token));
    }
  }

  return data;
};

export const getError = (err) => async (dispatch) => {
  dispatch({
    type: GET_ERROR,
    payload: err,
  });
  if (!err.response) {
    dispatch(customNoti("Network not detected", "error"));

    console.log(err);
  } else {
    if (err.response.status === 401) {
      dispatch(logoutUser());
    }
    if (err.response.data && err.response.data.errors) {
      dispatch(setInputError(err.response.data.errors));
    }
    dispatch(customNoti(err.response.data.message, "error"));
    console.log(err.response.data);
  }
};

export const setInputError =
  (err = []) =>
  (dispatch) => {
    dispatch({
      type: SET_INPUT_ERROR,
      payload: err,
    });
  };
