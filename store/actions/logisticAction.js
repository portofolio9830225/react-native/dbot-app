import axios from "axios";

import { GET_LOGISTIC_INFO } from "./index";
import { API_LINK } from "../../utils/key";

import { setLoading, setRefresh } from "./loadingAction";
import { clearNoti, customNoti } from "./alertAction";
import { retryWithRefresh } from "./errorAction";
import jwtDecode from "jwt-decode";
import isEmpty from "../../utils/isEmpty";
import { getStoreDetails } from "./profileAction";

export const getLogisticInfo = token => dispatch => {
	axios
		.get(`${API_LINK}/v1/logistic`, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch({
				type: GET_LOGISTIC_INFO,
				payload: {},
			});
			dispatch({
				type: GET_LOGISTIC_INFO,
				payload: res.data.info,
			});
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(getLogisticInfo(retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const editLogisticInfo = (data, token) => dispatch => {
	dispatch(setLoading(true));
	axios
		.patch(`${API_LINK}/v1/logistic`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(setLoading(false));
			dispatch(getStoreDetails(token));
			dispatch(getLogisticInfo(token));
			//console.log(res.data);
			dispatch(customNoti(res.data.message, "success", "editLogisticInfo"));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(editLogisticInfo(retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};
