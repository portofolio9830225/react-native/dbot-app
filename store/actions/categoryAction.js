import axios from "axios";
import { GET_ALL_CATEGORY, GET_SINGLE_CATEGORY } from "./index";
import { API_LINK, XANO_API_LINK } from "../../utils/key";

import { setLoading, setRefresh } from "./loadingAction";
import { clearNoti, customNoti } from "./alertAction";
import { getError, retryWithRefresh } from "./errorAction";
import isEmpty from "../../utils/isEmpty";

//get category
export const getAllCategory = token => dispatch => {
	axios
		.get(`${XANO_API_LINK}/category`, {
			headers: { Authorization: token },
		})
		.then(res => {
			// console.log(res.data)
			dispatch({
				type: GET_ALL_CATEGORY,
				payload: {
					data: [],
				},
				isFetched: false,
			});
			dispatch({
				type: GET_ALL_CATEGORY,
				payload: {
					data: res.data,
				},
				isFetched: true,
			});
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));
			if (!isEmpty(retryData)) {
				dispatch(getAllCategory(retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		})
		.finally(() => {
			dispatch(setRefresh(false));
		});
};

export const createFirstCategory = (data, token) => async dispatch => {
	let category = {};
	await axios
		.post(`${XANO_API_LINK}/category`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getAllCategory(token));
			category = res.data.category;
		})
		.catch(async err => {
			console.log(err.response.data);
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				let c = await dispatch(createFirstCategory(id, retryData.token));
				category = c.category;
			} else {
				category = { error_code: 404 };
				dispatch(setLoading(false));
			}
		});

	return category;
};

//get single category
export const getSingleCategory =
	(id = null, token) =>
	dispatch => {
		if (isEmpty(id)) {
			dispatch({
				type: GET_SINGLE_CATEGORY,
				payload: {
					data: {},
				},
			});
		} else {
			axios
				.get(`${XANO_API_LINK}/category/${id}/product`, {
					headers: { Authorization: token },
				})
				.then(res => {
					dispatch(setLoading(false));
					dispatch({
						type: GET_SINGLE_CATEGORY,
						payload: {
							data: res.data.category,
						},
					});
				})
				.catch(async err => {
					let retryData = await dispatch(retryWithRefresh(err));

					if (!isEmpty(retryData)) {
						dispatch(getSingleCategory(id, data, retryData.token));
					} else {
						dispatch(setLoading(false));
					}
				});
		}
	};

export const createCategory = (data, token) => dispatch => {
	dispatch(clearNoti());
	dispatch(setLoading(true));
	axios

		.post(`${XANO_API_LINK}/category`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getAllCategory(token));
			dispatch(setLoading(false));
			dispatch(customNoti(res.data.message, "success", "createCategory"));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));
			// console.log(err.response);
			if (!isEmpty(retryData)) {
				dispatch(createCategory(data, retryData.token));
			} else {
				dispatch(setLoading(false));
				dispatch(getError(err));
			}
		});
};

export const editCategory = (id, data, token) => dispatch => {
	dispatch(clearNoti());
	dispatch(setLoading(true));
	axios

		.patch(`${XANO_API_LINK}/category/${id}`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getAllCategory(token));
			dispatch(getSingleCategory(id, token));
			dispatch(setLoading(false));
			dispatch(customNoti(res.data.message, "success", "editCategory"));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));
			// console.log(err.response);
			if (!isEmpty(retryData)) {
				dispatch(editCategory(data, retryData.token));
			} else {
				dispatch(setLoading(false));
				dispatch(getError(err));
			}
		});
};

export const editCategories = (data, token) => dispatch => {
	dispatch(clearNoti());
	dispatch(setLoading(true));
	axios
		.patch(`${XANO_API_LINK}/category`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getAllCategory(token));
			dispatch(setLoading(false));
			dispatch(customNoti(res.data.message, "success", "editCategories"));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));
			// console.log(err.response);
			if (!isEmpty(retryData)) {
				dispatch(editCategory(data, retryData.token));
			} else {
				dispatch(setLoading(false));
				dispatch(getError(err));
			}
		});
};

export const checkCategory = (id, data, token) => dispatch => {
	dispatch(clearNoti());
	dispatch(setLoading(true));
	axios
		.post(`${XANO_API_LINK}/category/${id}/product`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getSingleCategory(id, token));
			dispatch(setLoading(false));
			dispatch(customNoti(res.data.message, "success", "checkCategory"));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));
			// console.log(err.response);
			if (!isEmpty(retryData)) {
				dispatch(checkCategory(data, retryData.token));
			} else {
				dispatch(setLoading(false));
				dispatch(getError(err));
			}
		});
};

export const deleteCategory = (id, token) => dispatch => {
	dispatch(clearNoti());
	dispatch(setLoading(true));
	axios
		.delete(`${XANO_API_LINK}/category/${id}`, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getAllCategory(token));
			dispatch(setLoading(false));
			dispatch(customNoti(res.data.message, "success", "deleteCategory"));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));
			// console.log(err.response);
			if (!isEmpty(retryData)) {
				dispatch(editCategory(data, retryData.token));
			} else {
				dispatch(setLoading(false));
				dispatch(getError(err));
			}
		});
};
