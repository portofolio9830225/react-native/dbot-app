import axios from "axios";
//import RNFetchBlob from "rn-fetch-blob";
//import RNFS from "react-native-fs";

import { GET_ALL_ORDERS, GET_SINGLE_ORDER, GET_COURIER_LIST } from "./index";
import { API_LINK, XANO_API_LINK } from "../../utils/key";

import { setLoading, setRefresh } from "./loadingAction";
import { clearNoti, customNoti } from "./alertAction";
import { getError, retryWithRefresh } from "./errorAction";
import jwtDecode from "jwt-decode";
import isEmpty from "../../utils/isEmpty";

export const getAllOrders =
	(token, status = null, page) =>
	dispatch => {
		// 0: UNPAID
		// 9: REJECTED

		// 1: PAID
		// 2: COMPLETED

		// 3: SHIPPED

		if (isEmpty(page)) {
			console.log("order: end of data");
		} else {
			axios
				.get(`${XANO_API_LINK}/order?page=${page}${!isEmpty(status) ? `&status=${status}` : ""}`, {
					headers: { Authorization: token },
				})
				.then(res => {
					dispatch({
						type: GET_ALL_ORDERS,
						payload: {
							//unpaid: status === "unpaid" ? [] : undefined,
							unpaid: [],
							paid: [],
							completed: [],
							rejected: [],
						},
						isOrderFetched: false,
					});
					dispatch({
						type: GET_ALL_ORDERS,
						payload: {
							unpaid: status === "unpaid" ? res.data.order.items : undefined,
							paid: status === "paid" ? res.data.order.items : undefined,
							completed: status === "completed" ? res.data.order.items : undefined,
							rejected: status === "rejected" ? res.data.order.items : undefined,
						},
						pagination: {
							current: res.data.order.curPage,
							prev: res.data.order.prevPage,
							next: res.data.order.nextPage,
						},
						isOrderFetched: true,
					});
				})
				.catch(async err => {
					let retryData = await dispatch(retryWithRefresh(err));

					if (!isEmpty(retryData)) {
						dispatch(getAllOrders(retryData.token, status, page));
					} else {
						dispatch(setLoading(false));
					}
				})
				.finally(() => {
					dispatch(setRefresh(false));
				});
		}
	};

export const getSingleOrder = (id, bID) => dispatch => {
	if (isEmpty(id)) {
		dispatch({
			type: GET_SINGLE_ORDER,
			payload: {},
		});
	} else {
		dispatch(setLoading(true));
		//let decoded = jwtDecode(token);

		axios
			.get(`${XANO_API_LINK}/order/${id}?business_id=${bID}&opened=true`)
			.then(res => {
				dispatch({
					type: GET_SINGLE_ORDER,
					payload: res.data.order,
				});
				// console.log(res.data.order);
				dispatch(setLoading(false));
			})
			.catch(async err => {
				dispatch(getError(err));
				dispatch(setLoading(false));
			});
	}
};

export const setOrderStatus =
	(id, data, bID, token, isShowNoti = true) =>
	dispatch => {
		dispatch(clearNoti());
		dispatch(setLoading(true));

		axios
			.patch(`${API_LINK}/v1/order/${id}/status`, data, {
				headers: { Authorization: token },
			})
			.then(res => {
				if (isShowNoti) {
					dispatch(customNoti(res.data.message, "success", "setOrderStatus"));
				}

				dispatch(getSingleOrder(id, bID));
				dispatch(getAllOrders(token));
				dispatch(setLoading(false));
			})
			.catch(async err => {
				let retryData = await dispatch(retryWithRefresh(err));

				if (!isEmpty(retryData)) {
					dispatch(setOrderStatus(id, data, bID, retryData.token, isShowNoti));
				} else {
					dispatch(setLoading(false));
				}
			});
	};

export const setOrderStatusRejected =
	(data, token, bID, isShowNoti = true) =>
	dispatch => {
		dispatch(clearNoti());
		dispatch(setLoading(true));

		axios
			.patch(`${API_LINK}/v1/order/status/reject`, data, {
				headers: { Authorization: token },
			})
			.then(res => {
				if (isShowNoti) {
					dispatch(customNoti(res.data.message, "success", "setMultipleOrderStatus"));
				}

				dispatch(getAllOrders(token, "unpaid"));
				dispatch(getAllOrders(token, "rejected"));
				dispatch(setLoading(false));
			})
			.catch(async err => {
				let retryData = await dispatch(retryWithRefresh(err));

				if (!isEmpty(retryData)) {
					dispatch(setOrderStatusRejected(data, retryData.token, isShowNoti));
				} else {
					dispatch(setLoading(false));
				}
			});
	};

export const setMultipleOrderStatus =
	(data, token, isShowNoti = true) =>
	dispatch => {
		dispatch(clearNoti());
		dispatch(setLoading(true));

		axios
			.patch(
				`${XANO_API_LINK}/order`,
				{ ...data, opened: true },
				{
					headers: { Authorization: token },
				}
			)
			.then(res => {
				dispatch(customNoti(isShowNoti ? res.data.message : "", "success", "setMultipleOrderStatus"));

				switch (data.status) {
					case "unpaid":
					case "rejected":
						{
							dispatch(getAllOrders(token, "unpaid", 1));
							dispatch(getAllOrders(token, "rejected", 1));
						}
						break;
					case "paid":
					case "completed": {
						dispatch(getAllOrders(token, "paid", 1));
						dispatch(getAllOrders(token, "completed", 1));
					}
				}

				dispatch(setLoading(false));
			})
			.catch(async err => {
				console.log("setOder err:", err.response.data);
				let retryData = await dispatch(retryWithRefresh(err));

				if (!isEmpty(retryData)) {
					dispatch(setMultipleOrderStatus(data, retryData.token));
				} else {
					dispatch(setLoading(false));
				}
			});
	};

export const getOrdersCsv = (data, token) => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNoti());
	axios
		.get(`${XANO_API_LINK}/csv/order?start_date=${data.start}&end_date=${data.end}`, {
			headers: {
				Authorization: token,
			},
		})
		.then(res => {
			// const pathToWrite = `${RNFetchBlob.fs.dirs.DownloadDir}/order.csv`;
			// console.log("pathToWrite", pathToWrite);

			// RNFetchBlob.fs
			// 	.writeFile(pathToWrite, res.data, "utf8")
			// 	.then(() => {
			// 		console.log(`wrote file ${pathToWrite}`);
			// 	})
			// 	.catch(error => console.error(error));

			// const pathToWrite = `${RNFS.DownloadDirectoryPath}/order.csv`;
			// console.log("pathToWrite", pathToWrite);

			// RNFS.writeFile(pathToWrite, res.data, "utf8")
			// 	.then(() => {
			// 		console.log(`wrote file ${pathToWrite}`);
			// 	})
			// 	.catch(error => console.error(error));

			dispatch(customNoti(res.data.message, "success"));
			dispatch(setLoading(false));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(getOrdersCsv(data, retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const getNoWeightOrder = (orders, token) => async dispatch => {
	let data = [];

	dispatch(setLoading(true));
	await axios
		.get(`${API_LINK}/v1/item/multiple/order?order_id=${orders}`, {
			headers: {
				Authorization: token,
			},
		})
		.then(res => {
			data = res.data.items;

			dispatch(setLoading(false));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(getNoWeightOrder(orders, retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});

	return data;
};

export const getShippingOrderReview = (orders, token) => async dispatch => {
	let data = [];

	dispatch(setLoading(true));
	await axios
		.get(`${API_LINK}/v1/order/business/shipping?order_id=${orders}`, {
			headers: {
				Authorization: token,
			},
		})
		.then(res => {
			data = res.data.orders;

			dispatch(setLoading(false));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(getShippingOrderReview(orders, retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});

	return data;
};

export const createShippingOrder = (data, token) => dispatch => {
	dispatch(clearNoti());
	dispatch(setLoading(true));
	axios
		.post(`${API_LINK}/v1/order/business/shipping`, data, {
			headers: {
				Authorization: token,
			},
		})
		.then(res => {
			dispatch(customNoti(res.data.message, "success", "createShippingOrder"));
			dispatch(setLoading(false));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(createShippingOrder(data, retryData.token));
			} else {
				dispatch(setLoading(false));
				dispatch(getError(err));
			}
		});
};

export const getCourierList = () => dispatch => {
	axios
		.get(`${API_LINK}/v1/shipping/courier`)
		.then(res => {
			dispatch({
				type: GET_COURIER_LIST,
				payload: [],
			});
			dispatch({
				type: GET_COURIER_LIST,
				payload: res.data.couriers,
			});
			dispatch(setLoading(false));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const setOrderCourier = (id, data, token, isSetShipped) => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNoti());
	axios
		.patch(`${API_LINK}/v1/order/${id}/shipping/tracking`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			// if (isSetShipped) {
			//   dispatch(setOrderStatus(id, { status: 3 }, token, false));
			// } else {
			dispatch(getSingleOrder(id, token));
			// }
			dispatch(customNoti(res.data.message, "success", "setCourier"));
			dispatch(setLoading(false));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(setOrderCourier(id, data, retryData.token, isSetShipped));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const setOrderPayment = (id, data, bID, token) => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNoti());
	axios
		.patch(`${XANO_API_LINK}/order/${id}/payment`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getAllOrders(token, "unpaid", 1));

			dispatch(customNoti(res.data.message, "success", "setOrderPayment"));
			dispatch(setLoading(false));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(setOrderPayment(id, data, bID, retryData.token));
			} else {
				dispatch(getError(err));
				dispatch(setLoading(false));
			}
		});
};
