import { SET_LOADING, REFRESH } from "./index";

export const setLoading = (state) => (dispatch) => {
  dispatch({
    type: SET_LOADING,
    payload: state,
  });
};

export const setRefresh = (state) => (dispatch) => {
  dispatch({
    type: REFRESH,
    payload: state,
  });
};
