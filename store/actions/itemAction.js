import axios from "axios";

import { GET_ALL_ITEMS, GET_SINGLE_ITEM, GET_IMAGE_POOL } from "./index";
import { API_LINK, XANO_API_LINK } from "../../utils/key";

import { setLoading, setRefresh } from "./loadingAction";
import { clearNoti, customNoti } from "./alertAction";
import { getError, retryWithRefresh } from "./errorAction";
import jwtDecode from "jwt-decode";
import isEmpty from "../../utils/isEmpty";
import { imagekitUpload } from "../../utils/imagekit";

export const getAllItems = token => dispatch => {
	axios
		.get(`${XANO_API_LINK}/product`, {
			headers: { Authorization: token },
		})
		.then(res => {
			// console.log(res.data)
			dispatch({
				type: GET_ALL_ITEMS,
				payload: [],
				isFetched: false,
			});
			dispatch({
				type: GET_ALL_ITEMS,
				payload: res.data.products,
				isFetched: true,
			});
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(getAllItems(retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		})

		.finally(() => {
			dispatch(setRefresh(false));
		});
};

export const createItem = (data, token) => dispatch => {
	dispatch(clearNoti());
	dispatch(setLoading(true));

	axios
		.post(`${XANO_API_LINK}/product`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getAllItems(token));
			dispatch(setLoading(false));
			dispatch(customNoti(res.data.message, "success", "createItem"));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(createItem(data, retryData.token));
			} else {
				dispatch(setLoading(false));
				dispatch(getError(err));
			}
		});
};

export const getSingleItem =
	(id = null, token) =>
	dispatch => {
		dispatch({
			type: GET_SINGLE_ITEM,
			payload: {},
		});
		if (!isEmpty(id)) {
			dispatch(clearNoti());

			axios
				.get(`${XANO_API_LINK}/product/${id}`, {
					headers: { Authorization: token },
				})
				.then(res => {
					dispatch(setLoading(false));
					let { variants, images, categories, ...details } = res.data.product;
					dispatch({
						type: GET_SINGLE_ITEM,
						payload: { details, variants, images, categories },
					});
				})
				.catch(async err => {
					console.log(err.response.data);
					let retryData = await dispatch(retryWithRefresh(err));

					if (!isEmpty(retryData)) {
						dispatch(getSingleItem(id, retryData.token));
					} else {
						dispatch(setLoading(false));
					}
				});
		}
	};

export const editItem = (id, data, token) => dispatch => {
	dispatch(clearNoti());

	axios
		.patch(`${XANO_API_LINK}/product/${id}`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getAllItems(token));

			dispatch(setLoading(false));
			dispatch(customNoti(res.data.message, "success", "editItem"));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(editItem(id, data, retryData.token));
			} else {
				dispatch(getError(err));
				dispatch(setLoading(false));
			}
		});
};

export const setItemStatus = (id, data, token, isBulk) => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNoti());

	axios
		.patch(`${API_LINK}/v1/item/${id}/status`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getAllItems(token));
			dispatch(setLoading(false));
			if (!isBulk) {
				dispatch(getSingleItem(id, token));
				dispatch(customNoti(res.data.message, "success"));
			}
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(setItemStatus(id, data, retryData.token, isBulk));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const deleteItem = (id, token) => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNoti());

	axios
		.delete(`${XANO_API_LINK}/product/${id}`, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getAllItems(token));
			dispatch(setLoading(false));
			dispatch(customNoti(res.data.message, "success", "deleteItem"));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(deleteItem(id, retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const setItemWeight = (data, token) => dispatch => {
	dispatch(clearNoti());
	axios
		.patch(`${API_LINK}/v1/item/multiple/weight`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getAllItems(token));
			dispatch(setLoading(false));
			dispatch(customNoti(res.data.message, "success", "setItemWeight"));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(setItemWeight(data, retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const requestItemBulk = token => dispatch => {
	axios
		.get(`${API_LINK}/v1/item/bulk`, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(customNoti(res.data.message, "success", "ItemBulkUpload"));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(requestItemBulk(retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const requestTrackingBulk = token => dispatch => {
	axios
		.post(`${API_LINK}/v1/shipping/bulk/tracking/request`, null, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(customNoti(res.data.message, "success", "TrackingBulkUpload"));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(requestItemBulk(retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const getImagePool = token => dispatch => {
	axios
		.get(`${XANO_API_LINK}/file`, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch({
				type: GET_IMAGE_POOL,
				payload: res.data,
			});
			dispatch(customNoti(res.data.message, "success", "TrackingBulkUpload"));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(getImagePool(retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const addImagePool = (data, token) => dispatch => {
	dispatch(setLoading(true));
	imagekitUpload(data)
		.then(resImg => {
			let dataDB = {
				url: resImg.data.url,
				imagekit_id: resImg.data.fileId,
			};

			dispatch(addImagePoolDB(dataDB, token));
		})
		.catch(imgErr => {
			this.props.setLoading(false);
			this.props.customNoti("Unknown error (Imagekit)", "error");
		});
};

export const addImagePoolDB = (data, token) => dispatch => {
	axios
		.post(`${XANO_API_LINK}/file`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(setLoading(false));
			dispatch(getImagePool(token));
			dispatch(customNoti(res.data.message, "success", "AddImagePool"));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(addImagePoolDB(data, retryData.token));
			} else {
				dispatch(setLoading(false));
				dispatch(getError(err));
			}
		});
};
