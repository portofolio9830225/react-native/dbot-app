import axios from "axios";

import { API_LINK, XANO_API_LINK } from "../../utils/key";

import { setLoading, setRefresh } from "./loadingAction";
import { getError, retryWithRefresh } from "./errorAction";
import { GET_PAYOUT_LIST } from ".";
import isEmpty from "../../utils/isEmpty";

export const getPayoutList = token => dispatch => {
	axios
		.get(`${XANO_API_LINK}/payout`, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch({
				type: GET_PAYOUT_LIST,
				payload: res.data.payouts,
			});
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(getPayoutList(retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};
