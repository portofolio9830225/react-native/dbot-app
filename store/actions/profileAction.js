import axios from "axios";

import {
	GET_PAYOUT_DETAILS,
	GET_STORE_DETAILS,
	GET_USER_DETAILS,
	GET_BANK,
	GET_REG_PROGRESS,
	GET_SOCIAL_MEDIA_LIST,
} from "./index";
import { activateAccLink, API_LINK, XANO_API_LINK } from "../../utils/key";

import { setLoading } from "./loadingAction";
import { clearNoti, customNoti } from "./alertAction";
import { getError, retryWithRefresh } from "./errorAction";
import isEmpty from "../../utils/isEmpty";
import jwtDecode from "jwt-decode";

export const getUser =
	(id = null, token = null) =>
	dispatch => {
		if (isEmpty(token)) {
			dispatch({
				type: GET_USER_DETAILS,
				payload: {},
			});
		} else {
			axios
				.get(`${XANO_API_LINK}/user/${id}`, {
					headers: { Authorization: token },
				})
				.then(res => {
					dispatch({
						type: GET_USER_DETAILS,
						payload: {},
					});
					dispatch({
						type: GET_USER_DETAILS,
						payload: res.data,
					});
				})
				.catch(async err => {
					let retryData = await dispatch(retryWithRefresh(err));

					if (!isEmpty(retryData)) {
						dispatch(getUser(id, retryData.token));
					} else {
						dispatch(setLoading(false));
					}
				});
		}
	};

export const editUser = (id, data, token) => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNoti());

	axios
		.patch(
			`${XANO_API_LINK}/user/${id}`,
			{ ...data, url: activateAccLink },
			{
				headers: { Authorization: token },
			}
		)
		.then(res => {
			dispatch(getUser(id, token));
			dispatch(customNoti(res.data.message, "success", "UserSaved"));
			dispatch(setLoading(false));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(editUser(id, data, retryData.token));
			} else {
				dispatch(getError(err));
				dispatch(setLoading(false));
			}
		});
};

export const verifyEmail = token => dispatch => {
	dispatch(setLoading(true));
	axios
		.get(`${API_LINK}/v1/user/account/verify-email`, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(setLoading(false));
			dispatch(customNoti(res.data.message, "success"));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(verifyEmail(retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const getStoreDetails =
	(id = null, token = null) =>
	dispatch => {
		if (isEmpty(token)) {
			dispatch({
				type: GET_STORE_DETAILS,
				payload: {},
			});
		} else {
			axios
				.get(`${XANO_API_LINK}/business/${id}`, {
					headers: { Authorization: token },
				})
				.then(res => {
					console.log(token);
					dispatch({
						type: GET_STORE_DETAILS,
						payload: {},
					});

					dispatch({
						type: GET_STORE_DETAILS,
						payload: res.data,
					});
				})
				.catch(async err => {
					let retryData = await dispatch(retryWithRefresh(err));

					if (!isEmpty(retryData)) {
						dispatch(getStoreDetails(id, retryData.token));
					} else {
						dispatch(setLoading(false));
					}
				});
		}
	};

export const editStoreDetails = (id, data, token) => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNoti());

	axios
		.patch(`${XANO_API_LINK}/business/${id}`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getStoreDetails(id, token));
			dispatch(customNoti(res.data.message, "success", "StoreSaved"));
			dispatch(setLoading(false));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(editStoreDetails(id, data, retryData.token));
			} else {
				dispatch(getError(err));
				dispatch(setLoading(false));
			}
		});
};

export const editStoreAddress = (id, data, token) => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNoti());
	axios
		.patch(`${XANO_API_LINK}/business/${id}/address`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getStoreDetails(id, token));
			dispatch(customNoti(res.data.message, "success", "StoreAddressSaved"));
			dispatch(setLoading(false));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(editStoreAddress(id, data, retryData.token));
			} else {
				dispatch(getError(err));
				dispatch(setLoading(false));
			}
		});
};

export const getBankDetails =
	(id = null, token = null) =>
	dispatch => {
		if (isEmpty(token) || isEmpty(id)) {
			dispatch({
				type: GET_PAYOUT_DETAILS,
				payload: {},
			});
		} else {
			axios
				.get(`${XANO_API_LINK}/business/${id}/bank`, {
					headers: { Authorization: token },
				})
				.then(res => {
					dispatch({
						type: GET_PAYOUT_DETAILS,
						payload: {},
					});
					dispatch({
						type: GET_PAYOUT_DETAILS,
						payload: res.data.business_bank || {
							acc_name: "",
							bank_code: "",
							bank_acc: "",
							id_type: "",
							id_number: "",
							verified: "",
						},
					});
				})
				.catch(async err => {
					let retryData = await dispatch(retryWithRefresh(err));

					if (!isEmpty(retryData)) {
						dispatch(getBankDetails(id, retryData.token));
					} else {
						dispatch({
							type: GET_PAYOUT_DETAILS,
							payload: {
								acc_name: "",
								bank_code: "",
								bank_acc: "",
								id_type: "",
								id_number: "",
								verified: "",
							},
						});
						dispatch(setLoading(false));
					}
				});
		}
	};

export const editBankDetails = (id, data, token) => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNoti());
	axios
		.patch(`${XANO_API_LINK}/business/${id}/bank`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getBankDetails(id, token));
			dispatch(customNoti(res.data.message, "success", "BankSaved"));
			dispatch(setLoading(false));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(editBankDetails(id, data, retryData.token));
			} else {
				dispatch(getError(err));
				dispatch(setLoading(false));
			}
		});
};

export const changeEmail = (data, token) => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNoti());
	axios
		.patch(`${API_LINK}/v1/user/account/email`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getUser());
			dispatch(getUser(token));
			dispatch(customNoti(res.data.message, "success"));
			dispatch(setLoading(false));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(changeEmail(data, retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const changePassword = (data, token) => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNoti());
	axios
		.patch(`${API_LINK}/v1/user/account/password`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(customNoti(res.data.message, "success"));
			dispatch(setLoading(false));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(changePassword(data, retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const getRegProgress =
	(token = null) =>
	dispatch => {
		if (isEmpty(token)) {
			dispatch({
				type: GET_REG_PROGRESS,
				payload: {},
			});
		} else {
			dispatch(clearNoti());
			axios
				.get(`${API_LINK}/v1/business/account/progress`, {
					headers: { Authorization: token },
				})
				.then(res => {
					dispatch({
						type: GET_REG_PROGRESS,
						payload: res.data,
					});
				})
				.catch(async err => {
					let retryData = await dispatch(retryWithRefresh(err));

					if (!isEmpty(retryData)) {
						dispatch(getRegProgress(retryData.token));
					}
				});
		}
	};

export const getBank = () => dispatch => {
	axios
		.get(`https://api.pinjam.co/bank-code`)
		.then(res => {
			dispatch({
				type: GET_BANK,
				payload: res.data.list,
			});
		})
		.catch(err => {
			dispatch(getError(err));
		});
};

export const clearBank = () => dispatch => {
	dispatch({
		type: GET_BANK,
		payload: [],
	});
};

export const getSocialMediaList = () => dispatch => {
	dispatch({
		type: GET_SOCIAL_MEDIA_LIST,
		payload: [
			{ name: "Facebook", code: 1 },
			{ name: "Twitter", code: 2 },
			{ name: "Instagram", code: 3 },
			{ name: "Youtube", code: 4 },
			{ name: "TikTok", code: 5 },
		],
	});
	// axios
	// 	.get(`${API_LINK}/v1/social`)
	// 	.then(res => {
	// 		let arr = [];
	// 		res.data.social_media_type.map(e => {
	// 			arr.push({ name: e.social_media_name, code: e.type });
	// 		});
	// 		dispatch({
	// 			type: GET_SOCIAL_MEDIA_LIST,
	// 			payload: arr,
	// 		});
	// 	})
	// 	.catch(err => {
	// 		dispatch(getError(err));
	// 	});
};
