import axios from "axios";
import isEmpty from "../../utils/isEmpty";

import {
  GET_HQ_LIST,
  GET_ALL_AGENT_ORDER,
  GET_SINGLE_AGENT_ORDER,
  GET_ALL_AGENT_PACKAGE,
  GET_SINGLE_AGENT_PACKAGE,
  GET_SINGLE_ITEM,
} from "./index";
import { API_LINK } from "../../utils/key";

import { setLoading, setRefresh } from "./loadingAction";
import { clearNoti, customNoti } from "./alertAction";
import { getError, retryWithRefresh } from "./errorAction";
import { getAllItems } from "./itemAction";

export const getHQList =
  (token = null) =>
  (dispatch) => {
    if (isEmpty(token)) {
      dispatch({
        type: GET_HQ_LIST,
        payload: [],
      });
    } else {
      dispatch(setLoading(true));
      axios
        .get(`${API_LINK}/v1/wholesale/hq`, {
          headers: { Authorization: token },
        })
        .then((res) => {
          dispatch({
            type: GET_HQ_LIST,
            payload: res.data.hq,
          });
        })
        .catch(async (err) => {
          let retryData = await dispatch(retryWithRefresh(err));

          if (!isEmpty(retryData)) {
            dispatch(getHQList(retryData.token));
          } else {
            dispatch(setLoading(false));
          }
        });
    }
  };

export const createAgentOrder = (hqID, data, token) => (dispatch) => {
  dispatch(clearNoti());
  dispatch(setLoading(true));
  axios
    .post(`${API_LINK}/v1/wholesale/order?hq=${hqID}`, data, {
      headers: { Authorization: token },
    })
    .then((res) => {
      dispatch(customNoti(res.data.message, "success", "createAgentOrder"));
      dispatch(getAllAgentOrders(token));
    })
    .catch(async (err) => {
      let retryData = await dispatch(retryWithRefresh(err));

      if (!isEmpty(retryData)) {
        dispatch(createAgentOrder(hqID, data, retryData.token));
      }
    })

    .finally(() => {
      dispatch(setLoading(false));
      dispatch(setRefresh(false));
    });
};

export const getAllAgentOrders = (token) => (dispatch) => {
  axios
    .get(`${API_LINK}/v1/wholesale/order`, {
      headers: { Authorization: token },
    })
    .then((res) => {
      dispatch({
        type: GET_ALL_AGENT_ORDER,
        payload: res.data.orders,
      });
    })
    .catch(async (err) => {
      let retryData = await dispatch(retryWithRefresh(err));

      if (!isEmpty(retryData)) {
        dispatch(getAllAgentOrders(retryData.token));
      } else {
        dispatch(setLoading(false));
      }
    })
    .finally(() => {
      dispatch(setRefresh(false));
    });
};

export const getSingleAgentOrder = (id, token) => (dispatch) => {
  if (isEmpty(id)) {
    dispatch({
      type: GET_SINGLE_AGENT_ORDER,
      payload: {},
    });
  } else {
    dispatch(setLoading(true));

    axios
      .get(`${API_LINK}/v1/wholesale/order/${id}`, {
        headers: { Authorization: token },
      })
      .then((res) => {
        dispatch({
          type: GET_SINGLE_AGENT_ORDER,
          payload: res.data.order,
        });
        dispatch(setLoading(false));
      })
      .catch(async (err) => {
        let retryData = await dispatch(retryWithRefresh(err));

        if (!isEmpty(retryData)) {
          dispatch(getSingleAgentOrder(id, retryData.token));
        } else {
          dispatch(setLoading(false));
        }
      });
  }
};

export const getAllAgentPackages = (hq, token) => (dispatch) => {
  axios
    .get(`${API_LINK}/v1/wholesale/package?hq=${hq}`, {
      headers: { Authorization: token },
    })
    .then((res) => {
      dispatch({
        type: GET_ALL_AGENT_PACKAGE,
        payload: res.data.packages,
      });
    })
    .catch(async (err) => {
      let retryData = await dispatch(retryWithRefresh(err));

      if (!isEmpty(retryData)) {
        dispatch(getAllAgentPackages(hq, retryData.token));
      } else {
        dispatch(setLoading(false));
      }
    })
    .finally(() => {
      dispatch(setRefresh(false));
    });
};

export const getSingleAgentPackage = (id, token) => (dispatch) => {
  if (isEmpty(id)) {
    dispatch({
      type: GET_SINGLE_AGENT_PACKAGE,
      payload: {},
    });
  } else {
    dispatch(setLoading(true));

    axios
      .get(`${API_LINK}/v1/wholesale/package/${id}`, {
        headers: { Authorization: token },
      })
      .then((res) => {
        dispatch({
          type: GET_SINGLE_AGENT_PACKAGE,
          payload: res.data.package,
        });
        dispatch(setLoading(false));
      })
      .catch(async (err) => {
        let retryData = await dispatch(retryWithRefresh(err));

        if (!isEmpty(retryData)) {
          dispatch(getSingleAgentPackage(id, retryData.token));
        } else {
          dispatch(setLoading(false));
        }
      });
  }
};

export const setAgentOrderStatus = (id, data, token) => (dispatch) => {
  dispatch(setLoading(true));

  axios
    .patch(`${API_LINK}/v1/wholesale/order/${id}/status`, data, {
      headers: { Authorization: token },
    })
    .then((res) => {
      dispatch(getSingleAgentOrder(id, token));
      if (data.status === 3) {
        dispatch(duplicateAgentProduct(id, token));
      } else {
        dispatch(setLoading(false));
      }
      dispatch(customNoti(res.data.message, "success"));
    })
    .catch(async (err) => {
      let retryData = await dispatch(retryWithRefresh(err));

      if (!isEmpty(retryData)) {
        dispatch(setAgentOrderStatus(id, data, retryData.token));
      } else {
        dispatch(setLoading(false));
      }
    });
};

export const duplicateAgentProduct = (id, token) => (dispatch) => {
  axios
    .post(`${API_LINK}/v1/wholesale/order/${id}/duplicate`, null, {
      headers: { Authorization: token },
    })
    .then((res) => {
      dispatch(getAllItems(token));
      dispatch(setLoading(false));
    })
    .catch(async (err) => {
      let retryData = await dispatch(retryWithRefresh(err));

      if (!isEmpty(retryData)) {
        dispatch(duplicateAgentProduct(id, retryData.token));
      } else {
        dispatch(setLoading(false));
      }
    });
};

export const getAgentItem = (id, token) => (dispatch) => {
  dispatch({
    type: GET_SINGLE_ITEM,
    payload: {},
  });
  if (!isEmpty(id)) {
    axios
      .get(`${API_LINK}/v1/wholesale/item/${id}`, {
        headers: { Authorization: token },
      })
      .then((res) => {
        dispatch({
          type: GET_SINGLE_ITEM,
          payload: { ...res.data, is_child: true },
        });
        dispatch(setLoading(false));
      })
      .catch(async (err) => {
        let retryData = await dispatch(retryWithRefresh(err));

        if (!isEmpty(retryData)) {
          dispatch(getAgentItem(id, retryData.token));
        } else {
          dispatch(setLoading(false));
        }
      });
  }
};
