import axios from "axios";

import { GET_DASHBOARD_INFO } from "./index";
import { API_LINK, XANO_API_LINK } from "../../utils/key";

import { setLoading, setRefresh } from "./loadingAction";
import { getError, retryWithRefresh } from "./errorAction";
import isEmpty from "../../utils/isEmpty";

export const getDashboardInfo = token => dispatch => {
	axios
		.get(`${XANO_API_LINK}/dashboard/analytic`, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch({
				type: GET_DASHBOARD_INFO,
				payload: res.data,
			});
			dispatch(setLoading(false));
			dispatch(setRefresh(false));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(getDashboardInfo(retryData.token));
			} else {
				dispatch(setRefresh(false));
				dispatch(setLoading(false));
			}
		});
};
