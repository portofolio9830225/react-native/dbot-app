import axios from "axios";

import { GET_ALL_CATALOG, GET_BUSINESS_SHIPPING, GET_PAYMENT_METHOD, GET_ALL_PAYMENT_METHOD } from "./index";
import { API_LINK, XANO_API_LINK } from "../../utils/key";

import { setLoading } from "./loadingAction";
import { getError } from "./errorAction";
import { retryWithRefresh } from "./errorAction";
import jwtDecode from "jwt-decode";
import { clearNoti, customNoti } from "./alertAction";
import isEmpty from "../../utils/isEmpty";
import { getStoreDetails } from "./profileAction";

export const getAllCatalog = token => dispatch => {
	let decoded = jwtDecode(token);
	axios
		.get(`${API_LINK}/v1/catalog/business/${decoded.business_id}`)
		.then(res => {
			dispatch({
				type: GET_ALL_CATALOG,
				payload: [],
			});
			dispatch({
				type: GET_ALL_CATALOG,
				payload: res.data.catalogs,
			});
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(getAllCatalog(retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const getBusinessShipping = token => dispatch => {
	axios
		.get(`${XANO_API_LINK}/shipping`, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch({
				type: GET_BUSINESS_SHIPPING,
				payload: {},
			});
			dispatch({
				type: GET_BUSINESS_SHIPPING,
				payload: res.data,
			});
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(getBusinessShipping(retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const editBusinessShipping = (data, token) => dispatch => {
	dispatch(clearNoti());
	dispatch(setLoading(true));

	axios
		.patch(`${XANO_API_LINK}/shipping`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(customNoti(res.data.message, "success", "editBusinessShipping"));
			dispatch(getStoreDetails(token));
			dispatch(getBusinessShipping(token));
			dispatch(setLoading(false));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(editBusinessShipping(data, retryData.token));
			} else {
				dispatch(getError(err));
				dispatch(setLoading(false));
			}
		});
};

export const getItemShipping = token => dispatch => {
	let decoded = jwtDecode(token);
	axios
		.get(`${API_LINK}/v1/shipping/template/business/${decoded.business_id}`)
		.then(res => {
			dispatch({
				type: GET_BUSINESS_SHIPPING,
				payload: {},
			});

			dispatch({
				type: GET_BUSINESS_SHIPPING,
				payload: {
					is_zone_based: 0,
					is_weight_based: 0,
					is_product_based: 1,
					shipping: { product: res.data.templates },
				},
			});
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(getItemShipping(retryData.token));
			}
		});
};

export const editItemShipping = (data, token) => dispatch => {
	dispatch(clearNoti());
	dispatch(setLoading(true));
	axios
		.patch(`${API_LINK}/v1/shipping/template`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(customNoti(res.data.message, "success", "editItemShipping"));
			dispatch(getItemShipping(token));
			dispatch(setLoading(false));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(editItemShipping(data, retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const getAllPaymentMethod = () => dispatch => {
	dispatch(clearNoti());
	axios
		.get(`${API_LINK}/v1/payment`)
		.then(res => {
			dispatch({
				type: GET_ALL_PAYMENT_METHOD,
				payload: res.data.payment_methods,
			});
			dispatch(customNoti("", "success", "getAllPaymentMethod"));
		})
		.catch(err => {
			dispatch(getError(err));
		});
};

export const getPaymentMethod = id => dispatch => {
	dispatch(clearNoti());
	axios
		.get(`${API_LINK}/v1/catalog/${id}/payment-method`)
		.then(res => {
			dispatch({
				type: GET_PAYMENT_METHOD,
				payload: [],
			});

			dispatch({
				type: GET_PAYMENT_METHOD,
				payload: res.data.payment_method,
			});
			dispatch(customNoti("", "success", "getCatalogPaymentMethod"));
		})
		.catch(err => {
			dispatch(getError(err));
		});
};

export const editPaymentMethod = (id, data, token) => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNoti());
	axios
		.patch(`${XANO_API_LINK}/business/${id}/payment-method`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getStoreDetails(id, token));
			dispatch(setLoading(false));
			dispatch(customNoti(res.data.message, "success", "editCatalogPaymentMethod"));
		})
		.catch(async err => {
			console.log("err", err.response.data);
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(editPaymentMethod(id, data, retryData.token));
			} else {
				dispatch(getError(err));
				dispatch(setLoading(false));
			}
		});
};

export const editTransactionFeeCharges = (id, data, token) => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNoti());
	axios
		.patch(`${XANO_API_LINK}/business/${id}/payment-fee-charge`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getStoreDetails(id, token));
			dispatch(setLoading(false));
			dispatch(customNoti(res.data.message, "success", "editTransactionCharges"));
		})
		.catch(async err => {
			console.log("err", err.response.data);
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(editPaymentMethod(id, data, retryData.token));
			} else {
				dispatch(getError(err));
				dispatch(setLoading(false));
			}
		});
};
