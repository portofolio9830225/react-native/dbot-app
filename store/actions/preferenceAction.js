import AsyncStorage from "@react-native-async-storage/async-storage";
import { clearNoti, customNoti } from "./alertAction";
import { SET_LANGUAGE } from "./index";

export const setLanguage =
  (data, isNoti = true) =>
  async (dispatch) => {
    await dispatch(clearNoti());
    AsyncStorage.setItem("@dbot:language", data);
    dispatch({
      type: SET_LANGUAGE,
      payload: data,
    });
    let text = "";
    if (isNoti) {
      switch (data) {
        case "ms":
          text = "Bahasa dipilih";
          break;
        default:
          text = "Language selected";
      }
    }
    dispatch(customNoti(text, "success", "LangSaved"));
  };
