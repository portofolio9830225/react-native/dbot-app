import axios from "axios";
import { GET_ALL_PROMO, GET_SINGLE_PROMO } from "./index";
import { API_LINK, XANO_API_LINK } from "../../utils/key";
import { setLoading, setRefresh } from "./loadingAction";
import { clearNoti, customNoti } from "./alertAction";
import { getError, retryWithRefresh } from "./errorAction";
import isEmpty from "../../utils/isEmpty";

export const getAllPromo = token => dispatch => {
	dispatch(setLoading(true));
	axios
		.get(`${XANO_API_LINK}/promo`, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(setLoading(false));
			dispatch({
				type: GET_ALL_PROMO,
				payload: {
					data: [],
				},
				isPromoFetched: false,
			});
			dispatch({
				type: GET_ALL_PROMO,
				payload: {
					data: res.data,
				},
				isPromoFetched: true,
			});
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(getAllPromo(retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const getSinglePromo = (id, token) => dispatch => {
	dispatch(setLoading(true));
	axios
		.get(`${XANO_API_LINK}/promo/${id}`, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(setLoading(false));

			dispatch({
				type: GET_SINGLE_PROMO,
				payload: {},
			});
			dispatch({
				type: GET_SINGLE_PROMO,
				payload: res.data.promo,
			});
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(getSinglePromo(id, retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const deleteSinglePromo = (id, token) => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNoti());
	axios
		.delete(`${XANO_API_LINK}/promo/${id}`, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(customNoti(res.data.message, "success", "deletePromo"));
			dispatch(getAllPromo(token));
			dispatch(setLoading(false));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(deleteSinglePromo(id, retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const createPromo = (data, token) => dispatch => {
	dispatch(clearNoti());
	dispatch(setLoading(true));
	console.log(data);
	axios
		.post(`${XANO_API_LINK}/promo`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getAllPromo(token));
			dispatch(setLoading(false));
			dispatch(customNoti(res.data.message, "success", "createPromo"));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));
			console.log(err.response.data);
			console.log("err");
			if (!isEmpty(retryData)) {
				dispatch(createPromo(data, retryData.token));
			} else {
				dispatch(customNoti(err.response.data.message, "error"));
				dispatch(setLoading(false));
			}
		})

		.finally(() => {
			dispatch(setRefresh(false));
		});
};

export const editSinglePromo = (id, data, token) => dispatch => {
	dispatch(clearNoti());
	dispatch(setLoading(true));
	axios
		.patch(`${XANO_API_LINK}/promo/${id}`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getAllPromo(token));
			dispatch(setLoading(false));
			dispatch(customNoti(res.data.message, "success", "editPromo"));
		})
		.catch(async err => {
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(editPromo(id, data, retryData.token));
			} else {
				dispatch(customNoti(err.response.data.message, "error"));
				dispatch(setLoading(false));
			}
		});
};
