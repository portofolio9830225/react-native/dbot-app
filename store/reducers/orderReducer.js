import isEmpty from "../../utils/isEmpty";
import { GET_ALL_ORDERS, GET_COURIER_LIST, GET_SINGLE_ORDER } from "../actions/index";

const initialState = {
	orderList: {
		paid: [],
		unpaid: [],
		// shipped: [],
		completed: [],
		rejected: [],
	},
	pagination: {
		current: null,
		prev: null,
		next: null,
	},
	isOrderFetched: false,
	orderDetails: {},
	orderItems: [],
	orderPayment: {},
	orderShipping: {},
	orderPromo: {},
	courierList: [],
};

export default function (state = initialState, action) {
	switch (action.type) {
		case GET_ALL_ORDERS:
			return {
				...state,
				orderList: !isEmpty(action.payload)
					? {
							...state.orderList,
							paid:
								action.payload.paid !== undefined
									? action.pagination?.current == 1
										? action.payload.paid
										: [...state.orderList.paid, ...action.payload.paid]
									: [],
							// unpaid:
							// 	action.payload.unpaid !== undefined ? action.payload.unpaid : state.orderList.unpaid,
							unpaid:
								action.payload.unpaid !== undefined
									? action.pagination?.current == 1
										? action.payload.unpaid
										: [...state.orderList.unpaid, ...action.payload.unpaid]
									: [],
							completed:
								action.payload.completed !== undefined
									? action.pagination?.current == 1
										? action.payload.completed
										: [...state.orderList.completed, ...action.payload.completed]
									: [],
							rejected:
								action.payload.rejected !== undefined
									? action.pagination?.current == 1
										? action.payload.rejected
										: [...state.orderList.rejected, ...action.payload.rejected]
									: [],
					  }
					: { paid: [], unpaid: [], shiped: [], completed: [], rejected: [] },
				pagination: !isEmpty(action.pagination) ? action.pagination : state.pagination,
				isOrderFetched: action.isOrderFetched,
			};
		case GET_SINGLE_ORDER:
			if (isEmpty(action.payload)) {
				return {
					...state,
					orderDetails: {},
					orderItems: [],
					orderPayment: {},
					orderShipping: {},
					orderBank: {},
					orderPromo: {},
				};
			} else {
				let subtotal = 0;
				action.payload.products.map(p => {
					subtotal += p.price * p.quantity;
				});

				let { products, payment, shipping, promo, ...detail } = action.payload;

				return {
					...state,
					orderDetails: {
						...detail,
						// items_subtotal: subtotal,
					},
					orderItems: products,
					orderPayment: payment,
					orderShipping: shipping,
					orderPromo: promo,
					//orderBank: action.payload.business_bank,
				};
			}
		case GET_COURIER_LIST:
			return {
				...state,
				courierList: action.payload,
			};

		default:
			return state;
	}
}
