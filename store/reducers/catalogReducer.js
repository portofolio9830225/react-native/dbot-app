import { GET_ALL_CATALOG, GET_ALL_PAYMENT_METHOD, GET_BUSINESS_SHIPPING, GET_PAYMENT_METHOD } from "../actions";

const initialState = {
	list: [],
	shipping: {},
	selectedPaymentMethod: [],
	paymentMethod: [],
};

export default function (state = initialState, action) {
	switch (action.type) {
		case GET_ALL_CATALOG:
			return {
				...state,
				list: action.payload,
			};
		case GET_BUSINESS_SHIPPING:
			return {
				...state,
				shipping: action.payload,
			};
		case GET_PAYMENT_METHOD:
			return {
				...state,
				selectedPaymentMethod: action.payload,
			};
		case GET_ALL_PAYMENT_METHOD:
			return {
				...state,
				paymentMethod: action.payload,
			};
		default:
			return state;
	}
}
