import isEmpty from "../../utils/isEmpty";
import { GET_ALL_CATEGORY, GET_SINGLE_CATEGORY } from "../actions/index";

const initialState = {
	categories: [],
	singleCategory: {},
	isFetched: false,
};

export default function (state = initialState, action) {
	switch (action.type) {
		case GET_ALL_CATEGORY:
			return {
				...state,
				categories: isEmpty(action.payload.data) ? [] : action.payload.data.categories,
				isFetched: action.isFetched,
			};
		case GET_SINGLE_CATEGORY:
			return {
				...state,
				singleCategory: isEmpty(action.payload.data) ? {} : action.payload.data,
			};

		default:
			return state;
	}
}

// const initialState = {
//     category: [],
//     isCategoryFetched: false,
// };

// export default function (state = initialState, action) {
//     switch (action.type) {
//         case GET_ALL_CATEGORY:
//             return {
//                 ...state,
//                 category: action.payload,
//                 isCategoryFetched: action.isFetched,
//             };
//         case GET_SINGLE_CATEGORY:
//             return {
//                 ...state,
//                 category: action.payload,
//                 isCategoryFetched: action.isFetched,
//             };
//         default:
//             return state;
//     }
// }
// import { GET_ALL_CATEGORY } from "../actions";
