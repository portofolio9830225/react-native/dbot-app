import { CLEAR_ALERT, ALERT, NOTI_REDIRECT } from "../actions";

const initialState = {
  message: "",
  status: "",
  redirect: "",
  notiScreen: "",
  notiParams: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CLEAR_ALERT:
      return {
        ...state,
        message: "",
        status: "",
        redirect: "",
      };
    case ALERT:
      return {
        ...state,
        message: action.message,
        status: action.status,
        redirect: action.redirect,
      };
    case NOTI_REDIRECT:
      return {
        ...state,
        notiScreen: action.screen,
        notiParams: action.params,
      };
    default:
      return state;
  }
}
