import {
	GET_PAYOUT_DETAILS,
	GET_STORE_DETAILS,
	GET_USER_DETAILS,
	GET_BANK,
	GET_REG_PROGRESS,
	GET_SOCIAL_MEDIA_LIST,
} from "../actions";

const initialState = {
	user: {},
	store: {},
	payout: {},
	bankList: [],
	regStatus: {},
	socialMediaList: [
		{ name: "Facebook", code: 1 },
		{ name: "Twitter", code: 2 },
		{ name: "Instagram", code: 3 },
		{ name: "Youtube", code: 4 },
		{ name: "TikTok", code: 5 },
	],
};

export default function (state = initialState, action) {
	switch (action.type) {
		case GET_USER_DETAILS:
			return {
				...state,
				user: action.payload,
			};
		case GET_STORE_DETAILS:
			return {
				...state,
				store: action.payload,
			};
		case GET_PAYOUT_DETAILS:
			return {
				...state,
				payout: action.payload,
			};
		case GET_BANK:
			return {
				...state,
				bankList: action.payload,
			};
		case GET_SOCIAL_MEDIA_LIST:
			return {
				...state,
				socialMediaList: action.payload,
			};
		case GET_REG_PROGRESS:
			return {
				...state,
				regStatus: action.payload,
			};
		default:
			return state;
	}
}
