import isEmpty from "../../utils/isEmpty";
import { GET_ALL_ITEMS, GET_IMAGE_POOL, GET_SINGLE_ITEM } from "../actions/index";

const initialState = {
	userItem: [],
	inProgress: [],
	itemPage: {
		details: {},
		images: [],
		shipping: [],
		stock: 0,
		variants: [],
		categories: [],
		isAgentItem: false,
	},
	isItemFetched: false,
	imagePool: [],
	imagePoolFetched: false,
};

export default function (state = initialState, action) {
	switch (action.type) {
		case GET_ALL_ITEMS:
			return {
				...state,
				userItem: action.payload,
				isItemFetched: action.isFetched,
			};
		case GET_SINGLE_ITEM:
			return {
				...state,
				itemPage: !isEmpty(action.payload)
					? {
							...state.itemPage,
							details: action.payload.details,
							images: action.payload.images,
							categories: action.payload.categories,
							//shipping: action.payload.shippings,
							//stock: action.payload.stocks,
							variants: action.payload.variants,
							isAgentItem: action.payload.is_child,
					  }
					: {
							details: {},
							images: [],
							//shipping: [],
							//stock: 0,
							variants: [],
							categories: [],
							isAgentItem: false,
					  },
			};
		case GET_IMAGE_POOL:
			return {
				...state,
				imagePool: action.payload,
				imagePoolFetched: true,
			};
		default:
			return state;
	}
}
