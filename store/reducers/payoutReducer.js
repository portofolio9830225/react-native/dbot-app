import { GET_PAYOUT_LIST } from "../actions";

const initialState = {
  list: [],
  isPayoutFetched: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_PAYOUT_LIST:
      return {
        ...state,
        list: action.payload,
        isPayoutFetched: true,
      };

    default:
      return state;
  }
}
