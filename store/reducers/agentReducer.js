import isEmpty from "../../utils/isEmpty.js";

import {
  LOGOUT,
  GET_HQ_LIST,
  GET_ALL_AGENT_PACKAGE,
  GET_SINGLE_AGENT_PACKAGE,
  GET_ALL_AGENT_ORDER,
  GET_SINGLE_AGENT_ORDER,
} from "../actions/index";

const initialState = {
  hqList: [],
  packageList: [],
  packageDetail: {},
  orderList: [],
  orderDetail: {},
  isOrderFetched: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOGOUT:
      return {
        ...initialState,
      };
    case GET_HQ_LIST:
      return {
        ...state,
        hqList: action.payload,
      };
    case GET_ALL_AGENT_PACKAGE:
      return {
        ...state,
        packageList: action.payload,
      };
    case GET_SINGLE_AGENT_PACKAGE:
      return {
        ...state,
        packageDetail: action.payload,
      };
    case GET_ALL_AGENT_ORDER:
      return {
        ...state,
        orderList: action.payload,
        isOrderFetched: true,
      };
    case GET_SINGLE_AGENT_ORDER:
      return {
        ...state,
        orderDetail: action.payload,
      };

    default:
      return state;
  }
}
