import { combineReducers } from "redux";
import authReducer from "./authReducer";
import dashboardReducer from "./dashboardReducer";
import loadingReducer from "./loadingReducer";
import alertReducer from "./alertReducer";
import errorReducer from "./errorReducer";
import orderReducer from "./orderReducer";
import itemReducer from "./itemReducer";
import profileReducer from "./profileReducer";
import catalogReducer from "./catalogReducer";
import payoutReducer from "./payoutReducer";
import agentReducer from "./agentReducer";
import preferenceReducer from "./preferenceReducer";
import logisticReducer from "./logisticReducer";
import promoReducer from "./promoReducer";
import categoryReducer from "./categoryReducer";

export default combineReducers({
	auth: authReducer,
	dashboard: dashboardReducer,
	order: orderReducer,
	item: itemReducer,
	profile: profileReducer,
	catalog: catalogReducer,
	payout: payoutReducer,
	logistic: logisticReducer,
	agent: agentReducer,
	preference: preferenceReducer,
	loading: loadingReducer,
	alert: alertReducer,
	error: errorReducer,
	promo: promoReducer,
	category: categoryReducer,
});
