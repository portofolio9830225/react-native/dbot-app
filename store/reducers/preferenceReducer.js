import isEmpty from "../../utils/isEmpty.js";
import { SET_LANGUAGE } from "../actions/index";

const initialState = {
  lang: "en",
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_LANGUAGE:
      return {
        ...state,
        lang: !isEmpty(action.payload) ? action.payload : "en",
      };
    default:
      return state;
  }
}
