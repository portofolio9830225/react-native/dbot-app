import { GET_ERROR, SET_INPUT_ERROR } from "../actions/index";

const initialState = {
  errResponse: {},
  errInput: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_ERROR:
      return {
        ...state,
        errResponse: action.payload,
      };
    case SET_INPUT_ERROR:
      return {
        ...state,
        errInput: action.payload || [],
      };
    default:
      return state;
  }
}
