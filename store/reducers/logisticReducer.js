import isEmpty from "../../utils/isEmpty";
import { GET_LOGISTIC_INFO } from "../actions/index";

const initialState = {
	balance: "",
	apiKey: "",
	additionalRate: "",
	isFetched: false,
};

export default function (state = initialState, action) {
	switch (action.type) {
		case GET_LOGISTIC_INFO:
			return {
				...state,
				balance: isEmpty(action.payload) ? "" : action.payload.balance,
				apiKey: isEmpty(action.payload) ? "" : action.payload.api_key,
				additionalRate: isEmpty(action.payload) ? "" : action.payload.additional_rate,
				isFetched: !isEmpty(action.payload),
			};
		default:
			return state;
	}
}
