import isEmpty from "../../utils/isEmpty.js";

import {
  SET_CURRENT_USER,
  DEVICE_TOKEN,
  LOGOUT,
  CHANGE_BUSINESS_ACC,
  GET_BUSINESS_LIST,
  SET_CURRENT_BUSINESS,
  GET_HQ_LIST,
  CHECK_EMAIL,
} from "../actions/index";

const initialState = {
  isAuthenticated: false,
  userToken: "",
  refreshToken: "",
  token: "",
  deviceToken: "",
  regNotiTry: false,
  businessList: [],
  hqList: [],
  preferences: {
    lang: "en",
  },
  checkEmail: {
    user_exist: false,
    google_exist: false,
    password_exist: false,
  },
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOGOUT:
      return {
        ...initialState,
      };
    case CHANGE_BUSINESS_ACC:
      return {
        ...state,
        token: "",
      };
    case SET_CURRENT_USER:
      return {
        ...state,
        // isAuthenticated: !isEmpty(action.payload),
        userToken: action.payload,
      };
    case SET_CURRENT_BUSINESS:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.token),
        token: action.token,
        refreshToken: action.refreshToken,
      };

    case DEVICE_TOKEN:
      return {
        ...state,
        deviceToken: !isEmpty(action.payload) ? action.payload : "",
        regNotiTry: action.isSignIn,
      };
    case GET_BUSINESS_LIST:
      return {
        ...state,
        businessList: action.payload,
      };
    case GET_HQ_LIST:
      return {
        ...state,
        hqList: action.payload,
      };
    case CHECK_EMAIL:
      return {
        ...state,
        checkEmail: action.payload,
      };
    default:
      return state;
  }
}
