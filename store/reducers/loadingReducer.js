import isEmpty from "../../utils/isEmpty.js";

import { SET_LOADING, REFRESH } from "../actions/index";

const initialState = {
  isLoading: false,
  refresh: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };
    case REFRESH:
      return {
        ...state,
        refresh: action.payload,
      };

    default:
      return state;
  }
}
