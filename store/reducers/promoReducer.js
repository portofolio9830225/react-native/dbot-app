import { GET_ALL_PROMO, GET_SINGLE_PROMO } from "../actions/index";

const inisialState = {
	promos: [],
	singlePromo: {},
	isPromoFetched: false,
};

export default function (state = inisialState, action) {
	switch (action.type) {
		case GET_ALL_PROMO:
			return {
				...state,
				promos: action.payload.data.promos,
				isPromoFetched: action.isPromoFetched,
			};
		case GET_SINGLE_PROMO:
			return {
				...state,
				singlePromo: action.payload,
			};

		default:
			return {
				...state,
			};
	}
}
