import { GET_DASHBOARD_INFO } from "../actions";

const initialState = {
  funnel: {},
  order: "",
  sale: "",
  sold: "",
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_DASHBOARD_INFO:
      return {
        ...state,
        funnel: action.payload.funnel,
        order: action.payload.total_order,
        sale: action.payload.total_sale,
        sold: action.payload.total_sold,
      };

    default:
      return state;
  }
}
